<?php 
    include "auth.php";
    include 'header.php';
    include "StdMtd.php";
    
    if(isset($_POST['FileName']))
    {
        $sql = "";
    
        $imgFile = $_POST['FileName'];
        $tmp_dir = $_FILES['file']['tmp_name'];
        $imgSize = $_FILES['file']['size'];
    
        $upload_dir = "dist/pdf/";
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
        $valid_extension = array('pdf', 'png', 'jpg', 'jpeg');
        $VdFilesLoc = $_SESSION['vdcode']."_RFQ.".$imgExt;
        $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;
    
        if(in_array($imgExt, $valid_extension)) // check kalau extensi nya valid
        {
            if($imgSize == 0)
            {
                echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                echo "<script type='text/javascript'>window.location='RequestQuotation.php'</script>";
            }
            else
            {
                if($imgSize < $mMaxFile) // if image size < 1025 KB
                {
                    echo "<script type='text/javascript'>alert('".$tmp_dir." || ".$upload_dir.$VdFilesLoc."');</script>";
                    move_uploaded_file($tmp_dir, $upload_dir.$VdFilesLoc);
                }
                else
                {
                    echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                    echo "<script type='text/javascript'>window.location='RequestQuotation.php'</script>";
                }
                }
        }
        else
        {
            echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
            echo "<script type='text/javascript'>window.location='RequestQuotation.php'</script>";
        }
    
    }

?>