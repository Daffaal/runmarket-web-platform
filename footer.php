<?php
  if(substr($_SERVER['REQUEST_URI'], -9) != "login.php" && substr($_SERVER['REQUEST_URI'], -12) != "register.php")
  {
    $ver = "v1.0.1";
?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> <?php echo $ver; ?>
    </div>
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="http://www.globalsuksessolusi.com" target="_blank">RUNSystem</a>.</strong> All rights
    reserved.
  </footer>

</div>
<!-- ./wrapper -->

<?php } // end if
?>

</body>
</html>
