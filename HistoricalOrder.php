<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";
?>
<style>
/* Important part */
.scrollbox{
    height: 80vh;
    overflow-y: auto;
    overflow-x: auto;
}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title">Historical Order</h4>
            </div>
            <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#ContactPerson" data-toggle="tab">Purchase Order History</a></li>
                            <li><a href="#Bank" data-toggle="tab">Payment History</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="ContactPerson">
                                <table class="table POHistory" id="tblpohistory">
                                    <thead>
                                        <tr>
                                            <th>Quotation Document#</th>
                                            <th>PO Document#</th>
                                            <th>PO Date</th>
                                            <th>Company (Entity)</th>
                                            <th>Amount</th>
                                            <th>#</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $sql = ("Select Distinct D.QtDocNo Quotation, A.PODocNo PO, Date_Format(E.DocDt, '%d-%m-%Y') As PODate, G.EntName, E.Amt
                                            From TblRecvVdDtl A
                                            Inner Join TblRecvVdHdr B On A.DocNo = B.DocNo And B.VdCode = '".$_SESSION['vdcode']."'
                                                And A.CancelInd = 'N'
                                                And A.Status = 'A'
                                            Inner Join TblPODtl C On A.PODocNo = C.DocNo And A.PODNo = C.DNo
                                                And C.ProcessInd = 'F'
                                            Inner Join TblPORequestDtl D On C.PORequestDocNo = D.DocNo And C.PORequestDNo = D.DNo
                                            Inner Join TblPOHdr E On C.DocNo = E.DocNo
                                            Inner Join TblMaterialRequestHdr F On D.MaterialRequestDocNo = F.DocNo
                                            Left Join
                                            (
                                                Select T1.DocNo, T4.EntName
                                                From TblMaterialRequestHdr T1
                                                Inner Join TblSite T2 On T1.SiteCode = T2.SiteCode
                                                Inner Join TblProfitCenter T3 On T2.ProfitCenterCode = T3.ProfitCenterCode
                                                Inner Join TblEntity T4 On T3.EntCode = T4.EntCode
                                            ) G On F.DocNo = G.DocNo;");

                                            $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

                                            while ($res = mysqli_fetch_assoc($result)) {
                                                echo "<tr>";
                                                echo "<td>". $res['Quotation'] ."</td>";
                                                echo "<td>". $res['PO'] ."</td>";
                                                echo "<td>". $res['PODate'] ."</td>";
                                                echo "<td>". $res['EntName'] ."</td>";
                                                echo "<td>".number_format($res['Amt'], 2, ",", ".")."</td>";
                                                echo "<td>
                                                        <a href='#detail' data-toggle='modal' 
                                                        data-qt='".$res['Quotation']."' 
                                                        data-po='".$res['PO']."' 
                                                        data-amt='".number_format($res['Amt'], 2, ",", ".")."'                                                         
                                                        data-date='".$res['PODate']."'
                                                        data-ent='".$res['EntName']."'                                                        
                                                        class='detail btn btn-info'>Detail</a>
                                                    </td>";
                                                echo "</tr>";
                                            }
                                        ?>
                                        <!-- <td>0007/xxx/Qt/07/21</td>
                                        <td>0007/xxx/Qt/07/21</td>
                                        <td>14/07/2021</td>
                                        <td>xxxx</td>
                                        <td>Rp 100.000.000</td>
                                        <td><input type="button" class="btnDetail btn btn-md btn-info" data-toggle="modal" data-target="#detail" value="Detail"></td> -->
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="tab-pane" id="Bank">
                                <table class="table PaymentHistory" id="tblpaymenthistory">
                                    <thead>
                                        <tr>
                                            <th>Invoice Document</th>
                                            <th>Invoice Date</th>
                                            <th>Company (Entity)</th>
                                            <th>Amount</th>
                                            <th>Payment Date</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <td>0007/xxx/VQ/07/21</td>
                                        <td>14/07/2021</td>
                                        <td>xxxx</td>
                                        <td>Rp 100.000.000</td>
                                        <td>14/07/2021</td>
                                        <td><input type="button" class="btnDetail btn btn-md btn-info" data-toggle="modal" data-target="#detail" value="Detail"></td>
                                    </tbody>
                                </table>
                            </div>
                        
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pohistorydtl">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <center><h4 class="modal-title" id="myModalLabel"></h4></center>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <label class="control-label " style="position:relative; top:7px;">Quotation Document#</label>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <input type="text" class="form-control qt" id="qt" name="qt" readonly />
                                    </div>
                                    
                                </div>
                                <div style="height:10px;"></div>
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <label class="control-label " style="position:relative; top:7px;">PO Document#</label>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <input type="text" class="form-control po" id="po" name="po" readonly />
                                    </div>
                                    
                                </div>
                                <div style="height:10px;"></div>
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <label class="control-label " style="position:relative; top:7px;">PO Amount</label>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <input type="text" class="form-control amt" id="amt" name="amt" readonly />
                                    </div>
                                    
                                </div>
                                <div style="height:10px;"></div>
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <label class="control-label " style="position:relative; top:7px;">PO Date</label>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <input type="text" class="form-control date" id="date" name="date" readonly />
                                    </div>
                                    
                                </div>
                                <div style="height:10px;"></div>
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <label class="control-label " style="position:relative; top:7px;">Company (Entity)</label>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <input type="text" class="form-control ent" id="ent" name="ent" readonly />
                                    </div>
                                    
                                </div>
                                <div style="height:10px;"></div>
                                <div style="height:10px;"></div>

                                <div class="scrollbox">
                                <div id="TblPoHistoryDtl"></div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$("#tblpohistory").DataTable({
    //responsive: true,
    //autoWidth: false,
    //scrollX: true,
    //fixedColumns: {
    //leftColumns: 1
    //}
});
$("#tblpaymenthistory").DataTable({
    //responsive: true,
    //autoWidth: false,
    //scrollX: true,
    //fixedColumns: {
    //leftColumns: 1
    //}
});

$("#TblPoHistoryDtl").DataTable({
    responsive: true,
    autoWidth: false,
    scrollY: true,
});
</script>
<script>
    $(document).on("click", ".detail", function () {
    var qt = $(this).data('qt');
    var po = $(this).data('po');
    var amt = $(this).data('amt');
    var date = $(this).data('date');
    var ent = $(this).data('ent');
    $("#qt").val(qt);
    $("#po").val(po);
    $("#ent").val(ent);
    $("#amt").val(amt);
    $("#date").val(date);
    
    $.ajax({
        url: 'POHistoryDtl.php',
        method: 'post',
        data: {Qt:qt, Po: po},
        error: function(err) {
            console.log('Error ' + err);
        },
        success:function(data){
            $('#TblPoHistoryDtl').html(data);                                        
            $('#pohistorydtl').modal("show");
        }
    });
});
</script>

<?php
include "footer.php";
?>