<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";
include "RequestQuotation2.php";

?>
<style>
    #PState .modal-body{
        height: 80vh;
        overflow-y: auto;
        overflow-x: auto;
    }
</style>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">List of Available Request</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="QTHistory" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>RFQ#</th>
                    <th>Sector</th>
                    <th>Company (Entity)</th>
                    <th>Procurement Schedule</th>
                    <th>Stage</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Total Price</th>
                    <th>Remark</th>
                    <th>#</th>
                    <th>Other</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sql = "
                Select A.DocNo, Date_Format(A.DocDt, '%d-%m-%Y') DocDt, B.SectorName, B.ProcurementTypeDesc, F.EntName, B.StageName, B.StartDt, B.EndDt, C.TotalPrice, A.Remark, 
                B.RFQSeqNo, B.RFQ3DNo, B.StartTm, B.EndTm, B.SubmitInd
                From TblMaterialRequestHdr A
                Left Join
                (
                    Select X.MaterialRequestDocNo, Group_Concat(Distinct X.StageName) StageName,
                    Group_Concat(Distinct X.StartDt) StartDt, Group_Concat(Distinct X.EndDt) EndDt,
                    Group_Concat(Distinct SectorName) SectorName, Group_Concat(Distinct X.ProcurementTypeDesc) ProcurementTypeDesc,
                    Group_Concat(Distinct RFQSeqNo) RFQSeqNo, Group_Concat(Distinct RFQ3DNo) RFQ3DNo, 
                    Group_Concat(Distinct StartTm) StartTm, Group_Concat(Distinct EndTm) EndTm, X.SubmitInd
                    From
                    (
                        Select Distinct T1.MaterialRequestDocNo, T4.StageName, Date_Format(T4.StartDt, '%d-%m-%Y') StartDt, 
                        Date_Format(T4.EndDt, '%d-%m-%Y') EndDt, Concat(Left(T4.StartTm, 2), ':', Right(T4.StartTm, 2)) StartTm, 
                        Concat(Left(T4.EndTm, 2), ':', Right(T4.EndTm, 2)) EndTm, T5.SectorName, T6.OptDesc As ProcurementTypeDesc, 
                        T1.SeqNo As RFQSeqNo, T3.DNo As RFQ3DNo, T4.SubmitInd
                        From TblRFQHdr T1
                        Inner Join TblRFQDtl T2 On T1.MaterialRequestDocNo = T2.MaterialRequestDocNo
                            And T1.SeqNo = T2.SeqNo
                        Inner Join TblVendorSector T3 On T3.VdCode = '".$_SESSION['vdcode']."'
                            And T2.SectorCode = T3.SectorCode
                            And T2.SubSectorCode = T3.SubSectorCode
                        Inner Join
                        (
                            Select X3.MaterialRequestDocNo, X3.SeqNo, X1.DNo, X1.StartDt, X1.StartTm, X1.EndDt, X1.EndTm, X4.OptDesc As ProcurementTypeDesc,
                            X1.StageName, X1.SubmitInd
                            From TblRFQDtl3 X1
                            Inner Join
                            (
                                Select MaterialRequestDocNo, Max(Concat(EndDt, EndTm)) MaxPeriod
                                From TblRFQDtl3
                                Where Concat(EndDt, EndTm) <= CurrentDateTime()
                                Group By MaterialRequestDocNo
                            ) X2 On X1.MaterialRequestDocno = X2.MaterialRequestDocNo And Concat(X1.EndDt, X1.EndTm) = X2.MaxPeriod
                            Inner Join TblRFQHdr X3 On X1.MaterialRequestDocNo = X3.MaterialRequestDocNo And X1.SeqNo = X3.SeqNo
                            Inner Join TblOption X4 On X3.ProcurementType = X4.OptCode And X4.OptCat = 'ProcurementType'
                        ) T4 On T1.MaterialRequestDocNo = T4.MaterialRequestDocNo And T1.SeqNo = T4.SeqNo
                        Inner Join TblSector T5 On T2.SectorCode = T5.SectorCode
                        Inner Join TblOption T6 On T1.ProcurementType = T6.OptCode And T6.OptCat = 'ProcurementType'
                        Where Not Exists
                        (
                            Select 1
                            From TblRFQDtl2
                            Where MaterialRequestDocNo = T1.MaterialRequestDocNo
                            And SeqNo = T1.SeqNo
                        )
                        Union All
                        Select Distinct T1.MaterialRequestDocNo, T3.StageName, Date_Format(T3.StartDt, '%d-%m-%Y') StartDt, 
                        Date_Format(T3.EndDt, '%d-%m-%Y') EndDt, Concat(Left(T3.StartTm, 2), ':', Right(T3.StartTm, 2)) StartTm, 
                        Concat(Left(T3.EndTm, 2), ':', Right(T3.EndTm, 2)) EndTm, T5.SectorName, T6.OptDesc As ProcurementTypeDesc, 
                        T1.SeqNo As RFQSeqNo, T3.DNo As RFQ3DNo, T3.SubmitInd
                        From TblRFQHdr T1
                        Inner Join TblRFQDtl2 T2 On T1.MaterialRequestDocNo = T2.MaterialRequestDocNo
                            And T1.SeqNo = T2.SeqNo
                            And T2.VdCode = '".$_SESSION['vdcode']."'
                        Inner Join
                        (
                            Select X3.MaterialRequestDocNo, X3.SeqNo, X1.DNo, X1.StartDt, X1.StartTm, X1.EndDt, X1.EndTm, X4.OptDesc As ProcurementTypeDesc,
                            X1.StageName, X1.SubmitInd
                            From TblRFQDtl3 X1
                            Inner Join
                            (
                                Select MaterialRequestDocNo, Max(Concat(EndDt, EndTm)) MaxPeriod
                                From TblRFQDtl3
                                Where Concat(EndDt, EndTm) <= CurrentDateTime()
                                Group By MaterialRequestDocNo
                            ) X2 On X1.MaterialRequestDocno = X2.MaterialRequestDocNo And Concat(X1.EndDt, X1.EndTm) = X2.MaxPeriod
                            Inner Join TblRFQHdr X3 On X1.MaterialRequestDocNo = X3.MaterialRequestDocNo And X1.SeqNo = X3.SeqNo
                            Inner Join TblOption X4 On X3.ProcurementType = X4.OptCode And X4.OptCat = 'ProcurementType'
                        ) T3 On T1.MaterialRequestDocNo = T3.MaterialRequestDocNo And T1.SeqNo = T3.SeqNo
                        Inner Join TblRFQDtl T4 On T1.MaterialRequestDocNo = T4.MaterialRequestDocno
                            And T1.SeqNo = T4.SeqNo
                        Inner Join TblSector T5 On T4.SectorCode = T5.SectorCode
                        Inner Join TblOption T6 On T1.ProcurementType = T6.OptCode And T6.OptCat = 'ProcurementType'
                ) X
                Group By X.MaterialRequestDocNo
                ) B On A.DocNo = B.MaterialRequestDocNo
                Inner Join
                (
                    Select DocNo, Sum(EstPrice * Qty) TotalPrice
                    From TblMaterialRequestDtl
                    Where CancelInd = 'N'
                    And Status = 'A'
                    Group By DocNo
                ) C On A.DocNo = C.DocNo
                Left Join TblSite D On A.SiteCode = D.SiteCode
                Left Join TblProfitCenter E On D.ProfitCenterCode = E.ProfitCenterCode
                Left Join TblEntity F On E.EntCode = F.EntCode
                Where A.RMInd = 'Y'
                And A.DocNo In 
                (
                    Select Distinct DocNo
                    From TblMaterialRequestDtl4
                )
                And A.DocNo Not In
                (
                    Select Distinct DocNo
                    From TblMaterialRequestDtl
                    Where (CancelInd = 'Y' Or Status In ('O', 'C'))
                );";

                $i=0;
                $newRfq = array();
                
                foreach ($rfqs as $key => $value) {
                    
                    array_push($newRfq, $value);
                }
                $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                while ($res = mysqli_fetch_assoc($result)) {
                                                                        
                    if(isset($newRfq[$i]->MaterialRequestDocNo) && $res['DocNo'] == $newRfq[$i]->MaterialRequestDocNo){
                        echo "<tr>";
                            echo "<td>" . $newRfq[$i]->MaterialRequestDocNo. "</td>";
                            echo "<td>" . $newRfq[$i]->SectorName . "</td>";
                            echo "<td>" . $res['EntName'] . "</td>";
                            echo "<td>" . $newRfq[$i]->ProcurementType . "</td>";                
                            echo "<td>" . $newRfq[$i]->StageName . "</td>";                
                            echo "<td>" . $newRfq[$i]->StartDt . "</td>";
                            echo "<td>" . $newRfq[$i]->EndDt . "</td>";
                            echo "<td align='right'>".number_format($res['TotalPrice'], 2, ',', '.')."</td>";
                            echo "<td>". $res['Remark'] ."</td>";
                            echo "<td>";
                            echo "<a class='view_data btn btn-xs btn-info' data-dimiss='modal' data-toggle='modal' id='" . $newRfq[$i]->MaterialRequestDocNo . "' 
                            href='#QTDetail'
                            data-id='" . $newRfq[$i]->MaterialRequestDocNo . "'
                            data-dt='" . $res['DocDt'] . "'
                            data-ent='" . $res['EntName'] . "'
                            data-rfqamt='".number_format($res['TotalPrice'], 2, ',', '.')."'
                            data-sector='" . $newRfq[$i]->SectorName . "'
                            data-startdt='".$newRfq[$i]->StartDt."'
                            data-enddt='".$newRfq[$i]->EndDt."'
                            data-starttm='".$newRfq[$i]->StartTm."'
                            data-endtm='".$newRfq[$i]->EndTm."'
                            data-seqno='".$newRfq[$i]->SeqNo."'
                            data-rfqdno='".$newRfq[$i]->RFQ3DNo."'
                            data-remark='". $res['Remark'] ."'
                            data-submitind='".$newRfq[$i]->SubmitInd."'
                            style='margin-bottom: 5px;'> Detail </a> <br>";
                            echo "<a style='margin-bottom: 5px;' class='qtHistory btn btn-xs btn-info' href='QtHistory.php?i=".str_replace('/', '-', $newRfq[$i]->MaterialRequestDocNo)."'><button type='button'></button>Quotation History</a> <br>";                   
                            echo "</td>";
                            echo "<td>";
                            echo "<a style='margin-bottom: 5px;' class='download btn btn-xs btn-danger' href='#downloadDtl' data-toggle='modal' id='" . $newRfq[$i]->MaterialRequestDocNo . "'><button type='button' class='fa fa-download'></button> Download File</a>";
                            echo "<button type='button' class='btn btn-xs btn-danger btnPState' data-docno='".$newRfq[$i]->MaterialRequestDocNo."' data-toggle='modal' data-target='#PState'>Procurement Schedule</button>";
                            echo "</td>";
                            echo "</tr>";
        
                            $i++;
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>

<!-- modal for detail -->
<div class="modal fade" id="QTDetail">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Request for Quotation</h4>
            </div>
            <div class="modal-body" id="">
                <div id="data-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;">RFQ#</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control ExpiredDt" id="rfq" name="ExpiredDt" readonly />
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-left: 10%;">
                                <label class="control-label " style="position:relative; top:7px;">Sector</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control ExpiredDt" id="sector" name="ExpiredDt" readonly />
                            </div>
                        </div>
                    </div>
                    <div style="height:10px;"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;">RFQ Amount</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control ExpiredDt" id="rfqamt" name="ExpiredDt" readonly style="text-align: end;" />
                            </div>
                            <div class="col-md-2 col-xs-2" style="padding-left: 10%;">
                                <label class="control-label " style="position:relative; top:7px;">Remark</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control ExpiredDt" id="remark" name="ExpiredDt" readonly />
                            </div>

                        </div>
                    </div>
                    <div style="height:10px;"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;">Company (Entity)</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control ExpiredDt" id="ent" name="ExpiredDt" readonly />
                            </div>

                        </div>
                    </div>
                    <div style="height:10px;"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;">RFQ Date</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control ExpiredDt" id="rfqdate" name="ExpiredDt" readonly />
                            </div>

                        </div>
                    </div>
                    <div style="height:20px;"></div>
                    <div id="RQDetail">
                </div>
                <div id="loading">
                    Loading...
                </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="downloadDtl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Your File</h4>
            </div>
            <div class="modal-body" id="RQDownload"></div>
        </div>
    </div>
</div>

<!-- modal for procurement status -->
<div class="modal fade" id="PState">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="procurementModal">Procurement Schedule</h4>
            </div>
            <div class="modal-body">                
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#QTHistory').DataTable({
            responsive: true,
            autoWidth: false,
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            fixedColumns: {
                leftColumns: 1
            }
        });

        $("#tblrfq").DataTable({
            responsive: true,
            autoWidth: false,
            // scrollX: true,
            scrollCollapse: true,
            paging: true,
            columnDefs: [{
                width: 200,
                targets: 0
            }]
        });
    });
</script>

<script>
    $(function() {
        $('.view_data').click(function() {
            var myBookId = $(this).data('id');
            var dno = $(this).data('dno');
            var uom = $(this).data('uom');
            var ent = $(this).data('ent');
            var dt = $(this).data('dt');
            var rfqAmt = $(this).data('rfqamt');
            var sector = $(this).data('sector');
            var remark = $(this).data('remark');
            var startDt = $(this).data('startdt');
            var endDt = $(this).data('enddt');
            var startTm = $(this).data('starttm');
            var endTm = $(this).data('endtm');
            var seqNo = $(this).data('seqno');
            var rfqDno = $(this).data('rfqdno');
            var submitInd = $(this).data('submitind');
            $("#rfq").val(myBookId);
            $("#uom").val(uom);
            $("#ent").val(ent);
            $("#rfqdate").val(dt);
            $("#rfqamt").val(rfqAmt);
            $('#sector').val(sector);
            $('#remark').val(remark);
            var DocNo = $(this).attr('id');
            $('#RQDetail').text("");

            startDt = startDt.split('-').reverse().join('') + startTm; 
            endDt = endDt.split('-').reverse().join('') + endTm;           
            // console.log(startDt, endDt);

            $.ajax({
                url: 'RQDetail.php',
                method: 'post',
                data: {
                    DocNo: DocNo,
                    StartDt: startDt,
                    EndDt: endDt,
                    SeqNo: seqNo,
                    Rfq3DNo: rfqDno,
                    SubmitInd: submitInd
                },
                beforeSend: function(){
                    $('#data-body').hide();
                    $('#loading').show();
                },
                complete: function(){
                    $('#data-body').show();
                    $('#loading').hide();
                },
                error: function(err) {
                    // console.log('Error ' + err);
                },
                success: function(data) {
                    // console.log('Success  ' + data);
                    $('#RQDetail').html(data);
                    $('#QTDetail').modal("show");
                }
            });
        });

        $('.btnPState').click(function (){

            var docNo = $(this).data('docno');

            $.ajax({
                url: 'ProcurementState.php',
                method: 'post',
                data: {
                    DocNo: docNo
                },
                success(data){
                
                    $('#PState .modal-body').html(data);
                    $('#PState').show();
                },
                error(err){
                    console.log(err.InnerHtmlText)
                }
            })
        })

    })
</script>

<script>
    $('.download').click(function() {
        var DocNo = $(this).attr("id");
        $('#RQDownload').text("");

        $.ajax({
            url: 'RQDownload.php',
            method: 'post',
            data: {
                DocNo: DocNo
            },
            error: function(err) {
                console.log('Error ' + err);
            },
            success: function(data) {
                // console.log('Success  ' + data);
                $('#RQDownload').html(data);
                $('#downloadDtl').modal("show");
            }
        })
    })
</script>

<?php
include "footer.php";
?>