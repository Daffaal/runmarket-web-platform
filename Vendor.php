<?php

if(isset($_POST['sector'])){

	require 'conn.php';
  
	$VdSector = $_POST['sector'];
	$sqlS = "SELECT SubSectorCode, SubSectorName FROM TblSubSector WHERE SectorCode = '".$VdSector."'";
  
	$data = array();
	$result  = mysqli_query($conn, $sqlS);
	while ($res = mysqli_fetch_assoc($result)) {
	  $data[] = $res;
	}
  
	print(json_encode($data));
	exit();
}

include "auth.php";
include 'header.php';
include "StdMtd.php";

if(isset($_POST['submit']))
{
    $sqlU = "";

    $R1 = array("<p>", "</p>", "'");
    $R2 = array("", "", "\'");

    //$mActInd = isset($_POST['ActInd']) && !empty($_POST['ActInd']) ? "Y" : "N";
    $mTaxInd = isset($_POST['TaxInd']) && !empty($_POST['TaxInd']) ? "Y" : "N";
    $mAddress = isset($_POST['MeeAddress']) ? str_replace($R1, $R2, $_POST['MeeAddress']) : "";
    $mPostal = isset($_POST['Postal']) ? str_replace($R1, $R2, $_POST['Postal']) : "";
    $mIDNumber = isset($_POST['IDNumber']) ? str_replace($R1, $R2, $_POST['IDNumber']) : "";
    $mTaxPayer = isset($_POST['TaxPayer']) ? str_replace($R1, $R2, $_POST['TaxPayer']) : "";
    $mPhone = isset($_POST['Phone']) ? str_replace($R1, $R2, $_POST['Phone']) : "";
    $mFax = isset($_POST['Fax']) ? str_replace($R1, $R2, $_POST['Fax']) : "";
    $mEmail = isset($_POST['Email']) ? str_replace($R1, $R2, $_POST['Email']) : "";
    $mMobile = isset($_POST['Mobile']) ? str_replace($R1, $R2, $_POST['Mobile']) : "";
    $mCreditLimit = isset($_POST['CreditLimit']) ? str_replace($R1, $R2, $_POST['CreditLimit']) : 0;
	$mRemark = isset($_POST['MeeRemark']) ? str_replace($R1, $R2, $_POST['MeeRemark']) : "";
	$mVdCtCode = isset($_POST['LueVdCategory']) ? str_replace($R1, $R2, $_POST['LueVdCategory']) : "";
	$mSDCode = isset($_POST['LueSDCode']) ? str_replace($R1, $R2, $_POST['LueSDCode']) : "";
	$mVilCode = isset($_POST['LueVillageCode']) ? str_replace($R1, $R2, $_POST['LueVillageCode']) : "";
	$mCityCode = isset($_POST['LueCityCode']) ? str_replace($R1, $R2, $_POST['LueCityCode']) : "";
	$mWebsite = isset($_POST['Website']) ? str_replace($R1, $R2, $_POST['Website']) : "";
	$mEstablishedYr = isset($_POST['EstablishedYr']) ? str_replace($R1, $R2, $_POST['EstablishedYr']) : "";

	$sqlU = $sqlU . "Delete From TblVendorUpdate Where VdCode = '".$_SESSION['vdcode']."'; ";
	
	$sqlU = $sqlU . "Insert Into TblVendorUpdate(VdCode, VdName, ShortName, Address, VdCtCode, CityCode, ";
	$sqlU = $sqlU . "SDCode, VilCode, PostalCd, IdentityNo, TIN, TaxInd, Phone, Fax, Email, Mobile, ";
	$sqlU = $sqlU . "CreditLimit, Website, EstablishedYr, Remark, CreateBy, CreateDt) ";
	$sqlU = $sqlU . "Values('".$_SESSION['vdcode']."', '".$_POST['VdName']."', '".$_POST['ShortName']."', '".$mAddress."', '".$mVdCtCode."', '".$mCityCode."', ";
	$sqlU = $sqlU . "'".$mSDCode."', '".$mVilCode."', '".$mPostal."', '".$mIDNumber."', '".$mTaxPayer."', '".$mTaxInd."', '".$mPhone."', '".$mFax."', '".$mEmail."', '".$mMobile."', ";
	$sqlU = $sqlU . "'".$mCreditLimit."', '".$mWebsite."', '".$mEstablishedYr."', '".$mRemark."', '".$_SESSION['vdusercode']."', CurrentDateTime()) ";
	$sqlU = $sqlU . "On Duplicate Key ";
	$sqlU = $sqlU . "Update ";
    $sqlU = $sqlU . "  ShortName = '".$_POST['ShortName']."', Address = '".$mAddress."', VdCtCode = '".$mVdCtCode."', CityCode = '".$mCityCode."', SDCode = '".$mSDCode."', ";
    $sqlU = $sqlU . "  VilCode = '".$mVilCode."', PostalCd = '".$mPostal."', IdentityNo = '".$mIDNumber."', TIN = '".$mTaxPayer."', TaxInd = '".$mTaxInd."', Phone = '".$mPhone."', ";
    $sqlU = $sqlU . "  Fax = '".$mFax."', Email = '".$mEmail."', Mobile = '".$mMobile."', CreditLimit = '".$mCreditLimit."', Website = '".$mWebsite."',  EstablishedYr = '".$mEstablishedYr."', Remark = '".$mRemark."', LastUpBy = '".$_SESSION['vdusercode']."', LastUpDt = CurrentDateTime() ";
	$sqlU = $sqlU . "; ";
	
	$sqlU = $sqlU . "Delete From TblVendorContactPersonUpdate Where VdCode='".$_SESSION['vdcode']."'; ";
	$sqlU = $sqlU . "Delete From TblVendorBankAccountUpdate Where VdCode='".$_SESSION['vdcode']."'; ";
	$sqlU = $sqlU . "Delete From TblVendorItemCategoryUpdate Where VdCode='".$_SESSION['vdcode']."'; ";
	$sqlU = $sqlU . "Delete From TblVendorSectorUpdate Where VdCode='".$_SESSION['vdcode']."'; ";

	/*
	$sqlU = $sqlU . "Update TblCOA A ";
	$sqlU = $sqlU . "Inner Join TblParameter B On B.ParCode='VendorAcNoDownPayment' And B.ParValue Is Not Null ";
	$sqlU = $sqlU . "Inner Join TblParameter C On C.ParCode='VendorAcDescDownPayment' And C.ParValue Is Not Null ";
	$sqlU = $sqlU . "Set A.AcDesc=Concat(C.ParValue, ' ', '".$_POST['VdName']."'), A.LastUpBy='".$_SESSION['vdusercode']."', A.LastUpDt=CurrentDateTime() ";
	$sqlU = $sqlU . "Where A.AcNo=Concat(B.ParValue, '".$_SESSION['vdcode']."'); ";

	$sqlU = $sqlU . "Update TblCOA A ";
	$sqlU = $sqlU . "Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ";
	$sqlU = $sqlU . "Inner Join TblParameter C On C.ParCode='VendorAcDescAP' And C.ParValue Is Not Null ";
	$sqlU = $sqlU . "Set A.AcDesc=Concat(C.ParValue, ' ', '".$_POST['VdName']."'), A.LastUpBy='".$_SESSION['vdusercode']."', A.LastUpDt=CurrentDateTime() ";
	$sqlU = $sqlU . "Where A.AcNo=Concat(B.ParValue, '".$_SESSION['vdcode']."'); ";

	$sqlU = $sqlU . "Update TblCOA A ";
	$sqlU = $sqlU . "Inner Join TblParameter B On B.ParCode='VendorAcNoUnInvoiceAP' And B.ParValue Is Not Null ";
	$sqlU = $sqlU . "Inner Join TblParameter C On C.ParCode='VendorAcDescUnInvoiceAP' And C.ParValue Is Not Null ";
	$sqlU = $sqlU . "Set A.AcDesc=Concat(C.ParValue, ' ', '".$_POST['VdName']."'), A.LastUpBy='".$_SESSION['vdusercode']."', A.LastUpDt=CurrentDateTime() ";
	$sqlU = $sqlU . "Where A.AcNo=Concat(B.ParValue, '".$_SESSION['vdcode']."'); ";

	$sqlU = $sqlU . "Update TblCOA A ";
	$sqlU = $sqlU . "Inner Join TblParameter B On B.ParCode='AcNoForGiroAP' And B.ParValue Is Not Null ";
	$sqlU = $sqlU . "Inner Join TblParameter C On C.ParCode='AcDescForGiroAP' And C.ParValue Is Not Null ";
	$sqlU = $sqlU . "Set A.AcDesc=Concat(C.ParValue, ' ', '".$_POST['VdName']."'), A.LastUpBy='".$_SESSION['vdusercode']."', A.LastUpDt=CurrentDateTime() ";
	$sqlU = $sqlU . "Where A.AcNo=Concat(B.ParValue, '".$_SESSION['vdcode']."'); ";
	*/
	
	for($i = 0; $i < 999; $i++)
	{
		if(isset($_POST['CPName' . $i . '']))
		{
			if(!empty($_POST['CPName' . $i . '']))
			{
				$mCPPos = isset($_POST['CPPos' . $i . '']) ? str_replace($R1, $R2, $_POST['CPPos' . $i . '']) : "";
				$mCPNo = isset($_POST['CPNo' . $i . '']) ? str_replace($R1, $R2, $_POST['CPNo' . $i . '']) : "";
				$mCPIDNumber = isset($_POST['CPIDNumber' . $i . '']) ? str_replace($R1, $R2, $_POST['CPIDNumber' . $i . '']) : "";
				
				$sqlU = $sqlU . "Insert Into TblVendorContactPersonUpdate(VdCode, DNo, ContactPersonName, IDNumber, Position, ContactNumber, CreateBy, CreateDt) ";
				$sqlU = $sqlU . "Values('".$_SESSION['vdcode']."', '".substr('000' . ($i + 1), -3)."', '".$_POST['CPName' . $i . '']."', '".$mCPIDNumber."', '".$mCPPos."', '".$mCPNo."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";
			}
		}
		//else
		//{
		//	break;
		//}
	}

	for($i = 0; $i < 999; $i++)
	{
		if(isset($_POST['LueBankCode' . $i . '']))
		{
			if(!empty($_POST['LueBankCode' . $i . '']))
			{
				$mBABankBranch = isset($_POST['BABankBranch' . $i . '']) ? str_replace($R1, $R2, $_POST['BABankBranch' . $i . '']) : "";
				$mBABankAcName = isset($_POST['BABankAcName' . $i . '']) ? str_replace($R1, $R2, $_POST['BABankAcName' . $i . '']) : "";
				$mBABankAcNo = isset($_POST['BABankAcNo' . $i . '']) ? str_replace($R1, $R2, $_POST['BABankAcNo' . $i . '']) : "";
				
				$sqlU = $sqlU . "Insert Into TblVendorBankAccountUpdate(VdCode, DNo, BankCode, BankBranch, BankAcName, BankAcNo, CreateBy, CreateDt) ";
				$sqlU = $sqlU . "Values('".$_SESSION['vdcode']."', '".substr('000' . ($i + 1), -3)."', '".$_POST['LueBankCode' . $i . '']."', '".$mBABankBranch."', '".$mBABankAcName."', '".$mBABankAcNo."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";
			}
		}
		//else
		//{
		//	break;
		//}
	}

	// for($i = 0; $i < 999; $i++)
	// {
	// 	if(isset($_POST['LueVdItCt' . $i . '']))
	// 	{
	// 		if(!empty($_POST['LueVdItCt' . $i . '']))
	// 		{				
	// 			$sqlU = $sqlU . "Insert Into tblvendoritemcategoryUpdate(VdCode, DNo, ItCtCode, CreateBy, CreateDt) ";
	// 			$sqlU = $sqlU . "Values('".$_SESSION['vdcode']."', '".substr('000' . ($i + 1), -3)."', '".$_POST['LueVdItCt' . $i . '']."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";
	// 		}
	// 	}
	// 	//else
	// 	//{
	// 	//	break;
	// 	//}
	// }

	for($i = 0; $i < 999; $i++)
	{
		if(isset($_POST['LueVdSector' . $i . '']))
		{
			if(!empty($_POST['LueVdSector' . $i . '']))
			{				
				$sqlU = $sqlU . "Insert Into tblvendorSectorUpdate(VdCode, DNo, SectorCode, SubSectorCode, CreateBy, CreateDt) ";
				$sqlU = $sqlU . "Values('".$_SESSION['vdcode']."', '".substr('000' . ($i + 1), -3)."', '".$_POST['LueVdSector' . $i . '']."', '".$_POST['LueVdSubSector' . $i . '']."','".$_SESSION['vdusercode']."', CurrentDateTime()); ";
			}
		}
		//else
		//{
		//	break;
		//}
	}

	//echo $sqlU;
    $expSQL = explode(";", $sqlU);
	$countSQL = count($expSQL);
	$success = false;

	for($i = 0; $i < $countSQL - 1; $i++)
	{
		if (mysqli_query($conn, $expSQL[$i])) {
			$success = true;
		} else {
			echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
		}
	}

	if($success)
	{
		mysqli_commit($conn);
		echo "<script type='text/javascript'>window.location='Vendor.php'</script>";
	}
}
else
{
	$q = "Select VdCode From TblVendorUpdate Where VdCode = '".$_SESSION['vdcode']."' And ApproveUserCode Is Null And ApproveStatus Is Null Limit 1; ";

	$rq = mysqli_query($conn, $q) or die(mysqli_error($conn));
	if(mysqli_num_rows($rq) > 0)
	{
		echo "<div class='callout callout-info'>";
		echo "<p>Your changes request is being reviewed.</p>";
		echo "</div>";
	}

    $sql = "";

    $sql = $sql . "SELECT A.*, B.OptDesc As ProcurementStatusDesc From tblvendor A INNER JOIN tbloption B ON A.ProcurementStatus=B.OptCode AND B.OptCat = 'VendorProcurementStatus' Where VdCode = '".$_SESSION['vdcode']."' Limit 1 ";
    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
	$row = mysqli_num_rows($result);

	if($row == 0) {
		echo "<div class='callout callout-info'>";
		echo "<p>Your account is being processed.</p>";
		echo "</div>";
	}
    while($res = mysqli_fetch_assoc($result))
    {
        $VdCode = $res['VdCode'];
        $VdName = $res['VdName'];
        $ShortName = $res['ShortName'];
        $ActInd = $res['ActInd'];
        $VdCtCode = $res['VdCtCode'];
        $Address = $res['Address'];
        $CityCode = $res['CityCode'];
        $SDCode = $res['SDCode'];
        $VilCode = $res['VilCode'];
        $PostalCd = $res['PostalCd'];
        $IdentityNo = $res['IdentityNo'];
        $TIN = $res['TIN'];
        $Phone = $res['Phone'];
        $Fax = $res['Fax'];
        $Email = $res['Email'];
        $Mobile = $res['Mobile']; 
        $CreditLimit = $res['CreditLimit'];
		$TaxInd = $res['TaxInd'];
		$Website = $res['Website'];
		$Remark = $res['Remark'];
		$EstablishedYr = $res['EstablishedYr'];
		$ProcurementStatusDesc = $res['ProcurementStatusDesc'];

?>

<button type="button" class="btn btn-flat" id="BtnEdit" onclick="BtnEditOnClick();" style="background-color: #3373BA; color:white"> 
	<i class="fa fa-edit"></i><strong> Edit</strong>
</button>
<br /><br />

<form class="form-horizontal" name="insertVendor" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-body">

				<br />
				<h4 class="registerLbl">Company Information</h4>
				<hr />
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Company#</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="VdCode" id="VdCode" readonly="readonly" value="<?php echo $VdCode; ?>" />
					</div>
					
					<div class="col-sm-3">
						<span class="error-message" id="eVdCode"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Company Name</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="VdName" id="VdName" readonly="readonly" value="<?php echo $VdName; ?>" />
					</div>
					<div class="col-sm-5">
						<span class="error-message" id="eVdName"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Procurement Status</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="ProcurementStatusDesc" id="ProcurementStatusDesc" readonly="readonly" value="<?php echo $ProcurementStatusDesc; ?>" />
					</div>
					<div class="col-sm-5">
						<span class="error-message" id="eProcurementStatusDesc"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label mandatory-field">Short Name</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="ShortName" id="ShortName" readonly="readonly"value="<?php echo $ShortName; ?>" required />
					</div>
					<div class="col-sm-5">
						<span class="error-message" id="eShortName"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Category</label>

					<div class="col-sm-5">
						<select class="form-control selectVdCategory" name="LueVdCategory" id="LueVdCategory" style="width: 100%;" disabled="disabled">
							<option value=""></option>
						<?php
							$q = "Select VdCtCode As Col1, VdCtName As Col2 From TblVendorCategory Order By VdCtName ";
							$q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
							while($q2 = mysqli_fetch_assoc($q1))
							{
								echo "<option value='".$q2['Col1']."' ";
								if($VdCtCode == $q2['Col1'])
								{
									echo " selected";
								}
								echo ">".$q2['Col2']."</option>";
							}
						?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Taxpayer ID#</label>

					<div class="col-sm-5">
						<input type="number" class="form-control" name="TaxPayer" id="TaxPayer" readonly="readonly" value="<?php echo $TIN; ?>" />
					</div>
					<div class="col-sm-2">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="TaxInd" id="TaxInd" disabled="disabled" 
								<?php
										if($TaxInd == "Y") echo "checked='checked'";
									?>
								/> Tax
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Email Address</label>

					<div class="col-sm-5">
						<input type="email" class="form-control" name="Email" id="Email" readonly="readonly" value="<?php echo $Email; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Establishment Year</label>
					<div class="col-sm-5">
					<input type="number" class="form-control" name="EstablishedYr" id="EstablishedYr" readonly="readonly" maxlength="4" value="<?php echo $EstablishedYr; ?>" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" />
					</div>
				</div>				

				<br />
				<h4 class="registerLbl">Company Address Information</h4>
				<hr />

				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Address</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="MeeAddress" id="MeeAddress" readonly="readonly" value="<?php echo $Address; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">City</label>

					<div class="col-sm-5">
						<select class="form-control selectCity" name="LueCityCode" id="LueCityCode" style="width: 100%;" disabled="disabled" required>
							<option value=""></option>
						<?php
							$q = "Select CityCode As Col1, Concat(A.CityName, ' ( ', IfNull(B.ProvName, ''), ' ', IfNull(C.CntName, ''), ' )')  As Col2 ";
							$q = $q . "From TblCity A ";
							$q = $q . "Left Join TblProvince B On A.ProvCode=B.ProvCode ";
							$q = $q . "Left Join TblCountry C On B.CntCode=C.CntCode ";
							$q = $q . "Order By C.CntName, A.CityName ";

							$q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
							while($q2 = mysqli_fetch_assoc($q1))
							{
								echo "<option value='".$q2['Col1']."' ";
								if($CityCode == $q2['Col1'])
								{
									echo " selected";
								}
								echo ">".$q2['Col2']."</option>";
							}
						?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Sub District</label>

					<div class="col-sm-5">
						<select class="form-control selectSubDistrict" name="LueSDCode" id="LueSDCode" style="width: 100%;" disabled="disabled">
							<option value=""></option>
						<?php
							if(strlen($CityCode) > 0)
							{
								$q = "Select SDCode As Col1, SDName As Col2 ";
								$q = $q . "From TblSubDistrict ";
								$q = $q . "Where Length(Trim(SDName)) > 0 ";
								$q = $q . "And SDName <> '#N/A' ";
								$q = $q . "And CityCode = '".$CityCode."' ";
								$q = $q . "Order By SDName ";

								$q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
								while($q2 = mysqli_fetch_assoc($q1))
								{
									echo "<option value='".$q2['Col1']."' ";
									if($SDCode == $q2['Col1'])
									{
										echo " selected";
									}
									echo ">".$q2['Col2']."</option>";
								}	
							}
						?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Village</label>

					<div class="col-sm-5">
						<select class="form-control selectVillage" name="LueVillageCode" id="LueVillageCode" style="width: 100%;" disabled="disabled">
							<option value=""></option>
						<?php
							if(strlen($SDCode) > 0)
							{
								$q = "Select VilCode As Col1, VilName As Col2 ";
								$q = $q . "From TblVillage ";
								$q = $q . "Where Length(Trim(VilName)) > 0 ";
								$q = $q . "And VilName <> '#N/A' ";
								$q = $q . "And SDCode = '".$SDCode."' ";
								$q = $q . "Order By VilName ";

								$q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
								while($q2 = mysqli_fetch_assoc($q1))
								{
									echo "<option value='".$q2['Col1']."' ";
									if($VilCode == $q2['Col1'])
									{
										echo " selected";
									}
									echo ">".$q2['Col2']."</option>";
								}
							}
						?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Postal</label>
					<div class="col-sm-5">
						<input type="number" class="form-control" name="Postal" id="Postal" readonly="readonly" value="<?php echo $PostalCd; ?>" />
					</div>
				</div>                    
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Phone</label>

					<div class="col-sm-5">
						<input type="number" class="form-control" name="Phone" id="Phone" readonly="readonly" value="<?php echo $Phone; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Fax</label>

					<div class="col-sm-5">
						<input type="number" class="form-control" name="Fax" id="Fax" readonly="readonly" value="<?php echo $Fax; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Mobile</label>

					<div class="col-sm-5">
						<input type="number" class="form-control" name="Mobile" id="Mobile" readonly="readonly" value="<?php echo $Mobile; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Identity Number</label>

					<div class="col-sm-5">
						<input type="number" class="form-control" name="IDNumber" id="IDNumber" readonly="readonly" value="<?php echo $IdentityNo; ?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Website</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="Website" id="Website" readonly="readonly" value="<?php echo $Website; ?>" />
					</div>
				</div>
				
				<hr />
				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Credit Limit</label>

					<div class="col-sm-5">
						<input type="number" class="form-control angka" name="CreditLimit" id="CreditLimit" readonly="readonly" value="<?php echo number_format($CreditLimit, 2, '.', ','); ?>" />
					</div>
				</div>					
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Other Address</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="MeeRemark" id="MeeRemark" readonly="readonly" value="<?php echo $Remark; ?>" />
					</div>
				</div>

			</div>
		</div>
	</div> <!-- /.col-md-12 -->
</div> <!-- /.row -->

<div class="row">
	<div class="col-xs-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#ContactPerson" data-toggle="tab">Contact Person</a></li>
				<li><a href="#Bank" data-toggle="tab">Bank</a></li>
				<!-- <li><a href="#ItCt" data-toggle="tab">Item's Category</a></li> -->
				<li><a href="#VdSector" data-toggle="tab">Sector</a></li>
			</ul>
			<div class="tab-content">
				<div class="active tab-pane" id="ContactPerson">
					<table class="table Contact-Person" id="tblBankContactPerson">
						<thead>
							<tr>
								<th>Contact Person Name</th>
								<th>ID Number</th>
								<th>Position</th>
								<th>Contact Number</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$sql = "";

							$sql = $sql . "Select * ";
							$sql = $sql . "From TblVendorContactPerson ";
							$sql = $sql . "Where VdCode = '".$_SESSION['vdcode']."' Order By DNo ";

							$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
							$row = 0;

							if(mysqli_num_rows($result) > 0)
							{
								$row = mysqli_num_rows($result);
								$no = 0;
								while($res = mysqli_fetch_assoc($result))
								{
									echo "<tr>";
									echo "<td class='col-sm-4'>";
									echo "<input type='text' name='CPName".$no."' class='form-control' value='".$res['ContactPersonName']."' />";
									echo "</td>";
									echo "<td class='col-sm-2'>";
									echo "<input type='text' name='CPIDNumber".$no."' class='form-control' value='".$res['IDNumber']."' />";
									echo "</td>";
									echo "<td class='col-sm-2'>";
									echo "<input type='text' name='CPPos".$no."' class='form-control' value='".$res['Position']."' />";
									echo "</td>";
									echo "<td class='col-sm-2'>";
									echo "<input type='number' name='CPNo".$no."' class='form-control' value='".$res['ContactNumber']."' />";
									echo "</td>";
									echo "<td><input type='button' id='CPbtnDel' class='CPbtnDel btn btn-md btn-danger' value='Delete' disabled='disabled'></td>";
									echo "</tr>";

									$no++;
								}
							}

							echo "<input type='hidden' name='countVdContactPerson' id='countVdContactPerson' value='".$row."' />";
						?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5" style="text-align: left;">
									<input type="button" class="btn btn-lg btn-block " id="CPaddrow" value="Add Row" style="visibility: hidden;"  />
								</td>
							</tr>
							<tr>
							</tr>
						</tfoot>
					</table>
				</div>
				
				<div class="tab-pane" id="Bank">
					<table class="table BankAccount" id="tblBankAccount">
						<thead>
							<tr>
								<th>Bank Name</th>
								<th>Branch Name</th>
								<th>Account Name</th>
								<th>Account Number</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$sql = "";

							$sql = $sql . "Select * ";
							$sql = $sql . "From TblVendorBankAccount ";
							$sql = $sql . "Where VdCode = '".$_SESSION['vdcode']."' Order By DNo ";

							$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
							$row = 0;

							if(mysqli_num_rows($result) > 0)
							{
								$row = mysqli_num_rows($result);
								$no = 0;
								while($res = mysqli_fetch_assoc($result))
								{
									echo "<tr>";
									echo "<td class='col-sm-3'>";
									echo "<select class='form-control selectBankCode".$no."' name='LueBankCode".$no."' id='LueBankCode".$no."' style='width: 100%;'>";
									echo "<option value=''></option>";
									$sqlB = "Select BankCode As Col1, BankName As Col2 From TblBank Order By BankName ";
									$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
									while($resB = mysqli_fetch_assoc($resultB))
									{
										echo "<option value='".$resB['Col1']."' ";
										if($res['BankCode'] == $resB['Col1'])
										{
											echo " selected";
										}
										echo " >".$resB['Col2']."</option>";
									}
									echo "</select>";
									echo "</td>";
									echo "<td class='col-sm-3'>";
									echo "<input type='text' name='BABankBranch".$no."' class='form-control' value='".$res['BankBranch']."' />";
									echo "</td>";
									echo "<td class='col-sm-3'>";
									echo "<input type='text' name='BABankAcName".$no."' class='form-control' value='".$res['BankAcName']."' />";
									echo "</td>";
									echo "<td class='col-sm-3'>";
									echo "<input type='number' name='BABankAcNo".$no."' class='form-control' value='".$res['BankAcNo']."' />";
									echo "</td>";
									echo "<td><input type='button' id='BAbtnDel' class='BAbtnDel btn btn-md btn-danger' value='Delete' disabled='disabled'></td>";
									echo "</tr>";

									$no++;
								}
							}

							echo "<input type='hidden' name='countVdBankAccount' id='countVdBankAccount' value='".$row."' />";
						?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5" style="text-align: left;">
									<input type="button" class="btn btn-lg btn-block " id="BAaddrow" value="Add Row" style="visibility: hidden;"  />
								</td>
							</tr>
							<tr>
							</tr>
						</tfoot>
					</table>
				</div>

				<!-- <div class="tab-pane" id="ItCt">
					<span id="eTblItCt" class="error-message"></span>
					<table class="table ItCt" id="tblItCt">
						<thead>
							<tr>
								<th>Item's Category Name</th>
							</tr>
						</thead>
						<tbody>
						<?php
							// $sql = "";

							// $sql = $sql . "Select * ";
							// $sql = $sql . "From TblVendorItemCategory ";
							// $sql = $sql . "Where VdCode = '".$_SESSION['vdcode']."' Order By DNo ";

							// $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
							// $row = 0;

							// if(mysqli_num_rows($result) > 0)
							// {
							// 	$row = mysqli_num_rows($result);
							// 	$no = 0;
							// 	while($res = mysqli_fetch_assoc($result))
							// 	{
							// 		echo "<tr>";
							// 		echo "<td class='col-sm-3'>";
							// 		echo "<select class='form-control selectVdItCt".$no."' name='LueVdItCt".$no."' id='LueVdItCt".$no."' style='width: 100%;'>";
							// 		echo "<option value=''></option>";
							// 		$sqlB = "Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory Order By ItCtName ";
							// 		$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
							// 		while($resB = mysqli_fetch_assoc($resultB))
							// 		{
							// 			echo "<option value='".$resB['Col1']."' ";
							// 			if($res['ItCtCode'] == $resB['Col1'])
							// 			{
							// 				echo " selected";
							// 			}
							// 			echo " >".$resB['Col2']."</option>";
							// 		}
							// 		echo "</select>";
							// 		echo "</td>";
							// 		echo "<td><input type='button' id='ItCtbtnDel' class='ItCtbtnDel btn btn-md btn-danger' value='Delete' disabled='disabled'></td>";
							// 		echo "</tr>";

							// 		$no++;
							// 	}
							// }

							// echo "<input type='hidden' name='countVdItCt' id='countVdItCt' value='".$row."' />";
						?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5" style="text-align: left;">
									<input type="button" class="btn btn-lg btn-block " id="ItCtaddrow" value="Add Row" style="visibility: hidden;"  />
								</td>
							</tr>
							<tr>
							</tr>
						</tfoot>
					</table>
				</div> -->

				<div class="tab-pane" id="VdSector">
					<span id="eTblVdSector" class="error-message"></span>
					<table class="table VdSector" id="tblVdSector">
						<thead>
							<tr>
								<th>Vendor's Sector</th>
								<th>Sub Sector</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$sql = "";

							$sql = $sql . "Select * ";
							$sql = $sql . "From TblVendorSector ";
							$sql = $sql . "Where VdCode = '".$_SESSION['vdcode']."' Order By DNo; ";

							$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
							$row = 0;

							if(mysqli_num_rows($result) > 0)
							{
								$row = mysqli_num_rows($result);
								$no = 0;
								while($res = mysqli_fetch_assoc($result))
								{
									echo "<script></script>";
									echo "<tr>";
									echo "<td class='col-sm-6'>";
									echo "<select style='text-overflow: ellipsis' class='form-control selectVdSector".$no."' name='LueVdSector".$no."' onchange='selectedSector(this, ".$no.");' id='LueVdSector".$no."' style='width: 100%;'>";
									echo "<option value=''></option>";
									$sqlB = "Select SectorCode As Col1, SectorName As Col2 From TblSector ";
									if(strlen($res['SectorCode']) <= 0 )
									{
										$sqlB = $sqlB . "Where ActInd = 'Y' ";
									}
									$sqlB = $sqlB . "Order By SectorName; ";
									$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
									while($resB = mysqli_fetch_assoc($resultB))
									{
										echo "<option value='".$resB['Col1']."' ";
										if($res['SectorCode'] === $resB['Col1'])
										{
											echo " selected";
										}
										echo " >".$resB['Col2']."</option>";
									}
									echo "</select>";
									echo "</td>";

									echo "<td class='col-sm-6'>";
									echo "<select style='text-overflow: ellipsis' class='form-control selectVdSubSector".$no."' name='LueVdSubSector".$no."' id='LueVdSubSector".$no."' style='width: 100%;'>";
									echo "<option value=''></option>";
									$sqlB = "Select SubSectorCode As Col1, SubSectorName As Col2 From TblSubSector ";									
									$sqlB = $sqlB . "Where ActInd = 'Y' AND SectorCode = '".$res['SectorCode']."' Order By SubSectorName; ";
									$resultB = mysqli_query($conn, $sqlB);
									while($resB = mysqli_fetch_assoc($resultB))
									{										
										echo "<option value='".$resB['Col1']."' ";
										if($res['SubSectorCode'] == $resB['Col1'])
										{
											echo " selected";
										}
										echo " >".$resB['Col2']."</option>";
									}
									echo "</select>";
									echo "</td>";

									echo "<td><input type='button' id='VdSectorbtnDel' class='VdSectorbtnDel btn btn-md btn-danger' value='Delete' disabled='disabled'></td>";
									echo "</tr>";

									$no++;
								}
							}

							echo "<input type='hidden' name='countVdSector' id='countVdSector' value='".$row."' />";
							echo "<input type='hidden' name='countVdSubSector' id='countVdSubSector' value='".$row."' />";
						?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5" style="text-align: left;">
									<input type="button" class="btn btn-lg btn-block " id="VdSectoraddrow" value="Add Row" style="visibility: hidden;"  />
								</td>
							</tr>
							<tr>
							</tr>
						</tfoot>
					</table>
				</div>
			
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- /.nav-tabs-custom -->

		<hr />

        <input type="reset" class="btn " onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="visibility: hidden; background-color: #58585A; color:white"/>
        <button type="submit" name="submit" class="btn btn-info pull-right" id="BtnSave" style="visibility: hidden;">Save</button>

	</div>
</div>

</form>

<script>
  $(function () {
    //Initialize Select2 Elements
	$(".selectVdCategory").select2();

    $(".selectVillage").select2();

    $(".selectCity").select2();

	$(".selectSubDistrict").select2();

	$(".selectHO").select2();
  });
</script>

<script type="text/javascript">

	$(function() {
		$('table tr').each(function() {
			$(this).find('td').each(function() {
				$(this).find('input,select').attr("readonly", true);
				$(this).find('input,select').attr("disabled", true);
			});
		});
	});

	function BtnEditOnClick()
	{	
        document.getElementById("BtnCancel").style.visibility = "visible";
		document.getElementById("BtnSave").style.visibility = "visible";
		document.getElementById("CPaddrow").style.visibility = "visible";
		document.getElementById("BAaddrow").style.visibility = "visible";
		// document.getElementById("ItCtaddrow").style.visibility = "visible";
		document.getElementById("VdSectoraddrow").style.visibility = "visible";
		document.getElementById("ShortName").readOnly = false;
        //document.getElementById("ActInd").disabled = false;
        document.getElementById("LueVdCategory").disabled = false;
        document.getElementById("MeeAddress").readOnly = false;
        document.getElementById("LueCityCode").disabled = false;
        if(document.getElementById("LueCityCode").value != "") { document.getElementById("LueSDCode").disabled = false; }
        if(document.getElementById("LueSDCode").value != "") { document.getElementById("LueVillageCode").disabled = false; }
        document.getElementById("Postal").readOnly = false;
        document.getElementById("IDNumber").readOnly = false;
        document.getElementById("TaxPayer").readOnly = false;
        document.getElementById("TaxInd").disabled = false;
        document.getElementById("Phone").readOnly = false;
        document.getElementById("Fax").readOnly = false;
        document.getElementById("Email").readOnly = false;
        document.getElementById("Mobile").readOnly = false;
		document.getElementById("CreditLimit").readOnly = false;
		document.getElementById("Website").readOnly = false;
		document.getElementById("MeeRemark").readOnly = false;
		document.getElementById("EstablishedYr").readOnly = false;

		for (let i = 0; i < 15; i++) {
			$(`.selectVdSector${i}`).select2();
			$(`.selectVdSubSector${i}`).select2();
		}
		$(function() {
			$('table tr').each(function() {
				$(this).find('td').each(function() {
					$(this).find('input,select').attr("disabled", false);
					$(this).find('input,select').attr("readonly", false);
				});
			});
		});
		
	}
</script>

<script type="text/javascript">

function selectedSector(sector, counterVdSubSector){
		var vdSector = sector.value;
		$.ajax({
			url : 'Vendor.php',
			method: 'POST',
			data: {
			sector : vdSector,
			},
			success (data){
			data = JSON.parse(data);
			$(`#LueVdSubSector${counterVdSubSector}`).html($("<option></option>"));
			$.each(data, function(key, value) {
				$(`#LueVdSubSector${counterVdSubSector}`)
					.append($("<option style='text-overflow:'ellipsis''></option>")
					.attr("value",value.SubSectorCode)
					.text(value.SubSectorName));
			});
			// $(`#LueVdSubSector${counterVdSubSector}`).html(option);

			},
			error (err){
			console.log(`Error ${err.innerHtmlText}`)
			}
		});
	};

$(document).ready(function () {
	var countVdBankAccount = document.getElementById("countVdBankAccount").value;
	var counterBA = Number(countVdBankAccount);

	$("#BAaddrow").on("click", function () {
		var newRow = $("<tr>");
		var cols = "";

		cols += '<td><select class="form-control selectBankCode' + counterBA + '" name="LueBankCode' + counterBA + '" id="LueBankCode' + counterBA + '" style="width: 100%;">';
		cols += '<option value=""></option>';
		<?php
			$sqlB = "Select BankCode As Col1, BankName As Col2 From TblBank Order By BankName ";
			$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
			while($resB = mysqli_fetch_assoc($resultB))
			{
			?>
				cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
			<?php
			}
		?>
		cols += '</select></td>';
		cols += '<td><input type="text" class="form-control" name="BABankBranch' + counterBA + '"/></td>';
		cols += '<td><input type="text" class="form-control" name="BABankAcName' + counterBA + '"/></td>';
		cols += '<td><input type="text" class="form-control" name="BABankAcNo' + counterBA + '"/></td>';

		cols += '<td><input type="button" class="BAbtnDel btn btn-md btn-danger" value="Delete"></td>';
		newRow.append(cols);
		$("table.BankAccount").append(newRow);

		for(var j = 0; j <= counterBA; j++)
			$(".selectBankCode" + j + "").select2();

		counterBA++;
	});

	$("table.BankAccount").on("click", ".BAbtnDel", function (event) {
		$(this).closest("tr").remove();       
		counterBA -= 1
	});

	// var countVdItCt = document.getElementById("countVdItCt").value;
	// var counterItCt = Number(countVdItCt);

	// $("#ItCtaddrow").on("click", function () {
	// 	var newRow = $("<tr>");
	// 	var cols = "";

	// 	cols += '<td><select class="form-control selectVdItCt' + counterItCt + '" name="LueVdItCt' + counterItCt + '" id="LueVdItCt' + counterItCt + '" style="width: 100%;">';
	// 	cols += '<option value=""></option>';
	// 	<?php
	// 		$sqlB = "Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory Order By ItCtName ";
	// 		$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
	// 		while($resB = mysqli_fetch_assoc($resultB))
	// 		{
	// 		?>
	// 			cols += '<option value="<?php //echo $resB['Col1']; ?>"><?php //echo str_replace("'", "\'", $resB['Col2']); ?></option>';
	// 		<?php
	// 		}
	// 	?>
	// 	cols += '</select></td>';

	// 	cols += '<td><input type="button" class="ItCtbtnDel btn btn-md btn-danger" value="Delete"></td>';
	// 	newRow.append(cols);
	// 	$("table.ItCt").append(newRow);

	// 	for(var j = 0; j <= counterItCt; j++)
	// 		$(".selectVdItCt" + j + "").select2();

	// 	counterItCt++;
	// });

	// $("table.ItCt").on("click", ".ItCtbtnDel", function (event) {
	// 	$(this).closest("tr").remove();       
	// 	counterItCt -= 1
	// });

	var blankComboBox = "<option value=''></option>";

	$("#LueCityCode").change(function(){
		var CityCode = $("#LueCityCode").val();
		$.ajax({
			url: "GetSubDistrict.php",
			data: "CityCode="+CityCode,
			cache: false,
			success: function(msg){
				$("#LueSDCode").html(msg);
				$("#LueVillageCode").html(blankComboBox);
				$("#LueVillageCode").attr("disabled", true);
			}
		});
	});

	$("#LueSDCode").change(function(){
		var SDCode = $("#LueSDCode").val();
		$.ajax({
			url: "GetVillage.php",
			data: "SDCode="+SDCode,
			cache: false,
			success: function(msg){
				$("#LueVillageCode").attr("disabled", false);
				$("#LueVillageCode").html(msg);
			}
		});
	});

	var countVdSector = document.getElementById("countVdSector").value;
	var counterVdSector = Number(countVdSector);
	var countVdSubSector = document.getElementById("countVdSubSector").value;
	var counterVdSubSector = Number(countVdSubSector);

	$("#VdSectoraddrow").on("click", function () {

		var newRow = $("<tr>");
		var cols = "";
		cols += '<td><select class="form-control selectVdSector' + counterVdSector + '" name="LueVdSector' + counterVdSector + '" id="LueVdSector' + counterVdSector + '" style="width: 100%;">';
		cols += '<option value=""></option>';
		<?php
			$sqlB = "Select SectorCode As Col1, SectorName As Col2 From TblSector Where ActInd = 'Y' Order By SectorName ";
			$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
			while($resB = mysqli_fetch_assoc($resultB))
			{
			?>
				cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
			<?php
			}
		?>
		cols += '</select></td>';

		cols += '<td><select class="form-control selectVdSubSector' + counterVdSubSector + '" name="LueVdSubSector' + counterVdSubSector + '" id="LueVdSubSector' + counterVdSubSector + '" style="width: 100%;">';
		cols += '<option value=""></option>';
		
		cols += '</select></td>';

		cols += '<td><input type="button" class="VdSectorbtnDel btn btn-md btn-danger" value="Delete"></td>';
		newRow.append(cols);
		$("table.VdSector").append(newRow);

		$(`#LueVdSector${counterVdSector}`).change( function(event){
			var vdSector = event.target.value;
			$.ajax({
				url : 'Vendor.php',
				method: 'POST',
				data: {
				sector : vdSector,
				},
				success (data){
				data = JSON.parse(data);
				$(`#LueVdSubSector${counterVdSubSector-1}`).html($("<option></option>"));
				$.each(data, function(key, value) {
					$(`#LueVdSubSector${counterVdSubSector-1}`)
						.append($("<option style='text-overflow:'ellipsis''></option>")
						.attr("value",value.SubSectorCode)
						.text(value.SubSectorName));
				});
				// $(`#LueVdSubSector${counterVdSubSector}`).html(option);

				},
				error (err){
				console.log(`Error ${err.innerHtmlText}`)
				}
			});
		});
	
		for(var j = 0; j <= counterVdSector; j++)
		{
			$(".selectVdSector" + j + "").select2();
		}

		for(var j = 0; j <= counterVdSubSector; j++)
		{
			$(".selectVdSubSector" + j + "").select2();
		}

		counterVdSector++; counterVdSubSector++;
	});

	$("table.VdSector").on("click", ".VdSectorbtnDel", function (event) {
		$(this).closest("tr").remove();       
		counterVdSector -= 1
	});

});
</script>

<script type="text/javascript" src="Vendorv1.0.4.js"></script>

<?php
    }
}
?>

<?php
include "footer.php";
?>