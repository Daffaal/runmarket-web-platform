<?php
session_start();
if(!isset($_SESSION['vdusercode']))
{
	header("Location: login.php");
	exit();
}
else
{
	if(isset($_SESSION['vdlast_activity']) && isset($_SESSION['vdexpire_time']))
	{
		if( $_SESSION['vdlast_activity'] < time()-$_SESSION['vdexpire_time'] ) 
		{ //have we expired?
		    //redirect to logout.php
		    echo "<script type='text/javascript'>window.location='logout.php'</script>";
		}
		else
		{ //if we haven't expired:
		    $_SESSION['vdlast_activity'] = time(); //this was the moment of last activity.
		}
	}
}

?>