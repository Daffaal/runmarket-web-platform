<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

if (isset($_POST['submit'])) {
    if (isset($_POST['DNo'])) {

        $countRow = $_POST['countRow'];

        $host = FTP_HOST;
        $user = FTP_USER;
        $pass = FTP_PASS;

        $sql = "";
        $SeqNo = $_POST['SeqNo'];
        $RFQ3DNo = $_POST['RFQ3DNo'];
        $MaterialRequestDocNoHdr = $_POST['DocNoHdr'];
        $MaterialRequestDocNo = $_POST['DocNo'];
        $MaterialRequestDNo = $_POST['DNo'];
        // $MaterialRequestDNoHdr = $_POST['DNoHdr'];
        $PtCode = $_POST['PtCode'];
        $CurCode = $_POST['CurCode'];
        $DTCode = strlen($_POST['DTCode']) <= 0 ? NULL : $_POST['DTCode'];
        $ExpiredDt = strlen($_POST['ExpiredDt']) <= 0 ? NULL : date('Ymd', strtotime($_POST['ExpiredDt']));
        $R1 = array(" ", ".", ",");
        $R2 = array("", "", ".");
        $UPrice = str_replace($R1, $R2, $_POST['unitPrice']);
        $Remark = isset($_POST['remark']) <= 0 ? NULL : $_POST['remark'];
        $RemarkHdr = $_POST['remarkHdr'];
        $DocDt = date("Ymd");
        $DocNo = GenerateDocNo($DocDt, "Qt", "TblQtHdr");
        $SystemNo = date("Ym") . substr($DocNo, 0, 4);
        // $ItCode = GetValue("Select ItCode From TblMaterialRequestDtl Where DocNo = '" . $MaterialRequestDocNo[$i] . "' And DNo = '" . $MaterialRequestDNo[$i] . "'; ");
        $Qty = $_POST['qty'];
        $Amt = str_replace($R1, $R2, $_POST['totalPrice']);
        $DNo = array();
        for ($j = 1; $j <= $countRow; $j++) {
            $generateDNo = str_pad($j, 3, "0", STR_PAD_LEFT);
            array_push($DNo, $generateDNo);
        }
        $sql = "Insert Into TblQtHdr(DocNo, SystemNo, DocDt, Status, CancelInd, VdCode, HiddenInd,PtCode, CurCode, DTCode, ExpiredDt, MaterialRequestDocNo, RFQSeqNo, RFQ3DNo ,Remark, CreateBy, CreateDt) ";
        $sql = $sql . "Values('" . $DocNo . "', '" . $SystemNo . "', '" . $DocDt . "', 'A', 'N', '" . $_SESSION['vdcode'] . "', 'Y' ,'" . $PtCode . "', '" . $CurCode . "', ";

        if (strlen($DTCode) <= 0)
            $sql = $sql . "NULL,";
        else
            $sql = $sql .  "'" . $DTCode . "', ";

        if (strlen($ExpiredDt) <= 0)
            $sql = $sql .  "NULL,";
        else
            $sql = $sql .  "'" . $ExpiredDt . "', ";

        $sql = $sql .  "'" . $MaterialRequestDocNoHdr . "', '".$SeqNo."', '".$RFQ3DNo."' ,'" . $RemarkHdr . "', '" . $_SESSION['vdusercode'] . "', CurrentDateTime()); ";

        for ($i = 0; $i < $countRow; $i++) {

            $ItCode = GetValue("Select ItCode From TblMaterialRequestDtl Where DocNo = '" . $MaterialRequestDocNo[$i] . "' And DNo = '" . $MaterialRequestDNo[$i] . "'; ");

            $sql = $sql . "Update TblQtDtl T1  ";
            $sql = $sql . "Inner Join TblQtHdr T2 ";
            $sql = $sql . "    On T1.DocNo=T2.DocNo ";
            $sql = $sql . "    And T2.Status='A' ";
            $sql = $sql . "    And T2.VdCode='" . $_SESSION['vdcode'] . "' ";
            $sql = $sql . "    And T2.PtCode='" . $PtCode . "' ";
            $sql = $sql . "    And T2.MaterialRequestDocNo='" . $MaterialRequestDocNo[$i] . "' ";
            $sql = $sql . "Set T1.ActInd='N', T1.LastUpBy='" . $_SESSION['vdusercode'] . "', T1.LastUpDt=CurrentDateTime() ";
            $sql = $sql . "Where T1.ActInd='Y' ";
            $sql = $sql . "And T1.ItCode = '" . $ItCode . "' ";
            $sql = $sql . "And Exists( ";
            $sql = $sql . "    Select DocNo From TblQtHdr Where DocNo='" . $DocNo . "' And Status='A' ";
            $sql = $sql . ");";

            $sql = $sql . "Insert Into TblQtDtl(DocNo, DNo, ItCode, ActInd, UPrice, UPriceInit, Remark, MaterialRequestDocNo, MaterialRequestDNo, CreateBy, CreateDt, Qty, Amt) ";
            $sql = $sql . "Values('" . $DocNo . "', '" . $DNo[$i] . "', '" . $ItCode . "', 'Y', '" . $UPrice[$i] . "', '" . $UPrice[$i] . "', '" . $Remark[$i] . "', '" . $MaterialRequestDocNo[$i] . "', '" . $MaterialRequestDNo[$i] . "' ,'" . $_SESSION['vdusercode'] . "', CurrentDateTime(), " . $Qty[$i] . ", '" . $Amt[$i] . "'); ";
            // mysqli_query($conn, $sql) or die (mysqli_error($conn));
            // var_dump($DNo);
        }

        for ($j=0; $j < count($DNo); $j++) { 
            if (isset($_FILES['VdFile'.($j+1).''])) {
                $imgFile = $_FILES['VdFile'.($j+1).'']['name'];
                $tmp_dir = $_FILES['VdFile'.($j+1).'']['tmp_name'];
                $imgSize = $_FILES['VdFile'.($j+1).'']['size'];
                $imgExt = array();
                $VdFilesLoc = array();
                $DNo2 = array();
                $i = 1;
                $upload_dir = "dist/pdf/";
                $valid_extension = array('pdf', 'png', 'jpg', 'jpeg');
                $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;
    
                // generate dno2
                $q = (" select (Max(t.DNo2) + 1) DNo2 
                        from tblqtdtl2 t 
                        inner join tblqthdr t2 on t2.DocNo = t.DocNo 
                        where t2.DocNo = '" . $DocNo . "'
                        ");
    
                $result = mysqli_query($conn, $q);
                $rows = GetValue($q);
                if (strlen($rows) == 0) $rows = "0";
                $generate = null;
    
                if ($rows == "0") {
                    for ($i = 0; $i < count($imgFile); $i++) {
                        $generate = str_pad(($rows + 1) + $i, 3, "0", STR_PAD_LEFT);
                        array_push($DNo2, $generate);
                    }
                } else {
                    // array_push($DNo2, str_pad($rows + 1, 3, "0", STR_PAD_LEFT));
                    for ($i = 0; $i < count($imgFile); $i++) {
                        $generate = str_pad($rows + $i, 3, "0", STR_PAD_LEFT);
                        array_push($DNo2, $generate);
                    }
                }
    
                //separating extension
                for ($i = 0; $i < count($imgFile); $i++) {
                    $ext = strtolower(pathinfo($imgFile[$i], PATHINFO_EXTENSION));
                    array_push($imgExt, $ext);
                }
    
                //generate file name
                for ($i = 0; $i < count($imgExt); $i++) {
    
                    $filesLoc = $_SESSION['vdcode'] . "_" . str_replace("/", "-", $DocNo) . "_".$DNo[$j]."_Item-" . $DNo2[$i] . "_" . date("Ymdhms") . "." . $imgExt[$i];
                    array_push($VdFilesLoc, $filesLoc);
                }
    
                // var_dump($imgFile);
                // echo "<br>";
                // var_dump($VdFilesLoc);
                // echo "<br>";
                // var_dump(basename($VdFilesLoc[0]));
                // echo "<br>";
                // var_dump($DNo2);
                //uploading and saving to db
    
                for ($i = 0; $i < count($imgFile); $i++) {
    
                    if (in_array($imgExt[$i], $valid_extension)) // check kalau extensi nya valid
                    {
    
                        if ($imgSize[$i] == 0) {
    
                            echo "<script type='text/javascript'>alert('Maximum document size is " . GetParameter("FileSizeMaxUploadFTPClient") . " MB.')</script>";
                            echo "<script type='text/javascript'>window.location='RequestQuotation.php'</script>";
                        } else {
    
                            if ($imgSize[$i] < $mMaxFile) // if image size < 1025 KB
                            {
                                // echo "<script type='text/javascript'>alert('".$tmp_dir[$i]." || ".$upload_dir.$VdFilesLoc[$i]."');</script>";
                                move_uploaded_file($tmp_dir[$i], $upload_dir . basename($VdFilesLoc[$i]));
    
                                $sql = $sql . "Insert Into TblQtDtl2(DocNo, DNo, DNo2, FileLocation, CreateBy, CreateDt) ";
                                $sql = $sql . "Values('" . $DocNo . "', '".$DNo[$j]."', '" . $DNo2[$i] . "', '" . $VdFilesLoc[$i] . "', 
                                '" . $_SESSION['vdcode'] . "', CurrentDateTime()); ";
                            } else {
    
                                echo "<script type='text/javascript'>alert('Maximum document size is " . GetParameter("FileSizeMaxUploadFTPClient") . " MB.')</script>";
                                echo "<script type='text/javascript'>window.location='RequestQuotation.php'</script>";
                            }
                        }
                    } else {
                        echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
                        echo "<script type='text/javascript'>window.location='RequestQuotation.php'</script>";
                    }
                }
    
                echo "<br>";
                if (FTP_STATUS === "enable") {
                    try {
                        $ftpConn = ftp_connect($host);
                        $login = ftp_login($ftpConn, $user, $pass);
                        $ftpPasv = ftp_pasv($ftpConn, true);
    
                        if ((!$ftpConn) || (!$login)) {
                            echo "FTP connection has failed! Attempted to connect to " . $host . ".";
                        }
    
                        if (!$ftpPasv) {
                            echo "Cannot switch to passive mode";
                        }
    
                        if ($login) {
    
                            $ftpSuccess = false;
                            for ($i = 0; $i < count($VdFilesLoc); $i++) {
    
                                if (!ftp_put($ftpConn, FTP_DESTINATION . $VdFilesLoc[$i], $upload_dir . $VdFilesLoc[$i], FTP_BINARY)) {
                                    echo "<script>alert('Error uploading file.');</script>";
                                } else {
                                    $ftpSuccess = true;
                                }
                            }
                        }
                    } catch (Exception $e) {
                        echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }
    
                    if ($ftpSuccess) {
                        echo "<script>alert('Success uploading File.');</script>";
                        ftp_close($ftpConn);
                    }
                }
            }
        }


        $expSQL = explode(";", $sql);
        $countSQL = count($expSQL);
        $success = false;

        for ($i = 0; $i < $countSQL - 1; $i++) {
            if (mysqli_query($conn, $expSQL[$i])) {
                $success = true;
            } else {
                echo "Error: " . $expSQL[$i] . "<br />" . mysqli_error($conn);
                $success = false;
                mysqli_rollback($conn);
                break;
            }
        }

        if ($success) {
            mysqli_commit($conn);
            echo "<script type='text/javascript'>window.location='RequestQuotation.php'</script>";
        }


        include "footer.php";
    }
}
