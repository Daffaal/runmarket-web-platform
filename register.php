<?php
require 'conn.php';

if(isset($_POST['sector'])){

  
  $VdSector = $_POST['sector'];
  $sqlS = "SELECT SubSectorCode, SubSectorName FROM TblSubSector WHERE SectorCode = '".$VdSector."'";

  $data = array();
  $result  = mysqli_query($conn, $sqlS);
  while ($res = mysqli_fetch_assoc($result)) {
    $data[] = $res;
  }

  print(json_encode($data));
  exit();
}

include 'header.php';
include 'StdMtd.php';

if(isset($_SESSION['vdusercode']))
{
	header("Location: index.php");
}

$host = FTP_HOST;
$user = FTP_USER;
$pass = FTP_PASS;

$errorMessage = "";
$successMessage = "";

// If form submitted, insert values into the database.
if (isset($_POST['submit'])){

  $sqlU = "";

  if(isset($_POST['VdName']) && isset($_POST['ShortName']) && isset($_POST['Email']))
  {
    // removes backslashes
    //$username = stripslashes($_REQUEST['username']);
      //escapes special characters in a string
    $VdName = mysqli_real_escape_string($conn,$_POST['VdName']);
    //$password = stripslashes($_REQUEST['password']);
    $ShortName = mysqli_real_escape_string($conn,$_POST['ShortName']);
    $emails = mysqli_real_escape_string($conn,$_POST['Email']);
    // $ContactPersonName = mysqli_real_escape_string($conn,$_POST['ContactPersonName']);
    // $IDNumber = mysqli_real_escape_string($conn,$_POST['IDNumber']);
    $TIN = mysqli_real_escape_string($conn,$_POST['TIN']);
    $NIB = mysqli_real_escape_string($conn, $_POST['NIB']);
    $Address = mysqli_real_escape_string($conn,$_POST['Address']);
    $LueCityCode = mysqli_real_escape_string($conn,$_POST['LueCityCode']);
    $PostalCd = mysqli_real_escape_string($conn,$_POST['PostalCd']);
    $Phone = mysqli_real_escape_string($conn,$_POST['Phone']);
    $Fax = mysqli_real_escape_string($conn,$_POST['Fax']);
    $Mobile = mysqli_real_escape_string($conn,$_POST['Mobile']);
    $Website = mysqli_real_escape_string($conn,$_POST['Website']);
    //$Parent = mysqli_real_escape_string($conn,$_POST['LueHOCode']);
    // $BankCode = mysqli_real_escape_string($conn,$_POST['LueBankCode']);
    // $BankBranch = mysqli_real_escape_string($conn,$_POST['BankBranch']);
    // $BankAcName = mysqli_real_escape_string($conn,$_POST['BankAcName']);
    // $BankAcNo = mysqli_real_escape_string($conn,$_POST['BankAcNo']);
    // $Position = mysqli_real_escape_string($conn,$_POST['Position']);
    // $ContactNumber = mysqli_real_escape_string($conn,$_POST['ContactNumber']);
    $Remark = mysqli_real_escape_string($conn,$_POST['Remark']);
    $EstablishedYr = mysqli_real_escape_string($conn,$_POST['EstablishedYr']);
    $VdCtCode = mysqli_real_escape_string($conn,$_POST['LueCtCode']);
    //Checking is user existing in the database or not

    $sql = "";

    $sql = $sql . "Select X.Email ";
    $sql = $sql . "From ";
    $sql = $sql . "( ";
    $sql = $sql . "  Select A.Email ";
    $sql = $sql . "  From TblVendor A ";
    $sql = $sql . "  Where A.Email = '".$emails."' ";
    $sql = $sql . "  Union All ";
    $sql = $sql . "  Select T.Email ";
    $sql = $sql . "  From TblVendorRegister T ";
    $sql = $sql . "  Where T.Email = '".$emails."' ";
    $sql = $sql . "  Union All ";
    $sql = $sql . "  Select B.VendorRegisterEmail As Email ";
    $sql = $sql . "  From TblVendor B ";
    $sql = $sql . "  Where B.VendorRegisterEmail = '".$emails."' ";
    $sql = $sql . ")X ";
    $sql = $sql . "Limit 1 ";


    $sqlU = $sqlU . "Insert Into TblVendorRegister(Email, VdName, ShortName, VdCtCode, TIN, NIB ,Address, CityCode, PostalCd, Phone, Fax, Mobile, Website, EstablishedYr, Remark, VerifiedInd, CreateBy, CreateDt) ";
    $sqlU = $sqlU . "Values('".$emails."', '".$VdName."', '".$ShortName."', '".$VdCtCode." ','".$TIN."', NullIf('".$NIB."', '') ,'".$Address."', '".$LueCityCode."', '".$PostalCd."', '".$Phone."', NullIf('".$Fax."', ''), '".$Mobile."', NullIf('".$Website."', ''),NullIf('".$EstablishedYr."',''), NullIf('".$Remark."', ''), 'N', 'System', CurrentDateTime());";
  }

  
  $ID = GetValue("Select IfNull(Max(RegisterID) + 1, 1) RegisterID From TblVendorRegister ; ");
  
  $R1 = array("<p>", "</p>", "'");
  $R2 = array("", "", "\'");
  
  for($i = 0; $i < 999; $i++)
	{
		if(isset($_POST['CPName' . $i . '']))
		{
			if(!empty($_POST['CPName' . $i . '']))
			{
				$mCPPos = isset($_POST['CPPos' . $i . '']) ? str_replace($R1, $R2, $_POST['CPPos' . $i . '']) : "";
				$mCPNo = isset($_POST['CPNo' . $i . '']) ? str_replace($R1, $R2, $_POST['CPNo' . $i . '']) : "";
				$mCPIDNumber = isset($_POST['CPIDNumber' . $i . '']) ? str_replace($R1, $R2, $_POST['CPIDNumber' . $i . '']) : "";
        $mLueType = $_POST['LueType'. $i . ''];
				
				$sqlU = $sqlU . "Insert Into TblVendorContactPersonRegister( RegisterID, DNo, ContactPersonName, IDNumber, Position, ContactNumber, Type ,CreateBy, CreateDt) ";
				$sqlU = $sqlU . "Values('".$ID."', '".substr('000' . ($i + 1), -3)."', '".$_POST['CPName' . $i . '']."', '".$mCPIDNumber."', '".$mCPPos."', '".$mCPNo."','".$mLueType."','".$ID."', CurrentDateTime()); ";
			}
		}
		//else
		//{
		//	break;
		//}
	}
  
  for($i = 0; $i < 999; $i++)
	{
		if(isset($_POST['LueBankCode' . $i . '']))
		{
			if(!empty($_POST['LueBankCode' . $i . '']))
			{
				$mBABankBranch = isset($_POST['BABankBranch' . $i . '']) ? str_replace($R1, $R2, $_POST['BABankBranch' . $i . '']) : "";
				$mBABankAcName = isset($_POST['BABankAcName' . $i . '']) ? str_replace($R1, $R2, $_POST['BABankAcName' . $i . '']) : "";
				$mBABankAcNo = isset($_POST['BABankAcNo' . $i . '']) ? str_replace($R1, $R2, $_POST['BABankAcNo' . $i . '']) : "";
				
				$sqlU = $sqlU . "Insert Into TblVendorBankAccountRegister(RegisterID, DNo, BankCode, BankBranch, BankAcName, BankAcNo, CreateBy, CreateDt) ";
				$sqlU = $sqlU . "Values('$ID', '".substr('000' . ($i + 1), -3)."', '".$_POST['LueBankCode' . $i . '']."', '".$mBABankBranch."', '".$mBABankAcName."', '".$mBABankAcNo."', '".$ID."', CurrentDateTime()); ";
			}
		}
		//else
		//{
		//	break;
		//}
	}

  // for($i = 0; $i < 999; $i++)
	// {
	// 	if(isset($_POST['LueVdItCt' . $i . '']))
	// 	{
	// 		if(!empty($_POST['LueVdItCt' . $i . '']))
	// 		{				
	// 			$sqlU = $sqlU . "Insert Into TblVendorItemCategoryRegister(RegisterID, DNo, ItCtCode, CreateBy, CreateDt) ";
	// 			$sqlU = $sqlU . "Values('".$ID."', '".substr('000' . ($i + 1), -3)."', '".$_POST['LueVdItCt' . $i . '']."', '".$ID."', CurrentDateTime()); ";
	// 		}
	// 	}
	// 	//else
	// 	//{
	// 	//	break;
	// 	//}
	// }

  for($i = 0; $i < 999; $i++)
	{
		if(isset($_POST['LueVdSector' . $i . '']))
		{
			if(!empty($_POST['LueVdSector' . $i . '']))
			{				
				$sqlU = $sqlU . "Insert Into TblVendorSectorRegister(RegisterID, DNo, SectorCode, SubSectorCode, CreateBy, CreateDt) ";
				$sqlU = $sqlU . "Values('".$ID."', '".substr('000' . ($i + 1), -3)."', '".$_POST['LueVdSector' . $i . '']."', '".$_POST['LueVdSubSector' . $i . '']."','".$ID."', CurrentDateTime()); ";
			}
		}
		//else
		//{
		//	break;
		//}
	}

  $j = 0;
  $ftpSuccess = false;
  $ftpConn = ftp_connect($host);
  $login = ftp_login($ftpConn,$user,$pass);
  $ftpPasv = ftp_pasv($ftpConn, true);

  for($i = 0; $i < 999; $i++)
	{        

		if(isset($_POST['LueFileCt' . $i . '']) && isset($_FILES['VdFile' . $i . '']))
		{    

			if(!empty($_POST['LueFileCt' . $i . '']))
			{				
				
        if($_FILES['VdFile' . $i . '']['size'] != 0 && $_FILES['VdFile' . $i . '']['error'] == 0){
            
            $j++;          
        
            $imgFile = $_FILES['VdFile' . $i . '']['name'];
            $tmp_dir = $_FILES['VdFile' . $i . '']['tmp_name'];
            $imgSize = $_FILES['VdFile' . $i . '']['size'];
            $upload_dir = "dist/pdf/";
            $valid_extension = array('pdf', 'png', 'jpg', 'jpeg');
            $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;
            
            $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));

            $VdFilesLoc = $ID.'_'.$_POST['LueFileCt' . $i . ''].'-'.str_pad($j, 3, "0", STR_PAD_LEFT).'-'. date("Ymdhms") .".".$imgExt;

            
            if(in_array($imgExt, $valid_extension)) // check kalau extensi nya valid
                {

                    if($imgSize == 0)
                    {

                        echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                        echo "<script type='text/javascript'>window.location='register.php'</script>";
                    }
                    else
                    {

                        if($imgSize < $mMaxFile) // if image size < 1025 KB
                        {
                          
                            // echo "<script type='text/javascript'>alert('".$tmp_dir." || ".$upload_dir.$VdFilesLoc."');</script>";
                            move_uploaded_file($tmp_dir, $upload_dir.$VdFilesLoc);


                            $sqlU = $sqlU . "Insert Into TblVendorFileUploadRegister(RegisterID, DNo ,CategoryCode, Status, FileLocation, CreateBy, CreateDt) ";
                            $sqlU = $sqlU . "Values('".$ID."','".str_pad($j, 3, "0", STR_PAD_LEFT)." ','".$_POST['LueFileCt' . $i . '']."', 'A', '".$VdFilesLoc."', ";                                    
                            $sqlU = $sqlU . "'".$ID."', CurrentDateTime());";

                        }
                        else
                        {

                            echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                            echo "<script type='text/javascript'>window.location='register.php'</script>";
                        }
                        }
                }
                else
                {                  
                    echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
                    echo "<script type='text/javascript'>window.location='register.php'</script>";
                }

            echo "<br>";

            if (FTP_STATUS === "enable") {
                try {                  

                    if ((!$ftpConn) || (!$login)) {
                        echo "FTP connection has failed! Attempted to connect to ". $host.".";
                    }else {
                      $ftpSuccess = true;
                    }

                    if (!$ftpPasv) {
                        echo "Cannot switch to passive mode";
                    }

                    if($login) {
                        
                        if (!ftp_put($ftpConn, FTP_DESTINATION.$VdFilesLoc, $upload_dir.$VdFilesLoc, FTP_BINARY)) {
                            echo "<script>alert('Error uploading ". $VdFilesLoc .".');</script>";
                        }

                    }
                } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
        

        }
			}
		}

	}
  
  if($ftpSuccess){
    echo "<script>alert('Success Uploading File.')</script>";
    ftp_close($ftpConn);
  }


  $expSQL = explode(";", $sqlU);
	$countSQL = count($expSQL);
	$success = false;

	for($i = 0; $i < $countSQL - 1; $i++)
	{
		if (mysqli_query($conn, $expSQL[$i])) {
			$success = true;
		} else {
			echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
		}
	}

	if($success)
	{
		mysqli_commit($conn);
    $successMessage = "<div class='container'><div class='callout callout-success'><h4>Success</h4>Please wait for usercode and password sent to your Email.</div></div>";
	}
} // end if(isset($_POST['submit']))

echo $successMessage;

?>

<link rel="stylesheet" href="plugins/tnc/base.min.css"/>
<link rel="stylesheet" href="plugins/tnc/fancy.min.css"/>
<link rel="stylesheet" href="plugins/tnc/main.css"/>
<style>
.modal-body {
    height: 75vh;
    /* overflow-y: auto; */
    /* overflow-x: auto; */
}
</style>
<form class="form-horizontal" action="" method="post" name="register" enctype="multipart/form-data" onsubmit="return validateRegister();">
<div class="container">
  <center><img src="dist/img/runmarket_logo.png" width="250px"></center><br />
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h4 class="registerLbl">Company Information</h4>
        </div>
				<div class="box-body">
          
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Company Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="VdName" id="VdName" maxlength="80" required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="eVdName"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Company Short Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="ShortName" id="ShortName" maxlength="40" required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="eShortName"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Category</label>
            <div class="col-sm-8">
              <select class="form-control selectCtCode" name="LueCtCode" id="LueCtCode" style="width: 100%;">
                    <option value=""></option>
                    <?php
                      $q = " Select VdCtCode As Col1, VdCtName As Col2 ";
                      $q = $q . " From tblvendorcategory ";
                      $q = $q . " Order By VdCtName; ";

                      $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                      while($q2 = mysqli_fetch_assoc($q1))
                      {
                        echo "<option value='".$q2['Col1']."' ";
                        echo ">".$q2['Col2']."</option>";
                      }
                  ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Taxpayer ID</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="TIN" id="TIN" maxlength="40" placeholder="Fill 0 (without quotes) if you have no Taxpayer ID." required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="eTIN"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">NIB</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="NIB" id="NIB" placeholder="NIB" />
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Email</label>
            <div class="col-sm-8">
              <input type="email" class="form-control" name="Email" id="Email" maxlength="400" onblur="ShowRegisteredEmail(this.value);" required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="eEmail"></span>
              <span class="error-message"><center><?php echo $errorMessage; ?></center></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Establishment Year</label>
            <div class="col-sm-8">
              <input type="number" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" class="form-control" name="EstablishedYr" id="EstablishedYr" maxlength="4" />
            </div>
          </div>
          <!-- <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Bank Name</label>
            <div class="col-sm-8">
              <select class="form-control selectBankCode" name="LueBankCode" id="LueBankCode" style="width: 100%;">
                <option value=""></option>
                  <?php
                    $q = " Select BankCode As Col1, BankName As Col2 ";
                    $q = $q . " From TblBank ";
                    $q = $q . " Order By BankName; ";

                    $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                    while($q2 = mysqli_fetch_assoc($q1))
                    {
                      echo "<option value='".$q2['Col1']."' ";
                      echo ">".$q2['Col2']."</option>";
                    }
                  ?>
              </select>
            </div>
          </div> -->
          <!-- <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Branch Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="BankBranch" id="BankBranch" />
            </div>
          </div> -->
          <!-- <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Account Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="BankAcName" id="BankAcName" />
            </div>
          </div> -->
          <!-- <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Account No.</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="BankAcNo" id="BankAcNo" />
            </div>
          </div> -->
          <!-- <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Head Office</label>
            <div class="col-sm-8">
              <select class="form-control selectHO" name="LueHOCode" id="LueHOCode" style="width: 100%;">
                <option value=""></option>
                  <?php
                    
                    /*$q = " Select VdCode As Col1, Concat(A.VdName, ' [ ', B.CityName, ' ]') As Col2 ";
                    $q = $q . " From TblVendor A ";
                    $q = $q . " Left Join TblCity B On A.CityCode = B.CityCode ";
                    $q = $q . " Where A.VdCode Not In (Select VdCode From TblVendor Where (Parent Is Not Null Or Length(Parent) > 0)) ";
                    $q = $q . " Order By A.VdName; ";

                    $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                    while($q2 = mysqli_fetch_assoc($q1))
                    {
                      echo "<option value='".$q2['Col1']."' ";
                      echo ">".$q2['Col2']."</option>";
                    }
                    */
                  ?>
              </select>
            </div>
          </div> -->

          <br />
          <h4 class="registerLbl">Company Address Information</h4>
          <hr />
          
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Address</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="Address" id="Address" maxlength="400" required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="eAddress"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">City</label>
            <div class="col-sm-8">
              <select class="form-control selectCity" name="LueCityCode" id="LueCityCode" style="width: 100%;" required>
                <option value=""></option>
                  <?php
                    $q = "Select CityCode As Col1, Concat(A.CityName, ' ( ', IfNull(B.ProvName, ''), ' ', IfNull(C.CntName, ''), ' )')  As Col2 ";
                    $q = $q . "From TblCity A ";
                    $q = $q . "Left Join TblProvince B On A.ProvCode=B.ProvCode ";
                    $q = $q . "Left Join TblCountry C On B.CntCode=C.CntCode ";
                    $q = $q . "Order By C.CntName, A.CityName ";

                    $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                    while($q2 = mysqli_fetch_assoc($q1))
                    {
                      echo "<option value='".$q2['Col1']."' ";
                      echo ">".$q2['Col2']."</option>";
                    }
                  ?>
              </select>
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="eCityCode"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Postal Code</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="PostalCd" id="PostalCd" maxlength="6" required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="ePostalCd"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Mobile</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="Mobile" id="Mobile" maxlength="15" required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="eMobile"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Phone</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="Phone" id="Phone" maxlength="15" placeholder="Fill 0 if you have no phone number." required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="ePhone"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Fax</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="Fax" id="Fax" maxlength="15" placeholder="Fill 0 if you have no fax number." />
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Website</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="Website" id="Website" maxlength="1000" placeholder="Fill '-' (without quotes) if you have no website address." />
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Other Address</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="Remark" id="Remark" maxlength="400" />
            </div>
          </div> 

          <br />
          <!-- <h4 class="registerLbl">Contact Person</h4>
          <hr /> -->

          <!-- <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Contact Person</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="ContactPersonName" id="ContactPersonName" maxlength="80" required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="eContactPersonName"></span>
            </div>
          </div> 
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">ID Number (KTP)</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="IDNumber" id="IDNumber" maxlength="80" required />
            </div>
            <div class="col-sm-2">
              <span class="error-message" id="eIDNumber"></span>
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Position</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="Position" id="Position" maxlength="1000" required />
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Contact Number</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="ContactNumber" id="ContactNumber" maxlength="1000" />
            </div>
          </div>             -->
          <div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#ContactPerson" data-toggle="tab">Contact Person</a></li>
                  <li><a href="#Bank" data-toggle="tab">Bank</a></li>
                  <!-- <li><a href="#ItCt" data-toggle="tab">Item's Category</a></li> -->
                  <li><a href="#VdSector" data-toggle="tab">Sector</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="ContactPerson">
                    <table class="table Contact-Person" id="tblBankContactPerson">
                      <thead>
                        <tr>
                          <th>Contact Person Name</th>
                          <th>ID Number</th>
                          <th>Position</th>
                          <th>Contact Number</th>
                          <th>Type</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        // $sql = "";

                        // $sql = $sql . "Select * ";
                        // $sql = $sql . "From TblVendorContactPersonRegister ";
                        // $sql = $sql . "Where VdCode = '".$_SESSION['vdcode']."' Order By DNo ";

                        // $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                        // $row = 0;

                        // if(mysqli_num_rows($result) > 0)
                        // {
                        //   $row = mysqli_num_rows($result);
                        //   $no = 0;
                        //   while($res = mysqli_fetch_assoc($result))
                        //   {
                        //     echo "<tr>";
                        //     echo "<td class='col-sm-4'>";
                        //     echo "<input type='text' name='CPName".$no."' class='form-control' value='".$res['ContactPersonName']."' />";
                        //     echo "</td>";
                        //     echo "<td class='col-sm-2'>";
                        //     echo "<input type='text' name='CPIDNumber".$no."' class='form-control' value='".$res['IDNumber']."' />";
                        //     echo "</td>";
                        //     echo "<td class='col-sm-2'>";
                        //     echo "<input type='text' name='CPPos".$no."' class='form-control' value='".$res['Position']."' />";
                        //     echo "</td>";
                        //     echo "<td class='col-sm-2'>";
                        //     echo "<input type='number' name='CPNo".$no."' class='form-control' value='".$res['ContactNumber']."' />";
                        //     echo "</td>";
                        //     echo "<td><input type='button' id='CPbtnDel' class='CPbtnDel btn btn-md btn-danger' value='Delete' disabled='disabled'></td>";
                        //     echo "</tr>";

                        //     $no++;
                        //   }
                        // }

                        echo "<input type='hidden' name='countVdContactPerson' id='countVdContactPerson' value='0' />";
                      ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="6" style="text-align: left;">
                            <input type="button" class="btn btn-lg btn-block " id="CPaddrow" value="Add Row"   />
                          </td>
                        </tr>
                        <tr>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  
                  <div class="tab-pane" id="Bank">
                    <table class="table BankAccount" id="tblBankAccount">
                      <thead>
                        <tr>
                          <th>Bank Name</th>
                          <th>Branch Name</th>
                          <th>Account Name</th>
                          <th>Account Number</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        // $sql = "";

                        // $sql = $sql . "Select * ";
                        // $sql = $sql . "From TblVendorBankAccount ";
                        // $sql = $sql . "Where VdCode = '".$_SESSION['vdcode']."' Order By DNo ";

                        // $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                        // $row = 0;

                        // if(mysqli_num_rows($result) > 0)
                        // {
                        //   $row = mysqli_num_rows($result);
                        //   $no = 0;
                        //   while($res = mysqli_fetch_assoc($result))
                        //   {
                        //     echo "<tr>";
                        //     echo "<td class='col-sm-3'>";
                        //     echo "<select class='form-control selectBankCode".$no."' name='LueBankCode".$no."' id='LueBankCode".$no."' style='width: 100%;'>";
                        //     echo "<option value=''></option>";
                        //     $sqlB = "Select BankCode As Col1, BankName As Col2 From TblBank Order By BankName ";
                        //     $resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
                        //     while($resB = mysqli_fetch_assoc($resultB))
                        //     {
                        //       echo "<option value='".$resB['Col1']."' ";
                        //       if($res['BankCode'] == $resB['Col1'])
                        //       {
                        //         echo " selected";
                        //       }
                        //       echo " >".$resB['Col2']."</option>";
                        //     }
                        //     echo "</select>";
                        //     echo "</td>";
                        //     echo "<td class='col-sm-3'>";
                        //     echo "<input type='text' name='BABankBranch".$no."' class='form-control' value='".$res['BankBranch']."' />";
                        //     echo "</td>";
                        //     echo "<td class='col-sm-3'>";
                        //     echo "<input type='text' name='BABankAcName".$no."' class='form-control' value='".$res['BankAcName']."' />";
                        //     echo "</td>";
                        //     echo "<td class='col-sm-3'>";
                        //     echo "<input type='number' name='BABankAcNo".$no."' class='form-control' value='".$res['BankAcNo']."' />";
                        //     echo "</td>";
                        //     echo "<td><input type='button' id='BAbtnDel' class='BAbtnDel btn btn-md btn-danger' value='Delete' disabled='disabled'></td>";
                        //     echo "</tr>";

                        //     $no++;
                        //   }
                        // }

                        echo "<input type='hidden' name='countVdBankAccount' id='countVdBankAccount' value='0' />";
                      ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="5" style="text-align: left;">
                            <input type="button" class="btn btn-lg btn-block " id="BAaddrow" value="Add Row"   />
                          </td>
                        </tr>
                        <tr>
                        </tr>
                      </tfoot>
                    </table>
                  </div>

                  <!-- <div class="tab-pane" id="ItCt">
                    <span id="eTblItCt" class="error-message"></span>
                    <table class="table ItCt" id="tblItCt">
                      <thead>
                        <tr>
                          <th>Item's Category Name</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        // $sql = "";

                        // $sql = $sql . "Select * ";
                        // $sql = $sql . "From TblVendorItemCategory ";
                        // $sql = $sql . "Where VdCode = '".$_SESSION['vdcode']."' Order By DNo ";

                        // $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                        // $row = 0;

                        // if(mysqli_num_rows($result) > 0)
                        // {
                        //   $row = mysqli_num_rows($result);
                        //   $no = 0;
                        //   while($res = mysqli_fetch_assoc($result))
                        //   {
                        //     echo "<tr>";
                        //     echo "<td class='col-sm-3'>";
                        //     echo "<select class='form-control selectVdItCt".$no."' name='LueVdItCt".$no."' id='LueVdItCt".$no."' style='width: 100%;'>";
                        //     echo "<option value=''></option>";
                        //     $sqlB = "Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory Order By ItCtName ";
                        //     $resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
                        //     while($resB = mysqli_fetch_assoc($resultB))
                        //     {
                        //       echo "<option value='".$resB['Col1']."' ";
                        //       if($res['ItCtCode'] == $resB['Col1'])
                        //       {
                        //         echo " selected";
                        //       }
                        //       echo " >".$resB['Col2']."</option>";
                        //     }
                        //     echo "</select>";
                        //     echo "</td>";
                        //     echo "<td><input type='button' id='ItCtbtnDel' class='ItCtbtnDel btn btn-md btn-danger' value='Delete' disabled='disabled'></td>";
                        //     echo "</tr>";

                        //     $no++;
                        //   }
                        // }

                        // echo "<input type='hidden' name='countVdItCt' id='countVdItCt' value='0' />";
                      ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="5" style="text-align: left;">
                            <input type="button" class="btn btn-lg btn-block " id="ItCtaddrow" value="Add Row"   />
                          </td>
                        </tr>
                        <tr>
                        </tr>
                      </tfoot>
                    </table>
                  </div> -->

                  <div class="tab-pane" id="VdSector">
                    <span id="eTblVdSector" class="error-message"></span>
                    <table class="table VdSector" id="tblVdSector">
                      <thead>
                        <tr>
                          <th>Vendor's Sector</th>
                          <th>Sub Sector</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        // $sql = "";

                        // $sql = $sql . "Select * ";
                        // $sql = $sql . "From TblVendorSector ";
                        // $sql = $sql . "Where VdCode = '".$_SESSION['vdcode']."' Order By DNo; ";

                        // $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                        // $row = 0;

                        // if(mysqli_num_rows($result) > 0)
                        // {
                        //   $row = mysqli_num_rows($result);
                        //   $no = 0;
                        //   while($res = mysqli_fetch_assoc($result))
                        //   {
                        //     echo "<tr>";
                        //     echo "<td class='col-sm-3'>";
                        //     echo "<select class='form-control selectVdSector".$no."' name='LueVdSector".$no."' id='LueVdSector".$no."' style='width: 100%;'>";
                        //     echo "<option value=''></option>";
                        //     $sqlB = "Select SectorCode As Col1, SectorName As Col2 From TblSector ";
                        //     if(strlen($res['SectorCode']) <= 0 )
                        //     {
                        //       $sqlB = $sqlB . "Where ActInd = 'Y' ";
                        //     }
                        //     $sqlB = $sqlB . "Order By SectorName; ";
                        //     $resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
                        //     while($resB = mysqli_fetch_assoc($resultB))
                        //     {
                        //       echo "<option value='".$resB['Col1']."' ";
                        //       if($res['SectorCode'] === $resB['Col1'])
                        //       {
                        //         echo " selected";
                        //       }
                        //       echo " >".$resB['Col2']."</option>";
                        //     }
                        //     echo "</select>";
                        //     echo "</td>";

                        //     echo "<td class='col-sm-3'>";
                        //     echo "<select class='form-control selectVdQualification".$no."' name='LueVdQualification".$no."' id='LueVdQualification".$no."' style='width: 100%;'>";
                        //     echo "<option value=''></option>";
                        //     $sqlB = "Select QualificationCode As Col1, QualificationName As Col2 From TblQualification ";
                        //     if(strlen($res['QualificationCode']) <= 0 )
                        //     {
                        //       $sqlB = $sqlB . "Where ActInd = 'Y' ";
                        //     }
                        //     $sqlB = $sqlB . "Order By QualificationName; ";
                        //     $resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
                        //     while($resB = mysqli_fetch_assoc($resultB))
                        //     {
                        //       echo "<option value='".$resB['Col1']."' ";
                        //       if($res['QualificationCode'] == $resB['Col1'])
                        //       {
                        //         echo " selected";
                        //       }
                        //       echo " >".$resB['Col2']."</option>";
                        //     }
                        //     echo "</select>";
                        //     echo "</td>";

                        //     echo "<td><input type='button' id='VdSectorbtnDel' class='VdSectorbtnDel btn btn-md btn-danger' value='Delete' disabled='disabled'></td>";
                        //     echo "</tr>";

                        //     $no++;
                        //   }
                        // }

                        echo "<input type='hidden' name='countVdSector' id='countVdSector' value='0' />";
                        echo "<input type='hidden' name='countVdSubSector' id='countVdSubSector' value='0' />";
                      ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="5" style="text-align: left;">
                            <input type="button" class="btn btn-lg btn-block " id="VdSectoraddrow" value="Add Row"   />
                          </td>
                        </tr>
                        <tr>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                
                </div>
                <!-- /.tab-content -->
              </div>
              <!-- /.nav-tabs-custom -->

              <!-- <hr />

                  <input type="reset" class="btn " onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="visibility: hidden; background-color: #58585A; color:white"/>
                  <button type="submit" name="submit" class="btn pull-right" id="BtnSave" style="visibility: hidden; background-color: #D61E2F; color:white">Save</button> -->

            </div>
          </div>

          <br />
          <h4 class="registerLbl">Compliance</h4>
          <hr />
          
          <table class="table compliance" id="tblCompliance">
            <thead>
              <tr>
                <th>Document Name</th>
                <th>File</th>
                <th>#</th>                
              </tr>
            </thead>
            <tbody>
            <?php
              // $sql = "";

              // $sql = $sql . "Select * ";
              // $sql = $sql . "From TblVendorContactPerson ";
              // $sql = $sql . "Where VdCode = '".$_SESSION['vdcode']."' Order By DNo ";

              // $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
              // $row = 0;

              // if(mysqli_num_rows($result) > 0)
              // {
              //   $row = mysqli_num_rows($result);
              //   $no = 0;
              //   while($res = mysqli_fetch_assoc($result))
              //   {
              //     echo "<tr>";
              //     echo "<td class='col-sm-4'>";
              //     echo "<input type='text' name='CPName".$no."' class='form-control' value='".$res['ContactPersonName']."' />";
              //     echo "</td>";
              //     echo "<td class='col-sm-2'>";
              //     echo "<input type='text' name='CPIDNumber".$no."' class='form-control' value='".$res['IDNumber']."' />";
              //     echo "</td>";
              //     echo "<td class='col-sm-2'>";
              //     echo "<input type='text' name='CPPos".$no."' class='form-control' value='".$res['Position']."' />";
              //     echo "</td>";
              //     echo "<td class='col-sm-2'>";
              //     echo "<input type='number' name='CPNo".$no."' class='form-control' value='".$res['ContactNumber']."' />";
              //     echo "</td>";
              //     echo "<td><input type='button' id='CPbtnDel' class='CPbtnDel btn btn-md btn-danger' value='Delete' disabled='disabled'></td>";
              //     echo "</tr>";

              //     $no++;
              //   }
              // }

              echo "<input type='hidden' name='countVdCompliance' id='countVdCompliance' value='0' />";
            ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="5" style="text-align: left;">
                  <input type="button" class="btn btn-lg btn-block " id="CompAddRow" value="Add Row"   />
                </td>
              </tr>
              <tr>
              </tr>
            </tfoot>
          </table>
          
          <div class="">
            <label class="col-sm-8 checkbox-inline">
              <input id="agree" type="checkbox" value="agree"  disabled>
              <strong>By register, you agree to our <a href="#tnc" class="tnc" data-toggle="modal">Term and Condition</a></strong>
              
            </label>
          </div>
          <br>
          <div class="">
            <label class="col-sm-8 checkbox-inline">
              <input id="download" type="checkbox" value="download"  disabled>
              <strong>Please <a id="manualBook" target="_blank" href="dist/man/Eproc_Manual_For_Vendor.pdf">Download Manual Book</a></strong>
              
            </label>
          </div>
        </div>
        <div class="box-footer">
          <div class="row">
            <div class="col-xs-4">
              <input type="submit" name="submit" class="btn" id="btnRegister" style="background-color:#3373BA; color:#ffffff; width: 30%; height: 40px" value="Register" disabled/>
            </div>
            <?php if(strlen($successMessage) > 0) {  ?>
            <div class="col-xs-4 col-xs-offset-4">
              <a href="login.php"><button type="button" name="login" class="btn btn-success btn-block">Login Page</button></a>
            </div>
            <?php } ?>
            <!-- /.col -->
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</form>

<!-- modal for TnC -->
<div class="modal fade" id="showTnc" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Term & Condition</h4>
      </div>
      <div class="modal-body" id="tncBody" onscroll="myFunction();">
        <div id="sidebar">
        <div id="outline">
        </div>
        </div>
        <div id="page-container">
        <div id="pf1" class="pf w0 h0" data-page-no="1"><div class="pc pc1 w0 h0"><div class="t m0 x0 h1 y0 ff1 fs0 fc0 sc0 ls0 ws0">SYARAT DAN KETENTUAN <span class="ff2">SELLER</span></div><div class="t m0 x1 h1 y1 ff1 fs0 fc0 sc0 ls0 ws0">RUN MARKET PROCUREMENT</div><div class="t m0 x2 h2 y2 ff3 fs1 fc0 sc0 ls0 ws0">Selamat <span class="_ _0"> </span>datang <span class="_ _0"> </span>di <span class="_ _0"> </span>Syarat <span class="_ _0"> </span>dan <span class="_ _0"> </span>Ketentuan <span class="_ _0"> </span><span class="ff4">Seller <span class="_ _0"> </span></span>pada <span class="_ _0"> </span><span class="ff4">platform <span class="_ _0"> </span></span>RUN <span class="_ _0"> </span>Market <span class="_ _0"> </span>Procurement <span class="_ _0"> </span>(selanjutnya </div><div class="t m0 x2 h2 y3 ff3 fs1 fc0 sc0 ls0 ws0">disebut <span class="ff1">“S&amp;K”</span>). <span class="_ _1"></span>Sebagai upaya <span class="_ _1"></span>pemberian layanan <span class="_ _1"></span>yang maksimal <span class="_ _1"></span>kepada setiap <span class="_ _1"></span><span class="ff4">Seller</span>, PT <span class="_ _1"></span>Global Sukses </div><div class="t m0 x2 h2 y4 ff3 fs1 fc0 sc0 ls0 ws0">Solusi <span class="_ _2"> </span>Tbk <span class="_ _2"> </span>(selanjutnya <span class="_ _2"> </span>disebut <span class="_ _2"> </span><span class="ff1">“Kami”</span>) <span class="_ _2"> </span>menerapkan <span class="_ _2"> </span>S&amp;K <span class="_ _2"> </span>untuk <span class="_ _2"> </span>digunakan <span class="_ _2"> </span>sebagai <span class="_ _2"> </span>acuan <span class="_ _2"> </span>apabila </div><div class="t m0 x2 h2 y5 ff3 fs1 fc0 sc0 ls0 ws0">ditemukan <span class="_ _2"> </span>permasalahan  dalam <span class="_ _2"> </span>penggunaan <span class="_ _2"> </span>RUN<span class="_ _3"></span> <span class="_ _2"> </span>Market  Procurement <span class="_ _2"> </span>(selanjutnya <span class="_ _4"> </span>disebut <span class="_ _4"> </span><span class="ff1">“RUN </span></div><div class="t m0 x2 h2 y6 ff1 fs1 fc0 sc0 ls0 ws0">Market”<span class="ff3">).  Kami <span class="_ _5"> </span>menyarankan  anda <span class="_ _5"> </span>sebagai  pengguna <span class="_ _5"> </span>atau  calon <span class="_ _5"> </span>pengguna  atau <span class="_ _5"> </span><span class="ff4">Seller  </span>(selanjutnya </span></div><div class="t m0 x2 h2 y7 ff3 fs1 fc0 sc0 ls0 ws0">disebut <span class="_ _0"> </span><span class="ff1">“<span class="ff2">Sell<span class="_ _3"></span>er<span class="ff1">”<span class="ff3">) <span class="_ _6"> </span>untuk <span class="_ _6"> </span>membaca <span class="_ _6"> </span>S&amp;K <span class="_ _6"> </span>ini <span class="_ _6"> </span>dan <span class="_ _6"> </span>turunannya <span class="_ _6"> </span>(selanjutnya <span class="_ _6"> </span>disebut <span class="_ _6"> </span></span>“S&amp;K <span class="_ _6"> </span>Turunan<span class="ff3">) <span class="_ _6"> </span>secara </span></span></span></span></div><div class="t m0 x2 h2 y8 ff3 fs1 fc0 sc0 ls0 ws0">berkala bersama <span class="_ _1"></span>dengan <span class="_ _1"></span>Kebijakan Privasi <span class="_ _1"></span>Data <span class="_ _1"></span>yang merupakan <span class="_ _1"></span>bagian <span class="_ _1"></span>yang tidak <span class="_ _1"></span>terpisahkan <span class="_ _1"></span>dari S&amp;K </div><div class="t m0 x2 h2 y9 ff3 fs1 fc0 sc0 ls0 ws0">ini.</div><div class="t m0 x2 h3 ya ff1 fs1 fc0 sc0 ls0 ws0">A.<span class="_ _7"> </span>Keberlakuan</div><div class="t m0 x3 h2 yb ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span>S&amp;K <span class="_ _9"> </span>ini <span class="_ _9"> </span>berlaku <span class="_ _9"> </span>sebagaimana <span class="_ _9"> </span>mestinya <span class="_ _9"> </span>dan <span class="_ _9"> </span>wajib <span class="_ _9"> </span>diperhatikan <span class="_ _9"> </span>oleh <span class="_ _9"> </span><span class="ff4">Seller <span class="_ _9"> </span></span>sebelum </div><div class="t m0 x4 h2 yc ff3 fs1 fc0 sc0 ls0 ws0">menggunakan layanan RUN Market.</div><div class="t m0 x3 h2 yd ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span>S&amp;K ini mulai berlaku sejak kali pertama <span class="ff4">Seller </span>menggunakan layanan RUN Market.</div><div class="t m0 x3 h2 ye ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span>Dengan <span class="_ _2"> </span>menggunakan <span class="_ _2"> </span>layanan <span class="_ _a"> </span>RUN <span class="_ _2"> </span>Market, <span class="_ _2"> </span><span class="ff4">Seller <span class="_ _a"> </span></span>telah <span class="_ _2"> </span>memberikan <span class="_ _2"> </span>penerimaan <span class="_ _a"> </span>dan </div><div class="t m0 x4 h2 yf ff3 fs1 fc0 sc0 ls0 ws0">persetujuan <span class="_ _b"></span>yang <span class="_ _b"></span>tidak <span class="_ _b"></span>dapat <span class="_ _b"></span>dicabut <span class="_ _b"></span>atas <span class="_ _b"></span>isi <span class="_ _b"></span>S&amp;K <span class="_ _b"></span>ini, <span class="_ _b"></span>termasuk <span class="_ _b"></span>namun <span class="_ _b"></span>tidak <span class="_ _b"></span>terbatas <span class="_ _b"></span>pada </div><div class="t m0 x4 h2 y10 ff3 fs1 fc0 sc0 ls0 ws0">S&amp;K Turunan dan Kebijakan Privasi.</div><div class="t m0 x3 h2 y11 ff3 fs1 fc0 sc0 ls0 ws0">4.<span class="_ _8"> </span>Dalam <span class="_ _5"> </span>hal <span class="_ _0"> </span><span class="ff4">Seller <span class="_ _5"> </span></span>tidak <span class="_ _0"> </span>menyetujui <span class="_ _5"> </span>ketentuan <span class="_ _0"> </span>S&amp;K <span class="_ _5"> </span>ini, <span class="_ _0"> </span><span class="ff4">Seller <span class="_ _5"> </span></span>dipersilahkan <span class="_ _5"> </span>untuk <span class="_ _0"> </span>berhenti </div><div class="t m0 x4 h2 y12 ff3 fs1 fc0 sc0 ls0 ws0">menggunakan layanan RUN Market.</div><div class="t m0 x3 h2 y13 ff3 fs1 fc0 sc0 ls0 ws0">5.<span class="_ _8"> </span>Segala perubahan <span class="_ _3"></span>terhadap RUN <span class="_ _3"></span>Market beserta <span class="_ _3"></span>fasilitasnya akan <span class="_ _3"></span>tetap tunduk <span class="_ _3"></span>pada S&amp;K <span class="_ _3"></span>ini.</div><div class="t m0 x2 h3 y14 ff1 fs1 fc0 sc0 ls0 ws0">B.<span class="_ _c"> </span>Definisi</div><div class="t m0 x3 h2 y15 ff3 fs1 fc0 sc0 ls0 ws0">Kecuali ditentukan lain dalam <span class="_ _1"></span>hubungan kalimat di dalam S&amp;K ini, <span class="_ _1"></span>istilah-istilah yang dipakai dalam </div><div class="t m0 x3 h2 y16 ff3 fs1 fc0 sc0 ls0 ws0">S&amp;K ini haruslah diartikan sebagai berikut:</div><div class="t m0 x3 h2 y17 ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span><span class="ff1">“RUN Market”</span>, adalah layanan <span class="ff4">e-Purchasing</span> yang menghubungkan <span class="ff4">Buyer </span>dengan <span class="ff4">Seller</span>;</div><div class="t m0 x3 h2 y18 ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span><span class="ff1">“<span class="ff2">Buyer</span>”<span class="ff4"> <span class="_ _5"> </span></span></span>adalah  badan <span class="_ _5"> </span>usaha <span class="_ _5"> </span>yang <span class="_ _5"> </span>menggunakan  layanan <span class="_ _5"> </span>RUN <span class="_ _5"> </span>Market <span class="_ _5"> </span>untuk  melakukan </div><div class="t m0 x4 h2 y19 ff3 fs1 fc0 sc0 ls0 ws0">transaksi pengadaan dan pembelian barang dan jasa kepada <span class="ff4">Seller</span>;</div><div class="t m0 x3 h2 y1a ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span><span class="ff1">“<span class="ff2">Cross <span class="_ _1"></span>Selling</span>” </span>adalah <span class="_ _1"></span>transaksi <span class="_ _1"></span>antara <span class="_ _1"></span><span class="ff4">Seller</span> <span class="_ _1"></span>dengan <span class="ff4">Buyer <span class="_ _1"></span></span>yang <span class="_ _1"></span>bukan <span class="_ _1"></span>merupakan <span class="_ _1"></span>asal dari </div><div class="t m0 x4 h2 y1b ff4 fs1 fc0 sc0 ls0 ws0">Seller<span class="ff3"> tersebut atau bisa disebut dengan </span>Buyer<span class="ff3"> baru pada </span>platform <span class="ff3">RUN Market </span></div><div class="t m0 x3 h2 y1c ff3 fs1 fc0 sc0 ls0 ws0">4.<span class="_ _8"> </span><span class="ff1">“<span class="ff2">Seller</span>”<span class="ff4"> <span class="_ _4"> </span></span></span>adalah <span class="_ _2"> </span><span class="ff4">vendor <span class="_ _2"> </span></span>penyedia <span class="_ _4"> </span>produk <span class="_ _2"> </span>dan <span class="_ _2"> </span>jasa <span class="_ _4"> </span>yang <span class="_ _2"> </span>dipasarkan <span class="_ _2"> </span>kepada <span class="_ _4"> </span><span class="ff4">Buyer <span class="_ _2"> </span></span>pada </div><div class="t m0 x4 h2 y1d ff4 fs1 fc0 sc0 ls0 ws0">platform <span class="ff3">RUN Market;</span></div><div class="t m0 x3 h2 y1e ff3 fs1 fc0 sc0 ls0 ws0">5.<span class="_ _8"> </span><span class="ff1">“Wanprestasi”</span>, <span class="_ _d"></span>berarti <span class="_ _d"></span>segala <span class="_ _d"></span>perbuatan <span class="_ _d"></span>sebagaimana <span class="_ _d"></span>diatur <span class="_ _d"></span>dalam <span class="_ _d"></span>Pasal <span class="_ _d"></span>1243 <span class="_ _d"></span>Kitab <span class="_ _d"></span>Undang-</div><div class="t m0 x4 h2 y1f ff3 fs1 fc0 sc0 ls0 ws0">Undang Hukum Perdata.</div></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>
        <div id="pf2" class="pf w0 h0" data-page-no="2"><div class="pc pc2 w0 h0"><div class="t m0 x2 h3 y20 ff1 fs1 fc0 sc0 ls0 ws0">C.<span class="_ _e"> </span>Persyaratan <span class="ff2">Seller</span></div><div class="t m0 x3 h2 y21 ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _4"> </span></span>dapat  b<span class="_ _1"></span>erbentuk  badan <span class="_ _4"> </span>maupun <span class="_ _4"> </span>perseorangan, <span class="_ _4"> </span>yang <span class="_ _4"> </span>terbukti  mampu <span class="_ _4"> </span>melakukan </div><div class="t m0 x4 h2 y22 ff3 fs1 fc0 sc0 ls0 ws0">perbuatan hukum.</div><div class="t m0 x3 h2 y23 ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _5"> </span></span>wajib <span class="_ _0"> </span>memberikan <span class="_ _5"> </span><span class="ff4">copy <span class="_ _5"> </span></span>Tanda <span class="_ _5"> </span>Daftar <span class="_ _5"> </span>Perusahaan <span class="_ _0"> </span>(<span class="ff1">“TDP”</span>), <span class="_ _5"> </span>Nomor <span class="_ _5"> </span>Induk <span class="_ _5"> </span>Berusaha </div><div class="t m0 x4 h2 y24 ff3 fs1 fc0 sc0 ls0 ws0">(<span class="ff1">“NIB”</span>),  Tanda  Daftar  Usaha <span class="_ _4"> </span>Mikro  (<span class="ff1">“TDUM”</span>),  Kartu  Tanda  Penduduk  (<span class="ff1">“KTP”</span>), <span class="_ _4"> </span>maupun </div><div class="t m0 x4 h2 y25 ff3 fs1 fc0 sc0 ls0 ws0">dokumen lainnya <span class="_ _d"></span>yang berlaku <span class="_ _3"></span>menurut <span class="_ _3"></span>peraturan perundang-perundangan <span class="_ _d"></span>Negara Republik </div><div class="t m0 x4 h2 y26 ff3 fs1 fc0 sc0 ls0 ws0">Indonesia sebagai tanda pengenal.</div><div class="t m0 x3 h2 y27 ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span>Kami berhak menolak <span class="ff4">copy </span>tanda pengenal tersebut sesuai kebijakan Kami.</div><div class="t m0 x2 h3 y28 ff1 fs1 fc0 sc0 ls0 ws0">D.<span class="_ _f"> </span>Penggunaan Layanan RUN Market</div><div class="t m0 x3 h2 y29 ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span>Untuk <span class="_ _2"> </span>d<span class="_ _1"></span>apat <span class="_ _2"> </span>menggunakan <span class="_ _a"> </span>layanan <span class="_ _a"> </span>RUN <span class="_ _a"> </span>Market <span class="_ _a"> </span>sebagai <span class="_ _2"> </span><span class="ff4">Seller</span>, <span class="_ _a"> </span>terlebih <span class="_ _a"> </span>dahulu <span class="_ _a"> </span><span class="ff4">Seller </span></div><div class="t m0 x4 h2 y2a ff3 fs1 fc0 sc0 ls0 ws0">melakukan registrasi secara <span class="ff4">online</span> dengan data yang benar dan akurat melalui <span class="ff4">platform </span>RUN </div><div class="t m0 x4 h2 y2b ff3 fs1 fc0 sc0 ls0 ws0">Market.</div><div class="t m0 x3 h2 y2c ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span>Setelah proses registrasi pada <span class="_ _1"></span>poin D.1 tersebut di atas, <span class="ff4">Seller <span class="_ _1"></span></span>akan mendapat <span class="ff4">username </span>dan </div><div class="t m0 x4 h2 y2d ff4 fs1 fc0 sc0 ls0 ws0">password <span class="ff3">yang dapat digunakan untuk </span>log in<span class="ff3"> akun RUN Market.</span></div><div class="t m0 x3 h2 y2e ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span><span class="ff4">Seller</span> <span class="_ _6"> </span>menyetujui <span class="_ _6"> </span>bahwa <span class="_ _6"></span><span class="ff4">username</span> <span class="_ _10"></span>dan <span class="_ _10"> </span><span class="ff4">password</span> <span class="_ _6"> </span>yang <span class="_ _6"> </span>diperoleh <span class="_ _6"> </span>merupakan <span class="_ _10"> </span>representasi </div><div class="t m0 x4 h2 y2f ff4 fs1 fc0 sc0 ls0 ws0">Seller<span class="ff3"> <span class="_ _1"></span>atas <span class="_ _1"></span>segala <span class="_ _1"></span>aktivitas <span class="_ _1"></span>dalam <span class="_ _1"></span></span>platform<span class="ff3"> <span class="_ _1"></span>RUN <span class="_ _1"></span>Market <span class="_ _b"></span>dan <span class="_ _1"></span>penggunaan <span class="_ _1"></span>atas <span class="_ _1"></span></span>username<span class="ff3"> <span class="_ _1"></span>dan </span></div><div class="t m0 x4 h2 y30 ff4 fs1 fc0 sc0 ls0 ws0">password<span class="ff3"> tersebut akan terasosiasikan dengan segala aktivitas dalam sistem Kami. </span></div><div class="t m0 x3 h2 y31 ff3 fs1 fc0 sc0 ls0 ws0">4.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _6"> </span></span>baru <span class="_ _6"> </span>dapat <span class="_ _6"> </span>melaksanakan <span class="_ _6"> </span>transaksi <span class="_ _6"> </span>melalui <span class="_ _6"> </span>RUN <span class="_ _0"> </span>Market <span class="_ _6"> </span>setelah <span class="_ _6"> </span>melakukan <span class="_ _6"> </span>aktivasi </div><div class="t m0 x4 h2 y32 ff3 fs1 fc0 sc0 ls0 ws0">akun.</div><div class="t m0 x3 h2 y33 ff3 fs1 fc0 sc0 ls0 ws0">5.<span class="_ _8"> </span><span class="ff4">Seller </span>bersedia untuk menggunakan <span class="ff4">virtual account</span> yang disediakan.</div><div class="t m0 x3 h2 y34 ff3 fs1 fc0 sc0 ls0 ws0">6.<span class="_ _8"> </span><span class="ff4">Seller </span>bersedia untuk dikenakan biaya transaction fee atas aktivitas <span class="ff4">cross selling</span>.</div><div class="t m0 x3 h2 y35 ff3 fs1 fc0 sc0 ls0 ws0">7.<span class="_ _8"> </span><span class="ff4">Seller</span> <span class="_ _9"> </span>bertanggung <span class="_ _11"> </span>jawab <span class="_ _9"> </span>atas <span class="_ _11"> </span>penjagaan <span class="_ _9"> </span>kerahasiaan <span class="_ _11"> </span><span class="ff4">username</span> <span class="_ _9"> </span>dan <span class="_ _11"> </span><span class="ff4">password</span> <span class="_ _11"> </span>dan </div><div class="t m0 x4 h2 y36 ff3 fs1 fc0 sc0 ls0 ws0">bertanggung jawab atas transaksi dan kegiatan lain yang menggunakan akun miliknya. </div><div class="t m0 x3 h2 y37 ff3 fs1 fc0 sc0 ls0 ws0">8.<span class="_ _8"> </span><span class="ff4">Seller</span> <span class="_ _d"></span>wajib <span class="_ _d"></span>memutakhirkan <span class="_ _d"></span>data <span class="_ _3"></span>diri <span class="_ _d"></span>atau <span class="_ _d"></span>badan <span class="_ _d"></span>dalam <span class="_ _d"></span>hal<span class="_ _1"></span> <span class="_ _d"></span>terdapat <span class="_ _d"></span>perubahan <span class="_ _d"></span>atau <span class="_ _d"></span>jika <span class="_ _d"></span>tidak </div><div class="t m0 x4 h2 y38 ff3 fs1 fc0 sc0 ls0 ws0">sesuai dengan S&amp;K ini.</div><div class="t m0 x3 h2 y39 ff3 fs1 fc0 sc0 ls0 ws0">9.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _d"></span><span class="ff3">yang telah <span class="_ _d"></span>melakukan aktivasi <span class="_ _d"></span>berhak <span class="_ _3"></span>untuk <span class="_ _d"></span>menggunakan layanan <span class="_ _d"></span>RUN <span class="_ _3"></span>Market <span class="_ _d"></span>selama </span></span></div><div class="t m0 x4 h2 y3a ff3 fs1 fc0 sc0 ls0 ws0">7 (tujuh) hari seminggu dan 24 (dua puluh empat) jam sehari.</div><div class="t m0 x3 h2 y3b ff3 fs1 fc0 sc0 ls0 ws0">10.<span class="_ _12"> </span><span class="ff4">Seller</span> <span class="_ _b"></span>setuju <span class="_ _10"></span>untuk <span class="_ _b"></span>segera <span class="_ _10"></span>memberikan <span class="_ _b"></span>informasi <span class="_ _b"></span>kepada <span class="_ _10"></span>Kami <span class="_ _b"></span>apabil<span class="_ _1"></span>a <span class="_ _b"></span>mengetahui <span class="_ _b"></span>adanya </div><div class="t m0 x4 h2 y3c ff3 fs1 fc0 sc0 ls0 ws0">penyalahgunaan <span class="_ _0"> </span>akun <span class="_ _5"> </span>miliknya <span class="_ _6"> </span>o<span class="_ _1"></span>leh <span class="_ _0"> </span>pihak <span class="_ _0"> </span>lain <span class="_ _5"> </span>yang <span class="_ _6"> </span>ti<span class="_ _1"></span>dak <span class="_ _0"> </span>berhak <span class="_ _0"> </span>atau <span class="_ _5"> </span>jika <span class="_ _6"> </span>ada <span class="_ _5"> </span>gangguan </div><div class="t m0 x4 h2 y3d ff3 fs1 fc0 sc0 ls0 ws0">keamanan atas akun miliknya.</div><div class="t m0 x2 h3 y3e ff1 fs1 fc0 sc0 ls0 ws0">E.<span class="_ _8"> </span>Aktivitas Transaksi</div><div class="t m0 x3 h2 y3f ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _b"></span></span>wajib <span class="_ _10"> </span>untuk <span class="_ _10"></span>menggunakan <span class="_ _10"></span><span class="ff4">Virtual <span class="_ _10"></span>Account</span> <span class="_ _10"></span>yang <span class="_ _b"></span>tersedia <span class="_ _10"></span>pada <span class="_ _10"></span><span class="ff4">platform <span class="_ _10"></span></span>RUN <span class="_ _10"></span>Market </div><div class="t m0 x4 h2 y40 ff3 fs1 fc0 sc0 ls0 ws0">atas setiap aktivitas transaksi yang dilakukan dengan <span class="ff4">Buyer</span>.</div></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>
        <div id="pf3" class="pf w0 h0" data-page-no="3"><div class="pc pc3 w0 h0"><div class="t m0 x3 h2 y41 ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span><span class="ff4">Seller  </span>akan  dikenakan <span class="_ _5"> </span>biaya  untuk  setiap  aktivitas <span class="_ _5"> </span>transaksi  yang  dilakukan  berdasarkan </div><div class="t m0 x4 h2 y42 ff3 fs1 fc0 sc0 ls0 ws0">proses <span class="ff4">Cross Selling</span> (selanjutnya disebut <span class="ff1">“<span class="ff2">Transaction Fee</span>”</span>).</div><div class="t m0 x3 h2 y43 ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span>Besaran <span class="_ _3"></span><span class="ff4">Transaction <span class="_ _3"></span>Fee <span class="_ _3"></span><span class="ff3">adalah <span class="_ _3"></span>sebesar <span class="_ _3"></span>0,5% <span class="_ _3"></span>(nol <span class="_ _3"></span>koma <span class="_ _d"></span>l<span class="_ _1"></span>ima <span class="_ _d"></span>persen) dari <span class="_ _d"></span>nilai transaksi <span class="_ _3"></span><span class="ff4">Cross </span></span></span></div><div class="t m0 x4 h2 y44 ff4 fs1 fc0 sc0 ls0 ws0">Selling<span class="ff3">.</span></div><div class="t m0 x2 h3 y45 ff1 fs1 fc0 sc0 ls0 ws0">F.<span class="_ _13"> </span>Pelayanan (<span class="ff2">Customer Service</span>)</div><div class="t m0 x3 h2 y46 ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _b"></span></span>berhak <span class="_ _10"></span>untuk <span class="_ _10"></span>mendapatkan <span class="_ _b"></span>bantu<span class="_ _1"></span>an <span class="_ _b"></span>dari <span class="_ _10"></span><span class="ff4">Customer <span class="_ _b"></span>Service <span class="_ _10"> </span></span>Kami <span class="_ _10"></span>terkait <span class="_ _b"></span>penggunaan </div><div class="t m0 x4 h2 y47 ff3 fs1 fc0 sc0 ls0 ws0">layanan RUN Market.</div><div class="t m0 x3 h2 y48 ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span>Kami <span class="_ _5"> </span>akan <span class="_ _5"> </span>selalu <span class="_ _0"> </span>memberikan <span class="_ _5"> </span>informasi <span class="_ _5"> </span>terkait <span class="_ _5"> </span><span class="ff4">platform <span class="_ _0"> </span></span>RUN <span class="_ _5"> </span>Market <span class="_ _5"> </span>melalui <span class="_ _5"> </span>salah <span class="_ _5"> </span>satu </div><div class="t m0 x4 h2 y49 ff3 fs1 fc0 sc0 ls0 ws0">saluran komunikasi, <span class="ff4">website</span>, teks berjalan (<span class="ff4">scrolling text</span>), atau pada media-media lainnya.</div><div class="t m0 x2 h3 y4a ff1 fs1 fc0 sc0 ls0 ws0">G.<span class="_ _f"> </span>Batasan Penggunaan Layanan RUN Market</div><div class="t m0 x3 h2 y4b ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span><span class="ff4">Seller </span>dilarang untuk:</div><div class="t m0 x4 h2 y4c ff3 fs1 fc0 sc0 ls0 ws0">a.<span class="_ _14"> </span>mengenakan biaya kepada orang <span class="_ _3"></span>atau pihak lain untuk <span class="_ _3"></span>menikmati layanan RUN Mark<span class="_ _3"></span>et;</div><div class="t m0 x4 h2 y4d ff3 fs1 fc0 sc0 ls0 ws0">b.<span class="_ _15"> </span>memberikan <span class="_ _1"></span>atau <span class="_ _b"></span>menayangkan <span class="_ _1"></span>ulang <span class="_ _b"></span>atau <span class="_ _1"></span>mendistribusikan <span class="_ _1"></span>atau <span class="_ _b"></span>memperluas <span class="_ _1"></span>sendiri </div><div class="t m0 x5 h2 y4e ff3 fs1 fc0 sc0 ls0 ws0">layanan RUN Market dengan cara apapun kepada pihak lain;</div><div class="t m0 x4 h2 y4f ff3 fs1 fc0 sc0 ls0 ws0">c.<span class="_ _16"> </span>memperbanyak <span class="_ _0"> </span>dan/atau <span class="_ _5"> </span>menggandakan <span class="_ _0"> </span>dalam <span class="_ _0"> </span>bentuk <span class="_ _5"> </span>apapun <span class="_ _0"> </span>semua <span class="_ _5"> </span>layanan <span class="_ _6"> </span>RUN </div><div class="t m0 x5 h2 y50 ff3 fs1 fc0 sc0 ls0 ws0">Market;</div><div class="t m0 x4 h2 y51 ff3 fs1 fc0 sc0 ls0 ws0">d.<span class="_ _15"> </span>menggunakan <span class="_ _b"></span>segala <span class="_ _1"></span>bentuk <span class="_ _b"></span>layanan <span class="_ _b"></span>maupun <span class="_ _b"></span>identitas <span class="_ _1"></span>RUN <span class="_ _b"></span>Market, <span class="_ _b"></span>termasuk <span class="_ _1"></span>n<span class="_ _1"></span>amun </div><div class="t m0 x5 h2 y52 ff3 fs1 fc0 sc0 ls0 ws0">tidak <span class="_ _0"> </span>terbatas <span class="_ _0"> </span>pada <span class="_ _0"> </span>l<span class="_ _1"></span>ogo, <span class="_ _6"> </span><span class="ff4">trademark</span>, <span class="_ _5"> </span>dan <span class="_ _6"> </span>lain <span class="_ _0"> </span>sebagainya <span class="_ _5"> </span>untuk <span class="_ _6"> </span>kepentingan <span class="_ _0"> </span>p<span class="_ _1"></span>ribadi </div><div class="t m0 x5 h2 y53 ff3 fs1 fc0 sc0 ls0 ws0">dan/atau pihak lain manapun;</div><div class="t m0 x4 h2 y54 ff3 fs1 fc0 sc0 ls0 ws0">e.<span class="_ _14"> </span>memberikan <span class="_ _10"></span>kesempatan <span class="_ _6"> </span>kepada <span class="_ _6"> </span>lain <span class="_ _10"> </span>u<span class="_ _1"></span>ntuk <span class="_ _10"></span>memanfaatkan <span class="_ _6"> </span>sistem <span class="_ _6"> </span>RUN <span class="_ _6"> </span>Market <span class="_ _6"> </span>yang </div><div class="t m0 x5 h2 y55 ff3 fs1 fc0 sc0 ls0 ws0">tidak sebagaimana mestinya tanpa izin tertulis sebelumnya dari Kami.</div><div class="t m0 x3 h2 y56 ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _5"> </span></span>menerima <span class="_ _5"> </span>dan <span class="_ _5"> </span>menyetujui <span class="_ _5"> </span>bahwa <span class="_ _5"> </span>seluruh <span class="_ _0"> </span>sistem  dan <span class="_ _0"> </span><span class="ff4">platform  </span>RUN <span class="_ _0"> </span>Market  harus </div><div class="t m0 x4 h2 y57 ff3 fs1 fc0 sc0 ls0 ws0">digunakan  untuk <span class="_ _5"> </span>tujuan  sebagaimana  mestinya <span class="_ _5"> </span>yang  sesuai <span class="_ _5"> </span>dengan  etika  bisnis, <span class="_ _5"> </span>norma-</div><div class="t m0 x4 h2 y58 ff3 fs1 fc0 sc0 ls0 ws0">norma, <span class="_ _17"> </span>hukum, <span class="_ _17"> </span>dan <span class="_ _17"> </span>peraturan <span class="_ _17"> </span>perundang-undangan <span class="_ _17"> </span>yang <span class="_ _17"> </span>berlaku <span class="_ _17"> </span>di <span class="_ _17"> </span>Negara <span class="_ _17"> </span>Republik </div><div class="t m0 x4 h2 y59 ff3 fs1 fc0 sc0 ls0 ws0">Indonesia.</div><div class="t m0 x3 h2 y5a ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span><span class="ff4">Seller</span> <span class="_ _0"> </span>menyadari <span class="_ _5"> </span>bahwa <span class="_ _5"> </span>usaha <span class="_ _0"> </span>apapun <span class="_ _5"> </span>untuk <span class="_ _0"> </span>dapat <span class="_ _5"> </span>menembus <span class="_ _0"> </span>sistem <span class="_ _5"> </span>komputer <span class="_ _0"> </span>dengan </div><div class="t m0 x4 h2 y5b ff3 fs1 fc0 sc0 ls0 ws0">tujuan <span class="_ _6"> </span>memanipulasi <span class="_ _0"> </span>data <span class="_ _6"> </span>dan <span class="_ _6"> </span>inf<span class="_ _1"></span>ormasi <span class="_ _6"> </span>pada <span class="_ _6"> </span><span class="ff4">platform <span class="_ _6"> </span></span>RUN <span class="_ _0"> </span>System <span class="_ _6"> </span>merupakan <span class="_ _0"> </span>tindakan </div><div class="t m0 x4 h2 y5c ff3 fs1 fc0 sc0 ls0 ws0">melanggar hukum.</div><div class="t m0 x3 h2 y5d ff3 fs1 fc0 sc0 ls0 ws0">4.<span class="_ _8"> </span><span class="ff4">Seller</span> <span class="_ _1"></span>bersedia <span class="_ _1"></span>untuk tidak <span class="_ _1"></span>membuka, <span class="_ _1"></span>mengeluarkan, <span class="_ _1"></span>maupun <span class="_ _1"></span>memberikan <span class="_ _1"></span>setiap <span class="_ _1"></span>informasi </div><div class="t m0 x4 h2 y5e ff3 fs1 fc0 sc0 ls0 ws0">dan <span class="_ _10"></span>data <span class="_ _10"></span>kepada <span class="_ _10"></span>pihak <span class="_ _10"></span>lainnya, <span class="_ _10"></span>dan/atau <span class="_ _10"></span>penggunaannya <span class="_ _10"></span>dengan <span class="_ _10"></span>cara <span class="_ _10"></span>bagaimanapun <span class="_ _10"></span>baik </div><div class="t m0 x4 h2 y5f ff3 fs1 fc0 sc0 ls0 ws0">langsung <span class="_ _5"> </span>maupun  tidak <span class="_ _5"> </span>langsung <span class="_ _5"> </span>terhadap <span class="_ _5"> </span>setiap  informasi <span class="_ _5"> </span>dan <span class="_ _5"> </span>data <span class="_ _5"> </span>yang <span class="_ _5"> </span>berhubungan </div><div class="t m0 x4 h2 y60 ff3 fs1 fc0 sc0 ls0 ws0">dengan kegiatan transaksi pada <span class="ff4">platform</span> RUN Market.</div><div class="t m0 x2 h3 y61 ff1 fs1 fc0 sc0 ls0 ws0">H.<span class="_ _f"> </span>Pembekuan Akun, Pemutusan, Pemberhentian Penggunaan Layanan RUN Market</div></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>
        <div id="pf4" class="pf w0 h0" data-page-no="4"><div class="pc pc4 w0 h0"><div class="t m0 x3 h2 y41 ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span>Kami secara sepihak berhak melakukan pembekuan terhadap akun <span class="ff4">Seller</span>, dalam hal:</div><div class="t m0 x4 h2 y62 ff3 fs1 fc0 sc0 ls0 ws0">a.<span class="_ _14"> </span><span class="ff4">Seller </span>diduga <span class="_ _1"></span>atau <span class="_ _1"></span>terbukti melakukan <span class="_ _1"></span>pelanggaran atas <span class="_ _1"></span>satu, beberapa, <span class="_ _1"></span>ketentuan S<span class="_ _1"></span>&amp;K </div><div class="t m0 x5 h2 y63 ff3 fs1 fc0 sc0 ls0 ws0">ini maupun S&amp;K Turunan dan/atau Kebijakan Privasi RUN Market;</div><div class="t m0 x4 h2 y64 ff3 fs1 fc0 sc0 ls0 ws0">b.<span class="_ _15"> </span><span class="ff4">track record </span>transaksi <span class="ff4">Seller </span>tidak baik;</div><div class="t m0 x4 h2 y65 ff3 fs1 fc0 sc0 ls0 ws0">c.<span class="_ _16"> </span><span class="ff4">Seller </span>diduga melakukan penipuan dalam melaksanakan transaksi melalui <span class="ff4">platform </span>RUN </div><div class="t m0 x5 h2 y66 ff3 fs1 fc0 sc0 ls0 ws0">Market.</div><div class="t m0 x3 h2 y67 ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _5"> </span></span>dapat <span class="_ _5"> </span>mendapatkan <span class="_ _0"> </span>Kembali  hak <span class="_ _0"> </span>akses <span class="_ _5"> </span>atas <span class="_ _5"> </span>akunnya <span class="_ _5"> </span>dan <span class="_ _5"> </span>menikmati <span class="_ _0"> </span>layanan  RUN </div><div class="t m0 x4 h2 y68 ff3 fs1 fc0 sc0 ls0 ws0">Market  setelah <span class="_ _4"> </span>terbukti  tidak  bersalah <span class="_ _4"> </span>melakukan  hal-hal <span class="_ _4"> </span>sebagaimana  yang <span class="_ _4"> </span>tercantum </div><div class="t m0 x4 h2 y69 ff3 fs1 fc0 sc0 ls0 ws0">dalam poin G.1, atau atas kebijakan Kami.</div><div class="t m0 x3 h2 y6a ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span>Kami <span class="_ _1"></span>secara <span class="_ _1"></span>sepihak <span class="_ _1"></span>berhak <span class="_ _b"></span>untuk <span class="_ _1"></span>melakukan <span class="_ _1"></span>pemutusan <span class="_ _1"></span>layanan <span class="_ _b"></span>RUN <span class="_ _1"></span>Market <span class="_ _1"></span><span class="ff4">Seller</span> <span class="_ _1"></span>dalam </div><div class="t m0 x4 h2 y6b ff3 fs1 fc0 sc0 ls0 ws0">hal:</div><div class="t m0 x4 h2 y6c ff3 fs1 fc0 sc0 ls0 ws0">a.<span class="_ _14"> </span><span class="ff4">Seller <span class="_ _10"> </span></span>terbukti <span class="_ _6"> </span>melakukan <span class="_ _6"> </span>pelanggaran <span class="_ _10"> </span>S&amp;K, <span class="_ _6"> </span>S&amp;K <span class="_ _6"> </span>Turunan, <span class="_ _6"> </span>dan/atau <span class="_ _10"> </span>Kebijakan <span class="_ _6"> </span>Privasi </div><div class="t m0 x5 h2 y6d ff3 fs1 fc0 sc0 ls0 ws0">RUN Market;</div><div class="t m0 x4 h2 y6e ff3 fs1 fc0 sc0 ls0 ws0">b.<span class="_ _15"> </span><span class="ff4">Seller <span class="_ _d"></span><span class="ff3">terbukti <span class="_ _d"></span>melakukan <span class="_ _d"></span>penipuan <span class="_ _d"></span>dalam <span class="_ _d"></span>melaksanakan <span class="_ _d"></span>transaksi <span class="_ _d"></span>melalui <span class="_ _d"></span><span class="ff4">platform <span class="_ _d"></span><span class="ff3">RUN </span></span></span></span></div><div class="t m0 x5 h2 y6f ff3 fs1 fc0 sc0 ls0 ws0">Market;</div><div class="t m0 x4 h2 y70 ff3 fs1 fc0 sc0 ls0 ws0">c.<span class="_ _16"> </span><span class="ff4">Seller  </span>masuk <span class="_ _5"> </span>kedalam  <span class="ff4">black <span class="_ _5"> </span>lists  </span>Kami <span class="_ _5"> </span>atas  alasan-alasan <span class="_ _5"> </span>yang  dianggap  meme<span class="_ _3"></span>nuhi </div><div class="t m0 x5 h2 y71 ff3 fs1 fc0 sc0 ls0 ws0">kriteria <span class="ff4">black lists</span> sesuai kebijakan Kami.</div><div class="t m0 x3 h2 y72 ff3 fs1 fc0 sc0 ls0 ws0">4.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _5"> </span></span>berhak <span class="_ _0"> </span>melakukan <span class="_ _5"> </span>pemutusan <span class="_ _0"> </span>atau <span class="_ _5"> </span>penghentian <span class="_ _5"> </span>penggunaan <span class="_ _0"> </span>layanan <span class="_ _5"> </span>RUN <span class="_ _5"> </span>Market </div><div class="t m0 x4 h2 y73 ff3 fs1 fc0 sc0 ls0 ws0">dengan cara mengirimkan <span class="ff4">e-mail </span>atau surat kepada Kami.</div><div class="t m0 x2 h3 y74 ff1 fs1 fc0 sc0 ls0 ws0">I.<span class="_ _18"> </span>Batasan Tanggung Jawab Kami</div><div class="t m0 x3 h2 y75 ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span>Kami <span class="_ _0"> </span>tidak <span class="_ _0"> </span>b<span class="_ _1"></span>ertanggungjawab <span class="_ _6"> </span>atas <span class="_ _5"> </span>segala <span class="_ _0"> </span>bentuk <span class="_ _0"> </span>Wanprestasi <span class="_ _5"> </span>yang <span class="_ _6"> </span>dilakukan <span class="_ _5"> </span>oleh <span class="_ _6"> </span><span class="ff4">Buyer </span></div><div class="t m0 x4 h2 y76 ff3 fs1 fc0 sc0 ls0 ws0">terkait transaksi yang dilakukan dengan <span class="ff4">Seller</span>.</div><div class="t m0 x3 h2 y77 ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span><span class="ff4">Seller </span>dengan <span class="_ _1"></span>ini <span class="_ _1"></span>membebaskan Kami <span class="_ _1"></span>dari segala <span class="_ _1"></span>bentuk tanggung <span class="_ _1"></span>jawab atas <span class="_ _1"></span>transaksi <span class="_ _1"></span>yang </div><div class="t m0 x4 h2 y78 ff3 fs1 fc0 sc0 ls0 ws0">dilakukan antara <span class="ff4">Seller </span>dan <span class="ff4">Buyer </span>di luar <span class="ff4">platform </span>RUN Market.</div><div class="t m0 x3 h2 y79 ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span>Kami <span class="_ _6"> </span>tidak <span class="_ _6"> </span>bertanggung <span class="_ _0"> </span>jawab <span class="_ _6"> </span>dan <span class="_ _0"> </span><span class="ff4">Seller</span> <span class="_ _6"> </span>setuju <span class="_ _0"> </span>untuk <span class="_ _6"> </span>membebaskan <span class="_ _0"> </span>Kami <span class="_ _6"> </span>dari <span class="_ _6"> </span>tanggung </div><div class="t m0 x4 h2 y7a ff3 fs1 fc0 sc0 ls0 ws0">jawab <span class="_ _19"> </span>atas <span class="_ _19"> </span>terjadinya <span class="_ _19"> </span>keadaan <span class="_ _19"> </span>kahar <span class="_ _19"> </span>atau <span class="_ _19"> </span><span class="ff4">force <span class="_ _19"> </span>majeure</span>, <span class="_ _19"> </span>yaitu <span class="_ _19"> </span>kondisi <span class="_ _19"> </span>diluar </div><div class="t m0 x4 h2 y7b ff3 fs1 fc0 sc0 ls0 ws0">kehendak/kemampuan <span class="_ _3"></span>Kami, <span class="_ _3"></span>termasuk <span class="_ _3"></span>namun <span class="_ _3"></span>tidak <span class="_ _3"></span>terbatas <span class="_ _3"></span>pada <span class="_ _3"></span>bencana <span class="_ _3"></span>alam, <span class="_ _3"></span>huru-hara, </div><div class="t m0 x4 h2 y7c ff3 fs1 fc0 sc0 ls0 ws0">revolusi, <span class="_ _1"></span>blokade, <span class="_ _1"></span>embargo, <span class="_ _1"></span>tindakan <span class="_ _1"></span>dari <span class="_ _1"></span>pemerintah, <span class="_ _1"></span>pemogokan, <span class="_ _1"></span>penutupan <span class="_ _b"></span>perusahaan, </div><div class="t m0 x4 h2 y7d ff3 fs1 fc0 sc0 ls0 ws0">wabah, perintah <span class="_ _1"></span>hukum <span class="_ _1"></span>apapun, proklamasi, <span class="_ _1"></span>regulasi, ordonansi, <span class="_ _1"></span>saluran yang <span class="_ _1"></span>hilang karena </div><div class="t m0 x4 h2 y7e ff3 fs1 fc0 sc0 ls0 ws0">gangguan <span class="_ _5"> </span><span class="ff4">server <span class="_ _5"> </span></span>yang <span class="_ _5"> </span>bukan <span class="_ _5"> </span>merupakan <span class="_ _5"> </span>kesalahan <span class="_ _5"> </span>Kami, <span class="_ _5"> </span>dan <span class="_ _5"> </span>hal-hal <span class="_ _5"> </span>lain <span class="_ _5"> </span>yang <span class="_ _5"> </span>mungkin </div><div class="t m0 x4 h2 y7f ff3 fs1 fc0 sc0 ls0 ws0">terjadi <span class="_ _b"></span>selama <span class="_ _1"></span>masa <span class="_ _b"></span>penggunaan <span class="_ _1"></span>yang <span class="_ _b"></span>berada <span class="_ _1"></span>di <span class="_ _b"></span>luar <span class="_ _b"></span>jangkauan <span class="_ _1"></span>kemampuan <span class="_ _b"></span>manusia <span class="_ _1"></span>pada </div><div class="t m0 x4 h2 y80 ff3 fs1 fc0 sc0 ls0 ws0">umumnya. <span class="_ _a"> </span>Dalam <span class="_ _17"> </span>hal <span class="_ _a"> </span>terjadi <span class="_ _17"> </span>keadaan <span class="_ _17"> </span>kahar <span class="_ _a"> </span>atau <span class="_ _17"> </span><span class="ff4">force <span class="_ _a"> </span>majeure <span class="_ _17"> </span></span>tersebut, <span class="_ _17"> </span>Kami <span class="_ _a"> </span>tidak </div><div class="t m0 x4 h2 y81 ff3 fs1 fc0 sc0 ls0 ws0">berkewajiban <span class="_ _10"></span>untuk <span class="_ _10"></span>memberikan <span class="_ _10"> </span>ganti <span class="_ _10"> </span>rugi <span class="_ _6"> </span>dalam <span class="_ _10"></span>bentuk <span class="_ _10"></span>apapun <span class="_ _10"> </span>kepada <span class="_ _6"> </span><span class="ff4">Seller</span>, <span class="_ _10"></span>termasuk </div><div class="t m0 x4 h2 y82 ff3 fs1 fc0 sc0 ls0 ws0">tidak berkewajiban pada penyesuaian Biaya Layanan, biaya admin, dan biaya lainnya.</div><div class="t m0 x2 h3 y83 ff1 fs1 fc0 sc0 ls0 ws0">J.<span class="_ _1a"> </span>Hukum dan Sanksi</div></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>
        <div id="pf5" class="pf w0 h0" data-page-no="5"><div class="pc pc5 w0 h0"><div class="t m0 x3 h2 y41 ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span>S&amp;K <span class="_ _10"></span>ini <span class="_ _10"></span>diatur, <span class="_ _10"></span>ditafsirkan, <span class="_ _10"> </span>dan <span class="_ _10"> </span>dilaksanakan <span class="_ _10"> </span>sesuai <span class="_ _6"> </span>dengan <span class="_ _10"></span>hukum <span class="_ _10"></span>yang <span class="_ _10"></span>berlaku <span class="_ _10"></span>di <span class="_ _10"> </span>Negara </div><div class="t m0 x4 h2 y62 ff3 fs1 fc0 sc0 ls0 ws0">Republik Indonesia.</div><div class="t m0 x3 h2 y63 ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span>Dalam <span class="_ _5"> </span>hal  terdapat <span class="_ _5"> </span>salah  satu <span class="_ _5"> </span>maupun <span class="_ _5"> </span>beberapa  ketentuan <span class="_ _5"> </span>dari <span class="_ _5"> </span>S&amp;K  ini <span class="_ _5"> </span>yang <span class="_ _5"> </span>dianggap </div><div class="t m0 x4 h2 y64 ff3 fs1 fc0 sc0 ls0 ws0">melanggar <span class="_ _b"></span>hukum, <span class="_ _b"></span>batal, <span class="_ _b"></span>atau <span class="_ _b"></span>karena <span class="_ _b"></span>alasan <span class="_ _b"></span>apapun <span class="_ _10"></span>tidak <span class="_ _b"></span>dapat <span class="_ _b"></span>diberlakukan <span class="_ _b"></span>berdasarkan </div><div class="t m0 x4 h2 y65 ff3 fs1 fc0 sc0 ls0 ws0">hukum yang berlaku <span class="_ _1"></span>di Negara Republik <span class="_ _1"></span>Indonesia, maka ketentuan tersebut <span class="_ _1"></span>harus dianggap </div><div class="t m0 x4 h2 y84 ff3 fs1 fc0 sc0 ls0 ws0">dapat <span class="_ _6"> </span>dipi<span class="_ _1"></span>sahkan <span class="_ _6"> </span>dari <span class="_ _0"> </span>S&amp;K <span class="_ _0"> </span>ini <span class="_ _0"> </span>dan <span class="_ _0"> </span>tidak <span class="_ _6"> </span>akan <span class="_ _0"> </span>mempengaruhi <span class="_ _0"> </span>keabsahan <span class="_ _0"> </span>dan <span class="_ _0"> </span>keberlakuan </div><div class="t m0 x4 h2 y85 ff3 fs1 fc0 sc0 ls0 ws0">setiap  ketentuan <span class="_ _4"> </span>yang <span class="_ _4"> </span>lainnya  maupun <span class="_ _4"> </span>keabsahan  dan <span class="_ _4"> </span>keberlakuan <span class="_ _4"> </span>ketentuan  tersebut </div><div class="t m0 x4 h2 y86 ff3 fs1 fc0 sc0 ls0 ws0">berdasarkan hukum yang berlaku di Negara Republik Indonesia.</div><div class="t m0 x3 h2 y69 ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span>Pelanggaran <span class="_ _b"></span>atas <span class="_ _b"></span>ketentuan <span class="_ _b"></span>S&amp;K, <span class="_ _b"></span>S&amp;K <span class="_ _b"></span>Turunan, <span class="_ _1"></span>d<span class="_ _1"></span>an/atau <span class="_ _b"></span>Kebijakan <span class="_ _1"></span>Privasi <span class="_ _b"></span>oleh <span class="_ _b"></span><span class="ff4">Seller <span class="_ _b"></span></span>akan </div><div class="t m0 x4 h2 y87 ff3 fs1 fc0 sc0 ls0 ws0">menimbulkan <span class="_ _10"></span>hak <span class="_ _10"> </span>bagi <span class="_ _6"> </span>Kami <span class="_ _10"> </span>untuk <span class="_ _6"> </span>melakukan <span class="_ _10"></span>tuntutan <span class="_ _10"> </span>pidana, <span class="_ _6"> </span>gugatan <span class="_ _10"></span>perdata, <span class="_ _10"> </span>maupun </div><div class="t m0 x4 h2 y88 ff3 fs1 fc0 sc0 ls0 ws0">pengenaan <span class="_ _a"> </span>sanksi <span class="_ _2"> </span>kepada <span class="_ _a"> </span><span class="ff4">Seller <span class="_ _a"> </span></span>sesuai <span class="_ _a"> </span>dengan <span class="_ _a"> </span>hukum <span class="_ _a"> </span>yang <span class="_ _a"> </span>berlaku <span class="_ _2"> </span>di<span class="_ _1"></span> <span class="_ _2"> </span>Indonesia <span class="_ _a"> </span>dan </div><div class="t m0 x4 h2 y89 ff3 fs1 fc0 sc0 ls0 ws0">kebijakan Kami.</div><div class="t m0 x2 h3 y8a ff1 fs1 fc0 sc0 ls0 ws0">K.<span class="_ _c"> </span>Pernyataan dan Jaminan <span class="ff2">Seller</span></div><div class="t m0 x3 h2 y8b ff4 fs1 fc0 sc0 ls0 ws0">Buyer <span class="ff3">menyatakan dan menjamin bahwa:</span></div><div class="t m0 x3 h2 y8c ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _6"> </span></span>memiliki <span class="_ _6"> </span>kapasitas <span class="_ _0"> </span>hukum, <span class="_ _6"> </span>hak, <span class="_ _6"> </span>kewajiban, <span class="_ _0"> </span>serta <span class="_ _6"> </span>wewenang <span class="_ _6"> </span>untuk <span class="_ _0"> </span>mengikatkan <span class="_ _6"> </span>diri </div><div class="t m0 x4 h2 y8d ff3 fs1 fc0 sc0 ls0 ws0">pada S&amp;K ini dan mematuhi seluruh isi S&amp;K ini; dan</div><div class="t m0 x3 h2 y8e ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span><span class="ff4">Seller <span class="_ _d"></span><span class="ff3">akan menggunakan <span class="_ _d"></span>layanan <span class="_ _3"></span>RUN <span class="_ _d"></span>Market untuk <span class="_ _d"></span>tujuan <span class="_ _3"></span>yang <span class="_ _d"></span>sah dan <span class="_ _d"></span>sesuai <span class="_ _3"></span>S&amp;K <span class="_ _d"></span>ini serta </span></span></div><div class="t m0 x4 h2 y8f ff3 fs1 fc0 sc0 ls0 ws0">peraturan <span class="_ _6"> </span>perundang-undangan, <span class="_ _0"> </span>kode, <span class="_ _0"> </span>arahan, <span class="_ _0"> </span>pedoman, <span class="_ _0"> </span>kebijakan, <span class="_ _6"> </span>dan <span class="_ _0"> </span>aturan <span class="_ _0"> </span>lain <span class="_ _0"> </span>yang </div><div class="t m0 x4 h2 y90 ff3 fs1 fc0 sc0 ls0 ws0">berlaku.</div><div class="t m0 x2 h3 y91 ff1 fs1 fc0 sc0 ls0 ws0">L.<span class="_ _1b"> </span>Lain-lain</div><div class="t m0 x3 h2 y92 ff3 fs1 fc0 sc0 ls0 ws0">1.<span class="_ _8"> </span>Kami <span class="_ _4"> </span>berhak <span class="_ _2"> </span>melakukan <span class="_ _4"> </span>promosi <span class="_ _2"> </span>kepada <span class="_ _4"> </span><span class="ff4">Seller</span>, <span class="_ _2"> </span>baik <span class="_ _4"> </span>langsung <span class="_ _2"> </span>maupun <span class="_ _4"> </span>tidak <span class="_ _2"> </span>langsung, </div><div class="t m0 x4 h2 y93 ff3 fs1 fc0 sc0 ls0 ws0">termasuk juga pengiriman informasi produk, atau jasa, atau berita langsung ke <span class="ff4">Seller</span>.</div><div class="t m0 x3 h2 y94 ff3 fs1 fc0 sc0 ls0 ws0">2.<span class="_ _8"> </span>Kami <span class="_ _2"> </span>berhak <span class="_ _2"> </span>melakukan <span class="_ _2"> </span>revisi, <span class="_ _2"> </span>perubahan, <span class="_ _a"> </span>penambahan, <span class="_ _2"> </span>pengurangan, <span class="_ _2"> </span>atau <span class="_ _2"> </span>perbaikan </div><div class="t m0 x4 h2 y95 ff3 fs1 fc0 sc0 ls0 ws0">terhadap S&amp;K ini setiap waktu, tanpa pemberitahuan terlebih dahulu kepada <span class="ff4">Seller</span>.</div><div class="t m0 x3 h2 y96 ff3 fs1 fc0 sc0 ls0 ws0">3.<span class="_ _8"> </span>Dalam <span class="_ _0"> </span>hal <span class="_ _5"> </span><span class="ff4">Seller</span> <span class="_ _0"> </span>berhenti <span class="_ _5"> </span>menggunakan <span class="_ _0"> </span>layanan <span class="_ _5"> </span>RUN <span class="_ _0"> </span>Market, <span class="_ _5"> </span>maka <span class="_ _0"> </span>data <span class="_ _5"> </span>informasi <span class="_ _0"> </span>yang </div><div class="t m0 x4 h2 y97 ff3 fs1 fc0 sc0 ls0 ws0">sudah diberikan oleh <span class="ff4">Seller</span> kepada Kami akan tetap menjadi milik Kami.</div><div class="t m0 x2 h3 y98 ff1 fs1 fc0 sc0 ls0 ws0">Dengan ini <span class="ff2">Seller </span>telah membaca dan menyetujui seluruh isi S&amp;K dan setiap revisi yang dilakukan.</div><div class="t m0 x2 h2 y99 ff3 fs1 fc0 sc0 ls0 ws0">Terakhir kali diubah: 30 Agustus 2021</div><div class="t m0 x2 h2 y9a ff3 fs1 fc0 sc0 ls0 ws0">©RUN Market</div></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>
        </div>
        <div class="loading-indicator">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btnAgree" class="btn btn-info" data-dismiss="modal">Agree</button>
      </div>
    </div>
  </div>
</div>

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script> 
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
<script src="plugins/select2/select2.full.min.js"></script>
<script src="plugins/tnc/compatibility.min.js"></script>
<script src="plugins/tnc/theViewer.min.js"></script>

<script>
  $(document).on('click', '#btnAgree', function(){
    document.getElementById("agree").checked = true;
    document.getElementById("agree").disabled= true;
    if(document.getElementById("agree").checked && document.getElementById("download").checked ){
    document.getElementById("btnRegister").disabled = false;

  }
  });
  $(document).on('click', '#manualBook', function(){
    document.getElementById("download").checked = true;
    document.getElementById("download").disabled= true;
    if(document.getElementById("agree").checked && document.getElementById("download").checked ){
    document.getElementById("btnRegister").disabled = false;

  }
  });
</script>
<script type="text/javascript">

  $(document).on('click', '.tnc', function(){
    console.log("test")
    $('#showTnc').modal("show");
    var x = 0;
    function myFunction() {
      console.log(x + 1);
    }
  });
</script>
<script type="text/javascript" language="JavaScript">
  $(function () 
  {
    var x = 0;
    function myFunction() {
      console.log(x + 1);
    }
    document.getElementById("agree").disabled= true;
    $(".selectCity").select2();
    $(".selectHO").select2();
    $(".selectBankCode").select2();
  });
</script>

<!-- <script type="text/javascript">
  function ShowRegisteredEmail(email)
  {
    if(email == "")
    {
        document.getElementById("eEmail").innerHTML = "";
        return;
    }
    else
    {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("eEmail").innerHTML = this.responseText;
                if(this.responseText.length > 0) return false;
            }
        };
        xmlhttp.open("GET","getregisteredemail.php?q="+email,true);
        xmlhttp.send();
    }
  }
  
  function validateRegister()
  {
    document.getElementById("eVdName").innerHTML = "";
    document.getElementById("eShortName").innerHTML = "";
    document.getElementById("eEmail").innerHTML = "";
    document.getElementById("eContactPersonName").innerHTML = "";
    document.getElementById("eIDNumber").innerHTML = "";
    document.getElementById("eTIN").innerHTML = "";
    document.getElementById("eAddress").innerHTML = "";
    document.getElementById("eCityCode").innerHTML = "";
    document.getElementById("ePostalCd").innerHTML = "";
    document.getElementById("ePhone").innerHTML = "";
    document.getElementById("eFax").innerHTML = "";
    document.getElementById("eMobile").innerHTML = "";
    document.getElementById("eWebsite").innerHTML = "";

    var VdName = document.getElementById("VdName").value;
    var ShortName = document.getElementById("ShortName").value;
    var Email = document.getElementById("Email").value;
    var ContactPersonName = document.getElementById("ContactPersonName").value;
    var IDNumber = document.getElementById("IDNumber").value;
    var TIN = document.getElementById("TIN").value;
    var Address = document.getElementById("Address").value;
    var City = document.getElementById("LueCityCode").value;
    var PostalCd = document.getElementById("PostalCd").value;
    var Phone = document.getElementById("Phone").value;
    var Fax = document.getElementById("Fax").value;
    var Mobile = document.getElementById("Mobile").value;
    var Website = document.getElementById("Website").value;

    if(VdName == "") { document.getElementById("eVdName").innerHTML = "Company's Name should not be empty."; return false; }
    if(ShortName == "") { document.getElementById("eShortName").innerHTML = "Company's Short Name should not be empty."; return false; }
    if(Email == "") { document.getElementById("eEmail").innerHTML = "Company's Email should not be empty."; return false; }
    if(ContactPersonName == "") { document.getElementById("eContactPersonName").innerHTML = "Contact Person should not be empty."; return false; }
    if(IDNumber == "") { document.getElementById("eIDNumber").innerHTML = "ID Number should not be empty."; return false; }
    if(TIN == "") { document.getElementById("eTIN").innerHTML = "NPWP should not be empty."; return false; }
    if(Address == "") { document.getElementById("eAddress").innerHTML = "Address should not be empty."; return false; }
    if(City == "") { document.getElementById("eCityCode").innerHTML = "City should not be empty."; return false; }
    if(PostalCd == "") { document.getElementById("ePostalCd").innerHTML = "Postal Code should not be empty."; return false; }
    if(Phone == "") { document.getElementById("ePhone").innerHTML = "Phone should not be empty."; return false; }
    if(Fax == "") { document.getElementById("eFax").innerHTML = "Fax should not be empty."; return false; }
    if(Mobile == "") { document.getElementById("eMobile").innerHTML = "Mobile should not be empty."; return false; }
    if(Website == "") { document.getElementById("eWebsite").innerHTML = "Website should not be empty."; return false; }
  }
</script> -->
<!-- VENDOR SECTOR -->
<script>

  var countVdContactPerson = document.getElementById("countVdContactPerson").value;
  var counter = Number(countVdContactPerson);

  $("#CPaddrow").on("click", function () {
      var newRow = $("<tr>");
      var cols = "";

      cols += '<td><input type="text" class="form-control" name="CPName' + counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" name="CPIDNumber' + counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" name="CPPos' + counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" name="CPNo' + counter + '"/></td>';
      cols += '<td><select class="form-control selectType' + counter + '" name="LueType' + counter + '" id="LueType' + counter + '">';
      cols += '<option value=""></option>';
      <?php
        $sql = "";
        $sql = "SELECT OptCode AS Col1, OptDesc AS Col2 FROM tbloption WHERE optcat = 'VendorContactType'";
        $result = mysqli_query($conn, $sql) or die (mysqli_error($conn));
        while($res = mysqli_fetch_assoc($result))
        {
        ?>
          cols += '<option value="<?php echo $res['Col1']; ?>"><?php echo str_replace("'", "\'", $res['Col2']); ?></option>';
        <?php
        }
      ?>
      cols += '</select></td>';

      cols += '<td><input type="button" class="CPbtnDel btn btn-md btn-danger" value="Delete"></td>';
      newRow.append(cols);
      $("table.Contact-Person").append(newRow);
      counter++;
      console.log(counter);
  });

  $("table.Contact-Person").on("click", ".CPbtnDel", function (event) {
      $(this).closest("tr").remove();       
      counter -= 1
  });

	var countVdBankAccount = document.getElementById("countVdBankAccount").value;
	var counterBA = Number(countVdBankAccount);

	$("#BAaddrow").on("click", function () {
		var newRow = $("<tr>");
		var cols = "";

		cols += '<td style="width:35%"><select class="form-control selectBankCode' + counterBA + '" name="LueBankCode' + counterBA + '" id="LueBankCode' + counterBA + '" style="width: 100%;">';
		cols += '<option value=""></option>';
		<?php
			$sqlB = "Select BankCode As Col1, BankName As Col2 From TblBank Order By BankName ";
			$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
			while($resB = mysqli_fetch_assoc($resultB))
			{
			?>
				cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
			<?php
			}
		?>
		cols += '</select></td>';
		cols += '<td><input type="text" class="form-control" name="BABankBranch' + counterBA + '"/></td>';
		cols += '<td><input type="text" class="form-control" name="BABankAcName' + counterBA + '"/></td>';
		cols += '<td><input type="text" class="form-control" name="BABankAcNo' + counterBA + '"/></td>';

		cols += '<td><input type="button" class="BAbtnDel btn btn-md btn-danger" value="Delete"></td>';
		newRow.append(cols);
		$("table.BankAccount").append(newRow);

		for(var j = 0; j <= counterBA; j++)
			$(".selectBankCode" + j + "").select2();

		counterBA++;
    console.log(counterBA);
	});

	$("table.BankAccount").on("click", ".BAbtnDel", function (event) {
		$(this).closest("tr").remove();       
		counterBA -= 1
	});

	// var countVdItCt = document.getElementById("countVdItCt").value;
	// var counterItCt = Number(countVdItCt);

	// $("#ItCtaddrow").on("click", function () {
	// 	var newRow = $("<tr>");
	// 	var cols = "";

	// 	cols += '<td><select class="form-control selectVdItCt' + counterItCt + '" name="LueVdItCt' + counterItCt + '" id="LueVdItCt' + counterItCt + '" style="width: 100%;">';
	// 	cols += '<option value=""></option>';
	// 	<?php
	// 		$sqlB = "Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory Order By ItCtName ";
	// 		$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
	// 		while($resB = mysqli_fetch_assoc($resultB))
	// 		{
	// 		?>
	// 			cols += '<option value="<?php //echo $resB['Col1']; ?>"><?// echo str_replace("'", "\'", $resB['Col2']); ?></option>';
	// 		<?php
	// 		}
	// 	?>
	// 	cols += '</select></td>';

	// 	cols += '<td><input type="button" class="ItCtbtnDel btn btn-md btn-danger" value="Delete"></td>';
	// 	newRow.append(cols);
	// 	$("table.ItCt").append(newRow);

	// 	for(var j = 0; j <= counterItCt; j++)
	// 		$(".selectVdItCt" + j + "").select2();

	// 	counterItCt++;
	// });

	// $("table.ItCt").on("click", ".ItCtbtnDel", function (event) {
	// 	$(this).closest("tr").remove();       
	// 	counterItCt -= 1
	// });

	var blankComboBox = "<option value=''></option>";

	$("#LueCityCode").change(function(){
		var CityCode = $("#LueCityCode").val();
		$.ajax({
			url: "GetSubDistrict.php",
			data: "CityCode="+CityCode,
			cache: false,
			success: function(msg){
				$("#LueSDCode").html(msg);
				$("#LueVillageCode").html(blankComboBox);
				$("#LueVillageCode").attr("disabled", true);
			}
		});
	});

	$("#LueSDCode").change(function(){
		var SDCode = $("#LueSDCode").val();
		$.ajax({
			url: "GetVillage.php",
			data: "SDCode="+SDCode,
			cache: false,
			success: function(msg){
				$("#LueVillageCode").attr("disabled", false);
				$("#LueVillageCode").html(msg);
			}
		});
	});

	var countVdSector = document.getElementById("countVdSector").value;
	var counterVdSector = Number(countVdSector);
	var countVdSubSector = document.getElementById("countVdSubSector").value;
	var counterVdSubSector = Number(countVdSubSector);

	$("#VdSectoraddrow").on("click", function () { 
		var newRow = $("<tr>");
		var cols = "";

    console.log(counterVdSector);
		cols += '<td style="width:50%;"><select style="text-overflow: ellipsis" class="form-control selectVdSector' + counterVdSector + '" name="LueVdSector' + counterVdSector + '" id="LueVdSector' + counterVdSector + '" style="width: 100%;">';
		cols += '<option value=""></option>';
		<?php
			$sqlB = "Select SectorCode As Col1, SectorName As Col2 From TblSector Where ActInd = 'Y' Order By SectorName ";
			$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
			while($resB = mysqli_fetch_assoc($resultB))
			{
			?>
				cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
			<?php
			}
		?>
		cols += '</select></td>';

    cols += '<td style="width:50%;"><select style="text-overflow: ellipsis" class="form-control selectVdSubSector' + counterVdSubSector + '" name="LueVdSubSector' + counterVdSubSector + '" id="LueVdSubSector' + counterVdSubSector + '" style="width: 100%;">';
		cols += '<option value=""></option>';

    cols += '</select></td>';

		cols += '<td><input type="button" class="VdSectorbtnDel btn btn-md btn-danger" value="Delete"></td>';
		newRow.append(cols);
		$("table.VdSector").append(newRow);


    $(`#LueVdSector${counterVdSector}`).change( function(event){
      var vdSector = event.target.value;
      $.ajax({
        url : 'register.php',
        method: 'POST',
        data: {
          sector : vdSector,
        },
        success (data){
          data = JSON.parse(data);
          $(`#LueVdSubSector${counterVdSubSector-1}`).html($("<option></option>"));
          $.each(data, function(key, value) {
              $(`#LueVdSubSector${counterVdSubSector-1}`)
                .append($("<option style='text-overflow:'ellipsis''></option>")
                .attr("value",value.SubSectorCode)
                .text(value.SubSectorName));
        });
          // $(`#LueVdQualification${counterVdQualification}`).html(option);

        },
        error (err){
          console.log(`Error ${err.innerHtmlText}`)
        }
      });
    });
		for(var j = 0; j <= counterVdSector; j++)
		{
			$(".selectVdSector" + j + "").select2();
		}

		for(var j = 0; j <= counterVdSubSector; j++)
		{
			$(".selectVdSubSector" + j + "").select2();
		}

		counterVdSector++; counterVdSubSector++;
	});

	$("table.VdSector").on("click", ".VdSectorbtnDel", function (event) {
		$(this).closest("tr").remove();       
		// counterVdSector -= 1;
    // counterVdSubSector -= 1;
	});

  var countVdCompliance = document.getElementById("countVdCompliance").value;
	var counterVdCompliance = Number(countVdCompliance);

	$("#CompAddRow").on("click", function () {
		var newRow = $("<tr>");
		var cols = "";

		cols += '<td><select class="form-control selectVdComp' + counterVdCompliance + '" name="LueFileCt' + counterVdCompliance + '" id="LueFileCt' + counterVdCompliance + '" style="width: 100%;">';
		cols += '<option value=""></option>';
		<?php
        $q = "Select CategoryCode As Col1, CategoryName As Col2 From TblVendorFileUploadCategory ";
        $q = $q . "Order By CategoryName; ";

        $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
        while($q2 = mysqli_fetch_assoc($q1))
        { ?>
          cols+= '<option value="<?php echo $q2['Col1']; ?>"><?php echo str_replace("'", "\'", $q2['Col2']); ?></option>'
       <?php }
    ?>
		cols += '</select></td>';

		cols += '<td><input type="file" name="VdFile' + counterVdCompliance + '" id="VdFile" accept="application/pdf, image/png, image/jpg, image/jpeg"/>';
		cols += '<td><input type="button" class="VdComplianceDel btn btn-md btn-danger" value="Delete"></td>';
		newRow.append(cols);
		$("table.compliance").append(newRow);

		for(var j = 0; j <= counterVdSector; j++)
		{
			$(".selectVdComp" + j + "").select2();
		}

		// for(var j = 0; j <= counterVdQualification; j++)
		// {
		// 	$(".selectVdQualification" + j + "").select2();
		// }

		counterVdCompliance++; 
	});

	$("table.compliance").on("click", ".VdComplianceDel", function (event) {
		$(this).closest("tr").remove();       
		counterVdCompliance -= 1
	});


</script>


<?php
include 'footer.php';
?>