<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

$VdCode = $_SESSION['vdcode'];
$sql = "SELECT T.EntName, T.DocNo ,T.LocalDocNo, T.PIDocNo, T.PIDocDt, T.Status, T.Amt, T.TaxAmt, T.TotalAmt, T.TotalOutstanding,
        if(T.TotalOutstanding = 0, 'Done', 'Payment On Progress') PaymentStatus, T.PtName, T.OptDesc, T.PaidAmount,
        T.DueDt, T.CurCode, T.Remark
        FROM
        (
            SELECT C.EntName, B.LocalDocNo, D.PIDocNo, D.PIDocDt, A.Remark, A.DocNo,
            case B.InvoiceStatusInd
                when 'A' then 'Approved'
                when 'P' then 'Verified'
                when 'C' then 'Not Verified'
                when 'O' then 'Outstanding'
            END AS Status, G.OptDesc, Date_Format(B.DueDt, '%d-%m-%Y') DueDt, B.CurCode,
            IFNULL(D.Amt, 0.00) Amt, IFNULL(D.TaxAmt, 0.00) TaxAmt,
            IFNULL(D.totalAmt, 0.00) TotalAmt,
            IFNULL(D.totalAmt - IFNULL(E.VoucherOPAmt, 0.00), 0.00) TotalOutstanding, F.PtName, IFNULL(E.VoucherOPAmt, 0.00) PaidAmount
            FROM tblvendorinvoicedtl A
            Inner JOIN tblvendorinvoicehdr B ON A.DocNo = B.DocNo
                AND B.CancelInd = 'N'
                AND B.VdCode = '$VdCode'
            Inner JOIN tblentity C ON B.EntCode = C.EntCode
            Inner JOIN tblOption G ON B.PaymentType = G.OptCode AND G.OptCat = 'VoucherPaymentType'
            LEFT JOIN
            (
                SELECT Distinct T1.DocNo, T1.DNo, T4.DocNo PIDocNo, T4.DocDt PIDocDt, T2.Amt, T2.TaxAmt,
                IFNULL(T2.Amt, 0.00) TotalAmt
                FROM tblvendorinvoicedtl T1
                Inner Join TblVendorInvoiceHdr T2 On T1.DocNo = T2.DocNo And T2.CancelInd = 'N'
                Left JOIN tblpurchaseinvoicedtl T3 ON T1.RecvVdDocNo = T3.RecvVdDocNo AND T1.RecvVdDNo = T3.RecvVdDNo
                Left JOIN tblpurchaseinvoicehdr T4 ON T1.DocNo = T4.VdInvNo
                    And T4.CancelInd = 'N'
                    And T4.Status = 'A'
            ) D ON A.DocNo = D.DocNo And A.DNo = D.DNo
            LEFT JOIN
            (
                SELECT T1.DocNo, IFNULL(Sum(T5.Amt), 0.00) VoucherOPAmt
                From TblVendorInvoiceHdr T1
                Inner Join TblPurchaseInvoiceHdr T2 On T1.DocNo = T2.VdInvNo
                    And T2.CancelInd = 'N'
                    And T2.Status = 'A'
                    And T1.CancelInd = 'N'
                Inner JOIN TblOutgoingPaymentDtl T3 ON T2.DocNo = T3.InvoiceDocno
                Inner JOIN TblOutgoingPaymentHdr T4 ON T3.DocNo = T4.DocNo
                    And T4.CancelInd = 'N'
                    And T4.Status = 'A'
                Inner JOIN tblvoucherhdr T5 ON T4.VoucherRequestDocNo = T5.VoucherRequestDocNo
                    And T5.CancelInd = 'N'
                Group By T1.DocNo
            ) E ON A.DocNo = E.DocNo
            INNER JOIN
            (
                SELECT Distinct T1.DocNo, T1.DNo, T7.PtName
                FROM tblvendorinvoicedtl T1
                INNER JOIN tblrecvvddtl T2 ON T1.RecvVdDocNo = T2.DocNo And T1.RecvVdDNo = T2.DNo
                INNER JOIN tblpodtl T3 ON T2.PODocNo = T3.DocNo AND T2.PODNo = T3.DNo
                INNER JOIN tblporequestdtl T4 ON T3.PORequestDocNo = T4.DocNo AND T3.POrequestDNo = T4.DNo
                INNER JOIN tblqtdtl T5 ON T4.QtDocNo = T5.DocNo AND T4.QtDNo = T5.DNo
                INNER JOIN tblqthdr T6 ON T5.DocNo = T6.DocNo
                INNER JOIN tblpaymentterm T7 ON T6.PtCode = T7.PtCode
            ) F ON A.DocNo = F.DocNo And A.DNo = F.DNo
        ) T
        ORDER BY T.LocalDocNo ";

?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header">
                <h4 class="box-title">Payment</h4>
            </div>
            <div class="box-body">
                <table id="Payment" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Company (Entity)</th>
                            <th>Invoice#</th>
                            <th>Document#</th>
                            <th>Invoice Date</th>
                            <th>Invoice Status</th>
                            <th>Total Amount</th>
                            <th>Paid Amount</th>
                            <th>Outstanding Amount</th>
                            <th>Payment Status</th>
                            <th>Cross Selling</th>
                            <th>Term Of Payment</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php
                    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                    while ($rows = mysqli_fetch_assoc($result)) {
                    ?>
                        <tbody>
                            <td><?= $rows['EntName']; ?></td>
                            <!-- <td style="display:none;"><?= $rows['DocNo']; ?></td>                         -->
                            <td><?= $rows['LocalDocNo']; ?></td>
                            <td><?= $rows['PIDocNo']; ?></td>
                            <td><?= $rows['PIDocDt']; ?></td>
                            <td><?= $rows['Status']; ?></td>
                            <td align="right"><?= number_format($rows['TotalAmt'], 2, ",", "."); ?></td>
                            <td align="right"><?= number_format($rows['PaidAmount'], 2, ",", "."); ?></td>
                            <td align="right"><?= number_format($rows['TotalOutstanding'], 2, ",", "."); ?></td>
                            <td><?= $rows['PaymentStatus']; ?></td>
                            <td></td>
                            <td><?= $rows['PtName'] ?></td>
                            <td><button type='button' class='btn_detail btn-sm btn-info' id="<?= $rows['DocNo'] ?>"> Detail</button></td>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>

<!-- modal for detail -->
<div class="modal fade" id="PaymentModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center>
                    <h4 class="modal-title" id="myModalLabel">Payment Detail</h4>
                </center>
            </div>
            <div class="modal-body" id="PaymentDtl"></div>
        </div>
    </div>
</div>

<?php
include "footer.php"
?>

<script>
    $(function() {
        $("#Payment").DataTable({
            responsive: true,
            autoWidth: true,
            scrollX: true,
            scrollCollapse: true,
            paging: true,            
        });        
    });
</script>
<script>
    $('.btn_detail').click(function() {
        var DocNo = $(this).attr("id");
        // $('#PaymentDtl').text("");
        console.log('====================================');
        console.log(DocNo);
        console.log('====================================');

        window.location.href = "invoice.php?d="+DocNo
    }); 
</script>