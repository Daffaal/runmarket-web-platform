<?php 
include "conn.php";
include "auth.php";
$DocNo = $_POST['DocNo'];

echo '<link rel="stylesheet" href="stepper/stepper.css">';
echo '<ol class="c-stepper">';

        $sqlP = "SELECT t.StageName , Date_Format(t.StartDt, '%d/%m/%Y') StartDt , Date_Format(t.EndDt, '%d/%m/%Y') EndDt, t.StartTm, t.EndTm ,
            t.StartDt StartDt2, t.EndDt EndDt2
            FROM TblRfqDtl3 t
            WHERE t.MaterialRequestDocNo = '".$DocNo."';";        

        $resultP = mysqli_query($conn, $sqlP); /*or die(mysqli_errno(($conn)));*/
        while ($res = mysqli_fetch_assoc($resultP)) {
            $start = $res['StartDt'].' '. substr($res['StartTm'],0, 2).':'. substr($res['StartTm'], 2, 2);
            $end = $res['EndDt'].' '. substr($res['EndTm'],0, 2).':'. substr($res['EndTm'], 2, 2);
            $isCurDate = (date('YmdHi') >= date('YmdHi', strtotime($res['StartDt2'].$res['StartTm']))  && date('YmdHi') <= date('YmdHi', strtotime($res['EndDt2'].$res['EndTm']))  ? 'now' : (date('YmdHi') > date('YmdHi', strtotime($res['EndDt2'].$res['EndTm'])) ? 'past' : 'later'));        
            echo '<li class="'. ($isCurDate == "now" ? "c-stepper__item-present" : ($isCurDate == "past" ? "c-stepper__item-past" : "c-stepper__item-future")).'">';
            echo '   <div class="c-stepper__content">';
            echo '   <h3 class="c-stepper__title">'.$res['StageName'].'</h3>';
            echo '            <p style="line-height:1.4rem" class="c-stepper__desc">'. $start. ' - '. $end .'</p>';
            echo '    </div>';
            echo '</li>';
     }     
echo '</ol>';

?>