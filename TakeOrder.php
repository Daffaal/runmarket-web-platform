<?php 

    include "conn.php";
    include "auth.php";
    include "StdMtd.php";

    if (isset($_POST['DocNo']) && isset($_POST['TakeOrderInd']) && isset($_POST['TakeOrderRemark']))
    {
        $sql = "
            Update TblPORequestDtl
            Set TakeOrderInd = '".$_POST['TakeOrderInd']."', TakeOrderRemark = NullIf('".$_POST['TakeOrderRemark']."', ''),
            LastUpBy = '".$_SESSION['vdusercode']."', LastUpDt = CurrentDateTime()
            Where DocNo = '".$_POST['DocNo']."';
        ";

        if ($_POST['TakeOrderInd'] == "Y")
        {
            $sql .= "                
                Update TblQtDtl A
                Inner Join TblPORequestDtl B On A.DocNo = B.QtDocNo
                    And A.DNo = B.QtDNo
                    And B.DocNo = '".$_POST['DocNo']."'
                Set A.UPrice = B.EstimatePrice, A.LastUpBy = '".$_SESSION['vdusercode']."', A.LastUpDt = CurrentDateTime()
                Where B.TakeOrderInd = 'Y';
            ";
        }
        
        $expSQL = explode(";", $sql);
        $countSQL = count($expSQL);
        $success = false;

        for($i = 0; $i < $countSQL - 1; $i++)
        {
            if (mysqli_query($conn, $expSQL[$i])) {
                $success = true;
            } else {
                echo "Error on take order process. ";
                $success = false;
                mysqli_rollback($conn);
                break;
            }
        }

        if($success)
        {
            mysqli_commit($conn);
            echo "<script type='text/javascript'>window.location='PurchaseOrder.php'</script>";
        }

    }

    // if (isset($_GET['p']) && isset($_GET['a']) && isset($_GET['m'])) {
        
    //     if ($_GET['a'] == '1') {
    //         $sql = "UPDATE TblPODtl A
    //         Inner Join TblPORequestDtl B On A.PORequestDocNo = B.DocNo And A.PORequestDNo = B.DNo
    //         Inner Join TblQtHdr C On B.QtDocNo = C.DocNo And C.VdCode = '" . $_SESSION['vdcode'] . "'
    //             And C.MaterialRequestDocNo Is Not Null
    //             And C.MaterialRequestDocNo = '".$_GET['m'] ."'
    //         Set A.CancelInd = 'Y', A.CancelReason = Concat('Cancelled by vendor ', '" . $_SESSION['vdname'] . "'),
    //         A.LastUpBy = '" . $_SESSION['vdusercode'] . "', A.LastUpDt = CurrentDateTime()
    //         Where A.DocNo = '". $_GET['p'] ."'
    //         And A.ProcessInd = 'O';";

    //         $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
    //         $row = mysqli_affected_rows($conn);
    //         echo $row;
    //         if ($row == '1') {
    //             echo "<script>alert('Sucess')</script>";
    //             echo "<script type='text/javascript'>window.location='PurchaseOrder.php'</script>";
                
    //         }else {
    //             echo "<script>alert('Failed')</script>";
    //             echo "<script type='text/javascript'>window.location='PurchaseOrder.php'</script>";
    //         }


    //     }
    //     if ($_GET['a'] == '0') {
            
    //         $sql = "UPDATE TblPODtl
    //         Set CancelInd = 'Y', CancelReason = Concat('Cancel take order by vendor ', '" . $_SESSION['vdname'] . "'),
    //         LastUpBy = '" . $_SESSION['vdusercode'] . "', LastUpDt = CurrentDateTime()
    //         Where DocNo = '". $_GET['p'] ."' ";

    //         $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
    //         $row = mysqli_affected_rows($conn);
    //         echo $row;
    //         if ($row == '1') {
    //             echo "<script>alert('Sucess')</script>";
    //             echo "<script type='text/javascript'>window.location='PurchaseOrder.php'</script>";
                
    //         }else {
    //             echo "<script>alert('Failed')</script>";
    //             echo "<script type='text/javascript'>window.location='PurchaseOrder.php'</script>";
    //         }

    //     }

    // }


?>