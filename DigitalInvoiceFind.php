<?php
include "auth.php";
include "header.php";
include "StdMtd.php";

?>

<a href="DigitalInvoice.php"><button type="button" class="btn btn-success btn-flat"> <strong>+ Insert</strong></button></a>
<br /><br />

<div class="box box-info">
  <div class="box-header with-border">
    <h4 class="box-title">Find Digital Invoice</h4>
  </div>

  <div class="box-body">
  <form class="form-horizontal" role="form" method="POST" name="LeaveRequestFind" enctype="multipart/form-data" onsubmit="return validate();">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-1 control-label">Date</label>

      <div class="col-sm-2">
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right DocDt1" id="DocDt1" name="DocDt1"
            <?php
              if(isset($_POST['submit']))
              {
                echo "value='" .$_POST['DocDt1']. "' ";
              }
              else
              {
                echo "value='" .date('d/m/Y', strtotime('-7 days')). "' ";
              }
            ?>
          />
        </div>
      </div>

      <div class="col-sm-2">
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right DocDt2" id="DocDt2" name="DocDt2"
            <?php
              if(isset($_POST['submit']))
              {
                echo "value='" .$_POST['DocDt2']. "' ";
              }
              else
              {
                echo "value='" .date('d/m/Y'). "' ";
              }
            ?>
          />
        </div>
      </div>
      <div class="col-sm-1">
        <input type="submit" name="submit" value="Refresh" class="btn btn-info btn-flat" />
      </div>
      <div class="col-sm-5">
        <span class="error-message" id="eDocDt"></span>
      </div>
    </div>
  </form>

  <hr />

    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No.</th>
        <th>Document#</th>
        <th>Date</th>
        <th>Doc. Status</th>
        <th>Received#</th>
        <th>#</th>
      </tr>
      </thead>
      <tbody>
      <?php

      if(isset($_POST['submit']))
      {
        $DocDt1 = dateFormat($_POST['DocDt1']);
        $DocDt2 = dateFormat($_POST['DocDt2']);

        $sql = "";

        $sql = $sql . "Select A.DocNo, Date_Format(A.DocDt, '%d/%b/%Y') DocDt, ";
        $sql = $sql . "Case A.CancelInd When 'N' Then 'Active' When 'Y' Then 'Cancelled' End As CancelInd, ";
        $sql = $sql . "A.RecvVdDocNo ";
        $sql = $sql . "From TblDigitalInvoiceHdr A ";
        $sql = $sql . "Inner Join TblRecvVdHdr B On A.RecvVdDocNo = B.DocNo And B.VdCode = '".$_SESSION['vdcode']."' ";
        $sql = $sql . "    And (A.DocDt Between '".$DocDt1."' And '".$DocDt2."') ";
        $sql = $sql . "Order By A.DocDt, A.DocNo; ";

        $resultOTR = mysqli_query($conn,$sql) or die(mysqli_error($conn));
        $nomor = 1;
        while($resOTR = mysqli_fetch_assoc($resultOTR))
        {
          echo "<tr>";
            echo "<td>". $nomor++ ."</td>";
            echo "<td>". $resOTR['DocNo'] ."</td>";
            echo "<td>". $resOTR['DocDt'] ."</td>";
            echo "<td>". $resOTR['CancelInd'] ."</td>";
            echo "<td>". $resOTR['RecvVdDocNo'] ."</td>";
            echo "<td><a href='DigitalInvoiceShow.php?DocNo=". $resOTR['DocNo'] ."'><i class='fa fa-search'></i></a></td>";
          echo "</tr>";
        }
      }

      ?>
      </tbody>
    </table>
  </div>
</div>

<script>
  $(function () {
    $("#example1").DataTable
    ({
      responsive: true,
      autoWidth: false,
      scrollX: true,
      fixedColumns: {
        leftColumns: 2
      }
    });

    //Date range picker
    $('#DocDt1').datepicker({
      autoclose: true
    });

    $('#DocDt2').datepicker({
      autoclose: true
    });

  });
</script>

<script type="text/javascript">
  function validate()
  {
    document.getElementById("eDocDt").innerHTML = "";

    var DocDt1 = document.getElementById("DocDt1").value;
    var DocDt2 = document.getElementById("DocDt2").value;

    if(DocDt1 == "" || DocDt2 == "")
    {
      document.getElementById("eDocDt").innerHTML = "Date should not be empty.";
      return false;
    }

    if(DocDt1 != "" && DocDt2 != "")
    {
      return compareDateWithSlash("DocDt1", "DocDt2", "eDocDt");
    }
  }
</script>

<?php

include "footer.php";
?>
