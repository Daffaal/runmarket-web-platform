<?php 
class RFQ {
    public $DocType;
    public $MaterialRequestDocNo;
    public $SeqNo;
    public $RFQ3DNo;
    public $SubmitInd;
    public $StageName;
    public $StartDt;
    public $StartTm;
    public $EndDt;
    public $EndTm;
    public $SectorName;
    public $ProcurementType;

}
    
    $rfqs = array();
    $sql = "Select Distinct T.* From (
        Select '1' DocType, A.MaterialRequestDocNo, A.SeqNo, B.DNo RFQ3DNo, B.SubmitInd, B.StageName, date_format( B.StartDt, '%d-%m-%Y') StartDt , B.StartTm , date_format( B.EndDt , '%d-%m-%Y') EndDt , B.EndTm,
        F.SectorName, H.OptDesc
        From TblRFQHdr A
        Inner Join TblRFQDtl3 B On A.MaterialRequestDocNo = B.MaterialRequestDocNo
            And A.SeqNo = B.SeqNo
            And CurrentDateTime() Between Concat(B.StartDt, B.StartTm) And Concat(B.EndDt, B.EndTm)
        Left Join TblRfqDtl D on B.MaterialRequestDocNo = D.MaterialRequestDocNo
        Left Join TblVendorSector E on E.VdCode = '".$_SESSION['vdcode']."'
            And D.SectorCode = E.SectorCode
            And D.SubSectorCode = E.SubSectorCode
        Left Join TblSector F on E.SectorCode = F.SectorCode
        Inner Join TblRfqHdr G on B.MaterialRequestDocNo = G.MaterialRequestDocNo
        Inner Join TblOption H on G.ProcurementType = H.OptCode And H.OptCat = 'ProcurementType'
        Union All
        Select '2' As DocType, A.MaterialRequestDocNo, A.SeqNo, B.DNo RFQ3DNo, 'N' as SubmitInd, B.StageName,date_format( B.StartDt, '%d-%m-%Y') StartDt , B.StartTm , date_format( B.EndDt , '%d-%m-%Y') EndDt , B.EndTm,
        F.SectorName, H.OptDesc
        From TblRFQHdr A
        Inner Join TblRFQDtl3 B On A.MaterialRequestDocNo = B.MaterialRequestDocNo
            And A.SeqNo = B.SeqNo
        Inner Join
        (
            Select MaterialRequestDocNo, Max(Concat(EndDt, EndTm)) MaxPeriod
            From TblRFQDtl3
            Where CurrentDateTime() Not Between Concat(StartDt, StartTm) And Concat(EndDt, EndTm)
            And Concat(EndDt, EndTm) <= CurrentDateTime()
            Group By MaterialRequestDocNo
        ) C On A.MaterialRequestDocNo = C.MaterialRequestDocNo and Concat(B.EndDt, B.EndTm) = C.MaxPeriod
        Left Join TblRfqDtl D on B.MaterialRequestDocNo = D.MaterialRequestDocNo
        Left Join TblVendorSector E on E.VdCode = '".$_SESSION['vdcode']."'
            And D.SectorCode = E.SectorCode
            And D.SubSectorCode = E.SubSectorCode
        Left Join TblSector F on E.SectorCode = F.SectorCode
        Inner Join TblRfqHdr G on B.MaterialRequestDocNo = G.MaterialRequestDocNo
        Inner Join TblOption H on G.ProcurementType = H.OptCode And H.OptCat = 'ProcurementType'
    ) T
    Order by T.MaterialRequestDocNo, T.DocType asc;"; // diisi query diatas

    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
    
    $i = 0;
    while ($res = mysqli_fetch_array($result)) {
        $rfqs[$i] = new RFQ();
        $rfqs[$i]->DocType = $res['DocType'];
        $rfqs[$i]->MaterialRequestDocNo = $res['MaterialRequestDocNo'];
        $rfqs[$i]->SeqNo = $res['SeqNo'];
        $rfqs[$i]->RFQ3DNo = $res['RFQ3DNo'];
        $rfqs[$i]->SubmitInd = $res['SubmitInd'];
        $rfqs[$i]->StageName = $res['StageName'];
        $rfqs[$i]->StartDt = $res['StartDt'];
        $rfqs[$i]->StartTm = $res['StartTm'];
        $rfqs[$i]->EndDt = $res['EndDt'];
        $rfqs[$i]->EndTm = $res['EndTm'];
        $rfqs[$i]->SectorName = $res['SectorName'];
        $rfqs[$i]->ProcurementType = $res['OptDesc'];
    
        $i += 1;
    }
    
    $MRDocNo = "";
    $DocType = "";
 
    $IsDataExist = false;
    
    for ($i = 0; $i < count($rfqs); $i++) {
        $IsDataExists = false;
        if (strlen($MRDocNo) == 0) {
            $MRDocNo = $rfqs[$i]->MaterialRequestDocNo;
        } else {
            if ($MRDocNo == $rfqs[$i]->MaterialRequestDocNo) $IsDataExists = true;
        }
        $DocType = $rfqs[$i]->DocType;
        //if ($MRDocNo == $rfqs[$i]->MaterialRequestDocNo) $IsDataExists = true;
        if ($DocType == "1") {
            $MRDocNo = $rfqs[$i]->MaterialRequestDocNo;
            $IsDataExists = true;
            continue;
        } else {
            if (!$IsDataExists) continue;
            else unset($rfqs[$i]);
        }
    }
    
?>