<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

if(!isset($_GET['DocNo']))
{
	echo "<script type='text/javascript'>alert('Invalid Request.')</script>";
	echo "<script type='text/javascript'>window.location='index.php'</script>";
}
else
{

$DocNo = $_GET['DocNo'];

$sql = "";

$sql = $sql . "Select A.DocNo, B.VdCode, DATE_FORMAT(A.DocDt, '%d/%m/%Y') DocDt, ";
$sql = $sql . "Case A.CancelInd When 'N' Then '' When 'Y' Then 'checked' End As CancelInd, ";
$sql = $sql . "A.CancelReason, A.RecvVdDocNo, Concat(A.RecvVdDocNo, ' - (', Date_Format(B.DocDt, '%d/%b/%Y'), ') ', C.WhsName) RecvVdDesc ";
$sql = $sql . "From TblDigitalInvoiceHdr A ";
$sql = $sql . "Inner Join TblRecvVdHdr B On A.RecvVdDocNo = B.DocNo And B.VdCode = '".$_SESSION['vdcode']."' ";
$sql = $sql . "Inner Join TblWarehouse C On B.WhsCode = C.WhsCode ";
$sql = $sql . "Where A.DocNo = '".$DocNo."' limit 1; ";

$result = mysqli_query($conn,$sql);
$row = mysqli_num_rows($result);
if($row <= 0)
{
	echo "<script type='text/javascript'>alert('No Data.')</script>";
	echo "<script type='text/javascript'>window.location='index.php'</script>";
}
else
{
	if(isset($_POST['submit']))
	{
		$sqlE = "";

		$sqlE = $sqlE . "Update TblDigitalInvoiceHdr Set ";
        $sqlE = $sqlE . "    CancelReason = '".$_POST['CancelReason']."', CancelInd='Y', LastUpBy='".$_SESSION['vdcode']."', LastUpDt=CurrentDateTime() ";
		$sqlE = $sqlE . "Where DocNo='".$DocNo."' And CancelInd='N' ";
		$sqlE = $sqlE . "And Not Exists ";
		$sqlE = $sqlE . "( ";
		$sqlE = $sqlE . "    Select 1 ";
		$sqlE = $sqlE . "    From TblPurchaseInvoiceDtl3 X1 ";
		$sqlE = $sqlE . "    Inner Join TblPurchaseInvoiceHdr X2 On X1.DocNo = X2.DocNo And X2.CancelInd = 'N' ";
		$sqlE = $sqlE . "        And X1.DigitalInvoiceDocNo = '".$DocNo."' ";
		$sqlE = $sqlE . "); ";

        /*if (mysqli_query($conn,$sqlE)) {
        	mysqli_commit($conn);
        	echo "<script type='text/javascript'>window.location='LeaveRequestShow.php?DocNo=".$DocNo."'</script>";
		} else {
			mysqli_rollback($conn);
		    echo "Error: " . $sqlE . "<br>" . mysqli_error($conn);
		}*/

		$expSQL = explode(";", $sqlE);
		$countSQL = count($expSQL);
		$success = false;

		for($i = 0; $i < $countSQL - 1; $i++)
		{
			if (mysqli_query($conn, $expSQL[$i])) {
				$success = true;
			} else {
				echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
				$success = false;
				mysqli_rollback($conn);
				break;
			}
		}

		if($success)
		{
			mysqli_commit($conn);
			echo "<script type='text/javascript'>window.location='DigitalInvoiceShow.php?DocNo=$DocNo'</script>";
		}
	}
	else
	{
		while($res = mysqli_fetch_assoc($result))
		{
			if($res['VdCode'] != $_SESSION['vdcode'])
			{
				echo "<script type='text/javascript'>alert('You have no access on this document.')</script>";
				echo "<script type='text/javascript'>window.location='index.php'</script>";
			}
			else
			{
				$DocDt = $res['DocDt'];
				$CancelInd = $res['CancelInd'];
				$CancelReason = $res['CancelReason'];
				$RecvVdDocNo = $res['RecvVdDocNo'];
				$RecvVdDesc = $res['RecvVdDesc'];
?>

<a href="DigitalInvoiceFind.php"><button type="button" class="btn btn-info btn-flat"> <i class="fa fa-search"></i><strong> Find</strong></button></a>
&nbsp;
<a href="DigitalInvoice.php"><button type="button" class="btn btn-success btn-flat"> <i class="fa fa-plus"></i><strong> Insert</strong></button></a>
&nbsp;
<button type="button" class="btn btn-primary btn-flat" id="BtnEdit" onclick="BtnEditOnClick();"> <i class="fa fa-edit"></i><strong> Edit</strong></button>
&nbsp;
<?php if(strlen($CancelReason) <= 0) { ?><a href="DigitalInvoiceUpload.php?DocNo=<?php echo $DocNo; ?>"><button type="button" class="btn btn-warning btn-flat"> <i class="fa fa-upload"></i><strong> Upload</strong></button></a> <?php } ?>
<br /><br />

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
            <h4 class="box-title">Digital Invoice</h4>
            </div>

            <form class="form-horizontal" name="createLeaveRequest" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">
				<div class="box-body">
		        	<div class="form-group">
		            	<label for="inputEmail3" class="col-sm-2 control-label">Document#</label>

		            	<div class="col-sm-5">
		            		<input type="text" class="form-control" name="DocNo" id="DocNo" readonly="readonly" value="<?php echo $DocNo; ?>" />
		            	</div>
		            </div>
					<div class="form-group">
		            	<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Date</label>

		            	<div class="col-sm-5">
			            	<div class="input-group date">
			                	<div class="input-group-addon">
			                		<i class="fa fa-calendar"></i>
			                	</div>
			                	<input type="text" class="form-control datepicker" id="DocDt" name="DocDt" disabled="disabled" value="<?php echo $DocDt; ?>">
			                </div>
		            	</div>
		            	<div class="col-sm-5">
			            	<span class="error-message" id="eDocDt"></span>
			            </div>
		            </div>
		            <div class="form-group">
		            	<label for="inputPassword3" class="col-sm-2 control-label">Cancel Reason</label>

		            	<div class="col-sm-5">
		            		<input type="text" class="form-control" name="CancelReason" id="CancelReason" readonly="readonly" value="<?php echo $CancelReason; ?>" />
		            	</div>
		            	<div class="col-sm-3">
			            	<div class="checkbox">
			                  	<label>
			                    	<input type="checkbox" name="CancelInd" id="CancelInd" disabled="disabled" <?php echo $CancelInd; ?> /> Cancel
			                  	</label>
			                </div>
			            </div>
			            <div class="col-sm-3">
			            	<span class="error-message" id="eCancelInd"></span>
			            </div>
		            </div>
		            <div class="form-group">
		            	<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Received#</label>

		            	<div class="col-sm-5">
		            		<select class="form-control selectRecvVdDocNo" name="LueRecvVdDocNo" id="LueRecvVdDocNo" style="width: 100%;" disabled="disabled">
			            		<option value=""></option>
			            	<?php
		                			echo "<option value='".$RecvVdDocNo."' selected>".$RecvVdDesc."</option>";
			                ?>
			                </select>
		            	</div>
		            </div>
		        </div>
		</div> <!-- box primary -->

		<div class="box box-primary">
				<div class="box-body table-responsive">
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>Type</th>
								<th>Document#</th>
								<th>Tax Invoice Date</th>
								<th>Amount</th>
								<th>Tax</th>
								<th>Indicator</th>
								<th>Remark</th>
								<th>File</th>
								<th>#</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$sql = "";

								$sql = $sql . "Select A.DocNo, A.DNo, A.DocType, B.OptDesc As DocTypeDesc, A.DocInd, C.OptDesc As DocIndDesc, ";
								$sql = $sql . "A.DocNumber, IfNull(Date_Format(A.TaxInvDt, '%d/%b/%Y'), '') TaxInvDt, A.Amt, A.TaxAmt, A.Remark, ";
								$sql = $sql . "Case When Length(A.`File`) > 0 Then 'checked' Else '' End As FileInd, A.File ";
								$sql = $sql . "From TblDigitalInvoiceDtl A ";
								$sql = $sql . "Left Join TblOption B On A.DocType = B.OptCode And B.OptCat = 'PurchaseInvoiceDocType' ";
								$sql = $sql . "Left Join TblOption C On A.DocInd = C.OptCode And C.OptCat = 'PurchaseInvoiceDocInd' ";
								$sql = $sql . "Where A.DocNo = '".$DocNo."'; ";

								$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
								while($res = mysqli_fetch_assoc($result))
								{
									echo "<tr>";
									echo "<td>" . $res['DocTypeDesc'] . "</td>";
									echo "<td>" . $res['DocNumber'] . "</td>";
									echo "<td>" . $res['TaxInvDt'] . "</td>";
									echo "<td align='right'>" . number_format($res['Amt'], 2, ".", ",") . "</td>";
									echo "<td align='right'>" . number_format($res['TaxAmt'], 2, ".", ",") . "</td>";
									echo "<td>" . $res['DocIndDesc'] . "</td>";
									echo "<td>" . $res['Remark'] . "</td>";
									echo "<td><input type='checkbox' " . $res['FileInd'] . " disabled='disabled' /></td>";
									if(strlen($res['FileInd']) > 0)
										echo "<td><a href='".$res['File']."' target='_blank'> <i class='fa fa-search'></i></a></td>";
									echo "</tr>";
								}
							?>
						</tbody>
					</table>
				</div>
		        <div class="box-footer">
		        	<input type="reset" class="btn btn-warning" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="visibility: hidden;" />
		            <button type="submit" name="submit" class="btn btn-success pull-right" id="BtnSave" style="visibility: hidden;">Save</button>
		        </div>
		        <!-- /.box-footer -->
			</form>

        </div> <!-- box primary -->
	</div>
</div>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".selectRecvVdDocNo").select2();

    //Date picker
    $('.datepicker').datepicker({
      autoclose: true
    });
  });
</script>

<script type="text/javascript">
	function BtnEditOnClick()
	{
		if(document.getElementById("CancelInd").checked == false)
		{
			document.getElementById("BtnCancel").style.visibility = "visible";
			document.getElementById("BtnSave").style.visibility = "visible";
			document.getElementById("CancelReason").readOnly = false;
		}
	}

	function validate()
	{
		document.getElementById("eCancelInd").innerHTML = "";
		

		if(document.getElementById("CancelReason").value == "")
		{
			document.getElementById("eCancelInd").innerHTML = "You need to cancel this document.";
			return false;
		}

		if(IsDigitalInvoiceProcessedToPI() == true) 
		{
			alert("This document is already processed to Purchase Invoice.");
			return false;
		}

		if(confirm("Do you want to save this document ?") == false) return false;
	}

	function IsDigitalInvoiceProcessedToPI()
	{
		var DigitalInvoiceDocNo = document.getElementById("DocNo").value;
		var IsDigitalInvoiceProcessedToPI = false;

		$.ajax({
			url: "IsDigitalInvoiceProcessedToPI.php",
			data: "DigitalInvoiceDocNo="+DigitalInvoiceDocNo,
			cache: false,
			success: function(msg4){
				if(msg4.length > 0)
				{
					IsDigitalInvoiceProcessedToPI = true;
				}
			}
		});

		return IsDigitalInvoiceProcessedToPI;
	}
</script>

<?php
			}
		}
	}
}

} // end else GET  DocNo
include 'footer.php';
?>
