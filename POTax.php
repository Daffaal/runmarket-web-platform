<?php 
    
include "auth.php";
include "conn.php";
include "StdMtd.php";

    if(isset($_POST['DocNo'])){

        $DocNo = $_POST['DocNo'];
        $TaxAmt = "0";

        $sql = ("
        SELECT C.TaxName, D.TaxName TaxName2, E.TaxName TaxName3, 
        (IfNull(B.TotalAmt * 0.01 * C.TaxRate, 0.00) + IfNull(B.TotalAmt * 0.01 * D.TaxRate, 0.00) + IfNull(B.TotalAmt * 0.01 * E.TaxRate, 0.00)) TotalTax
        From TblPOHdr A
        Inner Join 
        (
            Select T1.DocNo, Sum(T1.Qty * T3.UPrice) TotalAmt
            From TblPODtl T1
            Inner Join TblPORequestDtl T2 On T1.PORequestDocNo = T2.DocNo And T1.PORequestDNo = T2.DNo
                And T1.DocNo = '".$DocNo."'
            Inner Join TblQtDtl T3 On T2.QtDocNo = T3.DocNo And T2.QtDNo = T3.DNo
            Group By T1.DocNo
        ) B On A.DocNo = B.DocNo
        Left Join TblTax C On A.TaxCode1 = C.TaxCode
        Left Join TblTax D On A.TaxCode2 = D.TaxCode
        Left Join TblTax E On A.TaxCode3 = E.TaxCode
        Where A.DocNo = '".$DocNo."'");

        //var_dump($sql);die;
        $result1 = mysqli_query($conn, $sql) or die(mysqli_error($conn));
        while($row = mysqli_fetch_assoc($result1)) {
            $TaxAmt = $row['TotalTax'];
        }

        echo "<style>table-layout: fixed; word-wrap:break-word;</style>";
        echo    "<div class='row'>
                    <div class='col-md-8 col-xs-8'>
                        <label class='control-label ' style='position:relative; top:7px; text-align:end'>Total Tax</label>
                    </div>
                    <div class='col-md-4 col-xs-4'>
                        <p class='form-control angka'>".number_format($TaxAmt, 2, '.', ',')."</p>
                    </div>
                </div>";
        echo    "<table class='table Tax' id='tblTaxDtl'>";
        echo    "    <thead>";
        echo    "        <tr>";
        echo    "            <th>No.</th>";
        echo    "            <th>Tax</th>";
        echo    "        </tr>";
        echo    "    </thead>";

        $i = 0;
        $result2 = mysqli_query($conn, $sql) or die(mysqli_error($conn));

        while ($res = mysqli_fetch_assoc($result2)) {
            echo    "    <tbody>";
            echo    "    <tr>";
            echo   "        <td>1</td>";
            echo   "        <td>".$res['TaxName']."</td>";
            echo    "    </tr>";
            echo    "    <tr>";
            echo   "        <td>2</td>";
            echo   "        <td>".$res['TaxName2']."</td>";
            echo    "    </tr>";
            echo    "    <tr>";
            echo   "        <td>3</td>";
            echo   "        <td>".$res['TaxName3']."</td>";
            echo    "    </tr>";  
            echo    "    </tbody>";
            $i++;
        }

        echo    "</table>";
        echo    "</table>";
        echo "<script>$('#tblTaxDtl').DataTable({responsive: true,autoWidth: true ,
            scrollX: true, 
            scrollY: true
            }) 
          </script>";
    }

?>