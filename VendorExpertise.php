<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

function file_exist_on_ftp_server($fileName, $ftpConn){
    $fileSize = ftp_size($ftpConn,FTP_DESTINATION.$fileName);
    if ($fileSize < 0) {
        return false;
    }
    
    return true;
}

if(isset($_POST['submit']))
{
    $host = FTP_HOST;
    $user = FTP_USER;
    $pass = FTP_PASS;

    $DNo = GetValue("Select IfNull(Right(Concat('000', Max(DNo) + 1), 3), '') DNo From TblVendorProfile Where VdCode = '".$_SESSION['vdcode']."'; ");
    if(strlen($DNo) <= 0) $DNo = "001";
    $sql = "";

    if (isset($_FILES['VdFile'])) {
        $imgFile = $_FILES['VdFile']['name'];
        $tmp_dir = $_FILES['VdFile']['tmp_name'];
        $imgSize = $_FILES['VdFile']['size'];
        $imgExt = array();
        $VdFilesLoc = array();
        $DNo2 = array();
        $i = 1;
        $upload_dir = "dist/pdf/";
        $valid_extension = array('pdf', 'png', 'jpg', 'jpeg');
        $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;

        for ($i=0; $i < count($imgFile); $i++) { 
            $ext = strtolower(pathinfo($imgFile[$i],PATHINFO_EXTENSION));
            array_push($imgExt, $ext);
        }

        $sqlDNo = "
            Select (Max(DNo2) + 1) DNo
            From TblVendorProfileUpload
            Where VdCode = '".$_SESSION['vdcode']."'
            and FileLocation regexp '".$_POST['ProfileName']."'
            ;
        ";
        // generate dno
        //$rows = GetValue("Select IfNull(Right(Concat('000', Max(DNo2) + 1), 3), '') DNo From TblVendorProjectUpload Where VdCode = '".$_SESSION['vdcode']."' and FileLocation regexp '". $_POST['Project'] ."' ; ");
        $rows = GetValue($sqlDNo);
        $generate = null;

        if (strlen($rows) == 0) $rows = "0";
        
        if ($rows == "0") {
            for ($i = 0; $i < count($imgFile); $i++) {
                $generate = str_pad(($rows + 1) + $i, 3, "0", STR_PAD_LEFT);
                array_push($DNo2, $generate);
            }
        }else {
            for ($i = 0; $i < count($imgFile); $i++) {
                $generate = str_pad($rows + $i, 3, "0", STR_PAD_LEFT);
                array_push($DNo2, $generate);
            }
        }
        // end

        $j = 0;
        for ($i=0; $i < count($imgExt); $i++) { 
            $j++;
            $filesLoc = $_SESSION['vdcode']."_". $_POST['ProfileName'] ."-". $DNo2[$i]. "_". date("Ymdhms") .".".$imgExt[$i];
            array_push($VdFilesLoc, $filesLoc);
        }

        for($i = 0; $i < count($imgFile); $i++){

            if(in_array($imgExt[$i], $valid_extension)) // check kalau extensi nya valid
            {

                if($imgSize[$i] == 0)
                {

                    echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                    echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
                }
                else
                {

                    if($imgSize[$i] < $mMaxFile) // if image size < 1025 KB
                    {
                        // echo "<script type='text/javascript'>alert('".$tmp_dir[$i]." || ".$upload_dir.$VdFilesLoc[$i]."');</script>";
                        move_uploaded_file($tmp_dir[$i], $upload_dir.$VdFilesLoc[$i]);                    

                        $sql = $sql . "Insert Into TblVendorProfileUpload(VdCode, Dno, DNo2 ,Status, FileLocation, EndDt, CreateBy, CreateDt) ";
                        $sql = $sql . "Values('".$_SESSION['vdcode']."','".$DNo."','".$DNo2[$i]."','A', '".$VdFilesLoc[$i]."', ";                               
                        
                        if(strlen($_POST['ValidTo'][$i]) > 0)
                            $sql = $sql . "'".date("Ymd", strtotime($_POST['ValidTo'][$i]))."', ";
                        else
                            $sql = $sql . "NULL, ";
                        
                        $sql = $sql . "'".$_SESSION['vdusercode']."', CurrentDateTime());";

                    }
                    else
                    {

                        echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                        echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
                    }
                    }
            }
            else
            {                  
                echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
                echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
            }
        
        }
    }
    

    echo "<br>";
    // foreach (glob("dist/pdf/*.*") as $filename){
    //     echo "<br>". basename($filename). "<br>";
    // }

    if (FTP_STATUS === "enable") {
        try {
            $ftpConn = ftp_connect($host);
            $login = ftp_login($ftpConn,$user,$pass);
            $ftpPasv = ftp_pasv($ftpConn, true);

            if ((!$ftpConn) || (!$login)) {
                echo "FTP connection has failed! Attempted to connect to ". $host.".";
            }

            if (!$ftpPasv) {
                echo "Cannot switch to passive mode";
            }

            if($login) {
                
                $ftpSuccess = false;
                for ($i=0; $i < count($VdFilesLoc); $i++) { 
                    if (!ftp_put($ftpConn, FTP_DESTINATION.$VdFilesLoc[$i], $upload_dir.$VdFilesLoc[$i], FTP_BINARY)) {
                        echo "<script>alert('Error uploading ". $VdFilesLoc[$i] .".');</script>";
                    }else {
                        $ftpSuccess = true;
                    } 
                }
                if ($ftpSuccess) {
                    echo "<script>alert('Success uploading File.');</script>";
                }
                ftp_close($GLOBALS['ftpConn']);

            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

    $ChkActInd = "N"; if(isset($_POST['ActInd'])) $ChkActInd = "Y";
    $ChkOriInd = "N"; if(isset($_POST['OriInd'])) $ChkOriInd = "Y";
    $BirthDt = ""; 
    if(isset($_POST['BirthDt'])) 
        if(strlen($_POST['BirthDt']) == 10) 
            $BirthDt = date('Ymd', strtotime($_POST['BirthDt'])); 

            $sql = $sql . "Insert Into TblVendorProfile(VdCode, DNo, ProfileCode, ProfileName, ";
            $sql = $sql . "ActInd, Position, Company, CityCode, BirthDt, ID, NPWP, Education, Certificate,";
            $sql = $sql . "Language, Experience, OriInd, CntCode, Gender, Email, Address, Project, CreateBy, CreateDt) ";
            $sql = $sql . "Values(NullIf('" . $_SESSION['vdcode'] . "', ''), NullIf('" . $DNo . "', ''), NullIf('" . $DNo . "', ''), NullIf('" . $_POST['ProfileName'] . "', ''), ";
            $sql = $sql . "       NullIf('" . $ChkActInd . "', ''), NullIf('" . $_POST['Position'] . "', ''), NullIf('" . $_POST['Company'] . "', ''), NullIf('" . $_POST['LueBirthPlace'] . "', ''), ";
            $sql = $sql . "       NullIf('" . $BirthDt . "', ''), NullIf('" . $_POST['ID'] . "', ''), NullIf('" . $_POST['NPWP'] . "', ''), NullIf('" . $_POST['Education'] . "', ''), ";
            $sql = $sql . "       NullIf('" . $_POST['Certificate'] . "', ''), NullIf('" . $_POST['Language'] . "', ''), NullIf('" . $_POST['Experience'] . "', ''), NullIf('" . $ChkOriInd . "', ''), NullIf('" . $_POST['LueCntCode'] . "', ''), ";
            $sql = $sql . "       NullIf('" . $_POST['LueGender'] . "', ''), NullIf('" . $_POST['Email'] . "', ''), NullIf('" . $_POST['Address'] . "', ''), NullIf('". $_POST['Project'] ."', ''), NullIf('" . $_SESSION['vdusercode'] . "', ''), NullIf(CurrentDateTime(), '')); ";

    //echo $sql;

    $expSQL = explode(";", $sql);
    $countSQL = count($expSQL);
	$success = false;

    for($i = 0; $i < $countSQL - 1; $i++)
	{
		if (mysqli_query($conn, $expSQL[$i])) {
			$success = true;
		} else {
			echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
		}
	}

	if($success)
	{
		mysqli_commit($conn);
		echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
	}
}
else if(isset($_POST['DNoDel']))
{
    $host = FTP_HOST;
    $user = FTP_USER;
    $pass = FTP_PASS;

    $ProfileDNo = $_POST['DNoDel'];
    $sql1 = "select FileLocation from TblVendorProfileUpload Where DNo='".$ProfileDNo."' ;";
    $result1 = mysqli_query($conn, $sql1);
    
    $sql2 = "";
    
    
    $sql2 = $sql2 . "Delete From TblVendorProfile ";
    $sql2 = $sql2 . "Where VdCode = '".$_SESSION['vdcode']."' ";
    $sql2 = $sql2 . "And DNo = '".$ProfileDNo."'; ";

    $sql2 = $sql2 . "Delete From TblVendorProfileUpload ";
    $sql2 = $sql2 . "Where DNo='".$ProfileDNo."'; ";

    $expSQL = explode(";", $sql2);
	$countSQL = count($expSQL);
	$success = false;

	for($i = 0; $i < $countSQL - 1; $i++)
	{
		if (mysqli_query($conn, $expSQL[$i])) {
			$success = true;
		} else {
			echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
		}
	}

    if ($success) 
    {
        
        mysqli_commit($conn);
        if (FTP_STATUS === "enable") {      
            while ($res = mysqli_fetch_assoc($result1)) {
                try {
                    $ftpConn = ftp_connect($host);
                    $login = ftp_login($ftpConn,$user,$pass);
                    $ftpPasv = ftp_pasv($ftpConn, true);
        
                    if ((!$ftpConn) || (!$login)) {
                        echo "FTP connection has failed! Attempted to connect to ". $host.".";
                    }
        
                    if (!$ftpPasv) {
                        echo "Cannot switch to passive mode";
                    }
        
                    if($login) {
        
                        if(file_exist_on_ftp_server(basename($res['FileLocation']), $ftpConn)){
                            ftp_delete($ftpConn,FTP_DESTINATION.basename($res['FileLocation']));
                            // if (!$deleteFile) {
                            //     echo "Could not delete ". basename($res['FileLocation']);
                            // }
                            // return true;
                        }
                        ftp_close($GLOBALS['ftpConn']);

                    }
                } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
            
        }
        echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
    } 

} else if(isset($_POST['file'])){
    $host = FTP_HOST;
    $user = FTP_USER;
    $pass = FTP_PASS;
    
    $sql2 = "";
    $FileLoc = $_POST['file'];
    
    $sql2 = $sql2 . "Delete From TblVendorProfileUpload ";
    $sql2 = $sql2 . "Where FileLocation = '".$FileLoc."' ";

    if (mysqli_query($conn,$sql2)) 
    {
        mysqli_commit($conn);
        if (FTP_STATUS === "enable") {
            try {
                $ftpConn = ftp_connect($host);
                $login = ftp_login($ftpConn,$user,$pass);
                $ftpPasv = ftp_pasv($ftpConn, true);
    
                if ((!$ftpConn) || (!$login)) {
                    echo "FTP connection has failed! Attempted to connect to ". $host.".";
                }
    
                if (!$ftpPasv) {
                    echo "Cannot switch to passive mode";
                }
    
                if($login) {
    
                    if(file_exist_on_ftp_server(basename($FileLoc), $ftpConn)){
                        $deleteFile = ftp_delete($ftpConn,FTP_DESTINATION.basename($FileLoc));
                        if (!$deleteFile) {
                            echo "Could not delete $FileLoc";
                        }
                        ftp_close($GLOBALS['ftpConn']);
                        return true;
                    }
                }
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        } else {
            return false;
        }
        echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
    } 
    else 
    {
        echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
        mysqli_rollback($conn);
    }
} else if(isset($_POST['edit'])){

    $host = FTP_HOST;
    $user = FTP_USER;
    $pass = FTP_PASS; 

    $sql = "";
    if(isset($_FILES['VdFileDtl'])){
        $imgFile = $_FILES['VdFileDtl']['name'];
        $tmp_dir = $_FILES['VdFileDtl']['tmp_name'];
        $imgSize = $_FILES['VdFileDtl']['size'];
        $imgExt = array();
        $VdFilesLoc = array();
        $DNo2 = array();
        $i = 1;
        $upload_dir = "dist/pdf/";
        $valid_extension = array('pdf', 'png', 'jpg', 'jpeg');
        print_r($imgSize);
        $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;

        $sqlDNo = "
            Select (Max(DNo2) + 1) DNo
            From TblVendorProfileUpload
            Where VdCode = '".$_SESSION['vdcode']."'
            and FileLocation regexp '".$_POST['name']."'
            ;
        ";
        // generate dno
        //$rows = GetValue("Select IfNull(Right(Concat('000', Max(DNo2) + 1), 3), '') DNo From TblVendorProjectUpload Where VdCode = '".$_SESSION['vdcode']."' and FileLocation regexp '". $_POST['Project'] ."' ; ");
        $rows = GetValue($sqlDNo);
        $generate = null;

        if (strlen($rows) == 0) $rows = "0";
        echo $rows;
        if ($rows == "0") {
            for ($i = 0; $i < count($imgFile); $i++) {
                $generate = str_pad(($rows + 1) + $i, 3, "0", STR_PAD_LEFT);
                array_push($DNo2, $generate);
            }
        }else {
            for ($i = 0; $i < count($imgFile); $i++) {
                $generate = str_pad($rows + $i, 3, "0", STR_PAD_LEFT);
                array_push($DNo2, $generate);
            }
        }
        // end

        for ($i=0; $i < count($imgFile); $i++) { 
        $ext = strtolower(pathinfo($imgFile[$i],PATHINFO_EXTENSION));
        array_push($imgExt, $ext);
        }

        $j = 0;
        for ($i=0; $i < count($imgExt); $i++) { 
            $j++;
            $filesLoc = $_SESSION['vdcode']."_". $_POST['name'] ."-". $DNo2[$i]. "_". date("Ymdhms") .".".$imgExt[$i];
            array_push($VdFilesLoc, $filesLoc);
        }

        for($i = 0; $i < count($imgFile); $i++){

            if(in_array($imgExt[$i], $valid_extension)) // check kalau extensi nya valid
            {

                if($imgSize[$i] == 0)
                {

                    echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                    echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
                }
                else
                {

                    if($imgSize[$i] < $mMaxFile) // if image size < 1025 KB
                    {
                        // echo "<script type='text/javascript'>alert('".$tmp_dir[$i]." || ".$upload_dir.$VdFilesLoc[$i]."');</script>";
                        move_uploaded_file($tmp_dir[$i], $upload_dir.$VdFilesLoc[$i]);                            

                        $sql = $sql . "Insert Into TblVendorProfileUpload(VdCode, DNo, DNo2 ,Status, FileLocation, EndDt, CreateBy, CreateDt) ";
                        $sql = $sql . "Values('".$_SESSION['vdcode']."', '".$_POST['DNo']."' ,'".$DNo2[$i]."','A', '".$VdFilesLoc[$i]."', ";                        
                        
                        if(strlen($_POST['ValidToDtl'][$i]) > 0)
                            $sql = $sql . "'".date("Ymd", strtotime($_POST['ValidToDtl'][$i]))."', ";
                        else
                            $sql = $sql . "NULL, ";
                        
                        $sql = $sql . "'".$_SESSION['vdusercode']."', CurrentDateTime());";

                    }
                    else
                    {

                        echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                        echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
                    }
                    }
            }
            else
            {                  
                echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
                echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
            }
            
        }


        if (FTP_STATUS === "enable") {
            try {
                $ftpConn = ftp_connect($host);
                $login = ftp_login($ftpConn,$user,$pass);
                $ftpPasv = ftp_pasv($ftpConn, true);

                if ((!$ftpConn) || (!$login)) {
                    echo "FTP connection has failed! Attempted to connect to ". $host.".";
                }

                if (!$ftpPasv) {
                    echo "Cannot switch to passive mode";
                }

                if($login) {
                    $ftpSuccess = false;
                    for ($i=0; $i < count($VdFilesLoc); $i++) { 
                        if (!ftp_put($ftpConn, FTP_DESTINATION.$VdFilesLoc[$i], $upload_dir.$VdFilesLoc[$i], FTP_BINARY)) {
                            echo "<script>alert('Error uploading ". $VdFilesLoc[$i] .".');</script>";
                        }else {
                            $ftpSuccess = true;
                        } 
                    }
                    if ($ftpSuccess) {
                        echo "<script>alert('Success uploading File.');</script>";

                    }
                    ftp_close($ftpConn);
                    
                }
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
    }


    $ChkActInd = "N"; if(isset($_POST['active'])) $ChkActInd = "Y";
    $ChkOriInd = "N"; if(isset($_POST['ori'])) $ChkOriInd = "Y";
    $BirthDt = ""; 
    if(isset($_POST['birthDt'])){
        if(strlen($_POST['birthDt']) == 10){
            $BirthDt = date('Ymd', strtotime($_POST['birthDt']));
        }else {
            $BirthDt = date('Ymd', strtotime($_POST['birthDt']));
        }
    }
        
    $sql = $sql . "update tblvendorprofile a 
        left join tblcity b on b.CityCode = a.CityCode 
        left join tblcountry c on c.CntCode = a.CntCode 
        left join tbloption d on d.OptCode = a.Gender and d.OptCat = 'Gender'
        set a.ProfileName = NullIf('". $_POST['name'] ."', '') , a.ActInd = NullIf('". $ChkActInd ."', '') , a.Position = NullIf('". $_POST['position'] ."', '') , a.ID = NullIf('". $_POST['id'] ."', ''), 
            a.Company = NullIf('". $_POST['company'] ."', ''), a.BirthDt = NullIf('". $BirthDt ."', ''), a.CityCode = NullIf('". $_POST['LueBirthPlace'] ."', ''), a.Address = NullIf('". $_POST['address'] ."',''),
            a.NPWP = NullIf('". $_POST['npwp'] ."', ''), a.Education = NullIf('". $_POST['education'] ."', ''), a.Certificate = NullIf('". $_POST['certificate'] ."',''), a.Language = NullIf('". $_POST['lang'] ."', ''), 
            a.Experience = NullIf('". $_POST['exp'] ."',''), a.OriInd = NullIf('". $ChkOriInd ."',''), a.CntCode = NullIf('". $_POST['LueCntCode'] ."',''), a.Gender = NullIf('". $_POST['gender'] ."',''), a.Email = NullIf('". $_POST['email'] ."',''),
            a.Project = NullIf('". $_POST['project'] ."', ''), a.LastUpBy = '". $_SESSION['vdcode'] ."', a.LastUpDt = CurrentDateTime()
        where a.VdCode = '". $_SESSION['vdcode'] ."' and a.DNo = '". $_POST['DNo'] ."';";


    $expSQL = explode(";", $sql);
    $countSQL = count($expSQL);
    $success = false;

    for($i = 0; $i < $countSQL - 1; $i++)
	{
		if (mysqli_query($conn, $expSQL[$i])) {
			$success = true;
		} else {
			echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
		}
	}

	if($success)
	{
		mysqli_commit($conn);
		echo "<script type='text/javascript'>window.location='VendorExpertise.php'</script>";
	}
}
else
{

?>

<style>
    .modal-body {
        height: 80vh;
        overflow-y: auto;
        overflow-x: auto;
    }
</style>
<form class="form-horizontal" name="insertVendor" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
            <h4 class="box-title">Company Expertise</h4>
            </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Profile Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="ProfileName" id="ProfileName" required />
                        </div>
                        <div class="col-sm-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="ActInd" id="ActInd" checked /> Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Position</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Position" id="Position" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Company</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Company" id="Company" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Birth Place</label>

                        <div class="col-sm-5">
                            <select class="form-control selectBirthPlace" name="LueBirthPlace" id="city" style="width: 100%;">
                                <option value=""></option>
                            <?php
                                $q = "Select CityCode As Col1, Concat(A.CityName, ' ( ', IfNull(B.ProvName, ''), ' ', IfNull(C.CntName, ''), ' )')  As Col2 ";
                                $q = $q . "From TblCity A ";
                                $q = $q . "Left Join TblProvince B On A.ProvCode=B.ProvCode ";
                                $q = $q . "Left Join TblCountry C On B.CntCode=C.CntCode ";
                                $q = $q . "Order By C.CntName, A.CityName ";

                                $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                                while($q2 = mysqli_fetch_assoc($q1))
                                {
                                    echo "<option value='".$q2['Col1']."' ";
                                    echo ">".$q2['Col2']."</option>";
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Birth Date</label>

                        <div class="col-sm-5">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control BirthDt" id="BirthDt" name="BirthDt" data-field="date" />
                            </div>
                        </div>

                        <div id="DteBirthDt"></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">ID</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="ID" id="ID" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Address" id="Address" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">NPWP</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="NPWP" id="NPWP" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Education</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Education" id="Education" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Certificate</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Certificate" id="Certificate" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Language Proficiency</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Language" id="Language" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Working Experience</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Experience" id="Experience" />
                        </div>
                        <div class="col-sm-3">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="OriInd" id="OriInd" /> Original
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Country</label>

                        <div class="col-sm-5">
                            <select class="form-control selectCntCode" name="LueCntCode" id="cnt" style="width: 100%;">
                                <option value=""></option>
                            <?php
                                $q = "Select CntCode As Col1, CntName  As Col2 ";
                                $q = $q . "From TblCountry ";
                                $q = $q . "Order By CntName; ";

                                $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                                while($q2 = mysqli_fetch_assoc($q1))
                                {
                                    echo "<option value='".$q2['Col1']."' ";
                                    echo ">".$q2['Col2']."</option>";
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Gender</label>

                        <div class="col-sm-5">
                            <select class="form-control selectGender" name="LueGender" id="gender" style="width: 100%;">
                                <option value=""></option>
                            <?php
                                $q = "Select OptCode As Col1, OptDesc As Col2 ";
                                $q = $q . "From TblOption ";
                                $q = $q . "Where OptCat = 'Gender' ";
                                $q = $q . "Order By OptDesc; ";

                                $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                                while($q2 = mysqli_fetch_assoc($q1))
                                {
                                    echo "<option value='".$q2['Col1']."' ";
                                    echo ">".$q2['Col2']."</option>";
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-5">
                            <input type="email" class="form-control" name="Email" id="Email" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Project Experience</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Project" id="Project" />
                        </div>
                    </div>
                    <table class="table File" id="tblBankAccount">
                        <thead>
                            <tr>
                                <th>File</th>
                                <th>Valid To</th>
                                <th>#</th>								
                            </tr>
                        </thead>
                        <tbody>
                            <tr>									
                            <td class='col-sm-3'>
                            <input type='file' name='VdFile[]' id="file" accept="application/pdf, image/png, image/jpg, image/jpeg" required/>
                            </td>
                            <td class='col-sm-3'>
                                <div class="form-group">                              
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control validto" id="validto" name="ValidTo[]" data-field="date" required/>
                                        </div>
                                    </div>

                                    <div id="DteValidTo"></div>
                                </div>
                            </td>									
                            <td>
                                <input type='button' id='BAbtnDel' class='BAbtnDel btn btn-md btn-danger' value='Delete'>
                                <!-- <br>
                                <input type='button' id='btnUpload' name="upload" class='btnUpload btn btn-md btn-success' value='Upload'> -->
                            </td>
                            </tr>
                            <input type='hidden' name='countVdBankAccount' id='countVdBankAccount'/>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5" style="text-align: left;">
                                    <input type="button" class="btn btn-lg btn-block " id="addrow" value="Add Row"  />
                                </td>
                            </tr>
                            <tr>
                            </tr>
                        </tfoot>
                    </table> 
                <hr />
                <input type="reset" class="btn" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="background-color: #58585A; color:white"/>
                <button type="submit" name="submit" class="btn pull-right" id="BtnSave" style="background-color: #D61E2F; color:white">Save</button>    
                </div>
                <!-- /.box-body -->
        </div>
    </div>
</div>
</form>

<br />
<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header with-border">
            <h4 class="box-title">Manage your data</h4>
            </div>
                <div class="box-body">
                <form action="" name="manageUpload" role="form" enctype="multipart/form-data" method="POST">
                    <table id="VdExpertise" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Profile Name</th>
                                <th>Active</th>
                                <th>Position</th>
                                <th>Company</th>                                
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                $sql = "";

                                $sql = $sql . "Select A.DNo, A.ProfileCode, A.ProfileName, A.ActInd, A.Position, A.Company, A.Address, ";
                                $sql = $sql . "A.CityCode, B.Cityname, Date_format(A.BirthDt, '%d-%m-%Y') BirthDt, A.ID, A.NPWP, A.Education, A.Certificate, ";
                                $sql = $sql . "A.Language, A.Experience, A.OriInd, A.CntCode, C.CntName, A.Gender, D.OptDesc As Gender2, A.Email  ";
                                $sql = $sql . "From TblVendorProfile A ";
                                $sql = $sql . "Left Join TblCity B On A.CityCode = B.CityCode ";
                                $sql = $sql . "Left Join TblCountry C On A.CntCode = C.CntCode ";
                                $sql = $sql . "Left Join TblOption D On A.Gender = D.OptCode And D.Optcat = 'Gender'";
                                $sql = $sql . "Where A.VdCode = '".$_SESSION['vdcode']."' ";
                                $sql = $sql . "Order By A.DNo; ";

                                $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                                while($res = mysqli_fetch_assoc($result))
                                {
                                    echo "<tr>";
                                    echo "<td>" . $res['ProfileName'] . "</td>";
                                    echo "<td align='center'><input type='checkbox' disabled='disabled' ";
                                    if($res['ActInd'] == "Y") echo " checked ";
                                    echo " ></td>";
                                    echo "<td>" . $res['Position'] . "</td>";
                                    echo "<td>" . $res['Company'] . "</td>";
                                    echo "<input type='hidden' name='ProfileCode' value='".$res['ProfileCode']."' />";
                                    echo "<input type='hidden' name='ProfileDNo' value='".$res['DNo']."' />";
                                    echo "<input type='hidden' name='ProfileName' value='".$res['ProfileName']."' />";
                                    echo "<td>";
                                    echo "<button type='button' name='detail' class='veDetail btn-xs btn-info mb-8'
                                        data-toggle='modal'
                                        data-target='#VEDetail'
                                        id='" . $res['DNo'] . "'                                       

                                        > Detail </button>";                                                                   
                                    echo "<br>";
                                    echo "<div style='height:5px;'></div>";
                                    echo "<button type='button' name='delete' id='".$res['DNo']."' class='btnDelete btn-xs btn-danger'> Delete </button>";
                                    echo "</td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </form>                 
                </div>
                <!-- /.box-body -->
        </div>
    </div>
</div>
<!-- modal for detail -->
<div class="modal fade" id="VEDetail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Detail Expertise</h4></center>
            </div>
            <div id="Detail"></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="VendorExpertise.js"></script>

<script>
$(document).ready(function () {
	// var countVdBankAccount = document.getElementById("countVdBankAccount").value;
	var counterBA = 5;

	$("#addrow").on("click", function () {
		var newRow = $("<tr>");
		var cols = "";

		cols += '<td><input type="file" name="VdFile[]" id="file" accept="application/pdf, image/png, image/jpg, image/jpeg" required /></td>';
		cols += '<td><div class="form-group">';                              
        cols +=         '<div class="col-sm-8">'
        cols +=            '<div class="input-group date">'
        cols +=                 '<div class="input-group-addon">'
        cols +=                     '<i class="fa fa-calendar"></i>'
        cols +=                 '</div>'
        cols +=                '<input type="text" class="form-control validto" id="validto" name="ValidTo[]" data-field="date" required />'
        cols +=             '</div>'
        cols +=         '</div>'
        cols +=         '<div id="DteValidTo"></div>'
        cols +=    '</div>'
        cols +=  '</td>';
		cols += '<td>';
        cols += '<input type="button" class="BAbtnDel btn btn-md btn-danger" value="Delete"><br>';
        
        cols += '</td>';
		newRow.append(cols);
		$("table.File").append(newRow);

		for(var j = 0; j <= counterBA; j++)
			$(".selectBankCode" + j + "").select2();

		counterBA++;
	});

	$("table.File").on("click", ".BAbtnDel", function (event) {
		$(this).closest("tr").remove();       
		counterBA -= 1
	});
});
</script>
<script>
    $(function (){
        $('.veDetail').click(function(){

            // var name = $(this).data('name');
            var DNo = $(this).attr('id');
            // var profileName =  name;
            // var Dno = dno;
            
                
            $.ajax({
                url: 'VEDetail.php',
                method: 'post',
                data: {DNo:DNo},
                error: function(err) {
                    console.log('Error ' + err);
                },
                success:function(data){
                    console.log('Success  ' + data);
                    $('#Detail').html(data);                                        
                    $('#VEDetail').modal("show");
                }
            });
        });

        $('.btnDelete').click(function(){
            var dno = $(this).attr('id');

           $.ajax({
               url:'VendorExpertise.php',
               method:'post',
               data:{DNoDel: dno},
               error: function(err) {
                    console.log('Error ' + err);
                },
                success:function(data){
                    console.log('Success  ' + data);
                    window.location = 'VendorExpertise.php'
                }
           });
        });

    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<?php
} // else not submit
include "footer.php"
?>