<?php
include 'header.php';
require 'conn.php';
include 'StdMtd.php';

if(isset($_SESSION['vdusercode']))
{
	header("Location: index.php");
}

$errorMessage = "";
$successMessage = "";

// If form submitted, insert values into the database.
if (isset($_POST['submit'])){

  if(isset($_POST['VdName']) && isset($_POST['ShortName']) && isset($_POST['Email']))
  {
    // removes backslashes
    //$username = stripslashes($_REQUEST['username']);
      //escapes special characters in a string
    $VdName = mysqli_real_escape_string($conn,$_POST['VdName']);
    //$password = stripslashes($_REQUEST['password']);
    $ShortName = mysqli_real_escape_string($conn,$_POST['ShortName']);
    $emails = mysqli_real_escape_string($conn,$_POST['Email']);
    //Checking is user existing in the database or not

    $sql = "";

    $sql = $sql . "Select X.Email ";
    $sql = $sql . "From ";
    $sql = $sql . "( ";
    $sql = $sql . "  Select A.Email ";
    $sql = $sql . "  From TblVendor A ";
    $sql = $sql . "  Where A.Email = '".$emails."' ";
    $sql = $sql . "  Union All ";
    $sql = $sql . "  Select T.Email ";
    $sql = $sql . "  From TblVendorRegister T ";
    $sql = $sql . "  Where T.Email = '".$emails."' ";
    $sql = $sql . "  Union All ";
    $sql = $sql . "  Select B.VendorRegisterEmail As Email ";
    $sql = $sql . "  From TblVendor B ";
    $sql = $sql . "  Where B.VendorRegisterEmail = '".$emails."' ";
    $sql = $sql . ")X ";
    $sql = $sql . "Limit 1 ";

    $availableEmail = GetValue($sql);

    if(strlen($availableEmail) > 0)
    {
        $errorMessage = "This email (".$availableEmail.") has been used.";
    }
    else
    {
        $query = "";
        $query = $query . "Insert Into TblVendorRegister(Email, VdName, ShortName, VerifiedInd, CreateBy, CreateDt) ";
        $query = $query . "Values('".$emails."', '".$VdName."', '".$ShortName."', 'N', 'System', CurrentDateTime()); ";

        if (mysqli_query($conn, $query)) {
            $success = true;
        } else {
            echo "Error: " . $query . "<br>" . mysqli_error($conn);
            $success = false;
            mysqli_rollback($conn);
            break;
        }

        if($success)
        {
          mysqli_commit($conn);
          $successMessage = "<div class='container'><div class='callout callout-success'><h4>Success</h4>Please wait for usercode and password sent to your Email.</div></div>";
        }
    }
  }
} // end if(isset($_POST['submit']))

echo $successMessage;
?>

<div class="login-box">
  <div class="login-logo">
    <a href="#">
      <img src="TWC.png" width="320px">
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Register your Vendor</p>

    <form action="" method="post" name="login" enctype="multipart/form-data" onsubmit="return validateRegister();">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="VdName" id="VdName" placeholder="Company's Name" />
        <span class="fa fa-building-o form-control-feedback"></span>
        <span class="error-message" id="eVdName"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="ShortName" id="ShortName" placeholder="Company's Short Name" />
        <span class="fa fa-building-o form-control-feedback"></span>
        <span class="error-message" id="eShortName"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="Email" id="Email" placeholder="Company's Email" onblur="ShowRegisteredEmail(this.value);" />
        <span class="fa fa-envelope-o form-control-feedback"></span>
        <span class="error-message" id="eEmail"></span>
        <span class="error-message"><center><?php echo $errorMessage; ?></center></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <input type="submit" name="submit" class="btn btn-primary btn-block btn-flat" value="Register" />
        </div>
        <?php if(strlen($successMessage) > 0) {  ?>
        <div class="col-xs-4 col-xs-offset-4">
          <a href="login.php"><button type="button" name="login" class="btn btn-success btn-block btn-flat">Login Page</button></a>
        </div>
        <?php } ?>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>

<script type="text/javascript">
  function ShowRegisteredEmail(email)
  {
    if(email == "")
    {
        document.getElementById("eEmail").innerHTML = "";
        return;
    }
    else
    {
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("eEmail").innerHTML = this.responseText;
                if(this.responseText.length > 0) return false;
            }
        };
        xmlhttp.open("GET","getregisteredemail.php?q="+email,true);
        xmlhttp.send();
    }
  }
  
  function validateRegister()
  {
    document.getElementById("eVdName").innerHTML = "";
    document.getElementById("eShortName").innerHTML = "";
    document.getElementById("eEmail").innerHTML = "";

    var VdName = document.getElementById("VdName").value;
    var ShortName = document.getElementById("ShortName").value;
    var Email = document.getElementById("Email").value;

    if(VdName == "") { document.getElementById("eVdName").innerHTML = "Company's Name should not be empty."; return false; }
    if(ShortName == "") { document.getElementById("eShortName").innerHTML = "Company's Short Name should not be empty."; return false; }
    if(Email == "") { document.getElementById("eEmail").innerHTML = "Company's Email should not be empty."; return false; }
  }
</script>

<?php
include 'footer.php';
?>