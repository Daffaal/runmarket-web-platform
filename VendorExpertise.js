$(function () {
    $(".selectBirthPlace").select2();
    $(".selectCntCode").select2();
    $(".selectGender").select2();
  });
//Date picker
$("#DteBirthDt").DateTimePicker();
$("#DteValidTo").DateTimePicker();

$("#VdExpertise").DataTable({
    responsive: true,
    autoWidth: false,
    scrollX: true,
    fixedColumns: {
        leftColumns: 1
    }
});