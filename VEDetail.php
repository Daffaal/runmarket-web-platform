<?php 
include "auth.php";
include "conn.php";
include "StdMtd.php";

$Gender = "";
$pathFile = 'dist/pdf/';
if(isset($_POST['DNo'])) {

    // query detail
    $sql = ("Select A.DNo, A.ProfileCode, A.ProfileName, A.ActInd, A.Position, A.Company, A.Address,
        A.CityCode, B.Cityname, Date_format(A.BirthDt, '%d-%m-%Y') BirthDt, A.ID, A.NPWP, A.Education, A.Certificate,
        A.Language, A.Experience, A.OriInd, A.CntCode, C.CntName, A.Gender, D.OptDesc As Gender2, A.Email, A.Project
        From TblVendorProfile A 
        Left Join TblCity B On A.CityCode = B.CityCode
        Left Join TblCountry C On A.CntCode = C.CntCode
        Left Join TblOption D On A.Gender = D.OptCode And D.Optcat = 'Gender'
        Where A.VdCode = '". $_SESSION['vdcode'] ."' and A.DNo = '". $_POST['DNo'] ."'
        Order By A.DNo;");

    // query list file uploaded
    $sql1 = ("select t.FileLocation , date_format(t.EndDt, '%d-%m-%Y') EndDt
        from TblVendorProfileUpload t 
        where t.VdCode = '". $_SESSION['vdcode'] . "' and t.DNo regexp '". $_POST['DNo'] ."'
        ;");

    // query select city
    $q = "Select CityCode As Col1, Concat(A.CityName, ' ( ', IfNull(B.ProvName, ''), ' ', IfNull(C.CntName, ''), ' )')  As Col2 ";
    $q = $q . "From TblCity A ";
    $q = $q . "Left Join TblProvince B On A.ProvCode=B.ProvCode ";
    $q = $q . "Left Join TblCountry C On B.CntCode=C.CntCode ";
    $q = $q . "Order By C.CntName, A.CityName ";

    // query select country
    $q1 = "Select CntCode As Col1, CntName  As Col2 ";
    $q1 = $q1 . "From TblCountry ";
    $q1 = $q1 . "Order By CntName; ";

    // query select Gender
    $q2 = "Select OptCode As Col1, OptDesc As Col2 ";
    $q2 = $q2 . "From TblOption ";
    $q2 = $q2 . "Where OptCat = 'Gender' ";
    $q2 = $q2 . "Order By OptDesc; ";

    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

    while ($dtl = mysqli_fetch_assoc($result)) {
        
    echo ("<form method='POST' action='VendorExpertise.php' enctype='multipart/form-data'>
    <div class='modal-body'>
        <div class='container-fluid'>                        

                <input type='hidden' class='form-control id' id='name' name='DNo' value='" . $dtl['DNo'] . "'/>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Profile Name</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control id' id='name' name='name' value='" . $dtl['ProfileName'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Active</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='checkbox' class='active' id='active' name='active' ". ($dtl['ActInd'] == 'Y' ? 'checked' : '') ."/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>ID</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control id' id='id' name='id' value='" . $dtl['ID'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Position</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control position' id='position' name='position' value='" . $dtl['Position'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>                                        
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Company</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control company' id='company' name='company' value='" . $dtl['Company'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Bith Date</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control birthDt' id='birthDt' name='birthDt' value='" . $dtl['BirthDt'] . "' data-field='date' required/>
                    </div>
                    <div id='DteBirthDt'></div>
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Birth Place</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <select class='form-control selectBirthPlace' name='LueBirthPlace' id='LueBirthPlace' style='width: 100%;'>
                            <option value=''></option>");
                            $result2 = mysqli_query($conn, $q) or die(mysqli_error($conn));

                            while ($res = mysqli_fetch_assoc($result2)) {

                                if($dtl['CityCode'] == $res['Col1']){
                                    echo "<option value='".$res['Col1']."' selected";
                                }else {
                                    echo "<option value='".$res['Col1']."' ";
                                }
                                echo ">".$res['Col2']."</option>";
                            }

echo ("                            
                        </select>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Address</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control address' id='address' name='address' value='" . $dtl['Address'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>NPWP</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control npwp' id='npwp' name='npwp' value='" . $dtl['NPWP'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Education</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control education' id='education' name='education' value='" . $dtl['Education'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Certificate</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control certificate' id='certificate' name='certificate' value='" . $dtl['Certificate'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Language Proficiency</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control lang' id='lang' name='lang' value='" . $dtl['Language'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Working Experience</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='number' class='form-control exp' id='exp' name='exp' value='" . $dtl['Experience'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Original</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='checkbox' class='ori' id='ori' name='ori' ". ($dtl['OriInd'] == 'Y' ? 'checked' : '') ."/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Country</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <select class='form-control selectCntCode' name='LueCntCode' id='LueCntCode' style='width: 100%;'>
                            <option value=''></option>");

                            $result3 = mysqli_query($conn, $q1);
                            while ($res = mysqli_fetch_assoc($result3)) {

                                if($dtl['CntCode'] == $res['Col1']){
                                    echo "<option value='".$res['Col1']."' selected";
                                }else {
                                    echo "<option value='".$res['Col1']."' ";
                                }
                                echo ">".$res['Col2']."</option>";
                            }
echo ("                  </select>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Gender</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                    <select class='form-control selectGender' name='gender' id='gender' style='width: 100%;'>
                        <option value=''></option> ");

                            $result4 = mysqli_query($conn, $q2);
                            while ($res = mysqli_fetch_assoc($result4)) {

                                if($dtl['Gender'] == $res['Col1']){
                                    echo "<option value='".$res['Col1']."' selected";
                                    $Gender = $res['Col1'];
                                }else {
                                    echo "<option value='".$res['Col1']."' ";
                                    $Gender = $res['Col1'];
                                }
                                echo ">".$res['Col2']."</option>";                                

                            }

echo ("             </select>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Email</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control email' id='email' name='email' value='" . $dtl['Email'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Project Experience</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control project' id='project' name='project' value='" . $dtl['Project'] . "'/>
                    </div>
                    
                </div>
                <div style='height:30px;'></div>");
    }

    $result1 = mysqli_query($conn, $sql1) or die(mysqli_error($conn));

    echo    "<table class='table FileDtl' id=''>";
    echo    "    <thead>";
    echo    "        <tr>";
    echo    "            <th>File</th>";
    echo    "            <th>Valid To</th>";
    echo    "            <th>#</th>";   
    echo    "        </tr>";
    echo    "    </thead>";

    while($res = mysqli_fetch_assoc($result1))
    {        

    echo    "    <tbody>";
    echo    "       <tr>";
    echo    "        <td>". basename($res['FileLocation']) ."</td>";
    echo    "        <td>" . $res['EndDt'] ."</td>";
    echo    "        <td>";
    echo    "           <a href='".$pathFile.basename($res['FileLocation'])."' target='_blank'><button type='button' class='btn-xs btn-info'> Preview </button></a>";
    echo    "           <button type='button' id='". $res['FileLocation'] ."' name='deleteFile' class='delete btn-xs btn-danger'> Delete </button>";
    echo    "        </td>";
    echo    "       </tr>";
    echo    "        <input type='hidden' name='countUpload' id='countUpload'/>";
    echo    "    </tbody>";    
    }

    echo    "    <tfoot>";
    echo    "        <tr>";
    echo    "            <td colspan='5' style='text-align: left;'>";
    echo    "                <input type='button' class='btn btn-lg btn-block' id='addFile' value='Add Row'  />";
    echo    "            </td>";
    echo    "        </tr>";
    echo    "        <tr>";
    echo    "        </tr>";
    echo    "    </tfoot>";
    echo    "</table>";
                
echo ("         
                <input type='reset' class='btn' onclick='return confirmCancel();' value='Cancel' id='BtnCancel' style='background-color: #58585A; color:white'/>
                <button type='submit' name='edit' class='edit btn pull-right' id='BtnSave' style='background-color: #D61E2F; color:white'>Save</button>        
                </div>                            
        </div> 
    </div>                
</form>");    
    
}

?>

<script>
    $(function(){
        $('.delete').click(function (){
            var fileLoc = $(this).attr('id');
            
            if(confirm('Are you sure want to delete ?')){
                $.ajax({
                url: 'VendorExpertise.php',
                method: 'post',
                data: {file:fileLoc},
                error: function(err) {
                    console.log('Error ' + err);
                },
                success:function(data){
                    console.log('Success  ' + data);
                    window.location='VendorExpertise.php'                    
                }
            });
            }
        });       

        $(".selectBirthPlace").select2();
        $(".selectCntCode").select2();
        $(".selectGender").select2();
        $("#DteBirthDt").DateTimePicker();
        $("#DteValidTo").DateTimePicker();
    });
</script>

<script>
$(document).ready(function () {
	// var countVdBankAccount = document.getElementById("countUpload").value;
	var counterBA = 5;

	$("#addFile").on("click", function () {
		var newRow = $("<tr>");
		var cols = "";

		cols += '<td><input type="file" name="VdFileDtl[]" id="file" accept="application/pdf, image/png, image/jpg, image/jpeg"/></td>';
		cols += '<td><div class="form-group">';                              
        cols +=         '<div class="">'
        cols +=            '<div class="input-group date">'
        cols +=                 '<div class="input-group-addon">'
        cols +=                     '<i class="fa fa-calendar"></i>'
        cols +=                 '</div>'
        cols +=                '<input type="text" class="form-control validto" id="validto" name="ValidToDtl[]" data-field="date" />'
        cols +=             '</div>'
        cols +=         '</div>'
        cols +=         '<div id="DteValidTo"></div>'
        cols +=    '</div>'
        cols +=  '</td>';
		cols += '<td>';
        cols += '<input type="button" class="BAbtnDel btn btn-md btn-danger" value="Delete"><br>';
        
        cols += '</td>';
		newRow.append(cols);
		$("table.FileDtl").append(newRow);

		for(var j = 0; j <= counterBA; j++)
			$(".selectBankCode" + j + "").select2();

		counterBA++;
	});

	$("table.FileDtl").on("click", ".BAbtnDel", function (event) {
		$(this).closest("tr").remove();       
		counterBA -= 1
	});
});
</script>