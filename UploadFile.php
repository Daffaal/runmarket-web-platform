<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

if(isset($_POST['submit']))
{
    $sql = "";

    $imgFile = $_FILES['VdFile']['name'];
    $tmp_dir = $_FILES['VdFile']['tmp_name'];
    $imgSize = $_FILES['VdFile']['size'];

    $upload_dir = "dist/pdf/";
    $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
    $valid_extension = array('pdf');
    $VdFilesLoc = $_SESSION['vdcode']."_".$_POST['LueCategory'].".".$imgExt;
    $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;

    if(in_array($imgExt, $valid_extension)) // check kalau extensi nya valid
    {
        if($imgSize == 0)
        {
            echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
            echo "<script type='text/javascript'>window.location='UploadFile.php'</script>";
        }
        else
        {
            if($imgSize < $mMaxFile) // if image size < 1025 KB
            {
                echo "<script type='text/javascript'>alert('".$tmp_dir." || ".$upload_dir.$VdFilesLoc."');</script>";
                move_uploaded_file($tmp_dir, $upload_dir.$VdFilesLoc);

                $sql = "";

                $sql = $sql . "Insert Into TblVendorFileUpload(VdCode, CategoryCode, Status, FileLocation, LocalDocNo, LicensedBy, StartDt, EndDt, CreateBy, CreateDt) ";
                $sql = $sql . "Values('".$_SESSION['vdcode']."', '".$_POST['LueCategory']."', 'A', '".$upload_dir.$VdFilesLoc."', ";

                if(strlen($_POST['LocalDocNo']) > 0)
                    $sql = $sql . " '". mysqli_real_escape_string($conn,$_POST['LocalDocNo'])."', ";
                else
                    $sql = $sql . " NULL, ";

                if(strlen($_POST['LicensedBy']) > 0)
                    $sql = $sql . " '". mysqli_real_escape_string($conn,$_POST['LicensedBy'])."', ";
                else
                    $sql = $sql . " NULL, ";

                if(strlen($_POST['StartDt']) > 0)
                    $sql = $sql . "'".date("Ymd", strtotime($_POST['StartDt']))."', ";
                else
                    $sql = $sql . "NULL, ";
                
                if(strlen($_POST['EndDt']) > 0)
                    $sql = $sql . "'".date("Ymd", strtotime($_POST['EndDt']))."', ";
                else
                    $sql = $sql . "NULL, ";
                
                $sql = $sql . "'".$_SESSION['vdusercode']."', CurrentDateTime()); ";

                //echo $sql;

                if (mysqli_query($conn,$sql)) 
                {
                    mysqli_commit($conn);
                    echo "<script type='text/javascript'>window.location='UploadFile.php'</script>";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                    mysqli_rollback($conn);
                }
            }
            else
            {
                echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                echo "<script type='text/javascript'>window.location='UploadFile.php'</script>";
            }
            }
    }
    else
    {
        echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
        echo "<script type='text/javascript'>window.location='UploadFile.php'</script>";
    }

}
else if(isset($_POST['delete']))
{
    $sql2 = "";
    $oldName = $_POST['FileName1'];
    $newName = $_POST['FileName2'];
    
    if(rename($oldName, $newName))
    {
        $sql2 = $sql2 . "Update TblVendorFileUpload ";
        $sql2 = $sql2 . "    Set Status = 'C', FileLocation = '".$newName."', LastUpBy = '".$_SESSION['vdusercode']."', LastUpDt = CurrentDateTime() ";
        $sql2 = $sql2 . "Where VdCode = '".$_SESSION['vdcode']."' ";
        $sql2 = $sql2 . "And CategoryCode = '".$_POST['CategoryCode2']."' ";
        $sql2 = $sql2 . "And Status = 'A'; ";

        if (mysqli_query($conn,$sql2)) 
        {
            mysqli_commit($conn);
            echo "<script type='text/javascript'>window.location='UploadFile.php'</script>";
        } 
        else 
        {
            echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
            mysqli_rollback($conn);
        }
    }
}
else
{
?>

<div class="alert alert-info">
    <h4><i class="icon fa fa-info"></i> File Criteria</h4>
    Only allowed <strong>pdf file</strong> document, with maximum of <strong><?php echo GetParameter("FileSizeMaxUploadFTPClient"); ?> MB</strong> each file.
</div>

<form class="form-horizontal" name="insertVendor" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
            <h4 class="box-title">Compliance</h4>
            </div>
				<div class="box-body">
		        	<div class="form-group">
		            	<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Category</label>

		            	<div class="col-sm-5">
                            <select class="form-control selectCategory" required name="LueCategory" id="LueCategory" style="width: 100%;">
                                <option value=""></option>
                            <?php
                                $q = "Select CategoryCode As Col1, CategoryName As Col2 From TblVendorFileUploadCategory ";
                                $q = $q . "Where ActInd = 'Y' And CategoryCode Not In ( ";
                                $q = $q . "  Select CategoryCode From TblVendorFileUpload Where VdCode = '".$_SESSION['vdcode']."' And Status = 'A' ";
                                $q = $q . ") ";
                                $q = $q . "Order By CategoryName; ";

								$q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
								while($q2 = mysqli_fetch_assoc($q1))
								{
									echo "<option value='".$q2['Col1']."' ";
									echo ">".$q2['Col2']."</option>";
								}
			                ?>
			                </select>
		            	</div>
                        
                        <div class="col-sm-3">
			            	<span class="error-message" id="eCategoryCode"></span>
			            </div>
		            </div>
		            <div class="form-group">
		            	<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">File</label>

		            	<div class="col-sm-5">
		            		<input type="file" class="form-control" name="VdFile" id="VdFile" required accept="application/pdf" />
		            	</div>
                        <div class="col-sm-5">
			            	<span class="error-message" id="eVdFile"></span>
			            </div>
		            </div>
		            <div class="form-group">
		            	<label for="inputPassword3" class="col-sm-2 control-label">Valid From</label>

		            	<div class="col-sm-5">
                            <input type="text" class="form-control StartDt" id="StartDt" name="StartDt" data-field="date" />
		            	</div>
                        <div id="DteStartDt"></div>
		            </div>
		            <div class="form-group">
		            	<label for="inputPassword3" class="col-sm-2 control-label">Valid To</label>

		            	<div class="col-sm-5">
                            <input type="text" class="form-control EndDt" id="EndDt" name="EndDt" data-field="date" />
		            	</div>
                        <div id="DteEndDt"></div>
		            </div>
                    <div class="form-group">
		            	<label for="inputPassword3" class="col-sm-2 control-label">Local Document#</label>

		            	<div class="col-sm-5">
                            <input type="text" class="form-control" id="LocalDocNo" name="LocalDocNo" />
		            	</div>
		            </div>
                    <div class="form-group">
		            	<label for="inputPassword3" class="col-sm-2 control-label">Licensed By</label>

		            	<div class="col-sm-5">
                            <input type="text" class="form-control" id="LicensedBy" name="LicensedBy" />
		            	</div>
		            </div>
		        <hr />
                <input type="reset" class="btn" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="background-color: #58585A; color:white"/>
                <button type="submit" name="submit" class="btn pull-right" id="BtnSave" style="background-color: #D61E2F; color:white">Save</button>    
		        </div>
		        <!-- /.box-body -->
        </div>
	</div>
</div>
</form>

<br />
<div class="row">
	<div class="col-md-12">
		<div class="box box-default">
            <div class="box-header with-border">
            <h4 class="box-title">Manage your file</h4>
            </div>
				<div class="box-body">
                <form action="" name="manageUpload" role="form" enctype="multipart/form-data" method="POST">
                    <table id="VdFileHistory" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Valid From</th>
                                <th>Valid To</th>
                                <th>Local Document#</th>
                                <th>Licensed By</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                $sql = "";

                                $sql = $sql . "Select A.CategoryCode, B.CategoryName, A.FileLocation, IfNull(DATE_FORMAT(A.StartDt, '%d-%b-%Y'), '') StartDt, IfNull(DATE_FORMAT(A.EndDt, '%d-%b-%Y'), '') EndDt, A.ValidInd, A.LocalDocNo, A.LicensedBy ";
                                $sql = $sql . "From TblVendorFileUpload A ";
                                $sql = $sql . "Inner Join TblVendorFileUploadCategory B On A.CategoryCode = B.CategoryCode And ActInd = 'Y' ";
                                $sql = $sql . "Where A.VdCode = '".$_SESSION['vdcode']."' And A.Status = 'A' ";
                                $sql = $sql . "Order By B.CategoryName; ";

                                $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                                while($res = mysqli_fetch_assoc($result))
                                {
                                    echo "<tr>";
                                    echo "<td>" . $res['CategoryName'] . "</td>";
                                    echo "<td>" . $res['StartDt'] . "</td>";
                                    echo "<td>" . $res['EndDt'] . "</td>";
                                    echo "<td>" . $res['LocalDocNo'] . "</td>";
                                    echo "<td>" . $res['LicensedBy'] . "</td>";
                                    echo "<input type='hidden' name='CategoryCode2' value='".$res['CategoryCode']."' />";
                                    echo "<input type='hidden' name='FileName1' value='".$res['FileLocation']."' />";
                                    echo "<input type='hidden' name='FileName2' value='dist/pdf/".$_SESSION['vdcode']."_".$res['CategoryCode']."_".date("Ymdhis").".pdf' />";
                                    echo "<td><a href='".$res['FileLocation']."' target='_blank'><button type='button' class='btn-xs btn-info'> Detail </button></a>";
                                    if($res['ValidInd'] != "Y")
                                        echo " &nbsp;&nbsp; <button type='submit' name='delete' class='btn-xs btn-danger'> Delete </button></td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </form>		            
		        </div>
		        <!-- /.box-body -->
        </div>
	</div>
</div>


<script type="text/javascript" src="UploadFilev1.0.1.js"></script>

<?php
} // else not submit
include "footer.php"
?>