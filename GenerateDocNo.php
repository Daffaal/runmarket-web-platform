<?php
include "auth.php";
include "conn.php";
include "StdMtd.php";

$q = $_GET['q'];

$explodes = explode("-", $q);
$Mth = substr($explodes[0], 4, 2);
$Yr = substr($explodes[0], 2, 2);
$DocType = $explodes[1];
$Tbl = $explodes[2];

$sqlDocTitle="SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle'";
$resultDocTitle = mysqli_query($conn,$sqlDocTitle);
$query = "";
while($res = mysqli_fetch_assoc($resultDocTitle))
{
	$query = $query . "Select Concat( ";
	$query = $query . "IfNull(( ";
	$query = $query . "   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ";
	$query = $query . "       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " . $Tbl . "";
	$query = $query . "       Where Right(DocNo, 5)=Concat('" . $Mth . "','/', '" . $Yr . "') ";
	$query = $query . "       Order By Left(DocNo, 4) Desc Limit 1 ";
	$query = $query . "       ) As Temp ";
	$query = $query . "   ), '0001') ";
	$query = $query . ", '/', '" . ((strlen($res['ParValue']) == 0) ? "XXX" : $res['ParValue']) . "";
	$query = $query . "', '/', (Select IfNull(DocAbbr, '') DocAbbr From TblDocAbbreviation Where DocType = '" . $DocType . "'), '/', '" . $Mth . "','/', '" . $Yr . "' ";
	$query = $query . ") As DocNo ";

	$result = mysqli_query($conn,$query);
	while($DocNo = mysqli_fetch_assoc($result))
	{
		echo $DocNo['DocNo'];
	}
}

mysqli_close($conn);
?>
