<p align="center"><a href="https://dev-app.runmarket.id" target="_blank"><img src="dist/img/runmarket_logo.png" width="400"></a></p>

## About RUN Market

Run market is a web application to facilitate RUN System customers in the procurement process or the procurement of goods.
By using run market, customers can announce an item that is needed and can be seen by other customers according to the customer sector.

A Run Market is similar to a B2B e-commerce website, in which vendors and companies can carry out transactions such as bidding or offering an item, dealing with what has been offered and also being able to view the reporting of the item.

## Installation

Once you cloned the repo, you'll need to install a few of tools, such as :

### Tools

- [XAMPP PHP 8 / 7](https://www.apachefriends.org/download.html)
- [HeidiSQL](https://www.heidisql.com/) / [DBEaver Community Edition](https://dbeaver.io/download/)
- MariaDB version 10+.
- PHP version min. 7.3.
- Apache version min. 2.4.

### Environment

After done installation, create `env.php` file in root project directory following `env.example.php` file that there is in project.

## Running

Following this step below for running this website.

1. Open XAMPP Control panel and start Apache & MySQL
2. Open Browser then type localhost/path-to-directory
