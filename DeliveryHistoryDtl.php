<?php
include "auth.php";
include "conn.php";
include "StdMtd.php";

if ($_POST['DocNo']) {
    $DocNo = $_POST['DocNo'];

    $sql = "Select A.DocNo, A.Amt, B.EntName, Date_Format(A.DocDt, '%d-%m-%Y') DocDt, A.Amt, A.ShipTo, A.BillTo
            From TblPOHdr A
            Left Join
            (
                Select Distinct T1.DocNo, T7.EntName
                From TblPOHdr T1
                Inner Join TblPODtl T2 On T1.DocNo = T2.DocNo
                And T1.DocNo = '". $DocNo ."'
                Inner Join TblPORequestDtl T3 On T2.PORequestDocNo = T3.DocNo And T2.PORequestDNo = T3.DNo
                Inner Join TblMaterialRequestHdr T4 On T3.MaterialRequestDocNo = T4.DocNo
                Left Join TblSite T5 On T4.SiteCode = T5.SiteCode
                Left Join TblProfitCenter T6 On T5.ProfitCenterCode = T6.ProfitCenterCode
                Left Join TblEntity T7 On T6.EntCode = T7.EntCode
            ) B On A.DocNo = B.DocNo
            Where A.DocNo = '". $DocNo ."'
            Limit 1;";

    $sql2 = "Select T.DocNo, T.DNo, T.RecvVdDocNo, T.RecvVdDNo, T.DocDt, T.ItName, T.InventoryUomCode, T.EstRecvDt, T.POQty, T.RecvQty, T.DTName,
            T.CurCode, T.UPrice, T.TaxAmt, ((T.POQty * T.UPrice) + T.TaxAmt) TotalPrice, T.RemarkDtl, T.RemarkHdr
            From
            (
                Select A.DocNo, B.DNo, C.DocNo RecvVdDocNo, C.DNo RecvVdDNo, Date_Format(D.DocDt, '%d-%m-%Y') DocDt, E.ItName, E.InventoryUomCode,
                Date_Format(B.EstRecvDt, '%d-%m-%Y') EstRecvDt, B.Qty POQty, C.Qty RecvQty, I.DTName,
                A.CurCode, H.UPrice, C.Remark RemarkDtl, D.Remark RemarkHdr,
                (
                    ((B.Qty * H.UPrice) * IfNull((J.TaxRate * 0.01), 0.00)) +
                    ((B.Qty * H.UPrice) * IfNull((K.TaxRate * 0.01), 0.00)) +
                    ((B.Qty * H.UPrice) * IfNull((L.TaxRate * 0.01), 0.00))
                ) TaxAmt
                From TblPOHdr A
                Inner Join TblPODtl B On A.DocNo = B.DocNo
                    And A.DocNo = '". $DocNo ."'
                Inner Join TblRecvVdDtl C On B.DocNo = C.PODocNo And B.DNo = PODNo
                    And C.`Status` = 'A'
                    And C.CancelInd = 'N'
                Inner Join TblRecvVdHdr D On C.DocNo = D.DocNo
                Inner Join TblItem E On C.ItCode = E.ItCode
                Inner Join TblPORequestDtl F On B.PORequestDocNo = F.DocNo And B.PORequestDNo = F.DNo
                Inner Join TblQtHdr G On F.QtDocNo = G.DocNo And G.MaterialRequestDocNo Is Not Null
                Inner Join TblQtDtl H On G.DocNo = H.DocNo And F.QtDNo = H.DNo
                Left Join TblDeliveryType I On G.DTCode = I.DTCode
                Left Join TblTax J On A.TaxCode1 = J.TaxCode
                Left Join TblTax K On A.TaxCode2 = K.TaxCode
                Left Join TblTax L On A.TaxCode3 = L.TaxCode
            ) T 
            Order by T.DocNo, T.DNo, T.RecvVdDocNo, T.RecvVdDNo
            ";
    
    $result = mysqli_query($conn,$sql) or die (mysqli_error($conn));
    while($res = mysqli_fetch_assoc($result))
    {
        echo "<div class='container-fluid'> ";
        echo "<div id='InsertTender'></div> ";
        echo "<div class='row'> ";
        echo "<div class='form-group'> ";
        echo "<label for='inputEmail3' class='col-sm-2 control-label' style='padding-top: 10px;'>PO#</label> ";
        echo "<div class='col-sm-4'> ";
        echo "<input type='text' class='form-control' id='inputEmail3' placeholder='". $res['DocNo'] ."' disabled> ";
        echo "</div> ";
        echo "<label for='inputEmail3' class='col-sm-2 control-label' style='padding-top: 10px;'>PO Date</label> ";
        echo "<div class='col-sm-4'> ";
        echo "<input type='text' class='form-control' id='inputEmail3' placeholder='". $res['DocDt'] ."' disabled> ";
        echo "</div> ";
        echo "</div> ";
        echo "</div> ";
        echo "</div> ";

        echo "<div class='container-fluid'> ";
        echo "<div id='InsertTender'></div> ";
        echo "<div class='row'> ";
        echo "<div class='form-group'> ";
        echo "<label for='inputEmail3' class='col-sm-2 control-label' style='padding-top: 10px;'>Grand Total</label> ";
        echo "<div class='col-sm-4'> ";
        echo "<input type='text' class='form-control' style='text-align: right;' id='inputEmail3' placeholder='". number_format($res['Amt'], 2, ",", ".") ."' disabled> ";
        echo "</div> ";
        echo "<label for='inputEmail3' class='col-sm-2 control-label' style='padding-top: 10px;'>Ship To</label> ";
        echo "<div class='col-sm-4'> ";
        echo "<input type='text' class='form-control' id='inputEmail3' placeholder='". $res['ShipTo'] ."' disabled> ";
        echo "</div> ";
        echo "</div> ";
        echo "</div> ";
        echo "</div> ";

        echo "<div class='container-fluid'> ";
        echo "<div id='InsertTender'></div> ";
        echo "<div class='row'> ";
        echo "<div class='form-group'> ";
        echo "<label for='inputEmail3' class='col-sm-2 control-label' style='padding-top: 10px;'>Company (Entity)</label> ";
        echo "<div class='col-sm-4'> ";
        echo "<input type='text' class='form-control' id='inputEmail3' placeholder='". $res['EntName'] ."' disabled> ";
        echo "</div> ";
        echo "<label for='inputEmail3' class='col-sm-2 control-label' style='padding-top: 10px;'>Bill To</label> ";
        echo "<div class='col-sm-4'> ";
        echo "<input type='text' class='form-control' id='inputEmail3' placeholder='". $res['BillTo'] ."' disabled> ";
        echo "</div> ";
        echo "</div> ";
        echo "</div> ";
        echo "</div> ";
    }

    echo "<div class='box-body' style='margin-left:5px'> ";
    echo "    <div style='margin-top: 10px;'>";
    echo "        <h4 class='box-title' style='margin-bottom: 5px;'>Delivery Status</h4>";
    echo "    </div>";
    echo "    <table id='DetailTable' class='table table-bordered table-striped' width='100%'>";
    echo "        <thead>";
    echo "            <tr>";
    echo "                <th>Received#</th>";
    echo "                <th>Received Date</th>";
    echo "                <th>Item</th>";
    echo "                <th>Quantity</th>";
    echo "                <th>Uom</th>";
    echo "                <th>Estimated Receive Date</th>";
    echo "                <th>Outstanding PO</th>";
    echo "                <th>Received Item</th>";
    echo "                <th>Received Date</th>";
    echo "                <th>Delivery Type</th>";
    echo "                <th>Currency</th>";
    echo "                <th>Unit Price</th>";
    echo "                <th>Total</th>";
    echo "                <th>Remark</th>";
    echo "                <th>Remark Detail</th>";
    echo "                <th>File</th>";
    echo "            </tr>";
    echo "        </thead>";
    echo "        <tbody>";
                    $result2 = mysqli_query($conn,$sql2) or die (mysqli_error($conn));
                    $currOutstandingQty = 0;
                    $DNo = "";
                    $index = 0;
                    while ($res2 = mysqli_fetch_assoc($result2))
                    {
                        if (strlen($DNo) == 0) $DNo = $res2['DNo'];
                        
                        if ($DNo != $res2['DNo']) 
                        {
                            $index = 0;
                            $DNo = $res2['DNo'];
                        }

                        if ($index == 0) $currOutstandingQty = $res2['POQty'] - $res2['RecvQty'];
                        else $currOutstandingQty -= $res2['RecvQty'];

                        echo "<tr>";
                        echo "<td>". $res2['RecvVdDocNo'] ."</td>";
                        echo "<td>". $res2['DocDt'] ."</td>";
                        echo "<td>". $res2['ItName'] ."</td>";
                        echo "<td style='text-align: right'>". number_format($res2['POQty'], 2) ."</td>";
                        echo "<td>". $res2['InventoryUomCode'] ."</td>";
                        echo "<td>". $res2['EstRecvDt'] ."</td>";
                        echo "<td style='text-align: right'>". number_format($currOutstandingQty, 2) ."</td>";
                        echo "<td style='text-align: right'>". number_format($res2['RecvQty'], 2) ."</td>";
                        echo "<td>". $res2['DocDt'] ."</td>";
                        echo "<td>". $res2['DTName'] ."</td>";
                        echo "<td>". $res2['CurCode'] ."</td>";
                        echo "<td style='text-align: right'>". number_format($res2['UPrice'], 2, ",", ".") ."</td>";
                        echo "<td style='text-align: right'>". number_format($res2['TotalPrice'], 2, ",", ".") ."</td>";
                        echo "<td>". $res2['RemarkHdr'] ."</td>";
                        echo "<td>". $res2['RemarkDtl'] ."</td>";
                        echo "<td><button type='button' class='view_file btn-xs btn-danger' data-toggle='modal' href='#FileModal' id='". $res2['RecvVdDocNo'] ."'> File</button></td>";
                        echo "</tr>";

                        $index++;
                    }
    echo "        </tbody>";
    echo "     </table>";
    echo " </div>";
}

?>

<!-- modal for file -->

<div class="modal fade" id="FileModal" tabindex="-1" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <center><h4 class="modal-title" id="myModalLabel">Receiving Item File Download</h4></center>
                </div>
                    <div class="modal-body" id="FileDtl"></div>
            </div>
        </div>
    </div>

<script>
    $('.view_file').click(function(){
        var DocNo = $(this).attr("id");
            $.ajax({
                url: 'DeliveryHistoryFile.php',
                method: 'POST',
                data: {DocNo: DocNo},
                success:function(data){
                    $('#FileDtl').html(data);
                    $('#FileModal').modal("show");
                }
            });
    });
    setTimeout(function(){ 
        var table = $('#DetailTable').removeAttr('width').DataTable( {
            scrollY:        "200px",
            scrollX:        true,
            scrollCollapse: true,
            paging:         true,
            columnDefs: [
                { width: 200, targets: 0 }
            ],
            fixedColumns: {
				leftColumns: 1
			}
        } );
    }, 200);
</script>