<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

$sql = "Select VdCode From TblVendor Where VdCode = '".$_SESSION['vdcode']."' And ProcurementStatus Is Not Null And ProcurementStatus = 'V' Limit 1; ";
if(strlen(GetValue($sql)) > 0)
{
?>
<style type="text/css">
.btn-dark {
    color: #fff;
    background-color: #000000;
    border-color: #343a40;
    width: 70%;
}   
.btn-dark:hover {
    background-color: #343a40;
    color: white;
}

/* Team */

.team-member {
    background: #fff;
    text-align: center;
    transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
}

.team-member .team-photo {
    background: #fff;
    min-height: 200px;
    margin: 0 auto;
    padding: 24px 24px 32px 24px;
}

.team-member .team-attrs {
    padding: 2px 16px 16px 16px;
    color: #303030;
}

.team-member .team-attrs .team-name {
    font-size: 21px;
}

.team-member .team-attrs .team-position {
    font-size: 12px;
    letter-spacing: 2px;
    color: #a7a7a7;
}

.team-member .team-content {
    color: #303030;
    opacity: .8;
    padding: 16px 24px 40px 24px;
}

.team-member:hover {
    box-shadow: 2px 3px 9px rgba(0, 0, 0, 0.2);
}


/*------------------------------------------------------------------
[10. Hover Effects]
*/

.item-wrap {
    margin-bottom: 30px;
}

figure {
    position: relative;
    overflow: hidden;
    text-align: center;
}

figure img {
    position: relative;
    opacity: 1.0;
}

figure figcaption {
    padding: 1.0em;
    color: #303030;
    text-transform: uppercase;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
}

figure figcaption > .fig-description a {
    z-index: 1000;
    text-indent: 200%;
    white-space: nowrap;
    font-size: 0;
}

figure figcaption:before,
figure figcaption:after {
    pointer-events: none;
}

figure figcaption,
figure figcaption > a {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

figure h3 {
    word-spacing: -0.15em;
    font-family: "Montserrat", sans-serif;
}

figure h3 span {
    font-family: "Montserrat", sans-serif;
}

figure h3,
figure p {
    margin: 0;
}

figure p {
    letter-spacing: 1px;
    font-size: 68.5%;
}


/* Team Hover */

figure.effect-zoe {
    margin: 0;
    width: 100%;
    height: auto;
    min-width: 200px;
    max-height: none;
    max-width: none;
    float: none;
}

figure.effect-zoe img {
    display: inline-block;
    opacity: 1;
}

figure.effect-zoe p.icon-links {
    margin: 0px;
}

figure.effect-zoe p.icon-links a {
    color: #fff;
    -webkit-transition: -webkit-transform 0.35s;
    transition: transform 0.35s;
    -webkit-transform: translate3d(0, 200%, 0);
    transform: translate3d(0, 200%, 0);
}

figure.effect-zoe p.icon-links a i::before {
    color: #fff;
    font-size: 24px;
    display: inline-block;
    padding: 15px 10px;
    margin-left: 0;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

figure.effect-zoe p.icon-links a:hover i::before {
    color: #f2f2f2;
}

figure.effect-zoe p.phone-number a {
    color: #fff;
    font-size: 12px;
}

figure.effect-zoe p.phone-number a:hover {
    color: #f2f2f2;
    text-decoration: none;
}

figure.effect-zoe figcaption {
    top: auto;
    bottom: 0;
    padding: 5px;
    height: 4em;
    background: #a7a7a7;
    border-top: 3px solid #fff;
    color: #5d5d5d;
    -webkit-transition: -webkit-transform 0.5s;
    transition: transform 0.5s;
    -webkit-transform: translate3d(0, 100%, 0);
    transform: translate3d(0, 100%, 0);
}

figure.effect-zoe:hover figcaption,
figure.effect-zoe:hover h2,
figure.effect-zoe:hover p.icon-links a {
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
}

figure.effect-zoe:hover p.icon-links a:nth-child(3) {
    -webkit-transition-delay: 0.1s;
    transition-delay: 0.1s;
}

figure.effect-zoe:hover p.icon-links a:nth-child(2) {
    -webkit-transition-delay: 0.15s;
    transition-delay: 0.15s;
}

figure.effect-zoe:hover p.icon-links a:first-child {
    -webkit-transition-delay: 0.2s;
    transition-delay: 0.2s;
}
</style>

<div style="display:inline-block">
</div>
<br /><br />
<div class="box">
<div class="box-header">
   <h3 class="box-title">E Catalogue</h3>
</div>
<!-- /.box-header -->
<div class="box-body">
    <div class="col-md-4">
        <div class="team-member">
            <figure class="effect-zoe">
                <div class="team-photo">
                    <img src="https://asset.kompas.com/crops/MILdM6nj0jqm3o7tx74sX0DRPJM=/41x0:714x449/750x500/data/photo/2019/07/12/1543263844.png" alt="Rachel James Johnes" class="img-responsive">
                </div>
                <div class="team-attrs">
                    <div class="team-name font-accident-two-bold-italic">Laptop Lenovo</div>
                    <div class="team-position">Electronic</div>
                </div>
                <div class="team-content small" style="font-size: 20px">
                    Rp xx.xxxx.xxx
                </div>
                <figcaption>
                    <p class="phone-number">
                        <a href="#" type="button" class="mybuttonoverlap btn btn-dark" data-toggle="modal" data-target="#edit">Edit</a>
                    </p>
                </figcaption>
            </figure>
        </div>
        <div class="dividewhite4"></div>
    </div>
    <div class="col-md-4">
        <div class="team-member">
            <figure class="effect-zoe">
                <div class="team-photo">
                    <img src="https://asset.kompas.com/crops/MILdM6nj0jqm3o7tx74sX0DRPJM=/41x0:714x449/750x500/data/photo/2019/07/12/1543263844.png" alt="Rachel James Johnes" class="img-responsive">
                </div>
                <div class="team-attrs">
                    <div class="team-name font-accident-two-bold-italic">Laptop Lenovo</div>
                    <div class="team-position">Electronic</div>
                </div>
                <div class="team-content small" style="font-size: 20px">
                    Rp xx.xxxx.xxx
                </div>
                <figcaption>
                    <p class="phone-number">
                        <a href="#" type="button" class="mybuttonoverlap btn btn-dark" data-toggle="modal" data-target="#edit">Edit</a>
                    </p>
                </figcaption>
            </figure>
        </div>
        <div class="dividewhite4"></div>
    </div>
    <div class="col-md-4">
        <div class="team-member">
            <figure class="effect-zoe">
                <div class="team-photo">
                    <img src="https://asset.kompas.com/crops/MILdM6nj0jqm3o7tx74sX0DRPJM=/41x0:714x449/750x500/data/photo/2019/07/12/1543263844.png" alt="Rachel James Johnes" class="img-responsive">
                </div>
                <div class="team-attrs">
                    <div class="team-name font-accident-two-bold-italic">Laptop Lenovo</div>
                    <div class="team-position">Electronic</div>
                </div>
                <div class="team-content small" style="font-size: 20px">
                    Rp xx.xxxx.xxx
                </div>
                <figcaption>
                    <p class="phone-number">
                        <a href="#" type="button" class="mybuttonoverlap btn btn-dark" data-toggle="modal" data-target="#edit">Edit</a>
                    </p>
                </figcaption>
            </figure>
        </div>
        <div class="dividewhite4"></div>
    </div>
    <div class="col-md-4">
        <div class="team-member">
            <figure class="effect-zoe">
                <div class="team-photo">
                    <img src="https://asset.kompas.com/crops/MILdM6nj0jqm3o7tx74sX0DRPJM=/41x0:714x449/750x500/data/photo/2019/07/12/1543263844.png" alt="Rachel James Johnes" class="img-responsive">
                </div>
                <div class="team-attrs">
                    <div class="team-name font-accident-two-bold-italic">Laptop Lenovo</div>
                    <div class="team-position">Electronic</div>
                </div>
                <div class="team-content small" style="font-size: 20px">
                    Rp xx.xxxx.xxx
                </div>
                <figcaption>
                    <p class="phone-number">
                        <a href="#" type="button" class="mybuttonoverlap btn btn-dark" data-toggle="modal" data-target="#edit">Edit</a>
                    </p>
                </figcaption>
            </figure>
        </div>
        <div class="dividewhite4"></div>
    </div>
</div>
<!-- /.box-body -->
<div class="row">

<!-- //foreach disini -->
<!--//endforeach -->
</div>

</div>
</div>

<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel"></h4></center>
            </div>
            <form method="POST" action="QtMr.php">
                <div class="modal-body">
                    <div class="container-fluid">

                        <div id="QTHistory"></div>

                        <input type="hidden" id="DocNo" name="DocNo" />
                        <input type="hidden" id="DNo" name="DNo" />
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Term of Payment</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <?php

                                        $sql = "Select PtCode As Col1, PtName As Col2 From TblPaymentTerm Order By PtName;";
                                        $result = mysqli_query($conn,$sql) or die(mysql_error());

                                        while($res = mysqli_fetch_assoc($result))
                                        {
                                            echo "<option value='".$res['Col1']."'>".$res['Col2']."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Delivery Type</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select class="form-control selectDTCode" name="DTCode" id="DTCode" style="width: 100%;">
                                    <option value=""></option>
                                    <?php

                                        $sql = "Select DTCode As Col1, DTName As Col2 From TblDeliveryType Order By DTName;";
                                        $result = mysqli_query($conn,$sql) or die(mysql_error());

                                        while($res = mysqli_fetch_assoc($result))
                                        {
                                            echo "<option value='".$res['Col1']."'>".$res['Col2']."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Expired Date</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control ExpiredDt" id="ExpiredDt" name="ExpiredDt" data-field="date" />
                                </div>

                                <div id="DteExpiredDt"></div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Estimated Price</label>
                                </div>
                                <div class="estPrice col-md-9 col-xs-9"></div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Currency</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectCurCode" name="CurCode" id="CurCode" style="width: 100%;">
                                    <option value=""></option>
                                    <?php

                                        $sql = "Select CurCode As Col1, CurName As Col2 From TblCurrency Order By CurName;";
                                        $result = mysqli_query($conn,$sql) or die(mysql_error());

                                        while($res = mysqli_fetch_assoc($result))
                                        {
                                            echo "<option value='".$res['Col1']."'";
                                            if($res['Col1'] == "IDR") echo " selected";
                                            echo ">".$res['Col2']."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Price</label>
                                    <br /><small>(No decimal points, no symbols)</small>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" autocomplete="off" class="form-control angka numberOnly" name="UPrice" required />
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Remark</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control" name="Remark">
                                </div>
                            </div>
                    </div> 
                </div>
                <div class="modal-footer">
                <div style="display: inline-block;">
                        <button type="button" class="btn btn-secondary" style="height: 35px;" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                    </div>
                    <div style="display: inline-block;">
                        <button type="submit" class="btn btn-primary m-l-8" style="height: 35px;"><span class="glyphicon glyphicon-floppy-disk"></span> Save</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal fade" id="insert">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Insert Catalogue</h4></center>
            </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div id="InsertTender"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Item Category</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="">tess</option>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Item Code</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control" id="ExpiredDt" name="" data-field="" readonly>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Item</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="">tes</option>
                                    <option value="">tes</option>
                                    </select>
                                </div>

                                <div id="DteExpiredDt"></div>
                            </div>
                            <div style="height:10px;"></div>
                
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Price</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Brand</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="">tes</option>
                                    <option value="">tes</option>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Uom</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="">tes</option>
                                    <option value="">tes</option>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Term Of Payment</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="">tes</option>
                                    <option value="">tes</option>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Currency</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Specification</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                   <input type="text" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">HS Code</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                   <input type="text" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Upload Foto</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                   <input type="file" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Expired Date</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                   <input type="datetime-local" id="" name="" class="form-control">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Status</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value="">Active</option>
                                    <option value="">Inactive</option>
                                    </select>
                                </div>
                            </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <div style="display: inline-block;">
                        <button type="button" class="btn btn-secondary" style="height: 35px;" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                    </div>
                    <div style="display: inline-block;">
                        <button type="submit" class="btn btn-primary m-l-8" style="height: 35px;"><span class="glyphicon glyphicon-floppy-disk"></span> Save</a>
                    </div>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel">Edit Catalogue</h4></center>
            </div>
                <div class="modal-body">
                    <div class="container-fluid">

                        <div id="InsertTender"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Item Category</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control" id="" name="" data-field="" readonly>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Item Code</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control" id="" name="" data-field="" readonly>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Item</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="">tes</option>
                                    <option value="">tes</option>
                                    </select>
                                </div>

                                <div id="DteExpiredDt"></div>
                            </div>
                            <div style="height:10px;"></div>
                
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Price</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Brand</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="">tes</option>
                                    <option value="">tes</option>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Uom</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="">tes</option>
                                    <option value="">tes</option>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Term Of Payment</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <option value="">tes</option>
                                    <option value="">tes</option>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Currency</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Specification</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                   <input type="text" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">HS Code</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                   <input type="text" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Upload Foto</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                   <input type="file" class="form-control" id="" name="" data-field="">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Expired Date</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                   <input type="datetime-local" id="" name="" class="form-control">
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Status</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value="">Active</option>
                                    <option value="">Inactive</option>
                                    </select>
                                </div>
                            </div>
                    </div> 
                </div>
                <div class="modal-footer">
                <div style="display: inline-block;">
                        <button type="button" class="btn btn-secondary" style="height: 35px;" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                    </div>
                    <div style="display: inline-block;">
                        <button type="submit" class="btn btn-primary m-l-8" style="height: 35px;"><span class="glyphicon glyphicon-floppy-disk"></span> Save</a>
                    </div>
                </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="Tender_0.0.0.1.js"></script>

<?php
} // end Vendor Verified
else
{
    echo "<script type='text/javascript'>alert('You do not have access on this page.);</script>";
    echo "<script type='text/javascript'>window.location='index.php';</script>";
}
include "footer.php";
?>