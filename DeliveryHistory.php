<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header">
            <h4 class="box-title">Delivery History</h4>
            </div>
                <div class="box-body">
                    <table id="Detail" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>PO#</th>
                                <th>PO Date</th>
                                <th>Company (Entity)</th>
                                <th>Grand Total</th>
                                <th>Total Outstanding</th>
                                <th>Status</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $sql = "Select A.DocNo, Date_Format(A.DocDt, '%d-%m-%Y') DocDt, C.EntName, A.Amt,
                                        (B.POQty - IfNull(D.RecvQty, 0.00)) OutstandingQty, IfNull(D.RecvQty, 0.00) RecvQty,
                                        If(B.POQty = IfNull(D.RecvQty, 0.00), 'Done', 'Outstanding') StatusDesc, D.RemarkHdr
                                        From TblPOHdr A
                                        Inner Join 
                                        (
                                            Select T1.DocNo, Sum(T2.Qty) POQty
                                            From TblPOHdr T1
                                            Left Join TblPODtl T2 On T1.DocNo = T2.DocNo
                                                And T1.VdCode = '". $_SESSION['vdcode'] ."'
                                            --    And T2.TakeOrderInd = 'Y'
                                                And T2.CancelInd = 'N'
                                                And T1.Status = 'A'
                                            Inner Join TblPoRequestDtl T3 on T2.PORequestDocNo = T3.DocNo And T2.PORequestDNo = T3.DNo
                                                And T3.TakeOrderInd = 'Y'
                                            Group By T1.DocNo
                                        ) B On A.DocNo = B.DocNo
                                        Inner Join
                                        (
                                            Select Distinct T1.DocNo, T7.EntName
                                            From TblPOHdr T1
                                            Inner Join TblPODtl T2 On T1.DocNo = T2.DocNo And T1.VdCode = '". $_SESSION['vdcode'] ."'
                                            Inner Join TblPORequestDtl T3 On T2.PORequestDocNo = T3.DocNo And T2.PORequestDNo = T3.DNo
                                            Inner Join TblMaterialRequestHdr T4 On T3.MaterialRequestDocNo = T4.DocNo
                                            Left Join TblSite T5 On T4.SiteCode = T5.SiteCode
                                            Left Join TblProfitCenter T6 On T5.ProfitCenterCode = T6.ProfitCenterCode
                                            Left Join TblEntity T7 On T6.EntCode = T7.EntCode
                                        ) C On A.DocNo = C.DocNo
                                        Left Join
                                        (
                                            Select T2.DocNo, Sum(T3.Qty) RecvQty, T4.Remark RemarkHdr
                                                From TblPOHdr T1
                                                Left Join TblPODtl T2 On T1.DocNo = T2.DocNo
                                                    And T1.VdCode = '".$_SESSION['vdcode']."'
                                                Left Join TblRecvVdDtl T3 ON T2.DocNo = T3.PODocNo And T2.DNo = T3.PODNo
                                                    And T3.CancelInd = 'N'
                                                    And T3.Status = 'A'
                                                Left join TblRecvVdHdr T4 on T4.DocNo = T3.DocNo 
                                                Group By T2.DocNo
                                        ) D On A.DocNo = D.DocNo ";
                            
                                $result = mysqli_query($conn, $sql) or die (mysqli_error($conn));
                                while ($res = mysqli_fetch_assoc($result)) {
                                    echo "<tr>";
                                    echo "<td>". $res['DocNo'] ."</td>";
                                    echo "<td>". $res['DocDt'] ."</td>";
                                    echo "<td>". $res['EntName'] ."</td>";
                                    echo "<td style='text-align: right;'>". number_format($res['Amt'], 2, ",", ".") ."</td>";
                                    echo "<td style='text-align: right;'>". number_format($res['OutstandingQty'], 2) ."</td>";
                                    echo "<td>". $res['StatusDesc'] ."</td>";
                                    echo "<td>";
                                    echo "<button type='button' class='view_data btn-xs btn-info' data-toggle='modal' data-target='#DetailModal' id='". $res['DocNo'] ."'> Detail</button>";
                                    echo "</td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>         
                </div>
                <!-- /.box-body -->
        </div>
    </div>
</div>

<!-- modal for detail -->

    <div class="modal fade" id="DetailModal">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Detail Delivery History</h4></center>
                </div>
                    <div class="modal-body" id="DeliveryHistoryDtl"></div>
            </div>
        </div>
    </div>

<?php
 // else not submit
include "footer.php"
?>

<script>
    $(function () {
        $("#Detail").DataTable(
        {
        responsive: true,
        autoWidth: true,
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: 
        {
            leftColumns: 1
        }
        });
        
        $('.view_data').click(function(){
            var DocNo = $(this).attr("id");
            $('#DeliveryHistoryDtl').text("");
                
                $.ajax({
                    url: 'DeliveryHistoryDtl.php',
                    method: 'POST',
                    data: {DocNo},
                    success:function(data){
                        $('#DeliveryHistoryDtl').html(data);
                        $('#DetailModal').modal("show");
                    }
                });
        });
    });
</script>