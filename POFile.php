<?php 

include "auth.php";
include "conn.php";
include "StdMtd.php";

define('FTP_STATUS', 'enable');
define('FTP_HOST', '46.17.173.6');
define('FTP_USER', 'runsystem@runsystem.id');
define('FTP_PASS', 'runsystemUSD1B');
define('FTP_DESTINATION', '/runmarket/dev/');

$host = FTP_HOST;
$user = FTP_USER;
$pass = FTP_PASS;
$target_dir = "tmp".DIRECTORY_SEPARATOR;

if(isset($_GET['File'])){
    $fileName = $_GET['File'];
    $fileName = str_replace('%20', ' ', $fileName);
    if(file_exists($target_dir.$fileName)){
        if(is_file($target_dir.$fileName))
        {
            // required for IE
            if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }
        
            // get the file mime type using the file extension
            $mime = mime_content_type($target_dir.$fileName);
        
            // Build the headers to push out the file properly.
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($target_dir.$fileName).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($target_dir.$fileName));
            flush(); // Flush system output buffer
            readfile($target_dir.$fileName);
            exit();
        }
    }else{
        if (FTP_STATUS === "enable") {
            try {
                $ftpConn = ftp_connect($host);
                $login = ftp_login($ftpConn, $user, $pass);
                $ftpPasv = ftp_pasv($ftpConn, true);
        
                if ((!$ftpConn) || (!$login)) {
                    echo "FTP connection has failed! Attempted to connect to " . $host . ".";
                }
        
                if (!$ftpPasv) {
                    echo "Cannot switch to passive mode";
                }
        
                if ($login) {
        
                    $ftpSuccess = false;
                    if (!file_exists($target_dir)) {
                        mkdir($target_dir, 0777, true);
                    }
                    $downloadFile = null;
                    if($fileName != ''){
                        $downloadFile = ftp_get($ftpConn, $target_dir.$fileName, FTP_DESTINATION.$fileName, FTP_BINARY);
                        if(is_file($target_dir.$fileName))
                        { 
                          // required for IE
                          if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }
                    
                          // get the file mime type using the file extension
                          $mime = mime_content_type($target_dir.$fileName);
                    
                          // Build the headers to push out the file properly.
                          header('Pragma: public');     // required
                          header('Expires: 0');         // no cache
                          header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                          header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($target_dir.$fileName)).' GMT');
                          header('Cache-Control: private',false);
                          header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
                          header('Content-Disposition: attachment; filename="'.basename($fileName).'"');  // Add the file name
                          header('Content-Transfer-Encoding: binary');
                          header('Content-Length: '.filesize($target_dir.$fileName)); // provide file size
                          header('Connection: close');
                          readfile($target_dir.$fileName); // push it out
                          exit();
                        }
                    }else {
                        echo "<script>alert('No File Found.');</script>";
                        exit();
                    }
                    if (!$downloadFile) {
                        echo "<script>alert('Error while downloading file.')</script>";
                        exit();
                    }
                }
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        
            if ($ftpSuccess) {
               echo "<script>alert('Success Download File.');</script>";
               ftp_close($ftpConn);
            }
        }
    }
}else { 
   echo "<script>alert('No File Found.');</script>";
   exit();
} 

?>

