<?php
include "auth.php";
include "conn.php";
include "StdMtd.php";

if($_POST['DocNo'])
{
    $DocNo = $_POST['DocNo'];
    $sql = "";

    $sql = $sql . "Select A.DocNo, DATE_FORMAT(A.DocDt, '%d %b %Y') DocDt, ";
    $sql = $sql . "Case B.ActInd When 'Y' Then 'Active' When 'N' Then 'Inactive' End As ActInd, ";
    $sql = $sql . "E.DocNo As TenderDocNo, E.TenderName, C.PtName, D.DTName, IfNull(DATE_FORMAT(A.ExpiredDt, '%d %b %Y'), '') ExpiredDt, ";
    $sql = $sql . "F.CurCode As MRCurCode, F.EstPrice, A.CurCode, B.UPrice, A.Remark, H.TQRDocNo As TheChosen, ";
    $sql = $sql . "Case E.Status When 'O' Then 'Open' When 'C' Then 'Closed' End As TenderStatus ";
    $sql = $sql . "From TblQtHdr A ";
    $sql = $sql . "Inner Join TblQtDtl B On A.DocNo = B.DocNo ";
    $sql = $sql . "Inner Join TblPaymentTerm C On A.PtCode = C.PtCode ";
    $sql = $sql . "Left Join TblDeliveryType D On A.DTCode = D.DTCode ";
    $sql = $sql . "Inner Join TblTender E On A.TenderDocNo = E.DocNo ";
    $sql = $sql . "Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo = F.DocNo And E.MaterialRequestDNo = F.DNo ";
    $sql = $sql . "Left Join TblTenderQuotationRequest G On A.DocNo = G.QtDocNo And E.DocNo = G.TenderDocNo ";
    $sql = $sql . "Left Join TblTenderQuotation H On G.DocNo = H.TQRDocNo ";
    $sql = $sql . "Where A.DocNo = '".$DocNo."'; ";

    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

    while($res = mysqli_fetch_assoc($result))
    {
        echo "<table class='table table-bordered table-striped'>";
        echo "<tr><td><strong>QT#</strong></td><td>".$res['DocNo']."</td></tr>";
        echo "<tr><td><strong>Date</strong></td><td>".$res['DocDt']."</td></tr>";
        echo "<tr><td><strong>QT Status</strong></td><td>".$res['ActInd'];
        if($res['TheChosen'] != "")
        {
            echo " | <strong>Chosen</strong>";
        }
        echo "</td></tr>";
        echo "<tr><td><strong>Tender#</strong></td><td>".$res['TenderDocNo']."</td></tr>";
        echo "<tr><td><strong>Tender Name</strong></td><td><strong>[".$res['TenderStatus'] . "]</strong> ". $res['TenderName'];
        $sctr = "Select SectorName From TblSector Where SectorCode In (Select SectorCode From TblTenderDtl Where DocNo = '".$res['TenderDocNo']."'); ";
        $qs = mysqli_query($conn, $sctr) or die(mysqli_error($conn));
        if(mysqli_num_rows($qs) > 0) echo "<br />";
        while($rs = mysqli_fetch_assoc($qs))
        {
            echo "<span style='color:#273c75'>- " . $rs['SectorName'] . "</span><br />";
        }
        echo "</td></tr>";
        echo "<tr><td><strong>Payment Term</strong></td><td>".$res['PtName']."</td></tr>";
        echo "<tr><td><strong>Delivery Type</strong></td><td>".$res['DTName']."</td></tr>";
        echo "<tr><td><strong>Expired Date</strong></td><td>".$res['ExpiredDt']."</td></tr>";
        echo "<tr><td><strong>Estimated Price</strong></td><td>".$res['MRCurCode']." ".number_format($res['EstPrice'], 2, ",", ".")."</td></tr>";
        echo "<tr><td><strong>Your Price</strong></td><td>".$res['CurCode']." ".number_format($res['UPrice'], 2, ",", ".")."</td></tr>";
        echo "<tr><td><strong>Remark</strong></td><td>".$res['Remark']."</td></tr>";
        echo "</table>";
    }
}

?>