<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

if(strlen($_SESSION['vdcode']) > 0 && strlen($_SESSION['vdname']) > 0)
{
if(isset($_POST['submit']))
{
	$OldP = GetValue("Select Pwd From TblUserVendor Where UserCode = '".$_SESSION['vdusercode']."' Limit 1 ");
	//var_dump("p : " . $OldP . " , form : " . $_POST['OldPass']);
	if($OldP == $_POST['OldPass'])
	{
		$sql = "";

		$R1 = array("'");
		$R2 = array("\'");

		$pw = str_replace($R1, $R2, $_POST['NewPass']);

		$sql = $sql . "Update TblUserVendor ";
		$sql = $sql . "Set Pwd = '".$pw."', LastUpBy = '".$_SESSION['vdusercode']."', LastUpDt = CurrentDateTime() ";
		$sql = $sql . "Where UserCode = '".$_SESSION['vdusercode']."'; ";

		if (mysqli_query($conn,$sql)) {
			mysqli_commit($conn);
			echo "<script type='text/javascript'>alert('Password changed successful.');</script>";
	    	echo "<script type='text/javascript'>window.location='index.php'</script>";
		} else {
			mysqli_rollback($conn);
		    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	}
	else
	{
		echo "<script>alert('Your Old Password is incorrect.');</script>";
		echo "<script type='text/javascript'>window.location='ChangePassword.php'</script>";
	}
}
else
{

?>

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
            <h4 class="box-title">Change My Password</h4>
            </div>

            <form class="form-horizontal" name="createCPRequest" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">
				<div class="box-body">
		        	<div class="form-group">
		            	<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Old Password</label>

		            	<div class="col-sm-5">
		            		<input type="password" class="form-control" name="OldPass" id="OldPass">
		            	</div>
		            	<div class="col-sm-5">
			            	<span class="error-message" id="eOldPass"></span>
			            </div>
		            </div>
		            <div class="form-group">
		            	<label for="inputPassword3" class="col-sm-2 control-label mandatory-field">New Password</label>

		            	<div class="col-sm-5">
		            		<input type="password" class="form-control" name="NewPass" id="NewPass">
		            	</div>
		            	<div class="col-sm-5">
			            	<span class="error-message" id="eNewPass"></span>
			            </div>
		            </div>
		            <div class="form-group">
		            	<label for="inputPassword3" class="col-sm-2 control-label mandatory-field">Confirm New Password</label>

		            	<div class="col-sm-5">
		            		<input type="password" class="form-control" name="CNewPass" id="CNewPass">
		            	</div>
		            	<div class="col-sm-5">
			            	<span class="error-message" id="eCNewPass"></span>
			            </div>
		            </div>
		        </div>
		        <!-- /.box-body -->
		        <div class="box-footer">
		        	<input type="reset" class="btn btn-warning" onclick="return confirmCancel();" value="Cancel" />
		            <button type="submit" name="submit" class="btn btn-success pull-right">Save</button>
		        </div>
		        <!-- /.box-footer -->
			</form>

        </div>
	</div>
</div>

<script type="text/javascript" src="CPRequest.js"></script>

<?php
} // else if not submitting
} // end if isset empcode

include 'footer.php';
?>
