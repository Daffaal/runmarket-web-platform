<?php
include "auth.php";
include "conn.php";
include "StdMtd.php";

if (isset($_POST['DocNo'])) {

    $DocNo = $_POST['DocNo'];
    $StartDt = $_POST['StartDt'];
    $EndDt = $_POST['EndDt'];
    $SubmitInd = $_POST['SubmitInd'];

    $sql = "
    SELECT A.DocNo, B.DNo, DATE_FORMAT(A.DocDt, '%d-%m-%Y') DocDt, IFNULL(DATE_FORMAT(B.UsageDt, '%d-%m-%Y'), '-') UsageDt,
    C.ItName, B.Qty, If(B.EstPrice = 0, B.UPrice, B.EstPrice) EstPrice, IFNULL(B.UsageDt, '-') UsageDt2, A.DocDt AS DocDt2,
    B.TotalPrice, C.PurchaseUOMCode AS UOM, E.EntName, G.CityName, B.CurCode, B.Remark, H.PtName
    FROM TblMaterialRequestHdr A
    INNER JOIN TblMaterialRequestDtl B ON A.DocNo = B.DocNo
        AND A.RMInd = 'Y'
        AND B.`Status` = 'A'
        AND B.CancelInd = 'N'
    INNER JOIN TblItem C ON B.ItCode = C.ItCode 
    LEFT JOIN (
        SELECT A.DocNo, D.EntName
        FROM TblMaterialRequestHdr A
        INNER JOIN TblSite B ON A.SiteCode = B.SiteCode
        INNER JOIN TblProfitCenter C ON B.ProfitCenterCode = C.ProfitCenterCode
        INNER JOIN TblEntity D ON D.EntCOde = C.EntCode
    ) E ON A.DocNo = E.DocNo
    LEFT JOIN TblCity G ON G.CityCode = B.CityCode
    Left JOIN tblpaymentterm H ON B.PtCode = H.PtCode
    WHERE A.DocNo = '".$DocNo."'
    ORDER BY B.DNo";
?>

    <form method="post" action="submitQT.php?id=<?= $DocNo ?>" enctype="multipart/form-data">
        <input type="hidden" name="SeqNo" value="<?= $_POST['SeqNo'] ?>">
        <input type="hidden" name="Rfq3DNo" value="<?= $_POST['Rfq3DNo'] ?>">

        <div class="row">
            <div class="col-md-12">
                <!-- <style>table-layout: fixed; word-wrap:break-word;</style> -->
                <table class="table table-bordered table-striped" id="tblrfq">
                    <thead>
                        <tr>
                            <th style="text-align: center;"><input type="checkbox" id="checkAll"></th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>UoM</th>
                            <th>Unit Price</th>
                            <th>Currency</th>
                            <th>Term of Payment</th>
                            <th>Delivery Location</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Usage Date</th>
                            <th>Remark</th>
                        </tr>
                    </thead>
                    <?php
                    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                    while ($res = mysqli_fetch_array($result)) {

                    ?>

                        <tbody>
                            <td><input type="checkbox" class="checkItem" name="Dno[]" value="<?= $res['DNo'] ?>"></td>
                            <td><?= $res['ItName'] ?></td>
                            <td align="right"><?= number_format($res['Qty'], 2, ",", ".") ?></td>
                            <td><?= $res['UOM'] ?></td>
                            <td align="right">-</td>
                            <td><?= $res['CurCode'] ?></td>
                            <td><?= $res['PtName'] ?></td>
                            <td><?= $res['CityName'] ?></td>
                            <td><?= ($StartDt != '') ? date('d-m-Y', strtotime(($StartDt))) : '-' ?></td>
                            <td><?= ($EndDt != '') ? date('d-m-Y', strtotime($EndDt)) : '-' ?></td>
                            <td><?= $res['UsageDt'] ?></td>
                            <td><?= $res['Remark'] ?></td>
                        </tbody>
                    <?php } ?>
                <?php } ?>
                </table>
                <?php 
                    // echo date('YmdHi', strtotime($StartDt));
                    // echo " -- ";
                    // echo date('YmdHi', strtotime(date('YmdHi')));
                    // If(date('YmdHi') >= date('YmdHi', strtotime($StartDt))  && date('YmdHi') <= date('YmdHi', strtotime($EndDt)) ){
                    if($SubmitInd == 'Y'){
                ?>
                <div align="right">
                    <input type="button" class="btn" data-dismiss="modal" value="Cancel" id="BtnCancel" style="background-color: #58585A; color:white" />
                    <button type="submit" name="submit" class="btn btn-info btnSubmit">Submit Quotation</button>
                </div>
                <?php } ?>
            </div>
        </div>
    </form>

    <script>
        $(document).ready(function() {
            $('#checkAll').change(function() {
                var checkboxes = $(this).closest('form').find(':checkbox');
                checkboxes.prop('checked', $(this).is(':checked'));
            });

            $('.btnSubmit').click(function() {
                var checked = $("input[type=checkbox]:checked").length;

                if (!checked) {
                    alert("You must check at least one checkbox.");
                    return false;
                }

            });
        })

        setTimeout(function() {
            var table = $('#tblrfq').DataTable({
                responsive: true,
                autowidth: true,
                scrollY: "300px",
                scrollX: true,
                scrollCollapse: true,
                paging: false,
            });
        }, 200);
    </script>