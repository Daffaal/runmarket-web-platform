<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

$TenderDocNo = $_POST['DocNo'];
$PtCode = $_POST['PtCode'];
$CurCode = $_POST['CurCode'];
$DTCode = strlen($_POST['DTCode']) <= 0 ? NULL : $_POST['DTCode'];
$ExpiredDt = strlen($_POST['ExpiredDt']) <= 0 ? NULL : date("Ymd", strtotime($_POST['ExpiredDt']));
$R1 = array(" ", ".", ",");
$R2 = array("", "", ".");
$UPrice = str_replace($R1, $R2, $_POST['UPrice']);
$Remark = strlen($_POST['Remark']) <= 0 ? NULL : $_POST['Remark'];
$DocDt = date("Ymd");
$DocNo = GenerateDocNo($DocDt, "Qt", "TblQtHdr");
$SystemNo = date("Ym") . substr($DocNo, 0, 4);
$MRDocNo = GetValue("Select MaterialRequestDocNo From TblTender Where DocNo = '".$TenderDocNo."'; ");
$MRDNo = GetValue("Select MaterialRequestDNo From TblTender Where DocNo = '".$TenderDocNo."'; ");
$ItCode = GetValue("Select ItCode From TblMaterialRequestDtl Where DocNo = '".$MRDocNo."' And DNo = '".$MRDNo."'; ");

$sql = "Insert Into TblQtHdr(DocNo, SystemNo, DocDt, Status, CancelInd, VdCode, PtCode, CurCode, DTCode, ExpiredDt, TenderDocNo, Remark, CreateBy, CreateDt) ";
$sql = $sql . "Values('".$DocNo."', '".$SystemNo."', '".$DocDt."', 'A', 'N', '".$_SESSION['vdcode']."', '".$PtCode."', '".$CurCode."', ";
if(strlen($DTCode) <= 0)
    $sql = $sql . "NULL,";
else
    $sql = $sql .  "'".$DTCode."', ";

if(strlen($ExpiredDt) <= 0)
    $sql = $sql .  "NULL,";
else
    $sql = $sql .  "'".$ExpiredDt."', ";

$sql = $sql .  "'".$TenderDocNo."', '".$Remark."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";

$sql = $sql . "Update TblQtDtl T1  ";
$sql = $sql . "Inner Join TblQtHdr T2 ";
$sql = $sql . "    On T1.DocNo=T2.DocNo ";
$sql = $sql . "    And T2.Status='A' ";
$sql = $sql . "    And T2.VdCode='".$_SESSION['vdcode']."' ";
$sql = $sql . "    And T2.PtCode='".$PtCode."' ";
$sql = $sql . "    And T2.TenderDocNo='".$TenderDocNo."' ";
$sql = $sql . "Set T1.ActInd='N', T1.LastUpBy='".$_SESSION['vdusercode']."', T1.LastUpDt=CurrentDateTime() ";
$sql = $sql . "Where T1.ActInd='Y' ";
$sql = $sql . "And Exists( ";
$sql = $sql . "    Select DocNo From TblQtHdr Where DocNo='".$DocNo."' And Status='A' ";
$sql = $sql . ");";

$sql = $sql . "Insert Into TblQtDtl(DocNo, DNo, ItCode, ActInd, UPrice, Remark, CreateBy, CreateDt) ";
$sql = $sql . "Values('".$DocNo."', '001', '".$ItCode."', 'Y', ".$UPrice.", '".$Remark."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";

$expSQL = explode(";", $sql);
$countSQL = count($expSQL);
$success = false;

for($i = 0; $i < $countSQL - 1; $i++)
{
    if (mysqli_query($conn, $expSQL[$i])) {
        $success = true;
    } else {
        echo "Error: " . $expSQL[$i] . "<br />" . mysqli_error($conn);
        $success = false;
        mysqli_rollback($conn);
        break;
    }
}

if($success)
{
    mysqli_commit($conn);
    echo "<script type='text/javascript'>window.location='QtHistory.php'</script>";
}

include "footer.php";
?>