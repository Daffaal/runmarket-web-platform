<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

if (isset($_POST['file'])) {

    $host = FTP_HOST;
    $user = FTP_USER;
    $pass = FTP_PASS;

    $sql2 = "";
    $DNo2 = $_POST['file'];
    $Filename = $_POST['name'];
    $DocNo = $_POST['docno'];
    $DNo = $_POST['dno'];
    $sql2 = $sql2 . "Delete From TblQtDtl2 ";
    $sql2 = $sql2 . "Where DNo2 = '" . $DNo2 . "' and DocNo='" . $DocNo . "' and DNo='" . $DNo . "';";

    if (mysqli_query($conn, $sql2)) {

        mysqli_commit($conn);
        if (FTP_STATUS === "enable") {
            try {
                $ftpConn = ftp_connect($host);
                $login = ftp_login($ftpConn, $user, $pass);
                $ftpPasv = ftp_pasv($ftpConn, true);

                if ((!$ftpConn) || (!$login)) {
                    echo "FTP connection has failed! Attempted to connect to " . $host . ".";
                }

                if (!$ftpPasv) {
                    echo "Cannot switch to passive mode";
                }

                if ($login) {

                    if (file_exist_on_ftp_server(basename($Filename), $ftpConn)) {
                        $deleteFile = ftp_delete($ftpConn, FTP_DESTINATION . basename($Filename));
                        if (!$deleteFile) {
                            return "Could not delete $fileName";
                        } else {
                            echo "<script>location.reload</script>";
                        }

                        // return true;
                    }
                }
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        // echo "<script type='text/javascript'>window.location='QtHistory.php'</script>";
    } else {
        echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
        mysqli_rollback($conn);
    }
}

if (isset($_POST['submit'])) {
    if (isset($_POST['Qt'])) {

        $Qt = $_POST['Qt'];
        $DNo = $_POST['DNo'];
        $DocNo = $_POST['DocNo'];

        foreach ($DNo as $row) {
            $sql = "Select T.DocNo, T.Quotation, T.DNo, T.EntName, T.DocDt, T.UsageDt, T.ItName, T.Qty, T.Uom,
                    T.TotalPrice, T.RemarkHdr, T.Remark, T.UPrice, SUM(T.UPrice) totalAmt, T.CityName,
                    If (T.WinInd = 'Y', 'Win',
                        If (T.CloseInd = 'Y', 'Close',
                        If (T.ExpInd = 'Y', 'Expired',
                            If(T.CancelInd = 'Y', 'Cancel', '')
                            )
                        )
                    ) As StatusDesc, T.PtName, T.DTName, T.CurCode
                    From
                    (
                    Select C.DocNo, A.DocNo Quotation, C.DNo,  E.EntName, Date_Format(D.DocDt, '%d %b %Y') DocDt,
                        Date_Format(B.ExpiredDt, '%d %b %Y') UsageDt, F.ItName, A.Qty, F.PurchaseUOMCode UOM,
                        SUM(A.Qty * A.UPrice) TotalPrice, B.Remark RemarkHdr, A.Remark, A.UPrice, H.CityName
                        ,C.CancelInd, If(D.ExpDt < Replace(CurDate(), '-', ''), 'Y', 'N') ExpInd,
                        If(G.DocNo Is Null, 'N',
                            If(G.VdCode != B.VdCode, 'Y', 'N')
                        ) CloseInd,
                        If(G.DocNo Is Null, 'N',
                            If(G.VdCode = B.VdCode, 'Y', 'N')
                        ) WinInd, T1.PtName, T2.DTName, C.CurCode
                    from tblqtdtl A
                    inner join tblqthdr B on B.DocNo = A.DocNo
                    inner join tblpaymentterm T1 on B.PtCode = T1.PtCode
                    inner join tbldeliverytype T2 on B.DTCode = T2.DTCode
                    inner join tblmaterialrequestdtl C on C.DocNo = B.MaterialRequestDocNo and A.ItCode = C.ItCode
                    inner join tblmaterialrequesthdr D on D.DocNo = C.DocNo And D.ExpDt Is Not Null
                    inner join tblitem F on F.ItCode = C.ItCode
                    left join (
                        Select C.EntCode, A.DocNo, D.EntName
                        From TblMaterialRequestHdr A
                        Left Join TblSite B On A.SiteCode = B.SiteCode
                        Left Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode
                        left join tblentity D on D.EntCOde = C.EntCode
                    ) E on C.DocNo  = E.DocNo
                    LEFT JOIN (
                            SELECT Distinct T1.DocNo, T1.DNo, T4.VdCode
                            FROM tblqtdtl T1
                            Inner JOIN tblporequestdtl T2 ON T1.DocNo = T2.QtDocNo And T1.DNo = T2.QtDNo
                            Inner JOIN tblpodtl T3 ON T2.DocNo = T3.PORequestDocNo And T2.DNo = T3.PORequestDNo
                                And T3.CancelInd = 'N'
                            Inner JOIN tblpohdr T4 ON T3.DocNo = T4.DocNo
                        ) G ON B.DocNo = G.DocNo And A.DNo = G.DNo
                    left join TblCity H on H.CityCode = C.CityCode
                    WHERE B.VdCode = '" . $_SESSION['vdcode'] . "' and A.ActInd = 'Y'
                    Group By C.DocNo, A.DocNo, C.DNo, E.EntName, D.DocDt, B.ExpiredDt, F.ItName,
                    F.PurchaseUOMCode, B.Remark, A.UPrice, C.CancelInd, D.ExpDt, G.DocNo, G.VdCode
                    ) T
                    Where T.DocNo = '" . $DocNo . "' And T.Quotation = '" . $Qt . "' And T.DNo = '" . $row . "'
                    order by T.DocNo desc;
                    ";
            $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
            foreach ($result as $res) {
                $jumlah[] = $row;
                $sumRow = count($jumlah);
            }
        }
?>
        <form method="post" action="UpdateQtMr.php" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header">
                            <h4 class="box-title">Edit Your Quotation History</h4>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-2 col-xs-2">
                                    <label class="control-label " style="position:relative; top:7px;"> Request For Quotation</label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <input type="text" class="form-control" name="DocNoHdr" value="<?= $res['DocNo'] ?>" readonly />
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <label class="control-label " style="position:relative; top:7px;">Quotation</label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <input type="text" class="form-control" name="Qt[]" value="<?= $res['Quotation'] ?>" readonly />
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-2 col-xs-2">
                                    <label class="control-label " style="position:relative; top:7px;">Total Amount</label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <input type="text" style="text-align: right;" class="form-control totalAmt" id="totalAmt" name="totalAmt[]" onload="totalAmout()" readonly />
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <label class="control-label " style="position:relative; top:7px;">RFQ Date</label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <input type="text" class="form-control" name="DocDt[]" value="<?= $res['DocDt'] ?>" readonly />
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-2 col-xs-2">
                                    <label class="control-label " style="position:relative; top:7px;">Company(Enity)</label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <input type="text" class="form-control" name="EntName[]" value="<?= $res['EntName'] ?>" readonly />
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <label class="control-label " style="position:relative; top:7px;">RFQ Expired Date</label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <input type="text" class="form-control" name="UsageDt[]" value="<?= $res['UsageDt'] ?>" readonly />
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-2 col-xs-2">
                                    <label class="control-label " style="position:relative; top:7px;">Date</label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <input type="text" class="form-control" name="DocDt[]" value="<?= $res['DocDt'] ?>" readonly />
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <label class="control-label " style="position:relative; top:7px;">Remark</label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <input type="text" class="form-control" name="remarkHdr" value="<?= $res['RemarkHdr'] ?>" readonly />
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="modal-body" id="QtHistoryEdit"></div>
                            </div>
                            <div style="height: 20px;"></div>
                            <!-- Uload file Modal -->
                            <?php for ($i = 1; $i <= $sumRow; $i++) { ?>
                                <div class="modal fade" id="UploadFile<?= $i ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Quotation</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="container-fluid">
                                                    <table class="table File<?= $i ?>" id="tblBankAccount">
                                                        <thead>
                                                            <tr>
                                                                <th>File</th>
                                                                <th>#</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="file<?= $i ?>">

                                                            <input type='hidden' name='countVdBankAccount' id='countVdBankAccount' />
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="5" style="text-align: left;">
                                                                    <!-- <input type="button" class="btn btn-lg btn-block " id="addrow<?= $i ?>" value="Add File" /> -->
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <!-- <div align="right">
                                                        <input type="reset" class="btn" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="background-color: #58585A; color:white" />
                                                        <button type="submit" class="btn btn-info" data-dismiss="modal">Ok</button>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="modal-footer"></div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <table class="table table-bordered table-striped" id="tblrfq">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>UoM</th>
                                        <th>Unit Price</th>
                                        <th>Currency</th>
                                        <th>Term of Payment</th>
                                        <th>Delivery Type</th>
                                        <th>Received Date</th>
                                        <th>Expired Date</th>
                                        <th>Total Price</th>
                                        <th>Remark</th>
                                    </tr>
                                </thead>
                                <?php
                                $no = 0;
                                foreach ($DNo as $row) {
                                    $no++;
                                    $sqlgrid = "Select T.DocNo, T.Quotation, T.QtDNo, T.DNo, T.EntName, T.DocDt, T.UsageDt, T.ItName, T.Qty, T.Uom,
                                                T.TotalPrice, T.Remark, T.UPrice, T.CityName,
                                                If (T.WinInd = 'Y', 'Win',
                                                    If (T.CloseInd = 'Y', 'Close',
                                                    If (T.ExpInd = 'Y', 'Expired',
                                                        If(T.CancelInd = 'Y', 'Cancel', '')
                                                        )
                                                    )
                                                ) As StatusDesc, T.PtCode, T.DTCode, T.CurCode
                                                From
                                                (
                                                Select C.DocNo, A.DocNo Quotation, A.DNo QtDNo, C.DNo,  E.EntName, Date_Format(D.DocDt, '%d %b %Y') DocDt,
                                                    Date_Format(B.ExpiredDt, '%d %b %Y') UsageDt, F.ItName, A.Qty, F.PurchaseUOMCode UOM,
                                                    SUM(A.Qty * A.UPrice) TotalPrice, A.Remark, A.UPrice, H.CityName
                                                    ,C.CancelInd, If(D.ExpDt < Replace(CurDate(), '-', ''), 'Y', 'N') ExpInd,
                                                    If(G.DocNo Is Null, 'N',
                                                        If(G.VdCode != B.VdCode, 'Y', 'N')
                                                    ) CloseInd,
                                                    If(G.DocNo Is Null, 'N',
                                                        If(G.VdCode = B.VdCode, 'Y', 'N')
                                                    ) WinInd, T1.PtCode, T2.DTCode, C.CurCode
                                                from tblqtdtl A
                                                inner join tblqthdr B on B.DocNo = A.DocNo
                                                inner join tblpaymentterm T1 on B.PtCode = T1.PtCode
                                                inner join tbldeliverytype T2 on B.DTCode = T2.DTCode
                                                inner join tblmaterialrequestdtl C on C.DocNo = B.MaterialRequestDocNo and A.ItCode = C.ItCode
                                                inner join tblmaterialrequesthdr D on D.DocNo = C.DocNo And D.ExpDt Is Not Null
                                                inner join tblitem F on F.ItCode = C.ItCode
                                                left join (
                                                    Select C.EntCode, A.DocNo, D.EntName
                                                    From TblMaterialRequestHdr A
                                                    Left Join TblSite B On A.SiteCode = B.SiteCode
                                                    Left Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode
                                                    left join tblentity D on D.EntCOde = C.EntCode
                                                ) E on C.DocNo  = E.DocNo
                                                LEFT JOIN (
                                                        SELECT Distinct T1.DocNo, T1.DNo, T4.VdCode
                                                        FROM tblqtdtl T1
                                                        Inner JOIN tblporequestdtl T2 ON T1.DocNo = T2.QtDocNo And T1.DNo = T2.QtDNo
                                                        Inner JOIN tblpodtl T3 ON T2.DocNo = T3.PORequestDocNo And T2.DNo = T3.PORequestDNo
                                                            And T3.CancelInd = 'N'
                                                        Inner JOIN tblpohdr T4 ON T3.DocNo = T4.DocNo
                                                    ) G ON B.DocNo = G.DocNo And A.DNo = G.DNo
                                                left join TblCity H on H.CityCode = C.CityCode
                                                WHERE B.VdCode = '" . $_SESSION['vdcode'] . "' and A.ActInd = 'Y'
                                                Group By C.DocNo, A.DocNo, C.DNo, E.EntName, D.DocDt, B.ExpiredDt, F.ItName,
                                                F.PurchaseUOMCode, B.Remark, A.UPrice, C.CancelInd, D.ExpDt, G.DocNo, G.VdCode
                                                ) T
                                                Where T.DocNo = '" . $DocNo . "' And T.Quotation = '" . $Qt . "' And T.DNo = '" . $row . "'
                                                order by T.DocNo desc;
                                                ";
                                    // var_dump($sqlgrid);
                                    // die;
                                    $resultgrid = mysqli_query($conn, $sqlgrid) or die(mysqli_error($conn));

                                    foreach ($resultgrid as $res) {
                                        $ptcode = $res['PtCode'];
                                        $dtcode = $res['DTCode'];
                                ?>
                                        <input type="hidden" class="form-control" name="DNo[]" value="<?= $res['DNo'] ?>" />
                                        <input type="hidden" class="form-control" id="countRow" name="countRow" value="<?= $sumRow ?>" />
                                        <input type="hidden" class="form-control" name="DocNo[]" value="<?= $res['DocNo'] ?>">
                                        <tbody>
                                            <td>
                                                <a href="#UploadFile<?= $no ?>" id="btnFile<?= $no ?>" class="btn btn-danger" data-doc="<?= $res['Quotation'] ?>" data-dno="<?= $res['QtDNo'] ?>" data-toggle="modal"><button type="button" class="fa fa-upload"></button> Upload file</a>
                                                <!-- <input type="file" name="VdFile[]" id="VPFile" accept="application/pdf, image/png, image/jpg, image/jpeg" multiple /> -->
                                            </td>
                                            <td><input type="text" name="ItName[]" class="form-control" value="<?= $res['ItName'] ?>" readonly></input></td>
                                            <td><input type="number" oninput="quantityLiveChange('<?= $no; ?>')" id="qty<?= $no; ?>" name="qty[]" class="form-control qty" value="<?= number_format($res['Qty'], 0, '', '') ?>" style="text-align: right;" readonly/></td>
                                            <td><input type="text" name="UOM[]" class="form-control" value="<?= $res['Uom'] ?>" readonly></input></td>
                                            <td><input type="text" oninput="unitPriceLiveChange('<?= $no; ?>')" id="unitPrice<?= $no; ?>" name="unitPrice[]" class="form-control unitPrice" value="<?= number_format($res['UPrice'], 0, '', '') ?>" style="text-align: right;" readonly/></td>
                                            <td>
                                                <select disabled required class="form-control" name="CurCode" id="CurCode">
                                                    <option value=""></option>
                                                    <?php
                                                    $sql = "Select CurCode As Col1, CurName As Col2 From TblCurrency Order By CurName;";
                                                    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

                                                    foreach ($result as $row) {
                                                        echo "<option value='" . $row['Col1'] . "'";
                                                        if ($row['Col1'] == "IDR") echo " selected";
                                                        echo ">" . $row['Col2'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select disabled class="form-control" name="PtCode" id="PtCode">
                                                    <option value="">
                                                        <?php

                                                        $q = "Select PtCode As Col1, PtName As Col2 From TblPaymentTerm Order By PtName;";
                                                        $result = mysqli_query($conn, $q) or die(mysqli_error($conn));

                                                        foreach ($result as $rows) {
                                                            echo "<option value='" . $rows['Col1'] . "' ";
                                                            if ($ptcode == $rows['Col1']) {
                                                                echo " selected";
                                                            }
                                                            echo ">" . $rows['Col2'] . "</option>";
                                                        }

                                                        ?>

                                                    </option>
                                                </select>
                                            </td>
                                            <td>
                                                <select disabled class="form-control" name="DTCode" id="DTCode">
                                                    <option value=""></option>
                                                    <?php
                                                    $q1 = "Select DTCode As Col1, DTName As Col2 From TblDeliveryType Order By DTName;";
                                                    $result = mysqli_query($conn, $q1) or die(mysqli_error($conn));
                                                    foreach ($result as $rows) {
                                                        echo "<option value='" . $rows['Col1'] . "' ";
                                                        if ($dtcode == $rows['Col1']) {
                                                            echo " selected";
                                                        }
                                                        echo ">" . $rows['Col2'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="DocDt" name="DocDt[]" data-field="date" value="<?= $res['DocDt'] ?>" readonly></input>
                                                <div id="DateDocDt"></div>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="ExpDate" name="ExpiredDt" data-field="date" value="<?= $res['UsageDt'] ?>" readonly></input>
                                                <div id="DateExpDate"></div>
                                            </td>
                                            <td><input type="text" class="form-control totalPrice" id="totalPrice<?= $no; ?>" name="totalPrice[]" value="<?= number_format($res['TotalPrice'], 0, '', '') ?>" style="text-align: right;" readonly></input></td>
                                            <td><input type="text" name="remark[]" class="form-control" value="<?= $res['Remark'] ?>" readonly /></td>
                                        </tbody>
                                    <?php } ?>
                                <?php } ?>
                            </table>
                            <!-- <div align="right">
                                <input type="reset" class="btn" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="background-color: #58585A;    color:white" />
                                <button type="submit" name="submit" class="btn btn-info">Save Quotation</button>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </form>

    <?php } ?>

<?php } ?>

<script>
    var qtyValue;
    var qtyID;
    var unitPriceID;
    var totalPriceID;
    var unitPriceValue;
    var countTotalPrice;
    var countRow;
    var total;

    function quantityLiveChange(index) {
        qtyID = "qty" + index;
        unitPriceID = "unitPrice" + index;
        totalPriceID = "totalPrice" + index;

        qtyValue = document.getElementById(qtyID).value;
        unitPriceValue = document.getElementById(unitPriceID).value;
        document.getElementById(totalPriceID).value = (qtyValue.replaceAll(".", "") * unitPriceValue.replaceAll(".", ""));

        totalAmout();
    }

    function unitPriceLiveChange(index) {
        qtyID = "qty" + index;
        unitPriceID = "unitPrice" + index;
        totalPriceID = "totalPrice" + index;

        qtyValue = document.getElementById(qtyID).value;
        unitPriceValue = document.getElementById(unitPriceID).value;
        document.getElementById(totalPriceID).value = (qtyValue.replaceAll(".", "") * unitPriceValue.replaceAll(".", ""));

        totalAmout();
    }

    function totalAmout() {
        countTotalPrice = 0;
        countRow = document.getElementById('countRow').value;
        for (var i = 1; i <= countRow; i++) {
            totalPriceID = "totalPrice" + i;
            total = document.getElementById(totalPriceID).value;
            countTotalPrice = countTotalPrice + parseInt(total.replaceAll(".", ""));
        }
        if (!Number.isNaN(countTotalPrice)) {
            document.getElementById('totalAmt').value = new Intl.NumberFormat('id-ID', {style: 'currency', currency: 'IDR'}).format(countTotalPrice);
        } else {
            document.getElementById('totalAmt').value = ""
        }
    }

    console.log(totalAmout())
    console.log(countTotalPrice)

    countRow = document.getElementById('countRow').value;
    for (var i = 1; i <= countRow; i++) {
        $("#unitPrice" + i).inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': '.',
            'autoGroup': true,
            'digits': 2,
            'radixPoint': ".",
            'digitsOptional': false,
            'allowMinus': false,
            'placeholder': ''
        });

        $("#totalPrice" + i).inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': '.',
            'autoGroup': true,
            'digits': 2,
            'radixPoint': ".",
            'digitsOptional': false,
            'allowMinus': false,
            'placeholder': ''
        });
    }

    $(document).ready(function() {
        $('#tblrfq').DataTable({
            responsive: true,
            autoWidth: true,
            scrollX: true,
            paging: false,
            // columnDefs: [{
            //     width: 200,
            //     targets: 0
            // }]
        });

        $("#DateDocDt").DateTimePicker();
        $("#DateExpDate").DateTimePicker();
    });
</script>

<script>
    $(document).ready(function() {
        var countVdBankAccount = document.getElementById("countVdBankAccount").value;
        var counterBA = 5;
        var t = '<?= $sumRow ?>'

        for (let i = 1; i <= t; i++) {

            $("#addrow" + i).on("click", function() {
                var newRow = $("<tr>");
                var cols = "";

                cols += '<td><input type="file" name="VdFile' + i + '[]" id="VPFile' + i + '" accept="application/pdf, image/png, image/jpg, image/jpeg" required/></td>';
                // cols += '<td><div class="form-group">';                              
                // cols +=         '<div class="">'
                // cols +=            '<div class="input-group date">'
                // cols +=                '<input type="text" class="form-control validto" id="validto" name="ValidToDtl[]" data-field="date" />'
                // cols +=             '</div>'
                // cols +=         '</div>'
                // cols +=         '<div id="DteValidTo"></div>'
                // cols +=    '</div>'
                // cols +=  '</td>';
                cols += '<td>';
                cols += '<input type="button" class="BAbtnDel' + i + ' btn btn-xs btn-danger" value="Delete"><br>';

                cols += '</td>';
                newRow.append(cols);
                $("table.File" + i).append(newRow);

                for (var j = 0; j <= counterBA; j++)
                    $(".selectBankCode" + j + "").select2();

                // counterBA++;
            });


            $("table.File" + i).on("click", ".BAbtnDel" + i, function(event) {
                $(this).closest("tr").remove();
                // counterBA -= 1
            });
        }
    });
</script>

<script>
    $(document).ready(function() {
        var t = '<?= $sumRow ?>'
        var basePath = 'dist/pdf/';
        for (let i = 1; i <= t; i++) {
            $('#btnFile' + i).click(function() {
                var docNo = $(this).data('doc');
                var dNo = $(this).data('dno');
                $.ajax({
                    url: 'QtAttachment.php',
                    method: 'post',
                    data: {
                        DocNo: docNo,
                        DNo: dNo
                    },
                    success: function(data) {
                        var resp = JSON.parse(data);
                        var newRow = "";
                        var cols = "";
                        $('#file' + i).html("");
                        for (let j = 0; j < resp.length; j++) {
                            const data = resp[j];
                            cols += "<tr>";
                            cols += "    <td>" + data.FileLocation + "</td>";
                            cols += " <td>";
                            cols += " <a href='POFile.php?File=" + data.FileLocation + "' target='_blank'><button type='button' class='btn-xs btn-info'> Preview </button></a>";
                            cols += " <input type='button' data-file='" + data.FileLocation + "' data-docno='" + data.DocNo + "' data-dno='" + data.DNo + "' data-dnofile='" + data.DNo2 + "' value='Delete' class='delete" + j + " btn-xs btn-danger' ></input>";
                            cols += " </td>";
                            cols += "</tr>";
                        }
                        // onclick='deletefile("+data.FileLocation+", "+data.DNo2+", "+data.DocNo+", "+data.DNo+");'
                        $('table.File' + i).append(cols);
                        $('#UploadFile' + i).show();

                        for (let j = 0; j < resp.length; j++) {
                            $('.delete' + j).click(function() {
                                var fileName = $(this).data('file');
                                var DocNo = $(this).data('docno');
                                var DNo = $(this).data('dno');
                                var dno2 = $(this).data('dnofile');

                                if (confirm('Are you sure want to delete ?')) {
                                    $.ajax({
                                        url: 'QtHistoryEdit.php',
                                        method: 'post',
                                        data: {
                                            file: dno2,
                                            name: fileName,
                                            docno: DocNo,
                                            dno: DNo
                                        },
                                        error: function(err) {
                                            console.log('Error ' + err);
                                        },
                                        success: function(data) {
                                            location.reload();
                                        }
                                    });
                                }
                            })
                        }
                    },
                    error: function(err) {
                        console.log("err " + err)
                    }
                })
            })
        }
    })
</script>