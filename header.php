<?php
  require('conn.php');
  define('FTP_STATUS', 'enable');
  define('FTP_HOST', '46.17.173.6');
  define('FTP_USER', 'runsystem@runsystem.id');
  define('FTP_PASS', 'runsystemUSD1B');
  define('FTP_DESTINATION', '/runmarket/dev/');

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="expires" content="Mon, 26 Jul 1997 05:00:00 GMT"/>
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="cache" content="no-cache" />
  <meta http-equiv="cache-control" content="no-cache, must-revalidate" />
  <title>.:RUN Market:.</title>

  <link rel="icon" sizes="192x192" href="dist/img/runmarket_logo2.png">
  <link rel="icon" href="dist/img/runmarket_logo2.png" type="image/x-icon" />

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- icon sendiri -->
  <!-- <link rel="stylesheet" href="ic/demo-files/demo.css"> -->

  <link rel="stylesheet" href="ic2/style.css">
 
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/select2.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTEv1.0.11.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins-3.css">

  <!-- <link rel="stylesheet" type="text/css" href="nehakadam/DateTimePicker.css" /> -->
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/datetimepicker@latest/dist/DateTimePicker.min.css" />
  <link rel="stylesheet" type="text/css" href="plugins/colorlib/css/main.css">
	<link rel="stylesheet" type="text/css" href="plugins/colorlib/css/util.css">
  <style type="text/css">
    .angka
    {
      text-align: right;
    }

    .carousel-content 
    {
        color:#444;
        display:flex;
        align-items:center;
        text-align: justify;
    }
    body {
      font-family: 'Gotham Rounded';
    }
    #text-carousel 
    {
      width: 100%;
      height: auto;
      padding: 50px;
    }
    fa-info {
      color: white;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<?php
  // iis : $_SERVER['PATH_INFO']
  // apache : $_SERVER['REQUEST_URI']
  
  if(substr($_SERVER['REQUEST_URI'], -9) == "login.php" || substr($_SERVER['REQUEST_URI'], -12) == "register.php")
  {
    echo "<body class='hold-transition login-page'>";
  } // end if
  else
  {
    ?>

<body class="hold-transition skin-blue-light sidebar-mini" 

<?php
 if(substr($_SERVER['REQUEST_URI'], -16) == "EmployeeStat.php" || substr($_SERVER['REQUEST_URI'], -14) == "Attendance.php") 
 {
  ?>
  onload="cekLokasi();"
  <?php
 } 

  $sql = ("Select A.DocNo, Date_Format(A.DocDt, '%d-%m-%Y') DocDt, B.SectorName, B.ProcurementTypeDesc, F.EntName, B.StageName, B.StartDt, B.EndDt, C.TotalPrice, A.Remark, 
          B.RFQSeqNo, B.RFQ3DNo, B.StartTm, B.EndTm
          From TblMaterialRequestHdr A
          Left Join
          (
              Select X.MaterialRequestDocNo, Group_Concat(Distinct X.StageName) StageName,
              Group_Concat(Distinct X.StartDt) StartDt, Group_Concat(Distinct X.EndDt) EndDt,
              Group_Concat(Distinct SectorName) SectorName, Group_Concat(Distinct X.ProcurementTypeDesc) ProcurementTypeDesc,
              Group_Concat(Distinct RFQSeqNo) RFQSeqNo, Group_Concat(Distinct RFQ3DNo) RFQ3DNo, 
              Group_Concat(Distinct StartTm) StartTm, Group_Concat(Distinct EndTm) EndTm
              From
              (
                  Select Distinct T1.MaterialRequestDocNo, T4.StageName, Date_Format(T4.StartDt, '%d-%m-%Y') StartDt, 
                  Date_Format(T4.EndDt, '%d-%m-%Y') EndDt, Concat(Left(T4.StartTm, 2), ':', Right(T4.StartTm, 2)) StartTm, 
                Concat(Left(T4.EndTm, 2), ':', Right(T4.EndTm, 2)) EndTm, T5.SectorName, T6.OptDesc As ProcurementTypeDesc, 
                  T1.SeqNo As RFQSeqNo, T3.DNo As RFQ3DNo
                  From TblRFQHdr T1
                  Inner Join TblRFQDtl T2 On T1.MaterialRequestDocNo = T2.MaterialRequestDocNo
                      And T1.SeqNo = T2.SeqNo
                  Inner Join TblVendorSector T3 On T3.VdCode = '".$_SESSION['vdcode']."'
                      And T2.SectorCode = T3.SectorCode
                      And T2.SubSectorCode = T3.SubSectorCode
                  Inner Join
                  (
                      Select X3.MaterialRequestDocNo, X3.SeqNo, X1.DNo, X1.StartDt, X1.StartTm, X1.EndDt, X1.EndTm, X4.OptDesc As ProcurementTypeDesc,
                      X1.StageName
                      From TblRFQDtl3 X1
                      Inner Join
                      (
                          Select MaterialRequestDocNo, Max(Concat(EndDt, EndTm)) MaxPeriod
                          From TblRFQDtl3
                          Where Concat(EndDt, EndTm) <= CurrentDateTime()
                          Group By MaterialRequestDocNo
                      ) X2 On X1.MaterialRequestDocno = X2.MaterialRequestDocNo And Concat(X1.EndDt, X1.EndTm) = X2.MaxPeriod
                      Inner Join TblRFQHdr X3 On X1.MaterialRequestDocNo = X3.MaterialRequestDocNo And X1.SeqNo = X3.SeqNo
                      Inner Join TblOption X4 On X3.ProcurementType = X4.OptCode And X4.OptCat = 'ProcurementType'
                  ) T4 On T1.MaterialRequestDocNo = T4.MaterialRequestDocNo And T1.SeqNo = T4.SeqNo
                  Inner Join TblSector T5 On T2.SectorCode = T5.SectorCode
                  Inner Join TblOption T6 On T1.ProcurementType = T6.OptCode And T6.OptCat = 'ProcurementType'
                  Where Not Exists
                  (
                      Select 1
                      From TblRFQDtl2
                      Where MaterialRequestDocNo = T1.MaterialRequestDocNo
                      And SeqNo = T1.SeqNo
                  )
                  Union All
                  Select Distinct T1.MaterialRequestDocNo, T3.StageName, Date_Format(T3.StartDt, '%d-%m-%Y') StartDt, 
                    Date_Format(T3.EndDt, '%d-%m-%Y') EndDt, Concat(Left(T3.StartTm, 2), ':', Right(T3.StartTm, 2)) StartTm, 
                Concat(Left(T3.EndTm, 2), ':', Right(T3.EndTm, 2)) EndTm, T5.SectorName, T6.OptDesc As ProcurementTypeDesc, 
                    T1.SeqNo As RFQSeqNo, T3.DNo As RFQ3DNo
                  From TblRFQHdr T1
                  Inner Join TblRFQDtl2 T2 On T1.MaterialRequestDocNo = T2.MaterialRequestDocNo
                      And T1.SeqNo = T2.SeqNo
                      And T2.VdCode = '".$_SESSION['vdcode']."'
                  Inner Join
                  (
                      Select X3.MaterialRequestDocNo, X3.SeqNo, X1.DNo, X1.StartDt, X1.StartTm, X1.EndDt, X1.EndTm, X4.OptDesc As ProcurementTypeDesc,
                      X1.StageName
                      From TblRFQDtl3 X1
                      Inner Join
                      (
                          Select MaterialRequestDocNo, Max(Concat(EndDt, EndTm)) MaxPeriod
                          From TblRFQDtl3
                          Where Concat(EndDt, EndTm) <= CurrentDateTime()
                          Group By MaterialRequestDocNo
                      ) X2 On X1.MaterialRequestDocno = X2.MaterialRequestDocNo And Concat(X1.EndDt, X1.EndTm) = X2.MaxPeriod
                      Inner Join TblRFQHdr X3 On X1.MaterialRequestDocNo = X3.MaterialRequestDocNo And X1.SeqNo = X3.SeqNo
                      Inner Join TblOption X4 On X3.ProcurementType = X4.OptCode And X4.OptCat = 'ProcurementType'
                  ) T3 On T1.MaterialRequestDocNo = T3.MaterialRequestDocNo And T1.SeqNo = T3.SeqNo
                  Inner Join TblRFQDtl T4 On T1.MaterialRequestDocNo = T4.MaterialRequestDocno
                      And T1.SeqNo = T4.SeqNo
                  Inner Join TblSector T5 On T4.SectorCode = T5.SectorCode
                  Inner Join TblOption T6 On T1.ProcurementType = T6.OptCode And T6.OptCat = 'ProcurementType'
            ) X
            Group By X.MaterialRequestDocNo
          ) B On A.DocNo = B.MaterialRequestDocNo
          Inner Join
          (
              Select DocNo, Sum(EstPrice * Qty) TotalPrice
              From TblMaterialRequestDtl
              Where CancelInd = 'N'
              And Status = 'A'
              Group By DocNo
          ) C On A.DocNo = C.DocNo
          Left Join TblSite D On A.SiteCode = D.SiteCode
          Left Join TblProfitCenter E On D.ProfitCenterCode = E.ProfitCenterCode
          Left Join TblEntity F On E.EntCode = F.EntCode
          Where A.RMInd = 'Y'
          And A.DocNo In 
          (
              Select Distinct DocNo
              From TblMaterialRequestDtl4
          )
          And A.DocNo Not In
          (
              Select Distinct DocNo
              From TblMaterialRequestDtl
              Where (CancelInd = 'Y' Or Status In ('O', 'C'))
          );");

  $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
  if(isset($_COOKIE['newMR']) && $_COOKIE['newMR'] == 'click'){
      setcookie( "rows1", mysqli_num_rows($result), strtotime( '+30 days' ) );
      $_SESSION['newMR'] = false;
  }else {
      if(isset($_COOKIE['rows1'])){ //check if rows has been saved
          $row1 = mysqli_num_rows($result); //if was, save to variable
          $row2 = $_COOKIE['rows1']; // save new rows

          if($row1 == $row2){ // check if old rows is equal to new rows
            $_SESSION['newMR'] = false;
        }else {
            $_SESSION['newMR'] = true;
        }
      }else { //if wasn't save to cookie
          $row1 = mysqli_num_rows($result);
          setcookie( "rows1", $row1, strtotime( '+30 days' ) );
      }
  }

  $sqlPO = ("
        select A.DocNo PO, Date_Format(A.DocDt, '%d/%b/%Y') PODate, K.EntName,Date_Format(D.UsageDt, '%d %b %Y') UsageDt, A.Amt as GrandTotal,
        A.ShipTo, A.BillTo, D.DocNo
        from TblPOHdr A
        Inner Join TblPODtl B On A.DocNo = B.DocNo And A.VdCode = '". $_SESSION['vdcode'] ."' And A.Status = 'A' And B.CancelInd = 'N' and isnull(B.TakeOrderInd)
        Inner Join TblPORequestDtl C On B.PORequestDocNo = C.DocNo And B.PORequestDNo = C.DNo And B.PORequestDNo = C.DNo 
        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo = D.DocNo And C.MaterialRequestDNo = D.DNo 
        Inner Join TblItem E On D.ItCode = E.ItCode
        inner join TblMaterialRequestHdr F on D.DocNo = F.DocNo And F.CancelInd='N' And F.Status In ('A', 'O')
        inner join tblQtHdr G On C.QtDocNo = G.DocNo And G.MaterialRequestDocNo Is Not Null And G.MaterialRequestDocNo = F.DocNo
        inner join TblQtDtl H on G.DocNo = H.DocNo And C.QtDNo = H.DNo 
        left join (
            Select C.EntCode, A.DocNo, D.EntName
            From TblMaterialRequestHdr A
            Left Join TblSite B On A.SiteCode = B.SiteCode
            Left Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode
            left join tblentity D on D.EntCOde = C.EntCode
            ) K on F.DocNo  = K.DocNo
        group by A.DocNo, A.DocDt, K.EntName, D.UsageDt, A.ShipTo, A.BillTo
      ");

      $sqlPO = ("
        select A.DocNo PO, Date_Format(A.DocDt, '%d/%b/%Y') PODate, K.EntName,Date_Format(D.UsageDt, '%d %b %Y') UsageDt, A.Amt as GrandTotal,
        A.ShipTo, A.BillTo, D.DocNo
        from TblPOHdr A
        Inner Join TblPODtl B On A.DocNo = B.DocNo And A.VdCode = '". $_SESSION['vdcode'] ."' And A.Status = 'A' And B.CancelInd = 'N' and B.TakeOrderInd = 'Y'
        Inner Join TblPORequestDtl C On B.PORequestDocNo = C.DocNo And B.PORequestDNo = C.DNo And B.PORequestDNo = C.DNo 
        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo = D.DocNo And C.MaterialRequestDNo = D.DNo 
        Inner Join TblItem E On D.ItCode = E.ItCode
        inner join TblMaterialRequestHdr F on D.DocNo = F.DocNo And F.CancelInd='N' And F.Status In ('A', 'O')
        inner join tblQtHdr G On C.QtDocNo = G.DocNo And G.MaterialRequestDocNo Is Not Null And G.MaterialRequestDocNo = F.DocNo
        inner join TblQtDtl H on G.DocNo = H.DocNo And C.QtDNo = H.DNo 
        left join (
            Select C.EntCode, A.DocNo, D.EntName
            From TblMaterialRequestHdr A
            Left Join TblSite B On A.SiteCode = B.SiteCode
            Left Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode
            left join tblentity D on D.EntCOde = C.EntCode
            ) K on F.DocNo  = K.DocNo
        group by A.DocNo, A.DocDt, K.EntName, D.UsageDt, A.ShipTo, A.BillTo
      ");

      $resultsqlPO = mysqli_query($conn, $sqlPO) or die (mysqli_error($conn));

      if(isset($_COOKIE['newPO']) && $_COOKIE['newPO'] == 'click') {
          setcookie( "rows1", mysqli_num_rows($resultsqlPO), strtotime( '+30 days' ));
          $_SESSION['newPO'] = false;
      } else {
          if(isset($_COOKIE['rows1'])) {
            $row1 = mysqli_num_rows($resultsqlPO);
            $row2 = $_COOKIE['rows1'];
          if($row1 == $row2) {
            $_SESSION['newPO'] = false;
          } else {
            $_SESSION['newPO'] = true;
          }
          } else {
            $row1 = mysqli_num_rows($resultsqlPO);
            setcookie( "rows1", $row1, strtotime( '+30 days'));
          }          
      }

      $sqlRFQ = ("
        Select T.DocNo, T.Quotation, T.DNo, T.EntName, T.DocDt, T.UsageDt, T.ItName, T.Qty, T.Uom,
        T.TotalPrice, T.Remark, T.UPrice,
        If (T.WinInd = 'Y', 'Win',
            If (T.CloseInd = 'Y', 'Close',
            If (T.ExpInd = 'Y', 'Expired',
                If(T.CancelInd = 'Y', 'Cancel', '')
                )
            )
        ) As StatusDesc
        From
        (
        Select C.DocNo, A.DocNo Quotation, C.DNo,  E.EntName, Date_Format(D.DocDt, '%d %b %Y') DocDt,
            Date_Format(B.ExpiredDt, '%d %b %Y') UsageDt, F.ItName, A.Qty, F.PurchaseUOMCode UOM,
            SUM(A.Qty * A.UPrice) TotalPrice, B.Remark, A.UPrice
            ,C.CancelInd, If(D.ExpDt < Replace(CurDate(), '-', ''), 'Y', 'N') ExpInd,
            If(G.DocNo Is Null, 'N',
                If(G.VdCode != B.VdCode, 'Y', 'N')
            ) CloseInd,
            If(G.DocNo Is Null, 'N',
                If(G.VdCode = B.VdCode, 'Y', 'N')
            ) WinInd
        from tblqtdtl A
        inner join tblqthdr B on B.DocNo = A.DocNo
        inner join tblmaterialrequestdtl C on C.DocNo = B.MaterialRequestDocNo and A.ItCode = C.ItCode
        inner join tblmaterialrequesthdr D on D.DocNo = C.DocNo And D.ExpDt Is Not Null
        inner join tblitem F on F.ItCode = C.ItCode
        left join (
            Select C.EntCode, A.DocNo, D.EntName
            From TblMaterialRequestHdr A
            Left Join TblSite B On A.SiteCode = B.SiteCode
            Left Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode
            left join tblentity D on D.EntCOde = C.EntCode
        ) E on C.DocNo  = E.DocNo
        LEFT JOIN (
                SELECT Distinct T1.DocNo, T1.DNo, T4.VdCode
                FROM tblqtdtl T1
                Inner JOIN tblporequestdtl T2 ON T1.DocNo = T2.QtDocNo And T1.DNo = T2.QtDNo
                Inner JOIN tblpodtl T3 ON T2.DocNo = T3.PORequestDocNo And T2.DNo = T3.PORequestDNo
                    And T3.CancelInd = 'N'
                Inner JOIN tblpohdr T4 ON T3.DocNo = T4.DocNo
            ) G ON B.DocNo = G.DocNo And A.DNo = G.DNo
        WHERE B.VdCode = '". $_SESSION['vdcode'] ."' and A.ActInd = 'Y'
        Group By C.DocNo, A.DocNo, C.DNo, E.EntName, D.DocDt, B.ExpiredDt, F.ItName,
        F.PurchaseUOMCode, B.Remark, A.UPrice, C.CancelInd, D.ExpDt, G.DocNo, G.VdCode
        ) T
        order by T.DocNo desc;
      ");

      $resultsqlRFQ = mysqli_query($conn, $sqlRFQ) or die (mysqli_error($conn));

      if(isset($_COOKIE['newRFQ']) && $_COOKIE['newRFQ'] == 'click') {
          setcookie( "rows1", mysqli_num_rows($resultsqlRFQ), strtotime( '+30 days' ));
          $_SESSION['newRFQ'] = false;
      } else {
          if(isset($_COOKIE['rows1'])) {
            $row1 = mysqli_num_rows($resultsqlRFQ);
            $row2 = $_COOKIE['rows1'];
          if($row1 == $row2) {
            $_SESSION['newRFQ'] = false;
          } else {
            $_SESSION['newRFQ'] = true;
          }
          } else {
            $row1 = mysqli_num_rows($resultsqlRFQ);
            setcookie( "rows1", $row1, strtotime( '+30 days'));
          }
      }
?>

>

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables3.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.numeric.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker-2.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  //$.widget.bridge('uibutton', $.ui.button);
</script>
<script src="plugins/colorlib/js/main.js"></script>
<!-- load custom js -->
<script type="text/javascript" src="StdMtdv1.0.4.js"></script>

<!-- <script type="text/javascript" src="nehakadam/DateTimePicker.js"></script> -->
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/datetimepicker@latest/dist/DateTimePicker.min.js"></script>

<!-- <script type="text/javascript" src="ic/demo-files/demo2.js"></script> -->


<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php //echo $baseUrl; ?>index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- <span class="logo-mini"><img src="LogoTWCMini2.png" width="50px"></span> -->
      <span class="logo-mini"><img src="dist/img/runmarket logo 2.png" width="40px"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="dist/img/runmarket logo 1.png" width="150px"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <?php
            clearstatcache();
            $fileImg = "dist/img/null.png";

            if(isset($_SESSION['vdcode']))
            {
              if(file_exists("dist/img/".$_SESSION['vdcode'].".png"))
              {
                  $fileImg = "dist/img/".$_SESSION['vdcode'].".png";
              }
              else
              {
                $fileImg = "dist/img/null.png";
              }
            }
            else
            {
              $fileImg = "dist/img/null.png";
            }

          //  return $fileImg;
          ?>

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $fileImg; ?>?timestamp=1357571065" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['vdusername']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $fileImg; ?>?timestamp=1357571065" class="img-circle" alt="User Image">

                <p>
                  <?php echo $_SESSION['vdusername'] . "<br />" . $_SESSION['vdgrpname'] ?>
                </p>
              </li>

              <li class="user-footer">
                <div class="pull-left">
                  <a href="ChangePassword.php"><button class="btn btn-flat" style="background-color: #58585A;color: white">Change Password</button></a>
                </div>
                <div class="pull-right">
                  <a href="logout.php"><button class="btn btn-flat" style="background-color: #3373BA; color:white">Sign out</button></a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

<!-- ==================================================================================================================== -->

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>

        <li>
          <a href="index.php">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gear"></i> <span>General Information</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="Vendor.php"><i class="fa fa-circle-o"></i> Company Information</a></li>
            <li><a href="LegalDoc.php"><i class="fa fa-circle-o"></i> Legal Document</a></li>
            <li><a href="HistoricalOrder.php"><i class="fa fa-circle-o"></i> Historical Order</a></li>
            <li><a href="VendorExpertise.php"><i class="fa fa-circle-o"></i> Company Expertise</a></li>
            <li><a href="VendorProject.php"><i class="fa fa-circle-o"></i> Project Experience </a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-truck "></i> <span>Order</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="MR"><a href="RequestQuotation.php"><i class="fa fa-circle-o"></i> Available Request <span id="newMR" style="background-color: red; color:white; padding:2%; visibility:hidden">New</span></a></li>
            <!-- <li><a href="QtHistory.php"><i class="fa fa-circle-o"></i> Quotation History </a></li> -->
            <li><a href="PurchaseOrder.php"><i class="fa fa-circle-o"></i> Purchase Order</a></li>
            <li><a href="DeliveryHistory.php"><i class="fa fa-circle-o"></i> Delivery</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
              <i class="fa fa-credit-card-alt"></i> <span>Billing</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="Invoice.php">
                <i class="fa fa-circle-o"></i> <span>Invoice</span>
              </a>
            </li>
            <li>
              <a href="Payment.php">
                <i class="fa fa-circle-o"></i> <span>Payment</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
              <i class="fa fa-shopping-bag"></i> <span>Tender</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="Tender.php">
                <i class="fa fa-circle-o"></i> <span>Tender</span>
              </a>
            </li>
            <li>
              <a href="TenderQtHistory.php">
                <i class="fa fa-circle-o"></i> <span>Tender Quotation History</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-shopping-cart"></i> <span>E-Catalogue</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="ECatalogue.php"><i class="fa fa-circle-o"></i> Your Item</a></li>
            <li><a href="ECatalogue.php"><i class="fa fa-circle-o"></i> Add Item</a></li>
          </ul>
        </li>
        
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- ================================================================================================================ -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

    <noscript>
      <div class="alert alert-danger">
        <h4><i class="icon fa fa-warning"></i> Javascript disabled.</h4>
        This site need to use javascript to work properly. Unfortunately, your browser's javascript is disabled. Therefore, please enable javascript. <strong><a href="http://activatejavascript.org/en/instructions" target="_blank">Click here</a></strong> to see how to enable your browser's javascript. Thank you.
      </div>
    </noscript>

    <div class="alert " style="background-color: #58585A; color: white;">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="color: white;">&times;</button>
      <h4><i class="icon fa fa-info"></i> Info</h4>
      Best viewed in at least 10 inch monitor
    </div>

  <?php  //handle new notification
    if(isset($_SESSION['newMR'])){
      if($_SESSION['newMR']){
        echo "<script>
                  $(function (){
                    document.getElementById('newMR').style.visibility = 'visible';
                    $('#MR').click(function(){
                      document.getElementById('newMR').style.visibility = 'hidden';
                      createCookie('newMR', 'click', '1')
                    });
                  });
              </script>";
      }else {
        echo "<script>
                  $(function (){
                    document.getElementById('newMR').style.visibility = 'hidden';
                    $('#MR').click(function(){
                      createCookie('newMR', 'notClick', 1)
                    });
                  });
              </script>";
      }
    }
  ?>
<script>
  function createCookie(name, value, days) {
    var expires;
      
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
      
    document.cookie = escape(name) + "=" + 
        escape(value) + expires + "; path=/";
}
</script>
<?php } // end else

?>
