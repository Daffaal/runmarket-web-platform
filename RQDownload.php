<?php
include "auth.php";
include "conn.php";
include "StdMtd.php";

if (isset($_POST['DocNo'])) {

    $DocNo = $_POST['DocNo'];

    $sql = "SELECT FileName, FileName2, FileName3, FileName4, FileName5, FileName6
            FROM TblMaterialRequestHdr
            WHERE DocNo = '".$DocNo."'";

?>
    <div class="container-fluid">
        <div class="row">
            <table id="tbldownload" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>File</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <?php
                $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                while ($res = mysqli_fetch_assoc($result)) {
                    if (
                        $res['FileName'] === null && $res['FileName2'] === null &&
                        $res['FileName3'] === null && $res['FileName4'] === null &&
                        $res['FileName5'] === null && $res['FileName6'] === null
                    ) {
                        echo "  <tr>
                                    <td>No File Found</td>
                                    <td>#</td>
                                </tr>";
                    } else {

                ?>
                        <tbody>
                            <tr>
                                <td><?= $res['FileName'] ?></td>
                                <td><?php if ($res['FileName'] !== null) { ?>
                                        <a href="download.php?File=<?= $res['FileName'] ?>" class="btn btn-danger"><button type="button" class="fa fa-download"></button> Download</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?= $res['FileName2'] ?></td>
                                <td><?php if ($res['FileName2'] !== null) { ?>
                                        <a href="download.php?File=<?= $res['FileName2'] ?>" class="btn btn-danger"><button type="button" class="fa fa-download"></button> Download</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?= $res['FileName3'] ?></td>
                                <td><?php if ($res['FileName3'] !== null) { ?>
                                        <a href="download.php?File=<?= $res['FileName3'] ?>" class="btn btn-danger"><button type="button" class="fa fa-download"></button> Download</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?= $res['FileName4'] ?></td>
                                <td><?php if ($res['FileName4'] !== null) { ?>
                                        <a href="download.php?File=<?= $res['FileName4'] ?>" class="btn btn-danger"><button type="button" class="fa fa-download"></button> Download</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?= $res['FileName5'] ?></td>
                                <td><?php if ($res['FileName5'] !== null) { ?>
                                        <a href="download.php?File=<?= $res['FileName5'] ?>" class="btn btn-danger"><button type="button" class="fa fa-download"></button> Download</a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?= $res['FileName6'] ?></td>
                                <td><?php if ($res['FileName6'] !== null) { ?>
                                        <a href="download.php?File=<?= $res['FileName6'] ?>" class="btn btn-danger"><button type="button" class="fa fa-download"></button> Download</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        </tbody>
                    <?php } ?>
                <?php } ?>
            </table>
        </div>
    </div>
<?php } ?>