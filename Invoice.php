<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

$host = FTP_HOST;
$user = FTP_USER;
$pass = FTP_PASS;

$NewDocNo = GenerateDocNo(date("Ymd"), "VendorInvoice", "TblVendorInvoiceHdr");

if(isset($_POST['submit']))
{
    $sqlU = "";

    $R1 = array("<p>", "</p>", "'");
    $R2 = array("", "", "\'");
	$R3 = array(" ", ".", ",");
	$R4 = array("", "", ".");

	
	// Header
	$DocNo = $NewDocNo;
	$LocalDocNo = isset($_POST['LocalDocNo']) ? str_replace($R1, $R2, $_POST['LocalDocNo']) : "";
	$DocDt = isset($_POST['Date']) ? str_replace($R1, $R2, $_POST['Date']) : "";
	$EntCode = isset($_POST['EntCode']) ? $_POST['EntCode'] : "";
	$DueDt = isset($_POST['DueDate']) ? str_replace($R1, $R2, $_POST['DueDate']) : "";
	$PaymentType = isset($_POST['PaymentType']) ? str_replace($R1, $R2, $_POST['PaymentType']) : "";
	$TaxInvNo = isset($_POST['TaxNo1']) ? str_replace($R1, $R2, $_POST['TaxNo1']) : "";
	$TaxInvNo2 = isset($_POST['TaxNo2']) ? str_replace($R1, $R2, $_POST['TaxNo2']) : "";
	$TaxInvNo3 = isset($_POST['TaxNo3']) ? str_replace($R1, $R2, $_POST['TaxNo3']) : "";    
	$TaxCode1 = isset($_POST['LueTaxCode1']) ? str_replace($R1, $R2, $_POST['LueTaxCode1']) : "";
	$TaxCode2 = isset($_POST['LueTaxCode2']) ? str_replace($R1, $R2, $_POST['LueTaxCode2']) : "";
	$TaxCode3 = isset($_POST['LueTaxCode3']) ? str_replace($R1, $R2, $_POST['LueTaxCode3']) : "";
	$TaxAlias1 = isset($_POST['Alias1']) ? str_replace($R1, $R2, $_POST['Alias1']) : "";
	$TaxAlias2 = isset($_POST['Alias2']) ? str_replace($R1, $R2, $_POST['Alias2']) : "";
	$TaxAlias3 = isset($_POST['Alias3']) ? str_replace($R1, $R2, $_POST['Alias3']) : "";
	$TaxAmt = strlen($_POST['TotalTax'] > 0) ? str_replace($R3, $R4, $_POST['TotalTax']) : 0;
	$TaxAmt1 = strlen($_POST['TaxAmt1'] > 0) ? str_replace($R3, $R4, $_POST['TaxAmt1']) : 0;
	$TaxAmt2 = strlen($_POST['TaxAmt2'] > 0) ? str_replace($R3, $R4, $_POST['TaxAmt2']) : 0;
	$TaxAmt3 = strlen($_POST['TaxAmt3'] > 0) ? str_replace($R3, $R4, $_POST['TaxAmt3']) : 0;
	$CurCode = isset($_POST['CurCode']) ? str_replace($R1, $R2, $_POST['CurCode']) : "";
	$Amt = strlen($_POST['totalWithTax'] > 0) ? str_replace($R3, $R4, $_POST['totalWithTax']) : str_replace($R3, $R4, $_POST['totalNoTax']) ;
	$Remark = isset($_POST['Remark']) ? str_replace($R1, $R2, $_POST['Remark']) : "";

	$sqlU = "Insert Into TblVendorInvoiceHdr(DocNo, DocDt, LocalDocNo, EntCode, VdCode, DueDt, PaymentType, 
				TaxInvoiceNo, TaxInvoiceNo2, TaxInvoiceNo3, TaxCode1, TaxCode2, TaxCode3, TaxAlias1, TaxAlias2, TaxAlias3, TaxAmt,
				TaxAmt1, TaxAmt2, TaxAmt3, CurCode, Amt, Remark, CreateBy, CreateDt) ";
	$sqlU = $sqlU. "Values('".$DocNo."', '".date_format(date_create($DocDt), "Ymd")."','".$LocalDocNo."', '".$EntCode."', '".$_SESSION['vdcode']."','".date_format(date_create($DueDt), "Ymd")."', '".$PaymentType."', 
				NullIf('".$TaxInvNo."', ''), NullIf('".$TaxInvNo2."', ''), NullIf('".$TaxInvNo3."',''), NullIf('".$TaxCode1."', ''), 
				NullIf('".$TaxCode2."', ''), NullIf('".$TaxCode3."', ''), NullIf('".$TaxAlias1."', ''), NullIf('".$TaxAlias2."', ''),
				NullIf('".$TaxAlias3."', ''), ".$TaxAmt.",".$TaxAmt1.", ".$TaxAmt2.", ".$TaxAmt3.", NullIf('".$CurCode."', ''), ".$Amt.",
				NullIf('".$Remark."', ''), '".$_SESSION['vdcode']."', CurrentDateTime() );";
	//Detail 
	for($i = 0; $i < 999; $i++)
	{
		if(isset($_POST['RecvDocNo' . $i . '']))
		{
			if(!empty($_POST['RecvDocNo' . $i . '']))
			{
				$RecvVdDocNo = isset($_POST['RecvDocNo' . $i . '']) ? str_replace($R1, $R2, $_POST['RecvDocNo' . $i . '']) : "";
				$RecvVdDNo = isset($_POST['RecvDNo' . $i . '']) ? str_replace($R1, $R2, $_POST['RecvDNo' . $i . '']) : "";
				
				$sqlU = $sqlU . "Insert Into TblVendorInvoiceDtl(DocNo, DNo, RecvVdDocNo, RecvVdDNo, CreateBy, CreateDt) ";
				$sqlU = $sqlU . "Values('".$DocNo."', '".substr('000' . ($i + 1), -3)."', '".$RecvVdDocNo."', '".$RecvVdDNo."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";
			}
		}
	}
	//Detail2
	$j = 0;
	$ftpSuccess = false;
	$ftpConn = ftp_connect($host);
	$login = ftp_login($ftpConn,$user,$pass);
	$ftpPasv = ftp_pasv($ftpConn, true);

	for($i = 0; $i < 999; $i++)
	{
		if(isset($_POST['LueDocType' . $i . '']))
		{
			if(!empty($_POST['LueDocType' . $i . '']))
			{
				$DocType = isset($_POST['LueDocType'.$i.'']) ? str_replace($R1, $R2, $_POST['LueDocType'.$i.'']) : "";
				$DocNumber = isset($_POST['DocNo'.$i.'']) ? str_replace($R1, $R2, $_POST['DocNo'.$i.'']) : "";
				$DocInd = isset($_POST['LueDocInd'.$i.'']) ? str_replace($R1, $R2, $_POST['LueDocInd'.$i.'']) : "";
				$TaxInvDt = isset($_POST['TaxDate'.$i.'']) ? str_replace($R1, $R2, $_POST['TaxDate'.$i.'']) : "";
				$Amt = strlen($_POST['Amt'.$i.''] > 0) ? str_replace($R3, $R4, $_POST['Amt'.$i.'']) : 0;
				// $FileName = isset($_POST['VdFile'.$i.'']) ? str_replace($R1, $R2, $_POST['VdFile'.$i.'']) : "";
				$Remark = isset($_POST['Remark'.$i.'']) ? str_replace($R1, $R2, $_POST['Remark'.$i.'']) : "";	
				
				if($_FILES['VdFile' . $i . '']['size'] != 0 && $_FILES['VdFile' . $i . '']['error'] == 0){
					$j++;          
				
					$imgFile = $_FILES['VdFile' . $i . '']['name'];
					$tmp_dir = $_FILES['VdFile' . $i . '']['tmp_name'];
					$imgSize = $_FILES['VdFile' . $i . '']['size'];
					$upload_dir = "dist/pdf/";
					$valid_extension = array('pdf', 'png', 'jpg', 'jpeg');
					$mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;
					
					$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
		
					$VdFilesLoc = $_POST['LueDocType' . $i . ''].'-'.str_pad($j, 3, "0", STR_PAD_LEFT).'-'. date("Ymdhms") .".".$imgExt;
		
					
					if(in_array($imgExt, $valid_extension)) // check kalau extensi nya valid
						{
		
							if($imgSize == 0)
							{
		
								echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</scrip>";
								echo "<script type='text/javascript'>window.location='Invoice.php'</script>";
							}
							else
							{
		
								if($imgSize < $mMaxFile) // if image size < 1025 KB
								{
								  
									// echo "<script type='text/javascript'>alert('".$tmp_dir." || ".$upload_dir.$VdFilesLoc."');</script>";
									move_uploaded_file($tmp_dir, $upload_dir.$VdFilesLoc);
		
		
									$sqlU = $sqlU . "Insert Into TblVendorInvoiceDtl2(DocNo, DNo, DocType, DocNumber, DocInd, TaxInvDt, Amt, FileName, Remark, CreateBy, CreateDt) ";
									$sqlU = $sqlU . "Values('".$DocNo."', '".substr('000' . ($i + 1), -3)."', '".$DocType."', '".$DocNumber."', '".$DocInd."', '".date_format(date_create($TaxInvDt), "Ymd")."', ".$Amt.", '".$upload_dir.$VdFilesLoc."', '".$Remark."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";
		
								}
								else
								{
		
									echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
									echo "<script type='text/javascript'>window.location='Invoice.php'</script>";
								}
								}
						}
						else
						{                  
							echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
							echo "<script type='text/javascript'>window.location='Invoice.php'</script>";
						}
		
					echo "<br>";
		
					if (FTP_STATUS === "enable") {
						try {                  
		
							if ((!$ftpConn) || (!$login)) {
								echo "FTP connection has failed! Attempted to connect to ". $host.".";
							}else {
							  $ftpSuccess = true;
							}
		
							if (!$ftpPasv) {
								echo "Cannot switch to passive mode";
							}
		
							if($login) {
								
								if (!ftp_put($ftpConn, FTP_DESTINATION.$VdFilesLoc, $upload_dir.$VdFilesLoc, FTP_BINARY)) {
									echo "<script>alert('Error uploading ". $VdFilesLoc .".');</script>";
								}
		
							}
						} catch (Exception $e) {
							echo 'Caught exception: ',  $e->getMessage(), "\n";
						}
					}
				}else{
					$sqlU = $sqlU . "Insert Into TblVendorInvoiceDtl2(DocNo, DNo, DocType, DocNumber, DocInd, TaxInvDt, Amt, FileName, Remark, CreateBy, CreateDt) ";
					$sqlU = $sqlU . "Values('".$DocNo."', '".substr('000' . ($i + 1), -3)."', '".$DocType."', '".$DocNumber."', '".$DocInd."', '".date_format(date_create($TaxInvDt), "Ymd")."', '".$Amt."', NullIf('".$_POST['VdFile'.$i.'']."', '') , '".$Remark."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";
				}				
			}
		}
		//else
		//{
		//	break;
		//}
	}

	//echo $sqlU;
    $expSQL = explode(";", $sqlU);
	$countSQL = count($expSQL);
	$success = false;

	// var_dump($expSQL);
	for($i = 0; $i < $countSQL - 1; $i++)
	{
		if (mysqli_query($conn, $expSQL[$i])) {
			$success = true;
		} else {
			echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
		}
	}

	if($success)
	{
		mysqli_commit($conn);
		echo "<script type='text/javascript'>window.location='Invoice.php'</script>";
	}
}else if(isset($_REQUEST['d'])){

	
	$DocNo = $_REQUEST['d'];
	if(isset($_POST['update'])){

		$CancelReason = $_POST['CancelTxt'];
		$sql = "";
		$sql = "UPDATE TblVendorInvoiceHdr A SET A.CancelInd = 'Y', A.CancelReason = NullIf('".$CancelReason."', ''), 
				A.LastUpBy = '".$_SESSION['vdcode']."', A.LastUpDt = CurrentDateTime()
				WHERE A.DocNo = '".$DocNo."'";
		mysqli_query($conn, $sql) or die(mysqli_error($conn));
		mysqli_commit($conn);
		echo "<script type='text/javascript'>window.location='Payment.php'</script>";
	}
	$sql = "";

    $sql = $sql . "SELECT T.EntName, T.DocNo, T.DocDt,T.LocalDocNo, T.Status, T.Amt, T.TaxAmt, T.TotalNonTax,T.PaymentType,
	T.DueDt, T.CurCode, T.Remark, T.VdCode, T.EntCode, T.TaxCode1 , T.TaxCode2 , T.TaxCode3,
    T.TaxInvoiceNo , T.TaxInvoiceNo2 , T.TaxInvoiceNo3 , T.TaxAlias1 , T.TaxAlias2 , T.TaxAlias3 , 
    T.TaxAmt1 , T.TaxAmt2 , T.TaxAmt3, T.DocType , T.DocNumber , T.TaxInvDt , T.DocInfoAmt , T.FileName , 
    T.DocInfoRemark , T.DocInd
	FROM
	(
		SELECT C.EntName, B.LocalDocNo, B.Remark , B.DocNo , Date_Format(B.DocDt , '%d-%m-%Y') DocDt, B.VdCode,
		case B.InvoiceStatusInd
			when 'A' then 'Approved'
			when 'P' then 'Processed'
			when 'C' then 'Cancelled'
			when 'O' then 'Outstanding'
		END AS Status, B.PaymentType , Date_Format(B.DueDt, '%d-%m-%Y') DueDt, B.CurCode, B.EntCode,
		IFNULL(D.Amt, 0.00) Amt, IFNULL(D.TaxAmt, 0.00) TaxAmt, B.TaxCode1 , B.TaxCode2 , B.TaxCode3,
		B.TaxInvoiceNo , B.TaxInvoiceNo2 , B.TaxInvoiceNo3 , B.TaxAlias1 , B.TaxAlias2 , B.TaxAlias3 , 
		B.TaxAmt1 , B.TaxAmt2 , B.TaxAmt3 , F.DocType , F.DocNumber , F.TaxInvDt , F.Amt DocInfoAmt, F.FileName , 
		F.Remark DocInfoRemark, F.DocInd,
		IFNULL(D.TotalNonTax, 0.00) TotalNonTax
		FROM tblvendorinvoicedtl A
		inner JOIN tblvendorinvoicehdr B ON A.DocNo = B.DocNo
			AND B.CancelInd = 'N'
			AND B.VdCode = '".$_SESSION['vdcode']."'
		inner JOIN tblentity C ON B.EntCode = C.EntCode
		left join tblvendorinvoicedtl2 F on A.DocNo = F.DocNo and A.DNo = F.DNo
		LEFT JOIN
		(
			SELECT Distinct T1.DocNo, T1.DNo, T4.DocNo PIDocNo, T2.DocDt PIDocDt, T2.Amt, T2.TaxAmt,
			IFNULL(T2.Amt - T2.TaxAmt, 0.00) TotalNonTax
			FROM tblvendorinvoicedtl T1
			inner Join TblVendorInvoiceHdr T2 On T1.DocNo = T2.DocNo And T2.CancelInd = 'N'
			left JOIN tblpurchaseinvoicedtl T3 ON T1.RecvVdDocNo = T3.RecvVdDocNo AND T1.RecvVdDNo = T3.RecvVdDNo
			left JOIN tblpurchaseinvoicehdr T4 ON T1.DocNo = T4.VdInvNo
				And T4.CancelInd = 'N'
				And T4.Status = 'A'
		) D ON A.DocNo = D.DocNo And A.DNo = D.DNo
		LEFT JOIN
		(
			SELECT T1.DocNo, IFNULL(Sum(T5.Amt), 0.00) VoucherOPAmt
			From TblVendorInvoiceHdr T1
			inner Join TblPurchaseInvoiceHdr T2 On T1.DocNo = T2.VdInvNo
				And T2.CancelInd = 'N'
				And T2.Status = 'A'
				And T1.CancelInd = 'N'
			inner JOIN TblOutgoingPaymentDtl T3 ON T2.DocNo = T3.InvoiceDocno
			inner JOIN TblOutgoingPaymentHdr T4 ON T3.DocNo = T4.DocNo
				And T4.CancelInd = 'N'
				And T4.Status = 'A'
			inner JOIN tblvoucherhdr T5 ON T4.VoucherRequestDocNo = T5.VoucherRequestDocNo
				And T5.CancelInd = 'N'
			Group By T1.DocNo
		) E ON A.DocNo = E.DocNo
	) T
	Where T.DocNo = '".$DocNo."'
	ORDER BY T.DocNo;
	";
    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

    while($res = mysqli_fetch_assoc($result))
    {
        $VdCode = $res['VdCode'];
        $Invoice = $res['LocalDocNo'];
        $EntCode = $res['EntCode'];
        $Status = $res['Status'];
        $DocDt = $res['DocDt'];
        $DueDt = $res['DueDt'];
        $PaymentType = $res['PaymentType'];
        $CurCode = $res['CurCode'];
        $TotalTax = number_format($res['TaxAmt'], 2, '.', ',');
        $TotalWithTax = number_format($res['Amt'], 2, '.', ',');
        $TotalNonTax = number_format($res['TotalNonTax'], 2, '.', ',');
		$Remark = $res['Remark'];
		$TaxAmt1 = $res['TaxAmt1'];
		$TaxAmt2 = $res['TaxAmt2'];
		$TaxAmt3 = $res['TaxAmt3'];
		$TaxCode1 = $res['TaxCode1'];
		$TaxCode2 = $res['TaxCode2'];
		$TaxCode3 = $res['TaxCode3'];
		$TaxInvNo = $res['TaxInvoiceNo'];
		$TaxInvNo2 = $res['TaxInvoiceNo2'];
		$TaxInvNo3 = $res['TaxInvoiceNo3'];
		$TaxAlias1 = $res['TaxAlias1'];
		$TaxAlias2 = $res['TaxAlias2'];
		$TaxAlias3 = $res['TaxAlias3'];
		$DocType = $res['DocType'];
		$DocNumber = $res['DocNumber'];
		$TaxInvDt = $res['TaxInvDt'];
		$DocInfoAmt = $res['DocInfoAmt'];
		$FileName = $res['FileName'];
		$DocInd = $res['DocInd'];
		$DocInfoRemark = $res['DocInfoRemark'];
?>	
<style>
.hidden {
    display: none;
}

.selected {
    background-color: #bbdefb;
}
.modal-body {
	overflow-y: auto;
	overflow-x: auto;
}
</style>
<form class="form-horizontal" name="insertVendor" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-body">				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Document#</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="DocNo" id="DocNo" readonly value="<?= $DocNo ?>"/>
					</div>									
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Cancel Reason</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="CancelTxt" id="CancelTxt" <?= ($Status != 'Outstanding') ? 'readonly' : '' ?> />
					</div>
					<div class="col-sm-3">
						<input type="checkbox" class="checkbox" name="CancelInd" id="CancelInd" <?= ($Status != 'Outstanding') ? 'disabled' : '' ?> />
					</div>
					<div class="col-sm-5">
						<span class="error-message" id="eCancel"></span>
					</div>									
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Invoice#</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="LocalDocNo" id="LocalDocNo" readonly value="<?= $Invoice ?>"/>
					</div>
					<div class="col-sm-5">
						<span class="error-message" id="eVdName"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label mandatory-field">Entity</label>
					<div class="col-md-5 col-xs-5">
						<select required class="form-control selectPayType" name="EntCode" id="EntCode" disabled style="width: 100%;">
							<option value=""></option>
							<?php

								$sql = "";
								$sql = "select EntCode as Col1, EntName as Col2 from TblEntity";

								$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));

								while($res = mysqli_fetch_assoc($result))
								{
									if($EntCode == $res['Col1'])
										echo "<option value='".$res['Col1']."' selected";								
									else
									echo "<option value='".$res['Col1']."'";								
									echo ">".$res['Col2']."</option>";
								}
							?>
						</select>                        
					</div> 
					<div class="col-sm-5">
						<span class="error-message" id="eShortName"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label mandatory-field">Date</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="Date" id="Date" required data-field="date" readonly value="<?= $DocDt ?>"/>
					</div>
					<div id="DteExpiredDt"></div>
					<div class="col-sm-5">
						<span class="error-message" id="eShortName"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label mandatory-field">Payment Type</label>
					<div class="col-md-5 col-xs-5">
						<select required class="form-control selectPayType" name="PaymentType" id="PaymentType" disabled style="width: 100%;">
							<option value=""></option>
							<?php

								$sql = "";
								$sql = "select OptCode As Col1, OptDesc As Col2
								from TblOption 
								where OptCat = 'VoucherPaymentType'";

								$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));

								while($res = mysqli_fetch_assoc($result))
								{
									if($PaymentType == $res['Col1'])
										echo "<option value='".$res['Col1']."' selected";								
									else
									echo "<option value='".$res['Col1']."'";								
									echo ">".$res['Col2']."</option>";
								}
							?>
						</select>                        
					</div> 
					<div class="col-sm-5">
						<span class="error-message" id="eShortName"></span>
					</div>
				</div>							
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Due Date</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="DueDate" id="DueDate" data-field="date" readonly value="<?= $DueDt ?>"/>
					</div>
					<div id="DteExpiredDt"></div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Currency</label>
					<div class="col-sm-5">
					<input type="text" class="form-control" name="CurCode" id="CurCode" readonly="readonly" readonly value="<?= $CurCode ?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label ">Total Without Tax</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="totalNoTax" id="totalNoTax" readonly="readonly" value="<?= $TotalNonTax ?>" style="text-align: right;" />
					</div>
				</div>												
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label ">Total With Tax</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="totalWithTax" id="totalWithTax" readonly="readonly" value="<?= $TotalWithTax ?>" style="text-align: right;" />
					</div>
				</div>                    
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Total Tax</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="TotalTax" id="totalTax" readonly="readonly" value="<?= $TotalTax ?>" style="text-align: right;"/>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Invoice Status</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="InvoiceStatus" id="InvoiceStatus" readonly="readonly" value="<?= $Status?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label ">Remark</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="Remark" id="Remark" readonly value="<?= $Remark ?>"  />
					</div>
				</div>
			</div>
		</div>
	</div> <!-- /.col-md-12 -->
</div> <!-- /.row -->

<div class="row">
	<div class="col-xs-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#ContactPerson" data-toggle="tab">List of Received</a></li>
				<li><a href="#Bank" data-toggle="tab">Tax</a></li>
				<li><a href="#ItCt" data-toggle="tab">Document Info</a></li>				
			</ul>
			<div class="tab-content">
				<div class="active tab-pane" id="ContactPerson">
					<table class="table ListRecv" id="tblBankContactPerson">
						<thead>
							<tr>
								<th>Received#</th>
								<th>PO#</th>
								<th>Item Name</th>
								<th>Qty</th>
								<th>UoM</th>
								<th>Unit Price</th>
								<th>Total Price</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$sql = "Select A.DocNo, B.DNo, C.DocNo RecvVdDocNo, C.DNo RecvVdDNo, Date_Format(D.DocDt, '%d-%m-%Y') DocDt, E.ItName, E.InventoryUomCode,
								Date_Format(B.EstRecvDt, '%d-%m-%Y') EstRecvDt, C.Qty RecvQty, I.DTName,
								A.CurCode, H.UPrice, (H.UPrice * C.Qty) as TotalPrice
								From TblPOHdr A
								Inner Join TblPODtl B On A.DocNo = B.DocNo
								Inner Join TblRecvVdDtl C On B.DocNo = C.PODocNo And B.DNo = PODNo
									And C.`Status` = 'A'
									And C.CancelInd = 'N'
								Inner Join TblRecvVdHdr D On C.DocNo = D.DocNo
								Inner Join TblItem E On C.ItCode = E.ItCode
								Inner Join TblPORequestDtl F On B.PORequestDocNo = F.DocNo And B.PORequestDNo = F.DNo
								Inner Join TblQtHdr G On F.QtDocNo = G.DocNo And G.MaterialRequestDocNo Is Not Null and G.VdCode = '".$_SESSION['vdcode']."'
								Inner Join TblQtDtl H On G.DocNo = H.DocNo And F.QtDNo = H.DNo
								Left Join TblDeliveryType I On G.DTCode = I.DTCode
								inner join tblmaterialrequesthdr J on F.MaterialRequestDocNo = J.DocNo
								inner join tblsite K on J.SiteCode = K.SiteCode
								inner join tblprofitcenter L on K.ProfitCenterCode = L.ProfitCenterCode
								inner join tblentity M on L.EntCode = M.EntCode 
								inner join tblvendorinvoicedtl N on C.DocNo = N.RecvVdDocNo and C.DNo = N.RecvVdDNo and N.DocNo = '".$DocNo."'
								where D.VdCode = '".$_SESSION['vdcode']."' and M.EntCode = '".$EntCode."'
								Order by A.DocNo, B.DNo, C.DocNo, C.DNo;";

								$result = mysqli_query($conn, $sql);
								while ($res = mysqli_fetch_assoc($result)) {
									echo "<tr>";
									echo "<td>".$res['RecvVdDocNo']."</td>";
									echo "<td>".$res['DocNo']."</td>";
									echo "<td>".$res['ItName']."</td>";
									echo "<td>".$res['RecvQty']."</td>";
									echo "<td>".$res['InventoryUomCode']."</td>";
									echo "<td style='text-align: right'>". number_format($res['UPrice'], 2, ",", ".") ."</td>";
									echo "<td style='text-align: right'>". number_format($res['TotalPrice'], 2, ",", ".") ."</td>";
									echo "</tr>";
								}
							?>
						</tbody>						
					</table>
					<input type="button" class="btn btn-lg btn-default " disabled id="recvList" value="Add List" />
				</div>
				
				<div class="tab-pane" id="Bank">
					<table class="table BankAccount" id="tblBankAccount">
						<thead>
							<tr>
								<th style="width: 20%;">Tax</th>
								<th>Tax#</th>
								<th>Alias</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<?php 
										if(strlen($TaxCode1) >0){
											$sqlB = "select TaxCode , TaxName as Col2, TaxRate as Col1 from TblTax where TaxCode = '".$TaxCode1."';";
											$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
											while($resB = mysqli_fetch_assoc($resultB))
											{	?>								
												<p class="form-control"><?= $resB['Col2'] ?></p>
											<?php }
										}else{
											echo '<p class="form-control"></p>';
										}
									?>
								</td>
								<td><p class="form-control"><?= $TaxInvNo ?></p></td>
								<td><p class="form-control"><?= $TaxAlias1 ?></p></td>					
								<td><p class="form-control" style="text-align: right;"><?= number_format($TaxAmt1, 2, '.', ',') ?></p></td>
							</tr>
							<tr>
							<td>
									<?php 
										if(strlen($TaxCode2) >0){
											$sqlB = "select TaxCode , TaxName as Col2, TaxRate as Col1 from TblTax where TaxCode = '".$TaxCode2."';";
											$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
											while($resB = mysqli_fetch_assoc($resultB))
											{	?>								
												<p class="form-control"><?= $resB['Col2'] ?></p>
											<?php }
										}else{
											echo '<p class="form-control"></p>';
										}
									?>
								</td>
								<td><p class="form-control"><?= $TaxInvNo2 ?></p></td>
								<td><p class="form-control"><?= $TaxAlias2 ?></p></td>					
								<td><p class="form-control" style="text-align: right;"><?= number_format($TaxAmt2, 2, '.', ',') ?></p></td>
							</tr>
							<tr>
							<td>
									<?php 
										if(strlen($TaxCode3) >0){
											$sqlB = "select TaxCode , TaxName as Col2, TaxRate as Col1 from TblTax where TaxCode = '".$TaxCode3."';";
											$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
											while($resB = mysqli_fetch_assoc($resultB))
											{	?>								
												<p class="form-control"><?= $resB['Col2'] ?></p>
											<?php }
										}else{
											echo '<p class="form-control"></p>';
										}
									?>
								</td>
								<td><p class="form-control"><?= $TaxInvNo3 ?></p></td>
								<td><p class="form-control"><?= $TaxAlias3 ?></p></td>					
								<td><p class="form-control" style="text-align: right;"><?= number_format($TaxAmt3, 2, '.', ',') ?></p></td>
							</tr>			
						</tbody>						
					</table>
				</div>

				<div class="tab-pane" id="ItCt">
					<span id="eTblItCt" class="error-message"></span>
					<table class="table ItCt" id="tblItCt">
						<thead>
							<tr>
								<th style="width: 12%;">Document Type</th>
								<th style="width: 5%;">Document#</th>
								<th style="width: 8%;">Tax Invoice Date</th>
								<th style="width: 10%;">Amount</th>
								<th style="width: 10%;">Indicator</th>
								<th style="width: 20%;">Remark</th>
								<th style="width: 20%;">File</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$sql = "";
								$sql = "SELECT *, Date_Format(A.TaxInvDt, '%d-%m-%y') TaxInvDt from TblVendorInvoiceDtl2 A Where A.DocNo = '".$DocNo."'";
								$result = mysqli_query($conn, $sql);
								while ($res = mysqli_fetch_assoc($result)) {
									$i = 0;
									echo "<tr>";
									echo "<td>";
									$sqlB = "Select OptCode As Col1, OptDesc As Col2 From TblOption where OptCat ='PurchaseInvoiceDocType' and OptCode = '".$res['DocType']."' ";
										$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
										while($resB = mysqli_fetch_assoc($resultB))
										{
										?>
											<p class="form-control"><?= $resB['Col2'] ?></p>
										<?php
										}	
									echo "</td>";	
									echo"<td><p class='form-control'>".$res['DocNumber']."</p></td>";
									echo"<td><p class='form-control'>".$res['TaxInvDt']."</p></td>";
									echo"<td><p class='form-control'>".number_format($res['Amt'], 2, '.', ',')."</p></td>";
									echo "<td>";
									$sqlB = "Select OptCode As Col1, OptDesc As Col2 From TblOption where OptCat ='PurchaseInvoiceDocInd' and OptCode = '".$res['DocInd']."' ";
										$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
										while($resB = mysqli_fetch_assoc($resultB))
										{
										?>
											<p class="form-control"><?= $resB['Col2'] ?></p>
										<?php
										}	
									echo "</td>";
									echo"<td><p class='form-control'>".$res['Remark']."</p></td>";
									echo"<td><p class='form-control'>".basename($res['FileName'])."</p></td>";

									echo "</tr>";
									$i++;
								}
							?>
							<input type='hidden' name='countDoc' id='countDoc' value="0"/>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="8" style="text-align: left;">
									<button type="button" class="btn btn-lg btn-block " id="ItCtaddrow" value="Add Row"  />
								</td>
							</tr>
							<tr>
							</tr>
						</tfoot>
					</table>
				</div>
			
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- /.nav-tabs-custom -->

		<hr />

        <button type="button" class="btn btn-default" onclick="return window.location.href = 'Payment.php';" id="BtnCancel" background-color: #58585A; color:white">Back</button>
        <button type="submit" name="update" class="btn pull-right" id="BtnUpdate" style="background-color: #3373BA; color:white">Update</button>

	</div>
</div>

</form>

<script src="StdMtdv1.0.4.js"></script>
<script>
	document.getElementsByClassName('amt1').disabled = true;
  $(function () {
    //Initialize Select2 Elements
	$(".selectVdCategory").select2();
    $(".selectVillage").select2();
    $(".selectCity").select2();
	$(".selectSubDistrict").select2();
	$(".selectPayType").select2();
	$(".selectHO").select2();
	$(".selectTax1").select2();
	$(".selectTax2").select2();
	$(".selectTax3").select2();


	$("#DteExpiredDt").DateTimePicker();
	
  });
</script>

<script type="text/javascript">

	$(function() {
		$('table tr').each(function() {
			$(this).find('td').each(function() {
				$(this).find('input,select').attr("disabled", false);
				$(this).find('input,select').attr("readonly", false);
			});
		});
	});	

	$('#recvList').on('click',function(){
		console.log(getCookie("EntCode"));
		if(getCookie("EntCode")){
			$("#DetailTable .select :checked").attr("checked", false);
			$('#received').modal("show");
		}else {
			alert("Entity still empty.");
		}
	});
</script>
<script type="text/javascript">
	$(document).ready(function () {
		
		//Tax

			var totalNonTax = $("#totalNonTax").val();
			$("table.BankAccount").on('change','#LueTaxCode1',function(){
				var getValue = $('#totalNoTax').val();
				var split = getValue.split(",");
				var TotalNonTax = split[0].replaceAll(".", "");
				var tax = document.querySelector('#LueTaxCode1').value;
				var calcTax = (parseInt(TotalNonTax)) * (parseFloat(tax)/100);																

				if(Number.isNaN(calcTax)){
					$(".amt1").val("");	
				}else {
					$(".amt1").val(calcTax);							
				}

				var tax1 = $(".amt1").val();
				var tax2 = $(".amt2").val();
				var tax3 = $(".amt3").val();


				if(tax1 != "" && tax2 != "" && tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax2) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" && tax2 == "" && tax3 !=""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 == "" && tax2 != "" && tax3 != "") {
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax2) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax2) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 == ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax2)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax2))+ ",00";
					$("#totalWithTax").val(totalWithTax);
					$("#totalTax").val(totalTax);
				}else if(tax2 != "" && tax1 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax2))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" || tax2 != "" || tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1 != "" ? tax1 : 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1 != "" ? tax1: 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else {
					$("#totalWithTax").val("");
					$("#totalTax").val("");
				}						
			});

			$("table.BankAccount").on('change','#LueTaxCode2',function(){
				var getValue = $('#totalNoTax').val();
				var split = getValue.split(",");
				var TotalNonTax = split[0].replaceAll(".", "");
				var tax = document.querySelector('#LueTaxCode2').value;
				var calcTax = (parseInt(TotalNonTax)) * (parseFloat(tax)/100);																

				if(Number.isNaN(calcTax)){
					$(".amt2").val("");	
				}else {
					$(".amt2").val(calcTax);							
				}

				var tax1 = $(".amt1").val();
				var tax2 = $(".amt2").val();
				var tax3 = $(".amt3").val();

				if(tax1 != "" && tax2 != "" && tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax2) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" && tax2 == "" && tax3 !=""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 == "" && tax2 != "" && tax3 != "") {
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax2) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax2) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax2))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 == ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax2)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax2))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" || tax2 != "" || tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1 != "" ? tax1 : 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1 != "" ? tax1: 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else{
					$("#totalWithTax").val("");
					$("#totalTax").val("");
				}
			});

			$("table.BankAccount").on('change','#LueTaxCode3',function(){
				var getValue = $('#totalNoTax').val();
				var split = getValue.split(",");
				var TotalNonTax = split[0].replaceAll(".", "");
				var tax = document.querySelector('#LueTaxCode3').value;
				var calcTax = (parseInt(TotalNonTax)) * (parseFloat(tax)/100);																

				if(Number.isNaN(calcTax)){
					$(".amt3").val("");	
				}else {
					$(".amt3").val(calcTax);							
				}

				var tax1 = $(".amt1").val();
				var tax2 = $(".amt2").val();
				var tax3 = $(".amt3").val();

				if(tax1 != "" && tax2 != "" && tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2) + parseInt(tax3)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1) + parseInt(tax2) + parseInt(tax3));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" && tax2 == "" && tax3 !=""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax3)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1) + parseInt(tax3));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 == "" && tax2 != "" && tax3 != "") {
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax2) + parseInt(tax3)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax2) + parseInt(tax3));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1) + parseInt(tax2));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 == ""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax2)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax2));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" || tax2 != "" || tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1 != "" ? tax1 : 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1 != "" ? tax1: 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else{
					$("#totalWithTax").val("");
					$("#totalTax").val("");
				}
			});
		//End of Tax
		
		$("table.BankAccount").on("click", ".BAbtnDel", function (event) {
			var TotalNonTax = $('#totalNoTax').val();
			$(this).closest("tr").remove();       
			counterTax -= 1
			
			// var tax = $(".amt"+String(counterTax-=1)).val();
			// $("#totalWithTax").val(totalWithTax - tax);
			// $("#totalTax").val(totalTax - tax);
			if(counterTax == 2){
				var tax1 = $(".amt"+String(counterTax-2)).val();
				var tax2 = $(".amt"+String(counterTax-1)).val();
				var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2)));
				var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1) + parseInt(tax2));
				$("#totalWithTax").val(totalWithTax);
				$("#totalTax").val(totalTax);
			}else {
				var tax1 = $(".amt"+String(counterTax-1)).val();
				if(isNaN(tax1)){
					$("#totalWithTax").val("");
					$("#totalTax").val("");
				}else {				
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1));
					$("#totalWithTax").val(totalWithTax);
					$("#totalTax").val(totalTax);
				}
			}
		});	

		var countDoc = document.getElementById("countDoc").value;
		var counterDoc = Number(countDoc);

		$("#ItCtaddrow").on("click", function () {
			var newRow = $("<tr>");
			var cols = "";

			cols += '<td><select class="form-control selectType' + counterDoc + '" name="LueDocType' + counterDoc + '" id="LueDocType' + counterDoc + '" style="width: 100%;">';
			cols += '<option value=""></option>';
			<?php
				$sqlB = "Select OptCode As Col1, OptDesc As Col2 From TblOption where OptCat ='PurchaseInvoiceDocType' ";
				$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
				while($resB = mysqli_fetch_assoc($resultB))
				{
				?>
					cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
				<?php
				}
			?>
			cols += '</select></td>';
			cols += '<td><input type="text" class="form-control" name="DocNo' + counterDoc + '" id="DocNo' + counterDoc + '"/></td>';
			cols += '<td><input type="text" class="form-control" name="TaxDate' + counterDoc + '" id="TaxDate' + counterDoc + '" data-field="date"/><div id="DteExpiredDt"></div></td>';
			cols += '<td><input type="text" class="form-control" name="Amt' + counterDoc + '" id="Amt' + counterDoc + '" style="text-align: right;"/></td>';
			cols += '<td><select class="form-control selectInd' + counterDoc + '" name="LueDocInd' + counterDoc + '" id="LueDocInd' + counterDoc + '" style="width: 100%;">';
			cols += '<option value=""></option>';
			<?php
				$sqlB = "Select OptCode As Col1, OptDesc As Col2 From TblOption where OptCat ='PurchaseInvoiceDocInd' ";
				$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
				while($resB = mysqli_fetch_assoc($resultB))
				{
				?>
					cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
				<?php
				}
			?>
			cols += '</select></td>';
			cols += '<td><input type="text" class="form-control" name="Remark' + counterDoc + '" id="Remark' + counterDoc + '" /></td>';
			cols += '<td><input type="file" name="VdFile' + counterDoc + '" id="VdFile' + counterDoc + '" accept="application/pdf, image/png, image/jpg, image/jpeg" style="width:180px;" />';

			cols += '<td><input type="button" class="ItCtbtnDel btn btn-md btn-danger" value="Delete"></td>';
			newRow.append(cols);
			$("table.ItCt").append(newRow);

			$("#Amt" + counterDoc).on('keypress', function(e){
				if(this.selectionStart || this.selectionStart == 0){
					// selectionStart won't work in IE < 9
					if (e.which < 48 || e.which > 57)
					{
						e.preventDefault();
					}else {
						var key = e.which;
						var prevDefault = true;
						
						var thouSep = " ";  // your seperator for thousands
						var deciSep = ",";  // your seperator for decimals
						var deciNumber = 0; // how many numbers after the comma
						
						var thouReg = new RegExp(thouSep,"g");
						var deciReg = new RegExp(deciSep,"g");
						
						function spaceCaretPos(val, cPos){
							/// get the right caret position without the spaces
							
							if(cPos > 0 && val.substring((cPos-1),cPos) == thouSep)
							cPos = cPos-1;
							
							if(val.substring(0,cPos).indexOf(thouSep) >= 0){
							cPos = cPos - val.substring(0,cPos).match(thouReg).length;
							}
							
							return cPos;
						}
						
						function spaceFormat(val, pos){
							/// add spaces for thousands
							
							if(val.indexOf(deciSep) >= 0){
								var comPos = val.indexOf(deciSep);
								var int = val.substring(0,comPos);
								var dec = val.substring(comPos);
							} else{
								var int = val;
								var dec = "";
							}
							var ret = [val, pos];
							
							if(int.length > 3){
								
								var newInt = "";
								var spaceIndex = int.length;
								
								while(spaceIndex > 3){
									spaceIndex = spaceIndex - 3;
									newInt = thouSep+int.substring(spaceIndex,spaceIndex+3)+newInt;
									if(pos > spaceIndex) pos++;
								}
								ret = [int.substring(0,spaceIndex) + newInt + dec, pos];
							}
							return ret;
						}
						
						$(this).on('keyup', function(ev){
							
							if(ev.which == 8){
								// reformat the thousands after backspace keyup
								
								var value = this.value;
								var caretPos = this.selectionStart;
								
								caretPos = spaceCaretPos(value, caretPos);
								value = value.replace(thouReg, '');
								
								var newValues = spaceFormat(value, caretPos);
								this.value = newValues[0];
								this.selectionStart = newValues[1];
								this.selectionEnd   = newValues[1];
							}
						});
						
						if((e.ctrlKey && (key == 65 || key == 67 || key == 86 || key == 88 || key == 89 || key == 90)) ||
							(e.shiftKey && key == 9)) // You don't want to disable your shortcuts!
							prevDefault = false;
						
						if((key < 37 || key > 40) && key != 8 && key != 9 && prevDefault){
							e.preventDefault();
							
							if(!e.altKey && !e.shiftKey && !e.ctrlKey){
							
								var value = this.value;
								if((key > 95 && key < 106)||(key > 47 && key < 58) ||
								(deciNumber > 0 && (key == 110 || key == 188 || key == 190))){
									
									var keys = { // reformat the keyCode
									48: 0, 49: 1, 50: 2, 51: 3,  52: 4,  53: 5,  54: 6,  55: 7,  56: 8,  57: 9,
									96: 0, 97: 1, 98: 2, 99: 3, 100: 4, 101: 5, 102: 6, 103: 7, 104: 8, 105: 9,
									110: deciSep, 188: deciSep, 190: deciSep
									};
									
									var caretPos = this.selectionStart;
									var caretEnd = this.selectionEnd;
									
									if(caretPos != caretEnd) // remove selected text
									value = value.substring(0,caretPos) + value.substring(caretEnd);
									
									caretPos = spaceCaretPos(value, caretPos);
									
									value = value.replace(thouReg, '');
									
									var before = value.substring(0,caretPos);
									var after = value.substring(caretPos);
									var newPos = caretPos+1;
									
									if(keys[key] == deciSep && value.indexOf(deciSep) >= 0){
										if(before.indexOf(deciSep) >= 0){ newPos--; }
										before = before.replace(deciReg, '');
										after = after.replace(deciReg, '');
									}
									var newValue = before + keys[key] + after;
									
									if(newValue.substring(0,1) == deciSep){
										newValue = "0"+newValue;
										newPos++;
									}
									
									while(newValue.length > 1 && 
									newValue.substring(0,1) == "0" && newValue.substring(1,2) != deciSep){
										newValue = newValue.substring(1);
										newPos--;
									}
									
									if(newValue.indexOf(deciSep) >= 0){
										var newLength = newValue.indexOf(deciSep)+deciNumber+1;
										if(newValue.length > newLength){
										newValue = newValue.substring(0,newLength);
										}
									}
									
									newValues = spaceFormat(newValue, newPos);
									
									this.value = newValues[0];
									this.selectionStart = newValues[1];
									this.selectionEnd   = newValues[1];
								}
							}
						}
						
						$(this).on('blur', function(e){
							
							if(deciNumber > 0){
								var value = this.value;
								
								var noDec = "";
								for(var i = 0; i < deciNumber; i++)
								noDec += "0";
								
								if(value == "0"+deciSep+noDec)
								this.value = ""; //<-- put your default value here
								else
								if(value.length > 0){
									if(value.indexOf(deciSep) >= 0){
										var newLength = value.indexOf(deciSep)+deciNumber+1;
										if(value.length < newLength){
										while(value.length < newLength){ value = value+"0"; }
										this.value = value.substring(0,newLength);
										}
									}
									else this.value = value + deciSep + noDec;
								}
							}
						});
					}
				}
			});

			for(var j = 0; j <= counterDoc; j++)
				$(".selectType" + j + "").select2();

			for(var j = 0; j <= counterDoc; j++)
				$(".selectInd" + j + "").select2();


			counterDoc++;
		});

		$("table.ItCt").on("click", ".ItCtbtnDel", function (event) {
			$(this).closest("tr").remove();       
			counterItCt -= 1
		});

		var blankComboBox = "<option value=''></option>";

		$("#LueSDCode").change(function(){
			var SDCode = $("#LueSDCode").val();
			$.ajax({
				url: "GetVillage.php",
				data: "SDCode="+SDCode,
				cache: false,
				success: function(msg){
					$("#LueVillageCode").attr("disabled", false);
					$("#LueVillageCode").html(msg);
				}
			});
		});

	});
</script>

<script>
	var isDisabled = $("#CancelTxt").is('[readonly]');
	if(isDisabled == true){
		$("#BtnUpdate").attr('disabled', true);
	}
	function validate() {
		var error="";
		var cancelText = $("#CancelTxt").is('[readonly]');
		var cancelInd = $("#CancelInd").prop('checked');
		if(cancelText == false){
			if($('#CancelTxt').val() == "" || !cancelInd)
			{
				error = " Field is required. ";
				document.getElementById( "eCancel" ).innerHTML = error;
				document.getElementById( "eCancel" ).scrollIntoView();
				return false;
			}else {
				error = "";
				document.getElementById( "eLocalDocNo" ).innerHTML = error;
			}
		}
	}
</script>
<script>
	$('#DetailTable').DataTable({
		scrollX: true,
		scrollY: true,
		responsive: true,
		scrollCollapse: true,
		paging:         true,
		columnDefs: [
			{ width: 200, targets: 0 }
		],
	});
	function run(sel) {

		var i = sel.selectedIndex;
		if (i != -1) {
			//document.getElementById("car").value = sel.options[i].text;

			document.cookie = "EntCode="+sel.options[i].value;
			console.log(sel.options[i].value);
			location.reload();
		}

	}
</script>
<script>
    'use strict';
     $('#DetailTable tbody tr').click(function (e) {
        //when the row is clicked...
        var self = $(this), //cache this
            checkbox = self.find('.select > input[type=checkbox]'), //get the checkbox
            isChecked = checkbox.prop('checked'); //and the current state
        if (!isChecked) {
            //about to be checked so clear all other selections
            $('#DetailTable .select > input[type=checkbox]').prop('checked', false).parents('tr').removeClass('selected');
        }
        checkbox.prop('checked', !isChecked).parents('tr').addClass('selected'); //toggle current state
    });

	var counter = 0;
	var sumTotal = [];
	var Total = 0;

    $('#btnChoose').click(function (e) {
		counter++;
      	var selectedRow = $('#DetailTable .select :checked'),
            tr = selectedRow.parents('tr'), //get the parent row
            recvDocNo = tr.find('.recvDocNo').text(), 
			recvDNo = tr.find('.recvDNo').text(),
			poDocNo = tr.find('.poDocNo').text(),
            qty = parseInt(tr.find('.qty').text(), 10),
			// docDt = tr.find('.docDt').text(),
			itName = tr.find('.itName').text(),
			uom = tr.find('.uom').text(),
			dtName = tr.find('.dtName').text(),
			curCode = tr.find('.curCode').text(),
			price = tr.find('.price').text(),
			total = tr.find('.total').text();
			var newRow = $("<tr>");
			var cols = "";        
			cols += '<td></td>';
			cols += '<td><p class="RecvDocNo'+counter+'">'+ recvDocNo +'</p></td>';
			cols += '<td style="display:none">';
			cols += '<input type="hidden" name="RecvDocNo'+counter+'" value="'+recvDocNo+'"/>';
			cols += '<input type="hidden" name="RecvDNo'+counter+'" value="'+recvDNo+'"/>';
			cols += '</td>';
			cols += '<td><p class="PoDocNo'+counter+'">'+ poDocNo +'</p></td>';
			cols += '<td><p class="ItName'+counter+'">'+ itName +'</p></td>';
			cols += '<td><p class="Qty'+counter+'">'+ qty +'</p></td>';
			cols += '<td><p class="UoM'+counter+'">'+ uom +'</p></td>';
			cols += '<td><p class="Price'+counter+'">'+ price +'</p></td>';
			cols += '<td><p class="Total">'+ total +'</p></td>';
			cols += '<td><input type="button" class="ListRecvDel btn btn-md btn-danger" onclick="deleteData('+counter+', '+ total.split(",")[0].replaceAll(".", "").toString() +')" value="Delete"></td>';

			console.log("f " + counter)
			if(total != ""){
				newRow.append(cols);
				$('table.ListRecv').append(newRow);
				$('#received').modal('hide');

				var split = total.split(",")
				var replace = split[0].replaceAll(".", "");

				// console.log(replace);
				// console.log(parseInt(replace));  	
				// console.log("Total " + Total + " + " + replace)
				Total = Total + parseInt(replace);
				console.log(Total)
					
				var fTotal = Intl.NumberFormat("id-ID", {minimumFractionDigits:2}).format(Total);

				$('#CurCode').val(curCode);
				$('#totalNoTax').val(fTotal);
				console.log("l "+counter);

			}else {
				alert("You have not choose anything.");
			}			
    });

	function deleteData(counter, totalPrice) {

		Total = Total - (totalPrice);

		var fTotal = Intl.NumberFormat('id-ID', {minimumFractionDigits : 2}).format(Total);

		if (Total == 0) {
			$('#CurCode').val("");
			$('#totalNoTax').val("");
		}else {
			$('#totalNoTax').val(fTotal); 
		}

		$("table.ListRecv").on("click", ".ListRecvDel", function (event) {
			var self = $(this), //cache this
				checkbox = self.find('.select > input[type=checkbox]'), //get the checkbox
				isChecked = checkbox.prop('checked'); //and the current state
			if (!isChecked) {
				//about to be checked so clear all other selections
				$('#DetailTable .select > input[type=checkbox]').prop('checked', false).parents('tr').removeClass('selected');
			}
			checkbox.prop('checked', !isChecked).parents('tr').addClass('selected');
			$(this).closest("tr").remove();

		});
	}
</script>
<script>
	// get cookie
	function getCookie(cname) {
	let name = cname + "=";
	let decodedCookie = decodeURIComponent(document.cookie);
	let ca = decodedCookie.split(';');
	for(let i = 0; i <ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
		c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
		return c.substring(name.length, c.length);
		}
  }
  return "";
	}
</script>
<?php 
	}
}else
{
?>

<style>
.hidden {
    display: none;
}

.selected {
    background-color: #bbdefb;
}
.modal-body {
	overflow-y: auto;
	overflow-x: auto;
}
</style>
<form class="form-horizontal" name="insertVendor" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-body">				
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Document#</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="DocNo" id="DocNo" readonly value="<?= $NewDocNo ?>"/>
					</div>									
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Cancel Reason</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="CancelTxt" id="CancelTxt" readonly />
					</div>
					<div class="col-sm-3">
						<input type="checkbox" class="checkbox" name="CancelInd" id="CancelInd" disabled />
					</div>									
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field" >Invoice#</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="LocalDocNo" id="LocalDocNo"/>
					</div>
					<div class="col-sm-5">
						<span class="error-message" id="eLocalDocNo"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label mandatory-field">Entity</label>
					<div class="col-md-5 col-xs-5">
						<select class="form-control selectPayType" name="EntCode" id="EntCode" onchange="run(this);" style="width: 100%;">
							<option value=""></option>
							<?php

								$sql = "";
								$sql = "select EntCode as Col1, EntName as Col2 from TblEntity";

								$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));

								while($res = mysqli_fetch_assoc($result))
								{
									if($_COOKIE['EntCode'] == $res['Col1'])
										echo "<option value='".$res['Col1']."' selected";								
									else
									echo "<option value='".$res['Col1']."'";								
									echo ">".$res['Col2']."</option>";
								}
							?>
						</select>                        
					</div> 
					<div class="col-sm-5">
						<span class="error-message" id="eEntCode"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label mandatory-field" >Date</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="Date" id="Date" data-field="date"/>
					</div>
					<div id="DteExpiredDt"></div>
					<div class="col-sm-5">
						<span class="error-message" id="eDate"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label mandatory-field" >Payment Type</label>
					<div class="col-md-5 col-xs-5">
						<select class="form-control selectPayType" name="PaymentType" id="PaymentType" style="width: 100%;">
							<option value=""></option>
							<?php

								$sql = "";
								$sql = "select OptCode As Col1, OptDesc As Col2
								from TblOption 
								where OptCat = 'VoucherPaymentType'";

								$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));

								while($res = mysqli_fetch_assoc($result))
								{
									echo "<option value='".$res['Col1']."'";									
									echo ">".$res['Col2']."</option>";
								}
							?>
						</select>                        
					</div> 
					<div class="col-sm-5">
						<span class="error-message" id="ePaymentType"></span>
					</div>
				</div>							
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field" >Due Date</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="DueDate" id="DueDate" data-field="date" />
					</div>
					<div id="DteExpiredDt"></div>
					<div class="col-sm-5">
						<span class="error-message" id="eDueDate"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Currency</label>
					<div class="col-sm-5">
					<input type="text" class="form-control" name="CurCode" id="CurCode" readonly="readonly" value=""/>
					</div>
					<div class="col-sm-5">
						<span class="error-message" id="eCurrency"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label ">Total Without Tax</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="totalNoTax" id="totalNoTax" readonly="readonly" value="" style="text-align: right;" required/>
					</div>
				</div>												
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label ">Total With Tax</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="totalWithTax" id="totalWithTax" readonly="readonly" value="" style="text-align: right;" required />
					</div>
				</div>                    
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Total Tax</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="TotalTax" id="totalTax" readonly="readonly" value="" style="text-align: right;" required/>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Invoice Status</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="InvoiceStatus" id="InvoiceStatus" readonly="readonly" value="Outstanding" required/>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label ">Remark</label>

					<div class="col-sm-5">
						<input type="text" class="form-control" name="Remark" id="Remark"  />
					</div>
				</div>
			</div>
		</div>
	</div> <!-- /.col-md-12 -->
</div> <!-- /.row -->

<div class="row">
	<div class="col-xs-12">
		<span class="error-message" id="errorDtl"></span>
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#ContactPerson" data-toggle="tab">List of Received</a></li>
				<li><a href="#Bank" data-toggle="tab">Tax</a></li>
				<li><a href="#ItCt" data-toggle="tab">Document Info</a></li>				
			</ul>
			<div class="tab-content">
				<div class="active tab-pane" id="ContactPerson">
					<table class="table ListRecv" id="tblBankContactPerson">
						<thead>
							<tr>
								<th></th>
								<th>Received#</th>
								<th>PO#</th>
								<th>Item Name</th>
								<th>Qty</th>
								<th>UoM</th>
								<th>Unit Price</th>
								<th>Total Price</th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>						
					</table>
					<input type="button" class="btn btn-lg btn-default " id="recvList" value="Add List" />
				</div>
				
				<div class="tab-pane" id="Bank">
					<table class="table BankAccount" id="tblBankAccount">
						<thead>
							<tr>
								<th style="width: 20%;">Tax</th>
								<th>Tax#</th>
								<th>Alias</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select class="form-control selectTax1" name="LueTaxCode1" id="LueTaxCode1" style="width: 100%;">
										<option value=""></option>
										<?php 
											$sqlB = "select TaxCode , TaxName as Col2, TaxRate as Col1 from TblTax;";
											$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
											while($resB = mysqli_fetch_assoc($resultB))
											{	?>								
												<option value="<?php echo $resB['TaxCode']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>
											<?php }
										?>
									</select>
								</td>
								<td><input type="text" class="form-control" name="TaxNo1"/></td>
								<td><input type="text" class="form-control" name="Alias1"/></td>					
								<td><input type="text" class="form-control amt1" name="TaxAmt1" id="TaxAmt1" style="text-align: right;" readonly="readonly"/></td>
							</tr>
							<tr>
								<td>
									<select class="form-control selectTax2" name="LueTaxCode2" id="LueTaxCode2" style="width: 100%;">
										<option value=""></option>
										<?php 
											$sqlB = "select TaxCode , TaxName as Col2, TaxRate as Col1 from TblTax;";
											$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
											while($resB = mysqli_fetch_assoc($resultB))
											{	?>								
												<option value="<?php echo $resB['TaxCode']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>
											<?php }
										?>
									</select>
								</td>
								<td><input type="text" class="form-control" name="TaxNo2"/></td>
								<td><input type="text" class="form-control" name="Alias2"/></td>					
								<td><input type="text" class="form-control amt2" name="TaxAmt2" id="TaxAmt2" style="text-align: right;" readonly="readonly"/></td>
							</tr>
							<tr>
								<td>
									<select class="form-control selectTax3" name="LueTaxCode3" id="LueTaxCode3" style="width: 100%;">
										<option value=""></option>
										<?php 
											$sqlB = "select TaxCode , TaxName as Col2, TaxRate as Col1 from TblTax;";
											$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
											while($resB = mysqli_fetch_assoc($resultB))
											{	?>								
												<option value="<?php echo $resB['TaxCode']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>
											<?php }
										?>
									</select>
								</td>
								<td><input type="text" class="form-control" name="TaxNo3"/></td>
								<td><input type="text" class="form-control" name="Alias3"/></td>					
								<td><input type="text" class="form-control amt3" name="TaxAmt3" id="TaxAmt3" style="text-align: right;" readonly="readonly" /></td>
							</tr>			
						</tbody>						
					</table>
				</div>

				<div class="tab-pane" id="ItCt">
					<span id="eTblItCt" class="error-message"></span>
					<table class="table ItCt" id="tblItCt">
						<thead>
							<tr>
								<th style="width: 15%;">Document Type</th>
								<th>Document#</th>
								<th style="width: 15%;">Tax Invoice Date</th>
								<th style="width: 15%;">Amount</th>
								<th style="width: 10%;">Indicator</th>
								<th style="width: 15%;">Remark</th>
								<th style="width: 10%;">File</th>
							</tr>
						</thead>
						<tbody>
							<input type='hidden' name='countDoc' id='countDoc' value="0"/>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="8" style="text-align: left;">
									<input type="button" class="btn btn-lg btn-block " id="ItCtaddrow" value="Add Row"  />
								</td>
							</tr>
							<tr>
							</tr>
						</tfoot>
					</table>
				</div>
			
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- /.nav-tabs-custom -->

		<hr />

        <!-- <input type="reset" class="btn btn-default" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" background-color: #58585A; color:white"/> -->
        <button type="submit" name="submit" class="btn pull-right" id="BtnSave" style="background-color: #3373BA; color:white">Save</button>

	</div>
</div>

</form>
<!-- modal for choose list of received -->
<div class="modal fade" id="received">
	<div class="modal-dialog" style="width: 95%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<center><h4 class="modal-title" id="myModalLabel">List Of Received</h4></center>
			</div>
			<div class="modal-body">
				<?php 

					$sql = "Select A.DocNo, B.DNo, C.DocNo RecvVdDocNo, C.DNo RecvVdDNo, Date_Format(D.DocDt, '%d-%m-%Y') DocDt, E.ItName, E.InventoryUomCode,
					Date_Format(B.EstRecvDt, '%d-%m-%Y') EstRecvDt, C.Qty RecvQty, I.DTName,
					A.CurCode, H.UPrice, (H.UPrice * C.Qty) as TotalPrice
					From TblPOHdr A
					Inner Join TblPODtl B On A.DocNo = B.DocNo
					Inner Join TblRecvVdDtl C On B.DocNo = C.PODocNo And B.DNo = PODNo
						And C.`Status` = 'A'
						And C.CancelInd = 'N'
					Inner Join TblRecvVdHdr D On C.DocNo = D.DocNo
					Inner Join TblItem E On C.ItCode = E.ItCode
					Inner Join TblPORequestDtl F On B.PORequestDocNo = F.DocNo And B.PORequestDNo = F.DNo
					Inner Join TblQtHdr G On F.QtDocNo = G.DocNo And G.MaterialRequestDocNo Is Not Null and G.VdCode = '".$_SESSION['vdcode']."'
					Inner Join TblQtDtl H On G.DocNo = H.DocNo And F.QtDNo = H.DNo
					Left Join TblDeliveryType I On G.DTCode = I.DTCode
					inner join tblmaterialrequesthdr J on F.MaterialRequestDocNo = J.DocNo
				   inner join tblsite K on J.SiteCode = K.SiteCode
				   inner join tblprofitcenter L on K.ProfitCenterCode = L.ProfitCenterCode
				   inner join tblentity M on L.EntCode = M.EntCode 
				   where D.VdCode = '".$_SESSION['vdcode']."' and M.EntCode = '".$_COOKIE['EntCode']."'
				Order by A.DocNo, B.DNo, C.DocNo, C.DNo;";

					echo "<div class='box-body' style='margin-left:5px'> ";
					echo "    <div style='margin-top: 10px;'>";
					echo "        <h4 class='box-title' style='margin-bottom: 5px;'>Delivery Status</h4>";
					echo "    </div>";
					echo "    <table id='DetailTable' class='table table-bordered ' width='100%'>";
					echo "        <thead>";
					echo "            <tr>";
					echo "                <th width='200'>Received#</th>";
					echo "                <th width='200'>PO#</th>";
					echo "                <th>Received Date</th>";
					echo "                <th>Item</th>";
					echo "                <th>Uom</th>";
					echo "                <th>Received Item</th>";
					echo "                <th>Delivery Type</th>";
					echo "                <th>Currency</th>";
					echo "                <th>Unit Price</th>";
					echo "                <th>Total</th>";
					echo "                <th class='hidden'>checkbox</th>";
					echo "            </tr>";
					echo "        </thead>";
					echo "        <tbody>";
								$result2 = mysqli_query($conn,$sql) or die (mysqli_error($conn));
								$currOutstandingQty = 0;
								$DNo = "";
								$index = 0;
								while ($res2 = mysqli_fetch_assoc($result2))
								{
									// if (strlen($DNo) == 0) $DNo = $res2['DNo'];
									
									// if ($DNo != $res2['DNo']) 
									// {
									// 	$index = 0;
									// 	$DNo = $res2['DNo'];
									// }

									// if ($index == 0) $currOutstandingQty = $res2['POQty'] - $res2['RecvQty'];
									// else $currOutstandingQty -= $res2['RecvQty'];

									echo "<tr>";
									echo "<td class='recvDocNo'>". $res2['RecvVdDocNo'] ."</td>";
									echo "<td class='recvDNo' style='display:none;'>". $res2['RecvVdDNo'] ."</td>";
									echo "<td class='poDocNo'>". $res2['DocNo'] ."</td>";
									echo "<td class='docDt'>". $res2['DocDt'] ."</td>";
									echo "<td class='itName'>". $res2['ItName'] ."</td>";
									echo "<td class='uom'>". $res2['InventoryUomCode'] ."</td>";
									echo "<td class='qty' style='text-align: right'>". number_format($res2['RecvQty'], 2) ."</td>";
									echo "<td class='dtName'>". $res2['DTName'] ."</td>";
									echo "<td class='curCode'>". $res2['CurCode'] ."</td>";
									echo "<td class='price' style='text-align: right'>". number_format($res2['UPrice'], 2, ",", ".") ."</td>";
									echo "<td class='total' style='text-align: right'>". number_format($res2['TotalPrice'], 2, ",", ".") ."</td>";
									echo "<td class='select hidden'><input type='checkbox'></td>";
									echo "</tr>";

									$index++;
								}
					echo "        </tbody>";
					echo "     </table>";
					echo " </div>";						
				?>
			</div>
			<div class="modal-footer">
				<button id="btnChoose" class="btn btn-md btn-danger">Choose</button>
			</div>
		</div>
	</div>
</div>

<script src="StdMtdv1.0.4.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
	$(".selectVdCategory").select2();
    $(".selectVillage").select2();
    $(".selectCity").select2();
	$(".selectSubDistrict").select2();
	$(".selectPayType").select2();
	$(".selectHO").select2();
	$(".selectTax1").select2();
	$(".selectTax2").select2();
	$(".selectTax3").select2();


	$("#DteExpiredDt").DateTimePicker();
	
  });
</script>

<script type="text/javascript">

	$(function() {
		$('table tr').each(function() {
			$(this).find('td').each(function() {
				$(this).find('input,select').attr("disabled", false);
				$(this).find('input,select').attr("readonly", false);
			});
		});
	});	

	$('#recvList').on('click',function(){
		console.log(getCookie("EntCode"));
		if(getCookie("EntCode")){
			$("#DetailTable .select :checked").attr("checked", false);
			$('#received').modal("show");
		}else {
			alert("Entity still empty.");
		}
	});
</script>
<script type="text/javascript">
	$(document).ready(function () {
		
		//Tax

			var totalNonTax = $("#totalNonTax").val();
			$("table.BankAccount").on('change','#LueTaxCode1',function(){
				var getValue = $('#totalNoTax').val();
				var split = getValue.split(",");
				var TotalNonTax = split[0].replaceAll(".", "");
				var tax = document.querySelector('#LueTaxCode1').value;
				var calcTax = (parseInt(TotalNonTax)) * (parseFloat(tax)/100);																

				if(Number.isNaN(calcTax)){
					$(".amt1").val("");	
				}else {
					$(".amt1").val(calcTax);							
				}

				var tax1 = $(".amt1").val();
				var tax2 = $(".amt2").val();
				var tax3 = $(".amt3").val();


				if(tax1 != "" && tax2 != "" && tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax2) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" && tax2 == "" && tax3 !=""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 == "" && tax2 != "" && tax3 != "") {
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax2) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax2) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 == ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax2)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax2))+ ",00";
					$("#totalWithTax").val(totalWithTax);
					$("#totalTax").val(totalTax);
				}else if(tax2 != "" && tax1 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax2))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" || tax2 != "" || tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1 != "" ? tax1 : 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1 != "" ? tax1: 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else {
					$("#totalWithTax").val("");
					$("#totalTax").val("");
				}						
			});

			$("table.BankAccount").on('change','#LueTaxCode2',function(){
				var getValue = $('#totalNoTax').val();
				var split = getValue.split(",");
				var TotalNonTax = split[0].replaceAll(".", "");
				var tax = document.querySelector('#LueTaxCode2').value;
				var calcTax = (parseInt(TotalNonTax)) * (parseFloat(tax)/100);																

				if(Number.isNaN(calcTax)){
					$(".amt2").val("");	
				}else {
					$(".amt2").val(calcTax);							
				}

				var tax1 = $(".amt1").val();
				var tax2 = $(".amt2").val();
				var tax3 = $(".amt3").val();

				if(tax1 != "" && tax2 != "" && tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax2) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" && tax2 == "" && tax3 !=""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 == "" && tax2 != "" && tax3 != "") {
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax2) + parseInt(tax3)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax2) + parseInt(tax3))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1) + parseInt(tax2))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 == ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax2)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax2))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" || tax2 != "" || tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID').format((parseInt(TotalNonTax)) + (parseInt(tax1 != "" ? tax1 : 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0)))+ ",00";
					var totalTax = Intl.NumberFormat('id-ID').format(parseInt(tax1 != "" ? tax1: 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0))+ ",00";
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else{
					$("#totalWithTax").val("");
					$("#totalTax").val("");
				}
			});

			$("table.BankAccount").on('change','#LueTaxCode3',function(){
				var getValue = $('#totalNoTax').val();
				var split = getValue.split(",");
				var TotalNonTax = split[0].replaceAll(".", "");
				var tax = document.querySelector('#LueTaxCode3').value;
				var calcTax = (parseInt(TotalNonTax)) * (parseFloat(tax)/100);																

				if(Number.isNaN(calcTax)){
					$(".amt3").val("");	
				}else {
					$(".amt3").val(calcTax);							
				}

				var tax1 = $(".amt1").val();
				var tax2 = $(".amt2").val();
				var tax3 = $(".amt3").val();

				if(tax1 != "" && tax2 != "" && tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2) + parseInt(tax3)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1) + parseInt(tax2) + parseInt(tax3));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" && tax2 == "" && tax3 !=""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax3)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1) + parseInt(tax3));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 == "" && tax2 != "" && tax3 != "") {
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax2) + parseInt(tax3)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax2) + parseInt(tax3));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1) + parseInt(tax2));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax2 != "" && tax1 == ""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax2)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax2));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else if(tax1 != "" || tax2 != "" || tax3 != ""){
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1 != "" ? tax1 : 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1 != "" ? tax1: 0) + parseInt(tax2 != "" ? tax2 : 0) + parseInt(tax3 != "" ? tax3 : 0));
					if(totalTax !== "NaN,00" || totalWithTax !== "NaN,00"){
						$("#totalWithTax").val(totalWithTax);
						$("#totalTax").val(totalTax);
					}
				}else{
					$("#totalWithTax").val("");
					$("#totalTax").val("");
				}
			});
		//End of Tax
		
		$("table.BankAccount").on("click", ".BAbtnDel", function (event) {
			var TotalNonTax = $('#totalNoTax').val();
			$(this).closest("tr").remove();       
			counterTax -= 1
			
			// var tax = $(".amt"+String(counterTax-=1)).val();
			// $("#totalWithTax").val(totalWithTax - tax);
			// $("#totalTax").val(totalTax - tax);
			if(counterTax == 2){
				var tax1 = $(".amt"+String(counterTax-2)).val();
				var tax2 = $(".amt"+String(counterTax-1)).val();
				var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1) + parseInt(tax2)));
				var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1) + parseInt(tax2));
				$("#totalWithTax").val(totalWithTax);
				$("#totalTax").val(totalTax);
			}else {
				var tax1 = $(".amt"+String(counterTax-1)).val();
				if(isNaN(tax1)){
					$("#totalWithTax").val("");
					$("#totalTax").val("");
				}else {				
					var totalWithTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format((parseInt(TotalNonTax)) + (parseInt(tax1)));
					var totalTax = Intl.NumberFormat('id-ID', {minimumFractionDigits:2}).format(parseInt(tax1));
					$("#totalWithTax").val(totalWithTax);
					$("#totalTax").val(totalTax);
				}
			}
		});	

		var countDoc = document.getElementById("countDoc").value;
		var counterDoc = Number(countDoc);

		$("#ItCtaddrow").on("click", function () {
			var newRow = $("<tr>");
			var cols = "";

			cols += '<td><select class="form-control mandatory-field selectType' + counterDoc + '" required name="LueDocType' + counterDoc + '" id="LueDocType' + counterDoc + '" style="width: 100%;">';
			cols += '<option value=""></option>';
			<?php
				$sqlB = "Select OptCode As Col1, OptDesc As Col2 From TblOption where OptCat ='PurchaseInvoiceDocType' ";
				$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
				while($resB = mysqli_fetch_assoc($resultB))
				{
				?>
					cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
				<?php
				}
			?>
			cols += '</select></td>';
			cols += '<td><input type="text" class="form-control" name="DocNo' + counterDoc + '" id="DocNo' + counterDoc + '"/></td>';
			cols += '<td><input type="text" class="form-control" name="TaxDate' + counterDoc + '" id="TaxDate' + counterDoc + '" data-field="date"/><div id="DteExpiredDt"></div></td>';
			cols += '<td><input type="text" class="form-control mandatory-field" required name="Amt' + counterDoc + '" id="Amt' + counterDoc + '" style="text-align: right;"/></td>';
			cols += '<td><select class="form-control mandatory-field selectInd' + counterDoc + '" required name="LueDocInd' + counterDoc + '" id="LueDocInd' + counterDoc + '" style="width: 100%;">';
			cols += '<option value=""></option>';
			<?php
				$sqlB = "Select OptCode As Col1, OptDesc As Col2 From TblOption where OptCat ='PurchaseInvoiceDocInd' ";
				$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
				while($resB = mysqli_fetch_assoc($resultB))
				{
				?>
					cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
				<?php
				}
			?>
			cols += '</select></td>';
			cols += '<td><input type="text" class="form-control" name="Remark' + counterDoc + '" id="Remark' + counterDoc + '" /></td>';
			cols += '<td><input type="file" name="VdFile' + counterDoc + '" id="VdFile' + counterDoc + '" accept="application/pdf, image/png, image/jpg, image/jpeg" style="width:180px;" />';

			cols += '<td><input type="button" class="ItCtbtnDel btn btn-md btn-danger" value="Delete"></td>';
			newRow.append(cols);
			$("table.ItCt").append(newRow);

			$("#Amt" + counterDoc).on('keypress', function(e){
				if(this.selectionStart || this.selectionStart == 0){
					// selectionStart won't work in IE < 9
					if (e.which < 48 || e.which > 57)
					{
						e.preventDefault();
					}else {
						var key = e.which;
						var prevDefault = true;
						
						var thouSep = " ";  // your seperator for thousands
						var deciSep = ",";  // your seperator for decimals
						var deciNumber = 0; // how many numbers after the comma
						
						var thouReg = new RegExp(thouSep,"g");
						var deciReg = new RegExp(deciSep,"g");
						
						function spaceCaretPos(val, cPos){
							/// get the right caret position without the spaces
							
							if(cPos > 0 && val.substring((cPos-1),cPos) == thouSep)
							cPos = cPos-1;
							
							if(val.substring(0,cPos).indexOf(thouSep) >= 0){
							cPos = cPos - val.substring(0,cPos).match(thouReg).length;
							}
							
							return cPos;
						}
						
						function spaceFormat(val, pos){
							/// add spaces for thousands
							
							if(val.indexOf(deciSep) >= 0){
								var comPos = val.indexOf(deciSep);
								var int = val.substring(0,comPos);
								var dec = val.substring(comPos);
							} else{
								var int = val;
								var dec = "";
							}
							var ret = [val, pos];
							
							if(int.length > 3){
								
								var newInt = "";
								var spaceIndex = int.length;
								
								while(spaceIndex > 3){
									spaceIndex = spaceIndex - 3;
									newInt = thouSep+int.substring(spaceIndex,spaceIndex+3)+newInt;
									if(pos > spaceIndex) pos++;
								}
								ret = [int.substring(0,spaceIndex) + newInt + dec, pos];
							}
							return ret;
						}
						
						$(this).on('keyup', function(ev){
							
							if(ev.which == 8){
								// reformat the thousands after backspace keyup
								
								var value = this.value;
								var caretPos = this.selectionStart;
								
								caretPos = spaceCaretPos(value, caretPos);
								value = value.replace(thouReg, '');
								
								var newValues = spaceFormat(value, caretPos);
								this.value = newValues[0];
								this.selectionStart = newValues[1];
								this.selectionEnd   = newValues[1];
							}
						});
						
						if((e.ctrlKey && (key == 65 || key == 67 || key == 86 || key == 88 || key == 89 || key == 90)) ||
							(e.shiftKey && key == 9)) // You don't want to disable your shortcuts!
							prevDefault = false;
						
						if((key < 37 || key > 40) && key != 8 && key != 9 && prevDefault){
							e.preventDefault();
							
							if(!e.altKey && !e.shiftKey && !e.ctrlKey){
							
								var value = this.value;
								if((key > 95 && key < 106)||(key > 47 && key < 58) ||
								(deciNumber > 0 && (key == 110 || key == 188 || key == 190))){
									
									var keys = { // reformat the keyCode
									48: 0, 49: 1, 50: 2, 51: 3,  52: 4,  53: 5,  54: 6,  55: 7,  56: 8,  57: 9,
									96: 0, 97: 1, 98: 2, 99: 3, 100: 4, 101: 5, 102: 6, 103: 7, 104: 8, 105: 9,
									110: deciSep, 188: deciSep, 190: deciSep
									};
									
									var caretPos = this.selectionStart;
									var caretEnd = this.selectionEnd;
									
									if(caretPos != caretEnd) // remove selected text
									value = value.substring(0,caretPos) + value.substring(caretEnd);
									
									caretPos = spaceCaretPos(value, caretPos);
									
									value = value.replace(thouReg, '');
									
									var before = value.substring(0,caretPos);
									var after = value.substring(caretPos);
									var newPos = caretPos+1;
									
									if(keys[key] == deciSep && value.indexOf(deciSep) >= 0){
										if(before.indexOf(deciSep) >= 0){ newPos--; }
										before = before.replace(deciReg, '');
										after = after.replace(deciReg, '');
									}
									var newValue = before + keys[key] + after;
									
									if(newValue.substring(0,1) == deciSep){
										newValue = "0"+newValue;
										newPos++;
									}
									
									while(newValue.length > 1 && 
									newValue.substring(0,1) == "0" && newValue.substring(1,2) != deciSep){
										newValue = newValue.substring(1);
										newPos--;
									}
									
									if(newValue.indexOf(deciSep) >= 0){
										var newLength = newValue.indexOf(deciSep)+deciNumber+1;
										if(newValue.length > newLength){
										newValue = newValue.substring(0,newLength);
										}
									}
									
									newValues = spaceFormat(newValue, newPos);
									
									this.value = newValues[0];
									this.selectionStart = newValues[1];
									this.selectionEnd   = newValues[1];
								}
							}
						}
						
						$(this).on('blur', function(e){
							
							if(deciNumber > 0){
								var value = this.value;
								
								var noDec = "";
								for(var i = 0; i < deciNumber; i++)
								noDec += "0";
								
								if(value == "0"+deciSep+noDec)
								this.value = ""; //<-- put your default value here
								else
								if(value.length > 0){
									if(value.indexOf(deciSep) >= 0){
										var newLength = value.indexOf(deciSep)+deciNumber+1;
										if(value.length < newLength){
										while(value.length < newLength){ value = value+"0"; }
										this.value = value.substring(0,newLength);
										}
									}
									else this.value = value + deciSep + noDec;
								}
							}
						});
					}
				}
			});

			for(var j = 0; j <= counterDoc; j++)
				$(".selectType" + j + "").select2();

			for(var j = 0; j <= counterDoc; j++)
				$(".selectInd" + j + "").select2();


			counterDoc++;
		});

		$("table.ItCt").on("click", ".ItCtbtnDel", function (event) {
			$(this).closest("tr").remove();       
			counterItCt -= 1
		});

		var blankComboBox = "<option value=''></option>";

		$("#LueSDCode").change(function(){
			var SDCode = $("#LueSDCode").val();
			$.ajax({
				url: "GetVillage.php",
				data: "SDCode="+SDCode,
				cache: false,
				success: function(msg){
					$("#LueVillageCode").attr("disabled", false);
					$("#LueVillageCode").html(msg);
				}
			});
		});

	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script>
	function validate() {
		var error="";
		var localDocNo = document.getElementById( "LocalDocNo" );
		if( localDocNo.value == "" )
		{
			error = " Field is required. ";
			document.getElementById( "eLocalDocNo" ).innerHTML = error;
			document.getElementById( "eLocalDocNo" ).scrollIntoView();
			return false;
		}else {
			error = "";
			document.getElementById( "eLocalDocNo" ).innerHTML = error;
		}
		var error="";
		var entCode = document.getElementById( "EntCode" );
		if( entCode.value == "" )
		{
		error = " Field is required. ";
		document.getElementById( "eEntCode" ).innerHTML = error;
		document.getElementById( "eEntCode" ).scrollIntoView();
		return false;
		}else {
			error = "";
			document.getElementById( "eEntCode" ).innerHTML = error;
		}
		var error="";
		var date = document.getElementById( "Date" );
		if( date.value == "" )
		{
			error = " Field is required. ";
			document.getElementById( "eDate" ).innerHTML = error;
		document.getElementById( "eDate" ).scrollIntoView();

			return false;
		}else {
			error = "";
			document.getElementById( "eDate" ).innerHTML = error;
		}
		var error="";
		var pt = $("#PaymentType").find('option:selected');
		if( pt.val() == "" )
		{
			error = " Field is required. ";
			document.getElementById( "ePaymentType" ).innerHTML = error;
			document.getElementById( "ePaymentType" ).scrollIntoView();
			return false;
		}else {
			error = "";
			document.getElementById( "ePaymentType" ).innerHTML = error;
		}
		var error="";
		var dueDt = document.getElementById( "DueDate" );
		if( dueDt.value == "" )
		{
			error = " Field is required. ";
			document.getElementById( "eDueDate" ).innerHTML = error;
			document.getElementById( "eDueDate" ).scrollIntoView();
			return false;
		}else {
			error = "";
			document.getElementById( "eDueDate" ).innerHTML = error;
		}
		var error="";
		var curCode = document.getElementById( "CurCode" );
		if( curCode.value == "" )
		{
			error = " Field is required. ";
			document.getElementById( "eCurrency" ).innerHTML = error;
			document.getElementById( "eCurrency" ).scrollIntoView();
			return false;
		}else {
			error = "";
			document.getElementById( "eCurrency" ).innerHTML = error;
		}

	}	
</script>
<script>
	$('#DetailTable').DataTable({
		scrollX: true,
		scrollY: true,
		responsive: true,
		scrollCollapse: true,
		paging:         true,
		columnDefs: [
			{ width: 200, targets: 0 }
		],
	});
	$('#ListRecv').DataTable({
		scrollX: true,
		scrollY: true,
		responsive: true,
		scrollCollapse: true,
		paging:         true,
		columnDefs: [
			{ width: 200, targets: 0 }
		],
	});
	ListRecv
	function run(sel) {

		var i = sel.selectedIndex;
		if (i != -1) {
			//document.getElementById("car").value = sel.options[i].text;

			document.cookie = "EntCode="+sel.options[i].value;
			console.log(sel.options[i].value);
			location.reload();
		}

	}
</script>
<script>
    'use strict';
     $('#DetailTable tbody tr').click(function (e) {
        //when the row is clicked...
        var self = $(this), //cache this
            checkbox = self.find('.select > input[type=checkbox]'), //get the checkbox
            isChecked = checkbox.prop('checked'); //and the current state
        if (!isChecked) {
            //about to be checked so clear all other selections
            $('#DetailTable .select > input[type=checkbox]').prop('checked', false).parents('tr').removeClass('selected');
        }
        checkbox.prop('checked', !isChecked).parents('tr').addClass('selected'); //toggle current state
    });

	var counter = 0;
	var sumTotal = [];
	var Total = 0;

    $('#btnChoose').click(function (e) {
		counter++;
      	var selectedRow = $('#DetailTable .select :checked'),
            tr = selectedRow.parents('tr'), //get the parent row
            recvDocNo = tr.find('.recvDocNo').text(), 
			recvDNo = tr.find('.recvDNo').text(),
			poDocNo = tr.find('.poDocNo').text(),
            qty = parseInt(tr.find('.qty').text(), 10),
			// docDt = tr.find('.docDt').text(),
			itName = tr.find('.itName').text(),
			uom = tr.find('.uom').text(),
			dtName = tr.find('.dtName').text(),
			curCode = tr.find('.curCode').text(),
			price = tr.find('.price').text(),
			total = tr.find('.total').text();
			var newRow = $("<tr>");
			var cols = "";        
			cols += '<td></td>';
			cols += '<td><p class="RecvDocNo'+counter+'">'+ recvDocNo +'</p></td>';
			cols += '<td style="display:none">';
			cols += '<input type="hidden" name="RecvDocNo'+counter+'" value="'+recvDocNo+'"/>';
			cols += '<input type="hidden" name="RecvDNo'+counter+'" value="'+recvDNo+'"/>';
			cols += '</td>';
			cols += '<td><p class="PoDocNo'+counter+'">'+ poDocNo +'</p></td>';
			cols += '<td><p class="ItName'+counter+'">'+ itName +'</p></td>';
			cols += '<td><p class="Qty'+counter+'">'+ qty +'</p></td>';
			cols += '<td><p class="UoM'+counter+'">'+ uom +'</p></td>';
			cols += '<td><p class="Price'+counter+'">'+ price +'</p></td>';
			cols += '<td><p class="Total">'+ total +'</p></td>';
			cols += '<td><input type="button" class="ListRecvDel btn btn-md btn-danger" onclick="deleteData('+counter+', '+ total.split(",")[0].replaceAll(".", "").toString() +')" value="Delete"></td>';

			console.log("f " + counter)
			if(total != ""){
				newRow.append(cols);
				$('table.ListRecv').append(newRow);
				$('#received').modal('hide');

				var split = total.split(",")
				var replace = split[0].replaceAll(".", "");

				// console.log(replace);
				// console.log(parseInt(replace));  	
				// console.log("Total " + Total + " + " + replace)
				Total = Total + parseInt(replace);
				console.log(Total)
					
				var fTotal = Intl.NumberFormat("id-ID", {minimumFractionDigits:2}).format(Total);

				$('#CurCode').val(curCode);
				$('#totalNoTax').val(fTotal);
				console.log("l "+counter);

			}else {
				alert("You have not choose anything.");
			}			
    });

	function deleteData(counter, totalPrice) {

		Total = Total - (totalPrice);

		var fTotal = Intl.NumberFormat('id-ID', {minimumFractionDigits : 2}).format(Total);

		if (Total == 0) {
			$('#CurCode').val("");
			$('#totalNoTax').val("");
		}else {
			$('#totalNoTax').val(fTotal); 
		}

		$("table.ListRecv").on("click", ".ListRecvDel", function (event) {
			var self = $(this), //cache this
				checkbox = self.find('.select > input[type=checkbox]'), //get the checkbox
				isChecked = checkbox.prop('checked'); //and the current state
			if (!isChecked) {
				//about to be checked so clear all other selections
				$('#DetailTable .select > input[type=checkbox]').prop('checked', false).parents('tr').removeClass('selected');
			}
			checkbox.prop('checked', !isChecked).parents('tr').addClass('selected');
			$(this).closest("tr").remove();

		});
	}
</script>
<script>
	// get cookie
	function getCookie(cname) {
	let name = cname + "=";
	let decodedCookie = decodeURIComponent(document.cookie);
	let ca = decodedCookie.split(';');
	for(let i = 0; i <ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) == ' ') {
		c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
		return c.substring(name.length, c.length);
		}
  }
  return "";
	}
</script>

<?php
}
?>

<?php
include "footer.php";
?>