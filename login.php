<?php 
session_start();
include 'header.php';
require 'conn.php';
include 'StdMtd.php';
//require_once 'auth.php';

if(isset($_SESSION['vdusercode']))
{
	header("Location: index.php");
}

$errorMessage = "";
$emptyUCode = "";
$emptyPassword = "";

// If form submitted, insert values into the database.
if (isset($_POST['submit'])){

  if(isset($_POST['ucode']) && isset($_POST['password']))
  {
    // removes backslashes
    //$username = stripslashes($_REQUEST['username']);
      //escapes special characters in a string
    $usercode = mysqli_real_escape_string($conn,$_POST['ucode']);
    //$password = stripslashes($_REQUEST['password']);
    $password = mysqli_real_escape_string($conn,$_POST['password']);
    //Checking is user existing in the database or not
    
    $query = "Select A.UserCode, A.UserName, IfNull(A.GrpCode, '') GrpCode, IfNull(B.GrpName, '') GrpName, IfNull(C.VdCode, '') VdCode, IfNull(C.VdName, '') VdName ";
    $query = $query . "From TblUserVendor A ";
    $query = $query . "Left Join TblGroup B On A.GrpCode = B.GrpCode ";
    $query = $query . "Inner Join TblVendor C On A.UserCode = C.UserCode And C.ActInd = 'Y' ";
    $query = $query . "Where (A.UserCode ='$usercode' And A.Pwd = '$password'); ";

    $result = mysqli_query($conn,$query) or die(mysqli_error($conn));
    $rows = mysqli_num_rows($result);
    $res = mysqli_fetch_assoc($result);
    if($rows==1)
    {
      require_once ("MD/Mobile_Detect.php");

      $detect = new Mobile_Detect;
      $tmNow = GetValue("Select Concat(Replace(CurDate(), '-', ''), Replace(CurTime(), ':', '')) As tmNow ");
      $ver = "v1.0.1";

      if($detect->isMobile())
      {
        $insert = "Insert Into TblWebPortalLogin(UserCode, LogDate, IPAddr, LogTp, Mobile) Values('".$usercode."', CurrentDateTime(), '".$_SERVER['REMOTE_ADDR']."', 'I', 'Y'); ";

        $insert = $insert . "Insert Into TblLog(UserCode, LogIn, IP, Machine, SysVer, CreateBy, CreateDt) Values('".$usercode."', '".$tmNow."', '".$_SERVER['REMOTE_ADDR']."', 'Mobile', '".$ver."', '".$usercode."', CurrentDateTime()); ";

        $expSQL = explode(";", $insert);
        $countSQL = count($expSQL);
        $success = false;

        for($i = 0; $i < $countSQL - 1; $i++)
        {
          if (mysqli_query($conn, $expSQL[$i])) {
            $success = true;
          } else {
              echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
              $success = false;
              mysqli_rollback($conn);
              break;
          }
        }

        if($success)
        {
          mysqli_commit($conn);
        }

        //mysqli_query($conn,$insert) or die(mysqli_error($conn));
      }
      else
      {
        $insert = "Insert Into TblWebPortalLogin(UserCode, LogDate, IPAddr, LogTp, Mobile) Values('".$usercode."', CurrentDateTime(), '".$_SERVER['REMOTE_ADDR']."', 'I', 'N'); ";

        $insert = $insert . "Insert Into TblLog(UserCode, LogIn, IP, Machine, SysVer, CreateBy, CreateDt) Values('".$usercode."', '".$tmNow."', '".$_SERVER['REMOTE_ADDR']."', 'Desktop', '".$ver."', '".$usercode."', CurrentDateTime()); ";

        $expSQL = explode(";", $insert);
        $countSQL = count($expSQL);
        $success = false;

        for($i = 0; $i < $countSQL - 1; $i++)
        {
          if (mysqli_query($conn, $expSQL[$i])) {
            $success = true;
          } else {
              echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
              $success = false;
              mysqli_rollback($conn);
              break;
          }
        }

        if($success)
        {
          mysqli_commit($conn);
        }

        //mysqli_query($conn,$insert) or die(mysqli_error($conn));
      }

      $_SESSION['vdusercode'] = $usercode;
      $_SESSION['vdusername'] = $res['UserName'];
      $_SESSION['vdgrpname'] = $res['GrpName'];
      $_SESSION['vdgrpcode'] = $res['GrpCode'];
      $_SESSION['vdcode'] = strlen($res['VdCode']) > 0 ? $res['VdCode'] : '';
      $_SESSION['vdname'] = strlen($res['VdName']) > 0 ? $res['VdName'] : '';
      $_SESSION['vdlogged_in'] = true;
      $_SESSION['vdlast_activity'] = time();
      $_SESSION['vdexpire_time'] = 30*60; // 30 min
      
        // Redirect user to index.php
      header("Location: index.php");
    }
    else
    {
      $errorMessage = "Incorrect UserCode and/or Password";
    }
  }
} // end if(isset($_POST['submit']))

?>
<noscript>
  <div class="alert alert-danger">
    <h4><i class="icon fa fa-warning"></i> Javascript disabled.</h4>
    This site need to use javascript to work properly. Unfortunately, your browser's javascript is disabled. Therefore, please enable javascript. <strong><a href="http://activatejavascript.org/en/instructions" target="_blank">Click here</a></strong> to see how to enable your browser's javascript. Thank you.
  </div>
</noscript>

<!-- jQuery 2.2.3 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="bootstrap/css/loginv1.css">


<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="" method="post" name="login" enctype="multipart/form-data">
					<span class="login100-form-title p-b-18">
						<img src="dist/img/runmarket logo 1.png" alt="" style="width: 230px;">
					</span>
					<span class="login100-form-title p-b-43">
						Sign in to start your session
					</span>
					  
					
					<div class="wrap-input100 validate-input" data-validate = "Usercode is required">
						<input class="input100" type="text" name="ucode" id="ucode" placeholder="Usercode">
						<span class="focus-input100"></span>
            <span class="glyphicon glyphicon-user"></span>
						<span class="error-message" id="eUserCode"></span>
						
					</div>
					
					
					<div class="wrap-input100 validate-input form-group" data-validate="Password is required">
						<input class="input100" type="password" name="password" id="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="glyphicon glyphicon-eye-close"></span>
					</div>
          
          <span class="error-message" id="ePassword"></span>
          <span class="error-message" style="font-family:Gotham-Rounded;"><center><?php echo $errorMessage; ?></center></span>

					<div class="flex-sb-m w-full p-t-3 p-b-32">

          <button class="btn-secondary m-r-10" style="border-color: #3373BA;">
							<a href="register.php">
                Register
              </a>
						</button>

						<input type="submit" name="submit" id="MyBtn" value="Sign In" class="btn-primary">
							
						</input>
					</div>
					
					<div>
						<img src="dist/img/download.png" alt="">
						<a href="dist/man/Eproc_Manual_For_Vendor.pdf">Download Manual Book</a>
					</div>
				</form>
				<div class="login100-more" style="background-image: url('dist/img/runsystem bg.png');">
				</div>
			</div>
		</div>
	</div>


<script type="text/javascript">
  function validateLogin()
  {
    document.getElementById("eUserCode").innerHTML = "";
    document.getElementById("ePassword").innerHTML = "";

    var UserCode = document.getElementById("ucode").value;
    var Password = document.getElementById("pword").value;

    if(UserCode == "") { document.getElementById("eUserCode").innerHTML = "UserCode should not be empty."; return false; }
    if(Password == "") { document.getElementById("ePassword").innerHTML = "Password should not be empty."; return false; }
  }

  $('.carousel').carousel({
    pause: "false" /* Change to true to make it paused when your mouse cursor enter the background */
});

$(".glyphicon-eye-close").on("click", function() {
$(this).toggleClass("glyphicon-eye-open").toggleClass('glyphicon-eye-close');
  var type = $("#password").attr("type");
if (type == "text"){ 
  $("#password").prop('type','password');}
else{ 
  $("#password").prop('type','text'); }
});

var input = document.getElementById("password");
  input.addEventListener("keyup", (event) => {
    if (event.key === "Enter") {
      // Cancel the default action, if needed
      event.preventDefault();
      // Trigger the button element with a click
      document.getElementById("MyBtn").click();
    }
  });
</script>

<?php 
include 'footer.php'; 
?>
