<?php
include "auth.php";
include 'conn.php';
include "StdMtd.php";

if (isset($_POST['DocNo'])) {

    $DocNo = $_POST['DocNo'];

    $VdCode = $_SESSION['vdcode'];
    $sql = "SELECT T.EntName, T.DocNo, T.LocalDocNo, T.PIDocNo, T.PIDocDt, T.Status, T.Amt, T.TaxAmt, T.TotalAmt, T.TotalOutstanding,
    if(T.TotalOutstanding = 0, 'Done', 'Payment On Progress') PaymentStatus, T.PtName, T.OptDesc,
    T.DueDt, T.CurCode, T.Remark
    FROM
    (
        SELECT C.EntName, B.LocalDocNo, D.PIDocNo, D.PIDocDt, B.Remark , D.DocNo,
        case B.InvoiceStatusInd
            when 'A' then 'Approved'
            when 'P' then 'Processed'
            when 'C' then 'Cancelled'
            when 'O' then 'Outstanding'
        END AS Status, G.OptDesc, Date_Format(B.DueDt, '%d-%m-%Y') DueDt, B.CurCode,
        IFNULL(D.Amt, 0.00) Amt, IFNULL(D.TaxAmt, 0.00) TaxAmt,
        IFNULL(D.totalAmt, 0.00) TotalAmt,
        IFNULL(D.totalAmt - E.VoucherOPAmt, 0.00) TotalOutstanding, F.PtName
        FROM tblvendorinvoicedtl A
        inner JOIN tblvendorinvoicehdr B ON A.DocNo = B.DocNo
            AND B.CancelInd = 'N'
            AND B.VdCode = '".$VdCode."'
        inner JOIN tblentity C ON B.EntCode = C.EntCode
        inner JOIN tblOption G ON B.PaymentType = G.OptCode AND G.OptCat = 'VoucherPaymentType'
        LEFT JOIN
        (
            SELECT Distinct T1.DocNo, T1.DNo, T4.DocNo PIDocNo, T2.DocDt PIDocDt, T4.Amt, T4.TaxAmt,
            IFNULL(T4.Amt + T4.TaxAmt, 0.00) TotalAmt
            FROM tblvendorinvoicedtl T1
            inner Join TblVendorInvoiceHdr T2 On T1.DocNo = T2.DocNo And T2.CancelInd = 'N'
            left JOIN tblpurchaseinvoicedtl T3 ON T1.RecvVdDocNo = T3.RecvVdDocNo AND T1.RecvVdDNo = T3.RecvVdDNo
            left JOIN tblpurchaseinvoicehdr T4 ON T1.DocNo = T4.VdInvNo
                And T4.CancelInd = 'N'
                And T4.Status = 'A'
        ) D ON A.DocNo = D.DocNo And A.DNo = D.DNo
        LEFT JOIN
        (
            SELECT T1.DocNo, IFNULL(Sum(T5.Amt), 0.00) VoucherOPAmt
            From TblVendorInvoiceHdr T1
            inner Join TblPurchaseInvoiceHdr T2 On T1.DocNo = T2.VdInvNo
                And T2.CancelInd = 'N'
                And T2.Status = 'A'
                And T1.CancelInd = 'N'
            inner JOIN TblOutgoingPaymentDtl T3 ON T2.DocNo = T3.InvoiceDocno
            inner JOIN TblOutgoingPaymentHdr T4 ON T3.DocNo = T4.DocNo
                And T4.CancelInd = 'N'
                And T4.Status = 'A'
            inner JOIN tblvoucherhdr T5 ON T4.VoucherRequestDocNo = T5.VoucherRequestDocNo
                And T5.CancelInd = 'N'
            Group By T1.DocNo
        ) E ON A.DocNo = E.DocNo
        INNER JOIN
        (
            SELECT Distinct T1.DocNo, T1.DNo, T7.PtName
            FROM tblvendorinvoicedtl T1
            INNER JOIN tblrecvvddtl T2 ON T1.RecvVdDocNo = T2.DocNo And T1.RecvVdDNo = T2.DNo
            INNER JOIN tblpodtl T3 ON T2.PODocNo = T3.DocNo AND T2.PODNo = T3.DNo
            INNER JOIN tblporequestdtl T4 ON T3.PORequestDocNo = T4.DocNo AND T3.POrequestDNo = T4.DNo
            INNER JOIN tblqtdtl T5 ON T4.QtDocNo = T5.DocNo AND T4.QtDNo = T5.DNo
            INNER JOIN tblqthdr T6 ON T5.DocNo = T6.DocNo
            INNER JOIN tblpaymentterm T7 ON T6.PtCode = T7.PtCode
        ) F ON A.DocNo = F.DocNo And A.DNo = F.DNo
    ) T
    Where T.DocNo = '".$DocNo."'
    ORDER BY T.LocalDocNo;";

    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
    while ($rows = mysqli_fetch_assoc($result)) {

?>

        <div class="container-fluid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Document#</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="Doc" name="Doc" value="<?= $rows['PIDocNo'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Invoice#</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="Inv" name="Inv" value="<?= $rows['LocalDocNo'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Entity</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="Ent" name="Ent" value="<?= $rows['EntName'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Date</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="Date" name="Date" value="<?= $rows['PIDocDt'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Payment Type</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="Pay" name="Pay" value="<?= $rows['OptDesc'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Due Date</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="DueDt" name="DueDt" value="<?= $rows['DueDt'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Currency</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="Cur" name="Cur" value="<?php $rows['CurCode'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Total Without Tax</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="TotalWthTx" name="TotalWthTax" value="<?= $rows['Amt'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Total With Tax</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="TotalWtTax" name="TotalWtTax" value="<?= $rows['TotalAmt'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Total Tax</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="TotalTax" name="TotalTax" value="<?= $rows['TaxAmt'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Invoice Status</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="InvSt" name="InvSt" value="<?= $rows['Status'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Remark</label>
                    </div>
                    <div class="col-md-5 col-xs-5">
                        <input type="text" class="form-control item" id="Remark" name="Remark" value="<?= $rows['Remark'] ?>" readonly />
                    </div>
                </div>
                <div style="height:20px;"></div>
                <div>
                    <input type="reset" class="btn pull-right" onclick="return confirmCancel();" value="Close" id="BtnCancel" style="background-color: #58585A; color:white" />
                </div>
            </div>
        </div>

    <?php } ?>

<?php } ?>