<?php

function GenerateDocNo($DocDt, $Type, $Tbl)
{
	include "conn.php";

	$Mth = substr($DocDt, 4, 2);
	$Yr = substr($DocDt, 2, 2);
	$DocNo = "";

	$sqlDocTitle = "SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle'";
	$resultDocTitle = mysqli_query($conn, $sqlDocTitle);
	$query = "";
	while ($res = mysqli_fetch_assoc($resultDocTitle)) {
		$query = $query . "Select Concat( ";
		$query = $query . "IfNull(( ";
		$query = $query . "   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ";
		$query = $query . "       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " . $Tbl . "";
		$query = $query . "       Where Right(DocNo, 5)=Concat('" . $Mth . "','/', '" . $Yr . "') ";
		$query = $query . "       Order By Left(DocNo, 4) Desc Limit 1 ";
		$query = $query . "       ) As Temp ";
		$query = $query . "   ), '0001') ";
		$query = $query . ", '/', '" . ((strlen($res['ParValue']) == 0) ? "XXX" : $res['ParValue']) . "";
		$query = $query . "', '/', (Select IfNull(DocAbbr, '') DocAbbr From TblDocAbbreviation Where DocType = '" . $Type . "'), '/', '" . $Mth . "','/', '" . $Yr . "' ";
		$query = $query . ") As DocNo ";

		$result = mysqli_query($conn, $query);
		while ($Doc = mysqli_fetch_assoc($result)) {
			$DocNo = $Doc['DocNo'];
		}
	}

	return $DocNo;

	//mysqli_close($conn);
}

function GenerateVoucherRequestDocNo($DocDt)
{
	include "conn.php";

	$Mth = substr($DocDt, 4, 2);
	$Yr = substr($DocDt, 2, 2);

	$DocNo = "";
	$query = "";

	$query = $query . "Select Concat( ";
	$query = $query . "'" . $Yr . "','/', '" . $Mth . "', '/', ";
	$query = $query . "( ";
	$query = $query . "    Select IfNull((";
	$query = $query . "        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ";
	$query = $query . "        From ( ";
	$query = $query . "            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ";
	$query = $query . "            From TblVoucherRequestHdr ";
	$query = $query . "            Where Left(DocNo, 5)= Concat('" . $Yr . "','/', '" . $Mth . "') ";
	$query = $query . "            Order By Substring(DocNo, 7, 5) Desc Limit 1 ";
	$query = $query . "        ) As Temp ";
	$query = $query . "    ), '0001') As Number ";
	$query = $query . "), '/', ";
	$query = $query . "(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ";
	$query = $query . ") As DocNo ";

	$result = mysqli_query($conn, $query);
	while ($Doc = mysqli_fetch_assoc($result)) {
		$DocNo = $Doc['DocNo'];
	}

	return $DocNo;
}

function GenerateNewsCode($DocDt)
{
	include "conn.php";

	$Mth = substr($DocDt, 4, 2);
	$Yr = substr($DocDt, 2, 2);

	$DocNo = "";
	$query = "";

	$query = $query . "Select Concat( ";
	$query = $query . "'" . $Yr . "','/', '" . $Mth . "', '/', ";
	$query = $query . "( ";
	$query = $query . "    Select IfNull((";
	$query = $query . "        Select Right(Concat('0000', Convert(NewsCode+1, Char)), 4) As Numb ";
	$query = $query . "        From ( ";
	$query = $query . "            Select Convert(Substring(NewsCode, 7, 4), Decimal) As NewsCode ";
	$query = $query . "            From TblEmpNews ";
	$query = $query . "            Where Left(NewsCode, 5)= Concat('" . $Yr . "','/', '" . $Mth . "') ";
	$query = $query . "            Order By Substring(NewsCode, 7, 5) Desc Limit 1 ";
	$query = $query . "        ) As Temp ";
	$query = $query . "    ), '0001') As Number ";
	$query = $query . "), '/', ";
	$query = $query . "'NW' ";
	$query = $query . ") As DocNo ";

	$result = mysqli_query($conn, $query);
	while ($Doc = mysqli_fetch_assoc($result)) {
		$DocNo = $Doc['DocNo'];
	}

	return $DocNo;
}

function GetParameter($ParCode)
{
	require "conn.php";

	$query = "Select ParValue From TblParameter Where ParCode = '" . $ParCode . "';";
	$result = mysqli_query($conn, $query);
	while ($res = mysqli_fetch_assoc($result)) {
		return $res['ParValue'];
	}

	//mysqli_close($conn);
}

function GetValue($sql)
{
	require "conn.php";

	$result = mysqli_query($conn, $sql);
	$row = mysqli_num_rows($result);
	if ($row > 0) {
		while ($res = mysqli_fetch_array($result)) {
			return $res[0];
		}
	} else {
		return "";
	}

	//mysqli_close($conn);
}

function IsOTRequestNotValid()
{
	require "conn.php";

	$EmpCode = $_SESSION['empcode'];
	$sql = "";
	$ListOTDt = "";

	$sql = $sql . "Select Group_Concat(A.OTDt) OTDt From TblOTRequestHdr A ";
	$sql = $sql . "Inner Join TblOTRequestDtl B On A.DocNo=B.DocNo And B.EmpCode='" . $EmpCode . "' ";
	$sql = $sql . "Where A.CancelInd='N' And A.Status<>'C' Group By B.EmpCode; ";

	$result = mysqli_query($conn, $sql);
	$row = mysqli_num_rows($result);
	if ($row > 0) {
		while ($res = mysqli_fetch_assoc($result)) {
			$ListOTDt = $res['OTDt'];
		}
	}

	return $ListOTDt;
}

function IsLeaveRequestNotValid() // ini jangan dirubah format tanggalnya, efeknya ngerubah banyak di Leave Request javascriptnya
{
	require "conn.php";

	$EmpCode = $_SESSION['empcode'];
	$sql = "";
	$ListLeaveDt = "";

	$sql = $sql . "Select Group_Concat(T.LeaveDt) LeaveDt ";
	$sql = $sql . "From ";
	$sql = $sql . "( ";
	$sql = $sql . "	Select A.EmpCode, Concat(Substr(B.LeaveDt, 5, 2), '/', Right(B.LeaveDt, 2), '/', Left(B.LeaveDt, 4)) As LeaveDt ";
	$sql = $sql . "	From TblEmpLeaveHdr A ";
	$sql = $sql . "	Inner Join TblEmpLeaveDtl B On A.DocNo = B.DocNo ";
	$sql = $sql . "	Where CancelInd = 'N'  ";
	$sql = $sql . "	And IfNull(A.Status, 'O')<>'C'  ";
	$sql = $sql . "	And A.EmpCode = '" . $EmpCode . "' ";
	$sql = $sql . "	Union All ";
	$sql = $sql . "	Select B.EmpCode, Concat(Substr(C.LeaveDt, 5, 2), '/', Right(C.LeaveDt, 2), '/', Left(C.LeaveDt, 4)) As LeaveDt ";
	$sql = $sql . "	From TblEmpLeave2Hdr A ";
	$sql = $sql . "	Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo  ";
	$sql = $sql . "	Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ";
	$sql = $sql . "	Where A.CancelInd='N' ";
	$sql = $sql . "	And IfNull(A.Status, 'O')<>'C' ";
	$sql = $sql . "	And B.EmpCode='" . $EmpCode . "' ";
	$sql = $sql . ")T ";
	$sql = $sql . "Group By T.EmpCode; ";

	/*$sql = $sql . "	Select Group_Concat(B.LeaveDt) As LeaveDt ";
	$sql = $sql . "	From TblEmpLeaveHdr A ";
	$sql = $sql . "	Inner Join TblEmpLeaveDtl B On A.DocNo = B.DocNo ";
	$sql = $sql . "	Where CancelInd = 'N'  ";
	$sql = $sql . "	And IfNull(A.Status, 'O')<>'C'  ";
	$sql = $sql . "	And A.EmpCode = '".$EmpCode."' ";
	$sql = $sql . " Group By A.EmpCode; ";*/

	$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
	$row = mysqli_num_rows($result);
	if ($row > 0) {
		while ($res = mysqli_fetch_assoc($result)) {
			$ListLeaveDt = $res['LeaveDt'];
		}
	}

	return $ListLeaveDt;
}

function TotalAnnualLeave($EmpCode, $RLPType, $Yr, $LeaveCode, $YrJoin, $MthJoin, $AnnualLeaveNoOfYrToActive)
{
	require "conn.php";

	$sql = "";
	$TotalAnnualLeaveDay = "0";

	$TempYr = ($YrJoin + $AnnualLeaveNoOfYrToActive);
	$TempMth = substr($MthJoin, 0, 2);

	if ($TempMth == "12") {
		$TempYr = $TempYr + 1;
		$TempMth = "01";
	} else {
		$TempMth = substr("00" . ($TempMth + 1), -2);
	}

	if ($LeaveCode == GetParameter("AnnualLeaveCode") && date('Ymd') < $TempYr . $TempMth . "01") {
		$TotalAnnualLeaveDay = GetValue("Select NoOfDay From TblLeave Where LeaveCode = '" . GetParameter("AnnualLeaveCode") . "' ");
	} else {
		$sql = $sql . " Select Sum(T.UsedDay) UsedDay From ( ";
		$sql = $sql . "	Select A.EmpCode, ";
		$sql = $sql . "	Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . "	From TblEmpLeaveHdr A ";
		$sql = $sql . "	Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo ";
		$sql = $sql . "	Where A.CancelInd='N' ";
		$sql = $sql . "	And A.Status In ('O', 'A') ";
		$sql = $sql . "	And A.LeaveCode='" . $LeaveCode . "' ";
		$sql = $sql . "	And A.EmpCode='" . $EmpCode . "' ";
		if ($RLPType == "1") {
			$sql = $sql . "And (B.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		} else if ($RLPType == "2" || $RLPType == "3") {
			$nextYear = date("Ymd", strtotime($Yr . $MthJoin . " + 364 days"));
			$sql = $sql . "And (B.LeaveDt Between Concat('" . $Yr . "', '" . $MthJoin . "') And '" . $nextYear . "') ";
		}

		$sql = $sql . " Union All ";
		$sql = $sql . " Select B.EmpCode, ";
		$sql = $sql . " Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . " From TblEmpLeave2Hdr A ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ";
		$sql = $sql . " Where A.CancelInd='N' ";
		$sql = $sql . " And A.Status In ('O', 'A') ";
		$sql = $sql . " And A.LeaveCode='" . $LeaveCode . "' ";
		$sql = $sql . "	And B.EmpCode='" . $EmpCode . "' ";
		if ($RLPType == "1") {
			$sql = $sql . "And (C.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		} else if ($RLPType == "2" || $RLPType == "3") {
			$nextYear = date("Ymd", strtotime($Yr . $MthJoin . " + 364 days"));
			$sql = $sql . "And (C.LeaveDt Between Concat('" . $Yr . "', '" . $MthJoin . "') And '" . $nextYear . "') ";
		}

		if ($LeaveCode == GetParameter("AnnualLeaveCode")) {
			$sql = $sql . " Union All ";
			$sql = $sql . " Select B.EmpCode, B.NoOfDay As UsedDay ";
			$sql = $sql . " From TblRLPHdr A ";
			$sql = $sql . " Inner Join TblRLPDtl B On A.DocNo=B.DocNo ";
			$sql = $sql . " And B.EmpCode = '" . $EmpCode . "' ";
			$sql = $sql . " Where A.CancelInd='N' ";
			$sql = $sql . " And A.Status In ('O', 'A') ";
			$sql = $sql . " And A.RLPCode='03' ";
			$sql = $sql . " And A.Yr='" . $Yr . "' ";
		}

		$sql = $sql . ")T Group By T.EmpCode ";

		$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
		$row = mysqli_num_rows($result);
		if ($row > 0) {
			while ($res = mysqli_fetch_assoc($result)) {
				$TotalAnnualLeaveDay = $res['UsedDay'];
			}
		}
	}

	return $TotalAnnualLeaveDay;
}

function TotalLongServiceLeave($EmpCode, $RLPType, $Yr, $LeaveCode, $YrJoin, $MthJoin, $LongServiceLeaveNoOfYrToActive)
{
	require "conn.php";

	$sql = "";
	$TotalLongServiceLeaveDay = "0";

	$TempYr = ($YrJoin + $LongServiceLeaveNoOfYrToActive);
	$TempMth = substr($MthJoin, 0, 2);

	if ($TempMth == "12") {
		$TempYr = $TempYr + 1;
		$TempMth = "01";
	} else {
		$TempMth = substr("00" . ($TempMth + 1), -2);
	}

	if ($LeaveCode == GetParameter("LongServiceLeaveCode") && date('Ymd') < $TempYr . $TempMth . "01") {
		$TotalLongServiceLeaveDay = GetValue("Select NoOfDay From TblLeave Where LeaveCode = '" . GetParameter("LongServiceLeaveCode") . "' ");
	} else {

		$sql = $sql . "Select Sum(T.UsedDay) UsedDay From ( ";
		$sql = $sql . "	Select A.EmpCode, ";
		$sql = $sql . "	Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . "	From TblEmpLeaveHdr A ";
		$sql = $sql . "	Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo ";
		$sql = $sql . "	Where A.CancelInd='N' ";
		$sql = $sql . "	And A.Status In ('O', 'A') ";
		$sql = $sql . "	And A.LeaveCode='" . $LeaveCode . "' ";
		$sql = $sql . "	And A.EmpCode='" . $EmpCode . "' ";
		if ($RLPType == "1") {
			$sql = $sql . "And (B.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		} else if ($RLPType == "2" || $RLPType == "3") {
			$nextYear = date("Ymd", strtotime($Yr . $MthJoin . " + 365 days"));
			$sql = $sql . "And (B.LeaveDt Between Concat('" . $Yr . "', '" . $MthJoin . "') And '" . $nextYear . "') ";
		}

		$sql = $sql . " Union All ";
		$sql = $sql . " Select B.EmpCode, ";
		$sql = $sql . " Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . " From TblEmpLeave2Hdr A ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ";
		$sql = $sql . " Where A.CancelInd='N' ";
		$sql = $sql . " And A.Status In ('O', 'A') ";
		$sql = $sql . " And A.LeaveCode='" . $LeaveCode . "' ";
		$sql = $sql . "	And B.EmpCode='" . $EmpCode . "' ";
		if ($RLPType == "1") {
			$sql = $sql . "And (C.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		} else if ($RLPType == "2" || $RLPType == "3") {
			$nextYear = date("Ymd", strtotime($Yr . $MthJoin . " + 365 days"));
			$sql = $sql . "And (C.LeaveDt Between Concat('" . $Yr . "', '" . $MthJoin . "') And '" . $nextYear . "') ";
		}

		if ($LeaveCode == GetParameter("LongServiceLeaveCode")) {
			$sql = $sql . " Union All ";
			$sql = $sql . " Select B.EmpCode, B.NoOfDay As UsedDay ";
			$sql = $sql . " From TblRLPHdr A ";
			$sql = $sql . " Inner Join TblRLPDtl B On A.DocNo=B.DocNo ";
			$sql = $sql . " And B.EmpCode = '" . $EmpCode . "' ";
			$sql = $sql . " Where A.CancelInd='N' ";
			$sql = $sql . " And A.Status In ('O', 'A') ";
			$sql = $sql . " And A.RLPCode='02' ";
			$sql = $sql . " And A.Yr='" . $Yr . "' ";
		}

		$sql = $sql . ")T Group By T.EmpCode ";

		$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
		$row = mysqli_num_rows($result);
		if ($row > 0) {
			while ($res = mysqli_fetch_assoc($result)) {
				$TotalLongServiceLeaveDay = $res['UsedDay'];
			}
		}
	}

	return $TotalLongServiceLeaveDay;
}

function TotalLongServiceLeave2($EmpCode, $RLPType, $Yr, $LeaveCode, $YrJoin, $MthJoin, $LongServiceLeaveNoOfYrToActive2)
{
	require "conn.php";

	$sql = "";
	$TotalLongServiceLeaveDay = "0";

	$TempYr = ($YrJoin + $LongServiceLeaveNoOfYrToActive2);
	$TempMth = substr($MthJoin, 0, 2);


	if ($TempMth == "12") {
		$TempYr = $TempYr + 1;
		$TempMth = "01";
	} else {
		$TempMth = substr("00" . ($TempMth + 1), -2);
	}

	//echo $TempYr.$TempMth."01 -- ";

	if ($LeaveCode == GetParameter("LongServiceLeaveCode2") && date('Ymd') < $TempYr . $TempMth . "01") {
		$TotalLongServiceLeaveDay = GetValue("Select NoOfDay From TblLeave Where LeaveCode = '" . GetParameter("LongServiceLeaveCode2") . "' ");
	} else {

		$sql = $sql . "Select Sum(T.UsedDay) UsedDay From ( ";
		$sql = $sql . "	Select A.EmpCode, ";
		$sql = $sql . "	Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . "	From TblEmpLeaveHdr A ";
		$sql = $sql . "	Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo ";
		$sql = $sql . "	Where A.CancelInd='N' ";
		$sql = $sql . "	And A.Status In ('O', 'A') ";
		$sql = $sql . "	And A.LeaveCode='" . $LeaveCode . "' ";
		$sql = $sql . "	And A.EmpCode='" . $EmpCode . "' ";
		if ($RLPType == "1") {
			$sql = $sql . "And (B.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		} else if ($RLPType == "2" || $RLPType == "3") {
			$nextYear = date("Ymd", strtotime($Yr . $MthJoin . " + 365 days"));
			$sql = $sql . "And (B.LeaveDt Between Concat('" . $Yr . "', '" . $MthJoin . "') And '" . $nextYear . "') ";
		}

		$sql = $sql . " Union All ";
		$sql = $sql . " Select B.EmpCode, ";
		$sql = $sql . " Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . " From TblEmpLeave2Hdr A ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ";
		$sql = $sql . " Where A.CancelInd='N' ";
		$sql = $sql . " And A.Status In ('O', 'A') ";
		$sql = $sql . " And A.LeaveCode='" . $LeaveCode . "' ";
		$sql = $sql . "	And B.EmpCode='" . $EmpCode . "' ";
		if ($RLPType == "1") {
			$sql = $sql . "And (C.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		} else if ($RLPType == "2" || $RLPType == "3") {
			$nextYear = date("Ymd", strtotime($Yr . $MthJoin . " + 365 days"));
			$sql = $sql . "And (C.LeaveDt Between Concat('" . $Yr . "', '" . $MthJoin . "') And '" . $nextYear . "') ";
		}

		if ($LeaveCode == GetParameter("LongServiceLeaveCode2")) {
			$sql = $sql . " Union All ";
			$sql = $sql . " Select B.EmpCode, B.NoOfDay As UsedDay ";
			$sql = $sql . " From TblRLPHdr A ";
			$sql = $sql . " Inner Join TblRLPDtl B On A.DocNo=B.DocNo ";
			$sql = $sql . " And B.EmpCode = '" . $EmpCode . "' ";
			$sql = $sql . " Where A.CancelInd='N' ";
			$sql = $sql . " And A.Status In ('O', 'A') ";
			$sql = $sql . " And A.RLPCode='03' ";
			$sql = $sql . " And A.Yr='" . $Yr . "' ";
		}

		$sql = $sql . ")T Group By T.EmpCode ";

		$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
		$row = mysqli_num_rows($result);
		if ($row > 0) {
			while ($res = mysqli_fetch_assoc($result)) {
				$TotalLongServiceLeaveDay = $res['UsedDay'];
			}
		}
	}

	//echo "-- ".$TotalLongServiceLeaveDay." --";

	return $TotalLongServiceLeaveDay;
}

function BalanceAnnualLeave($EmpCode, $RLPType, $StartYr, $StartMth, $StartDt, $LeaveCode, $YrJoin, $MthJoin, $AnnualLeaveNoOfYrToActive)
{
	require "conn.php";

	$sql = "";
	$TotalAnnualLeaveDay = "0";

	/*$TempYr = ($YrJoin + $AnnualLeaveNoOfYrToActive);
	$TempMth = substr($MthJoin, 0, 2);

	if($TempMth == "12")
	{
		$TempYr = $TempYr + 1;
		$TempMth = "01";
	}
	else
	{
		$TempMth = substr("00" . ($TempMth + 1), -2);
	}

	if($LeaveCode == GetParameter("AnnualLeaveCode") && date('Ymd') < $TempYr.$TempMth."01")
	{
		$TotalAnnualLeaveDay = GetValue("Select IfNull((B.NoOfDays2 + B.NoOfDays3), A.NoOfDay) As NoOfDay From TblLeave A Left Join TblLeaveSummary B On B.EmpCode = '".$EmpCode."' And B.Yr = '".$Yr."' Where A.LeaveCode = '".$LeaveCode."' ");
	}
	else
	{
		$sql = $sql . " Select Sum(T.UsedDay) UsedDay From ( ";
		$sql = $sql . "	Select A.EmpCode, ";
		$sql = $sql . "	Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . "	From TblEmpLeaveHdr A ";
		$sql = $sql . "	Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo ";
		$sql = $sql . "	Where A.CancelInd='N' ";
		$sql = $sql . "	And A.Status In ('O', 'A') ";
		$sql = $sql . "	And A.LeaveCode='".$LeaveCode."' ";
		$sql = $sql . "	And A.EmpCode='".$EmpCode."' ";
		if($RLPType == "1")
		{
			$sql = $sql . "And (B.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		}
		else if($RLPType == "2" || $RLPType == "3")
		{
			$nextYear = date("Ymd", strtotime($Yr.$MthJoin. " + 364 days"));
			$sql = $sql . "And (B.LeaveDt Between Concat('".$Yr."', '".$MthJoin."') And '".$nextYear."') ";
		}

		$sql = $sql . " Union All ";
		$sql = $sql . " Select B.EmpCode, ";
		$sql = $sql . " Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . " From TblEmpLeave2Hdr A ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ";
		$sql = $sql . " Where A.CancelInd='N' ";
		$sql = $sql . " And A.Status In ('O', 'A') ";
		$sql = $sql . " And A.LeaveCode='".$LeaveCode."' ";
		$sql = $sql . "	And B.EmpCode='".$EmpCode."' ";
		if($RLPType == "1")
		{
			$sql = $sql . "And (C.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		}
		else if($RLPType == "2" || $RLPType == "3")
		{
			$nextYear = date("Ymd", strtotime($Yr.$MthJoin. " + 364 days"));
			$sql = $sql . "And (C.LeaveDt Between Concat('".$Yr."', '".$MthJoin."') And '".$nextYear."') ";
		}

		if($LeaveCode == GetParameter("AnnualLeaveCode"))
		{
			$sql = $sql . " Union All ";
			$sql = $sql . " Select B.EmpCode, B.NoOfDay As UsedDay ";
			$sql = $sql . " From TblRLPHdr A ";
			$sql = $sql . " Inner Join TblRLPDtl B On A.DocNo=B.DocNo ";
			$sql = $sql . " And B.EmpCode = '".$EmpCode."' ";
			$sql = $sql . " Where A.CancelInd='N' ";
			$sql = $sql . " And A.Status In ('O', 'A') ";
			$sql = $sql . " And A.RLPCode='03' ";
			$sql = $sql . " And A.Yr='".$Yr."' ";
		}

		$sql = $sql . ")T Group By T.EmpCode ";

		$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
		$row = mysqli_num_rows($result);
		if($row > 0)
		{
			while($res = mysqli_fetch_assoc($result))
			{
				$TotalAnnualLeaveDay = $res['UsedDay'];
			}
		}
	}

	return $TotalAnnualLeaveDay;*/

	$TempYr = ($YrJoin + $AnnualLeaveNoOfYrToActive);
	$TempMth = substr($MthJoin, 0, 2);
	$TglBerhakCuti = $TempYr . "0101";

	if ($TempYr == $StartYr) {
		$TglBerhakCuti = $TempYr . $MthJoin;
	}

	if ($TglBerhakCuti <= $StartYr . $StartMth . $StartDt) {
		//$TotalAnnualLeaveDay = GetValue("Select IfNull((B.NoOfDays2 + B.NoOfDays3), A.NoOfDay) As NoOfDay From TblLeave A Left Join TblLeaveSummary B On B.EmpCode = '".$EmpCode."' And B.Yr = '".$StartYr."' Where A.LeaveCode = '".$LeaveCode."' ");

		$TotalAnnualLeaveDay = GetValue("Select IfNull((B.NoOfDays2 + B.NoOfDays3), 0) As NoOfDay From TblLeave A Left Join TblLeaveSummary B On B.EmpCode = '" . $EmpCode . "' And B.Yr = '" . $StartYr . "' And B.LeaveCode = '" . $LeaveCode . "' Where A.LeaveCode = '" . $LeaveCode . "' ");
	}

	return $TotalAnnualLeaveDay;
}

function BalanceLongServiceLeave($EmpCode, $RLPType, $StartYr, $StartMth, $StartDt, $LeaveCode, $YrJoin, $MthJoin, $LongServiceLeaveNoOfYrToActive)
{
	require "conn.php";

	$sql = "";
	$TotalLongServiceLeaveDay = "0";

	/*$TempYr = ($YrJoin + $LongServiceLeaveNoOfYrToActive);
	$TempMth = substr($MthJoin, 0, 2);

	if($TempMth == "12")
	{
		$TempYr = $TempYr + 1;
		$TempMth = "01";
	}
	else
	{
		$TempMth = substr("00" . ($TempMth + 1), -2);
	}

	if($LeaveCode == GetParameter("LongServiceLeaveCode") && date('Ymd') < $TempYr.$TempMth."01")
	{
		$TotalLongServiceLeaveDay = GetValue("Select IfNull((B.NoOfDays2 + B.NoOfDays3), A.NoOfDay) As NoOfDay From TblLeave A Left Join TblLeaveSummary B On B.EmpCode = '".$EmpCode."' And B.Yr = '".$Yr."' Where A.LeaveCode = '".$LeaveCode."' ");
	}
	else
	{

		$sql = $sql . "Select Sum(T.UsedDay) UsedDay From ( ";
		$sql = $sql . "	Select A.EmpCode, ";
		$sql = $sql . "	Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . "	From TblEmpLeaveHdr A ";
		$sql = $sql . "	Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo ";
		$sql = $sql . "	Where A.CancelInd='N' ";
		$sql = $sql . "	And A.Status In ('O', 'A') ";
		$sql = $sql . "	And A.LeaveCode='".$LeaveCode."' ";
		$sql = $sql . "	And A.EmpCode='".$EmpCode."' ";
		if($RLPType == "1")
		{
			$sql = $sql . "And (B.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		}
		else if($RLPType == "2" || $RLPType == "3")
		{
			$nextYear = date("Ymd", strtotime($Yr.$MthJoin. " + 365 days"));
			$sql = $sql . "And (B.LeaveDt Between Concat('".$Yr."', '".$MthJoin."') And '".$nextYear."') ";
		}

		$sql = $sql . " Union All ";
		$sql = $sql . " Select B.EmpCode, ";
		$sql = $sql . " Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . " From TblEmpLeave2Hdr A ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ";
		$sql = $sql . " Where A.CancelInd='N' ";
		$sql = $sql . " And A.Status In ('O', 'A') ";
		$sql = $sql . " And A.LeaveCode='".$LeaveCode."' ";
		$sql = $sql . "	And B.EmpCode='".$EmpCode."' ";
		if($RLPType == "1")
		{
			$sql = $sql . "And (C.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		}
		else if($RLPType == "2" || $RLPType == "3")
		{
			$nextYear = date("Ymd", strtotime($Yr.$MthJoin. " + 365 days"));
			$sql = $sql . "And (C.LeaveDt Between Concat('".$Yr."', '".$MthJoin."') And '".$nextYear."') ";
		}

		if($LeaveCode == GetParameter("LongServiceLeaveCode"))
		{
			$sql = $sql . " Union All ";
			$sql = $sql . " Select B.EmpCode, B.NoOfDay As UsedDay ";
			$sql = $sql . " From TblRLPHdr A ";
			$sql = $sql . " Inner Join TblRLPDtl B On A.DocNo=B.DocNo ";
			$sql = $sql . " And B.EmpCode = '".$EmpCode."' ";
			$sql = $sql . " Where A.CancelInd='N' ";
			$sql = $sql . " And A.Status In ('O', 'A') ";
			$sql = $sql . " And A.RLPCode='02' ";
			$sql = $sql . " And A.Yr='".$Yr."' ";
		}

		$sql = $sql . ")T Group By T.EmpCode ";

		$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
		$row = mysqli_num_rows($result);
		if($row > 0)
		{
			while($res = mysqli_fetch_assoc($result))
			{
				$TotalLongServiceLeaveDay = $res['UsedDay'];
			}
		}
	}

	return $TotalLongServiceLeaveDay;*/

	$TempYr = ($YrJoin + $LongServiceLeaveNoOfYrToActive);
	$TempMth = substr($MthJoin, 0, 2);
	$TglBerhakCuti = $TempYr . "0101";

	if ($TglBerhakCuti <= $StartYr . $StartMth . $StartDt) {
		//$TotalLongServiceLeaveDay = GetValue("Select IfNull((B.NoOfDays2 + B.NoOfDays3), A.NoOfDay) As NoOfDay From TblLeave A Left Join TblLeaveSummary B On B.EmpCode = '".$EmpCode."' And B.Yr = '".$StartYr."' Where A.LeaveCode = '".$LeaveCode."' ");

		$TotalLongServiceLeaveDay = GetValue("Select IfNull((B.NoOfDays2 + B.NoOfDays3), 0) As NoOfDay From TblLeave A Left Join TblLeaveSummary B On B.EmpCode = '" . $EmpCode . "' And B.Yr = '" . $StartYr . "'  And B.LeaveCode = '" . $LeaveCode . "' Where A.LeaveCode = '" . $LeaveCode . "' ");
	}

	return $TotalLongServiceLeaveDay;
}

function BalanceLongServiceLeave2($EmpCode, $RLPType, $StartYr, $StartMth, $StartDt, $LeaveCode, $YrJoin, $MthJoin, $LongServiceLeaveNoOfYrToActive2)
{
	require "conn.php";

	$sql = "";
	$TotalLongServiceLeaveDay2 = "0";

	/*$TempYr = ($YrJoin + $LongServiceLeaveNoOfYrToActive2);
	$TempMth = substr($MthJoin, 0, 2);


	if($TempMth == "12")
	{
		$TempYr = $TempYr + 1;
		$TempMth = "01";
	}
	else
	{
		$TempMth = substr("00" . ($TempMth + 1), -2);
	}

	//echo $TempYr.$TempMth."01 -- ";

	if($LeaveCode == GetParameter("LongServiceLeaveCode2") && date('Ymd') < $TempYr.$TempMth."01")
	{
		$TotalLongServiceLeaveDay = GetValue("Select IfNull((B.NoOfDays2 + B.NoOfDays3), A.NoOfDay) As NoOfDay From TblLeave A Left Join TblLeaveSummary B On B.EmpCode = '".$EmpCode."' And B.Yr = '".$Yr."' Where A.LeaveCode = '".$LeaveCode."' ");
	}
	else
	{

		$sql = $sql . "Select Sum(T.UsedDay) UsedDay From ( ";
		$sql = $sql . "	Select A.EmpCode, ";
		$sql = $sql . "	Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . "	From TblEmpLeaveHdr A ";
		$sql = $sql . "	Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo ";
		$sql = $sql . "	Where A.CancelInd='N' ";
		$sql = $sql . "	And A.Status In ('O', 'A') ";
		$sql = $sql . "	And A.LeaveCode='".$LeaveCode."' ";
		$sql = $sql . "	And A.EmpCode='".$EmpCode."' ";
		if($RLPType == "1")
		{
			$sql = $sql . "And (B.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		}
		else if($RLPType == "2" || $RLPType == "3")
		{
			$nextYear = date("Ymd", strtotime($Yr.$MthJoin. " + 365 days"));
			$sql = $sql . "And (B.LeaveDt Between Concat('".$Yr."', '".$MthJoin."') And '".$nextYear."') ";
		}

		$sql = $sql . " Union All ";
		$sql = $sql . " Select B.EmpCode, ";
		$sql = $sql . " Case When A.LeaveType='F' Then 1 Else 0.5 End As UsedDay ";
		$sql = $sql . " From TblEmpLeave2Hdr A ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo ";
		$sql = $sql . " Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo ";
		$sql = $sql . " Where A.CancelInd='N' ";
		$sql = $sql . " And A.Status In ('O', 'A') ";
		$sql = $sql . " And A.LeaveCode='".$LeaveCode."' ";
		$sql = $sql . "	And B.EmpCode='".$EmpCode."' ";
		if($RLPType == "1")
		{
			$sql = $sql . "And (C.LeaveDt Between Concat($Yr, '0101') And Concat($Yr, '1231')) ";
		}
		else if($RLPType == "2" || $RLPType == "3")
		{
			$nextYear = date("Ymd", strtotime($Yr.$MthJoin. " + 365 days"));
			$sql = $sql . "And (C.LeaveDt Between Concat('".$Yr."', '".$MthJoin."') And '".$nextYear."') ";
		}

		if($LeaveCode == GetParameter("LongServiceLeaveCode2"))
		{
			$sql = $sql . " Union All ";
			$sql = $sql . " Select B.EmpCode, B.NoOfDay As UsedDay ";
			$sql = $sql . " From TblRLPHdr A ";
			$sql = $sql . " Inner Join TblRLPDtl B On A.DocNo=B.DocNo ";
			$sql = $sql . " And B.EmpCode = '".$EmpCode."' ";
			$sql = $sql . " Where A.CancelInd='N' ";
			$sql = $sql . " And A.Status In ('O', 'A') ";
			$sql = $sql . " And A.RLPCode='03' ";
			$sql = $sql . " And A.Yr='".$Yr."' ";
		}

		$sql = $sql . ")T Group By T.EmpCode ";

		$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
		$row = mysqli_num_rows($result);
		if($row > 0)
		{
			while($res = mysqli_fetch_assoc($result))
			{
				$TotalLongServiceLeaveDay = $res['UsedDay'];
			}
		}
	}

	//echo "-- ".$TotalLongServiceLeaveDay." --";

	return $TotalLongServiceLeaveDay;*/

	$TempYr = ($YrJoin + $LongServiceLeaveNoOfYrToActive2);
	$TempMth = substr($MthJoin, 0, 2);
	$TglBerhakCuti = $TempYr . "0101";

	if ($TglBerhakCuti <= $StartYr . $StartMth . $StartDt) {
		//$TotalLongServiceLeaveDay2 = GetValue("Select IfNull((B.NoOfDays2 + B.NoOfDays3), A.NoOfDay) As NoOfDay From TblLeave A Left Join TblLeaveSummary B On B.EmpCode = '".$EmpCode."' And B.Yr = '".$StartYr."' Where A.LeaveCode = '".$LeaveCode."' ");

		$TotalLongServiceLeaveDay2 = GetValue("Select IfNull((B.NoOfDays2 + B.NoOfDays3), 0) As NoOfDay From TblLeave A Left Join TblLeaveSummary B On B.EmpCode = '" . $EmpCode . "' And B.Yr = '" . $StartYr . "' And B.LeaveCode = '" . $LeaveCode . "' Where A.LeaveCode = '" . $LeaveCode . "' ");
	}

	return $TotalLongServiceLeaveDay2;
}

function ShowHolidayDate() // ini jangan dirubah format tanggalnya, efeknya ngerubah banyak di Leave Request javascriptnya
{
	require "conn.php";

	$EmpCode = $_SESSION['empcode'];
	$sql = "";
	$ListHolidayDt = "";

	$sql = $sql . "Select A.EmpCode, Group_Concat(DATE_FORMAT(A.Dt, '%m/%d/%Y')) As HolidayDt ";
	$sql = $sql . "From TblEmpWorkSchedule A ";
	$sql = $sql . "Inner Join TblWorkSchedule B On A.WSCode = B.WSCode and B.ActInd = 'Y' and B.HolidayInd = 'Y' ";
	$sql = $sql . "Where A.EmpCode = '" . $EmpCode . "' ";
	$sql = $sql . "Group By A.EmpCode; ";

	$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
	$row = mysqli_num_rows($result);
	if ($row > 0) {
		while ($res = mysqli_fetch_assoc($result)) {
			$ListHolidayDt = $res['HolidayDt'];
		}
	}

	return $ListHolidayDt;
}

function LeaveNoOfDay($EmpCode, $RLPType, $StartYr, $StartMth, $StartDt, $LeaveCode, $YrJoin, $MthJoin)
{
	require "conn.php";

	$sql = "";
	$TotalNoOfDay = "0";
	$NoOfYrToActive = "0";
	$NoOfDay = "0";


	if ($LeaveCode == GetValue("Select Parvalue From TblParameter Where Parcode = 'AnnualLeaveCode' ")) {
		$NoOfYrToActive = GetValue("Select IfNull(ParValue, 0) As ParValue From TblParameter Where Parcode = 'AnnualLeaveNoOfYrToActive' ");
	} else if ($LeaveCode == GetValue("Select Parvalue From TblParameter Where Parcode = 'LongServiceLeaveCode' ")) {
		$NoOfYrToActive = GetValue("Select IfNull(ParValue, 0) As ParValue From TblParameter Where Parcode = 'LongServiceLeaveNoOfYrToActive' ");
	} else if ($LeaveCode == GetValue("Select Parvalue From TblParameter Where Parcode = 'LongServiceLeaveCode2' ")) {
		$NoOfYrToActive = GetValue("Select IfNull(ParValue, 0) As ParValue From TblParameter Where Parcode = 'LongServiceLeaveNoOfYrToActive2' ");
	}

	$TempYr = ($YrJoin + $NoOfYrToActive);
	$TempMth = substr($MthJoin, 0, 2);

	if ($LeaveCode == GetValue("Select Parvalue From TblParameter Where Parcode = 'AnnualLeaveCode' ")) {
		$TglBerhakCuti = $TempYr . $MthJoin;
	} else if ($LeaveCode == GetValue("Select Parvalue From TblParameter Where Parcode = 'LongServiceLeaveCode' ") || $LeaveCode == GetValue("Select Parvalue From TblParameter Where Parcode = 'LongServiceLeaveCode2' ")) {
		$TglBerhakCuti = $TempYr . "0101";
	}


	if ($TglBerhakCuti <= $StartYr . $StartMth . $StartDt) {
		$NoOfDay = GetValue("Select NoOfDay From TblLeave Where LeaveCode = '" . $LeaveCode . "' ");
	}

	return $NoOfDay;
}

/*function AnnualLeaveStat($EmpCode, $RLPType, $StartYr, $StartMth, $StartDt, $LeaveCode, $YrJoin, $MthJoin, $AnnualLeaveNoOfYrToActive)
{
	require "conn.php";

	$sql = "";
	$TotalAnnualLeaveDay = "0";

	//$TempYr = ($YrJoin + $AnnualLeaveNoOfYrToActive);
	//$TempMth = substr($MthJoin, 0, 2);
	//$TglBerhakCuti = $TempYr . $MthJoin;

	//if($TglBerhakCuti <= $StartYr . $StartMth . $StartDt)
	//{
		$TotalAnnualLeaveDay = GetValue("Select IfNull(B.Balance, 0) As NoOfDay From TblLeaveSummary B Where B.EmpCode = '".$EmpCode."' And B.Yr = '".$StartYr."' And B.LeaveCode = '".$LeaveCode."' ");

		//$TotalAnnualLeaveDay = GetValue("Select IfNull(B.Balance, 0) As NoOfDay From TblLeaveSummary B Where B.EmpCode = '".$EmpCode."' And B.Yr = '".$StartYr."' And B.LeaveCode = '".$LeaveCode."' ");
	//}

	return $TotalAnnualLeaveDay;
}

function LongServiceLeaveStat($EmpCode, $RLPType, $StartYr, $StartMth, $StartDt, $LeaveCode, $YrJoin, $MthJoin, $LongServiceLeaveNoOfYrToActive)
{
	require "conn.php";

	$sql = "";
	$TotalLongServiceLeaveDay = "0";

	//$TempYr = ($YrJoin + $LongServiceLeaveNoOfYrToActive);
	//$TempMth = substr($MthJoin, 0, 2);
	//$TglBerhakCuti = $TempYr . "0101";

	//if($TglBerhakCuti <= $StartYr . $StartMth . $StartDt)
	//{
		$TotalLongServiceLeaveDay = GetValue("Select IfNull(B.Balance, 0) As NoOfDay From TblLeaveSummary B Where B.EmpCode = '".$EmpCode."' And B.Yr = '".$StartYr."' And B.LeaveCode = '".$LeaveCode."' ");

		//$TotalLongServiceLeaveDay = GetValue("Select IfNull(B.Balance, 0) As NoOfDay From TblLeaveSummary B Where B.EmpCode = '".$EmpCode."' And B.Yr = '".$StartYr."' And B.LeaveCode = '".$LeaveCode."' ");
	//}

	return $TotalLongServiceLeaveDay;
}

function LongServiceLeaveStat2($EmpCode, $RLPType, $StartYr, $StartMth, $StartDt, $LeaveCode, $YrJoin, $MthJoin, $LongServiceLeaveNoOfYrToActive2)
{
	require "conn.php";

	$sql = "";
	$TotalLongServiceLeaveDay2 = "0";

	//$TempYr = ($YrJoin + $LongServiceLeaveNoOfYrToActive2);
	//$TempMth = substr($MthJoin, 0, 2);
	//$TglBerhakCuti = $TempYr . "0101";

	//if($TglBerhakCuti <= $StartYr . $StartMth . $StartDt)
	//{
		$TotalLongServiceLeaveDay2 = GetValue("Select IfNull(B.Balance, 0) As NoOfDay From TblLeaveSummary B Where B.EmpCode = '".$EmpCode."' And B.Yr = '".$StartYr."' And B.LeaveCode = '".$LeaveCode."' ");

		//$TotalLongServiceLeaveDay2 = GetValue("Select IfNull(B.Balance, 0) As NoOfDay From TblLeaveSummary B Where B.EmpCode = '".$EmpCode."' And B.Yr = '".$StartYr."' And B.LeaveCode = '".$LeaveCode."' ");
	//}

	return $TotalLongServiceLeaveDay2;
}*/

function BalanceLeaveStat($EmpCode, $StartYr, $StartMth, $StartDt, $LeaveCode, $YrJoin, $MthJoin)
{
	require "conn.php";

	$sql = "";
	$BalanceLeave = 0;

	$BalanceLeave = GetValue("Select IfNull(Balance, 0) As NoOfDay From TblLeaveSummary Where EmpCode = '" . $EmpCode . "' And Yr = '" . $StartYr . "' And LeaveCode = '" . $LeaveCode . "' ");

	return $BalanceLeave;
}

function dateFormat($Dt)
{
	// string $Dt harus sudah ada nilainya, baik dari variable maupun dari $_POST atau $_GET
	$explodes = explode("/", $Dt);
	$Dates = $explodes[2] . $explodes[1] . $explodes[0];

	return $Dates;
}

// function savedDNo($DNo)
// {
// 	$_SESSION['arrayDNo'] = [];

// 	if($DNo != null){
// 		array_push($_SESSION['arrayDNo'], $DNo);
// 	}

// 	// var_dump($_SESSION['arrayDNo']);
// 	return $_SESSION['arrayDNo'];
// }

// function savedDocNo($DocNo)
// {
// 	$_SESSION['arrayDocNo'] = [];
// 	if($DocNo != null){
// 		array_push($_SESSION['arrayDocNo'], $DocNo);
// 	}
// 	// var_dump($_SESSION['arrayDocNo']);
// 	return $_SESSION['arrayDocNo'];
// }

function submitQT($DocNo, $row)
{
	require "conn.php";

	$sql = "SELECT A.DocNo, B.DNo, DATE_FORMAT(A.DocDt, '%d-%m-%Y') DocDt,IFNULL(DATE_FORMAT(A.ExpDt, '%d-%m-%Y'), DATE_FORMAT(CURRENT_DATE(), '%d-%m-%Y')) ExpDt, IFNULL(DATE_FORMAT(B.UsageDt, '%d-%m-%Y'), '-') UsageDt,
	C.ItName, B.Qty, If(B.EstPrice = 0, B.UPrice, B.EstPrice) EstPrice,  IFNULL(B.UsageDt, '-') UsageDt2, B.QtDocNo, A.DocDt AS DocDt2,
	B.TotalPrice, C.PurchaseUOMCode AS UOM, E.EntName, G.CityName, B.CurCode, H.PtName, A.Remark
	FROM TblMaterialRequestHdr A
	INNER JOIN TblMaterialRequestDtl B ON A.DocNo = B.DocNo
		AND A.RMInd = 'Y'
		AND B.`Status` = 'A'
		AND B.CancelInd = 'N'
	INNER JOIN TblItem C ON B.ItCode = C.ItCode 
	LEFT JOIN (
		SELECT A.DocNo, D.EntName
		FROM TblMaterialRequestHdr A
		INNER JOIN TblSite B ON A.SiteCode = B.SiteCode
		INNER JOIN TblProfitCenter C ON B.ProfitCenterCode = C.ProfitCenterCode
		INNER JOIN TblEntity D ON D.EntCOde = C.EntCode
	) E ON A.DocNo = E.DocNo
	LEFT JOIN TblCity G ON G.CityCode = B.CityCode
	Left JOIN tblpaymentterm H ON B.PtCode = H.PtCode
	WHERE A.DocNo = '".$DocNo."' AND B.DNo = '".$row."'
		-- AND ISNULL(T.MaterialRequestDocNo) AND ISNULL(T.MaterialRequestDNo) 
	AND A.TenderInd = 'N' 
	AND A.DocNo In
		(
			SELECT DISTINCT T1.MaterialRequestDocNo
			FROM TblRfqDtl T1
			INNER JOIN TblVendorSector T2 ON T1.SectorCode = T2.SectorCode AND T2.VdCode = '" . $_SESSION['vdcode'] . "'
		)               
	GROUP BY B.DNo
	ORDER BY A.CreateDt;";

	return $sql;
}
