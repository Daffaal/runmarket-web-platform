<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

error_reporting(0);
$host = FTP_HOST;
$user = FTP_USER;
$pass = FTP_PASS;

if(isset($_POST['submit']))
{
    $sql = "";

    $imgFile = $_FILES['VdFile']['name'];
    $tmp_dir = $_FILES['VdFile']['tmp_name'];
    $imgSize = $_FILES['VdFile']['size'];

    $upload_dir = "dist/pdf/";
    $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
    $valid_extension = array('pdf');
    $VdFilesLoc = $_SESSION['vdcode']."_".$_POST['LueCategory'].".".$imgExt;
    $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;

    if(in_array($imgExt, $valid_extension)) // check kalau extensi nya valid
    {
        if($imgSize == 0)
        {
            echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
            echo "<script type='text/javascript'>window.location='LegalDoc.php'</script>";
        }
        else
        {
            if($imgSize < $mMaxFile) // if image size < 1025 KB
            {
                // echo "<script type='text/javascript'>alert('".$tmp_dir." || ".$upload_dir.$VdFilesLoc."');</script>";
                move_uploaded_file($tmp_dir, $upload_dir.$VdFilesLoc);

                $sql = "";

                $sql = $sql . "Insert Into TblVendorFileUpload(VdCode, CategoryCode, Status, FileLocation, LocalDocNo, LicensedBy, StartDt, EndDt, CreateBy, CreateDt) ";
                $sql = $sql . "Values('".$_SESSION['vdcode']."', '".$_POST['LueCategory']."', 'A', '".$VdFilesLoc."', ";

                if(strlen($_POST['LocalDocNo']) > 0)
                    $sql = $sql . " '". mysqli_real_escape_string($conn,$_POST['LocalDocNo'])."', ";
                else
                    $sql = $sql . " NULL, ";

                if(strlen($_POST['LicensedBy']) > 0)
                    $sql = $sql . " '". mysqli_real_escape_string($conn,$_POST['LicensedBy'])."', ";
                else
                    $sql = $sql . " NULL, ";

                if(strlen($_POST['StartDt']) > 0)
                    $sql = $sql . "'".date("Ymd", strtotime($_POST['StartDt']))."', ";
                else
                    $sql = $sql . "NULL, ";
                
                if(strlen($_POST['EndDt']) > 0)
                    $sql = $sql . "'".date("Ymd", strtotime($_POST['EndDt']))."', ";
                else
                    $sql = $sql . "NULL, ";
                
                $sql = $sql . "'".$_SESSION['vdusercode']."', CurrentDateTime()); ";

                //echo $sql;

                if (mysqli_query($conn,$sql)) 
                {
                    mysqli_commit($conn);
                    echo "<script type='text/javascript'>window.location='LegalDoc.php'</script>";
                } 
                else 
                {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                    mysqli_rollback($conn);
                }
            }
            else
            {
                echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                echo "<script type='text/javascript'>window.location='LegalDoc.php'</script>";
            }
            }
    }
    else
    {
        echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
        echo "<script type='text/javascript'>window.location='LegalDoc.php'</script>";
    }

    if (FTP_STATUS === "enable") {
        try {
            $ftpConn = ftp_connect($host);
            $login = ftp_login($ftpConn,$user,$pass);
            $ftpPasv = ftp_pasv($ftpConn, true);

            if ((!$ftpConn) || (!$login)) {
                echo "FTP connection has failed! Attempted to connect to ". $host.".";
            }

            if (!$ftpPasv) {
                echo "Cannot switch to passive mode";
            }

            if($login) {
                
                $ftpSuccess = false;
                if (!ftp_put($ftpConn, FTP_DESTINATION.$VdFilesLoc, $upload_dir.$VdFilesLoc, FTP_BINARY)) {
                    echo "<script>alert('Error uploading ". $VdFilesLoc .".');</script>";
                }else {
                    $ftpSuccess = true;
                } 
                if($ftpSuccess){
                    echo "<script>alert('Success uploading File.');</script>";
                }
                ftp_close($ftpConn);
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
else if(isset($_POST['delete']))
{
    function file_exist_on_ftp_server($fileName, $ftpConn){
        $fileSize = ftp_size($ftpConn,FTP_DESTINATION.$fileName);
        if ($fileSize < 0) {
            return false;
        }
        
        return true;
    }
    $sql2 = "";
    $file = $_POST['delete'];
    // $oldName = $_POST['FileName1'];
    // $newName = $_POST['FileName2'];
    
    $sql2 = $sql2 . "Delete From TblVendorFileUpload ";
    $sql2 = $sql2 . "Where FileLocation = '".$file."' ";

    // $sql2 = $sql2 . "Update TblVendorFileUpload ";
    // $sql2 = $sql2 . "    Set Status = 'C', FileLocation = '".$newName."', LastUpBy = '".$_SESSION['vdusercode']."', LastUpDt = CurrentDateTime() ";
    // $sql2 = $sql2 . "Where VdCode = '".$_SESSION['vdcode']."' ";
    // $sql2 = $sql2 . "And CategoryCode = '".$_POST['CategoryCode2']."' ";
    // $sql2 = $sql2 . "And Status = 'A'; ";

    if (mysqli_query($conn,$sql2)) 
    {
        mysqli_commit($conn);
        if (FTP_STATUS === "enable") {      
            try {
                $ftpConn = ftp_connect($host);
                $login = ftp_login($ftpConn,$user,$pass);
                $ftpPasv = ftp_pasv($ftpConn, true);
    
                if ((!$ftpConn) || (!$login)) {
                    echo "FTP connection has failed! Attempted to connect to ". $host.".";
                }
    
                if (!$ftpPasv) {
                    echo "Cannot switch to passive mode";
                }
    
                if($login) {
    
                    if(file_exist_on_ftp_server(basename($file), $ftpConn)){
                        $deleteFile = ftp_delete($ftpConn,FTP_DESTINATION.basename($file));
                        if (!$deleteFile) {
                            echo "Could not delete ". basename($file);
                        }
                        return true;
                    }else {
                        echo "Error found file";
                    }
                    ftp_close($ftpConn);

                }
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
            
        }
        echo "<script type='text/javascript'>window.location='LegalDoc.php'</script>";
    } 
    else 
    {
        echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
        mysqli_rollback($conn);
    }
}
else
{
    $upload_dir = 'dist/pdf/';
?>

<div class="alert alert-info">
    <h4><i class="icon fa fa-info"></i> File Criteria</h4>
    Only allowed <strong>pdf file</strong> document, with maximum of <strong><?php echo GetParameter("FileSizeMaxUploadFTPClient"); ?> MB</strong> each file.
</div>

<form class="form-horizontal" name="insertVendor" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">
    <div class="modal fade" id="insert">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <center><h4 class="modal-title" id="myModalLabel">Insert Legal Document</h4></center>
                </div>
                    <div class="modal-body">
                        <div class="container-fluid">

                            <div id="InsertTender"></div>
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <label class="control-label mandatory-field" style="position:relative; top:7px;">Category</label>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <select class="form-control selectCategory" required name="LueCategory" id="LueCategory" style="width: 100%;">
                                        <option value=""></option>
                                        <?php
                                            $q = "Select CategoryCode As Col1, CategoryName As Col2 From TblVendorFileUploadCategory ";
                                            $q = $q . "Where ActInd = 'Y' And CategoryCode Not In ( ";
                                            $q = $q . "  Select CategoryCode From TblVendorFileUpload Where VdCode = '".$_SESSION['vdcode']."' And Status = 'A' ";
                                            $q = $q . ") ";
                                            $q = $q . "Order By CategoryName; ";

                                            $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                                            while($q2 = mysqli_fetch_assoc($q1))
                                            {
                                                echo "<option value='".$q2['Col1']."' ";
                                                echo ">".$q2['Col2']."</option>";
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div style="height:10px;"></div>
                                <div class="row">
                                    <div class="col-md-3 col-xs-3">
                                        <label class="control-label mandatory-field" style="position:relative; top:7px;">File</label>
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <input type="file" class="form-control" name="VdFile" id="VdFile" required accept="application/pdf" />
                                    </div>
                                    <div class="col-md-9 col-xs-9">
                                        <span class="error-message" id="eVdFile"></span>
                                    </div>
                                </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="background-color: #58585A; color:white"/>
                        <button type="submit" name="submit" class="btn pull-right" id="BtnSave" style="background-color: #3373BA; color:white">Save</button>
                    </div>
            </div>
        </div>
    </div>
</form>

<br />

<a href="#">
    <button type="button" class="btn btn-flat" data-toggle="modal" data-target="#insert" style="background-color: #3373BA; color:white"> 
        <strong> Insert</strong>
    </button>
</a>

<br /><br />
<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header with-border">
            <h4 class="box-title">Manage your file</h4>
            </div>
                <div class="box-body">
                <form action="" name="manageUpload" role="form" enctype="multipart/form-data" method="POST">
                    <table id="VdFileHistory" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Valid From</th>
                                <th>Valid To</th>
                                <th>Local Document#</th>
                                <th>Licensed By</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                $sql = "";

                                $sql = $sql . "Select A.CategoryCode, B.CategoryName, A.FileLocation, IfNull(DATE_FORMAT(A.StartDt, '%d-%m-%Y'), '') StartDt, IfNull(DATE_FORMAT(A.EndDt, '%d-%m-%Y'), '') EndDt, A.ValidInd, A.LocalDocNo, A.LicensedBy ";
                                $sql = $sql . "From TblVendorFileUpload A ";
                                $sql = $sql . "Inner Join TblVendorFileUploadCategory B On A.CategoryCode = B.CategoryCode And ActInd = 'Y' ";
                                $sql = $sql . "Where A.VdCode = '".$_SESSION['vdcode']."' And A.Status = 'A' ";
                                $sql = $sql . "Order By B.CategoryName; ";

                                $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                                while($res = mysqli_fetch_assoc($result))
                                {
                                    echo "<tr>";
                                    echo "<td>" . $res['CategoryName'] . "</td>";
                                    echo "<td>" . $res['StartDt'] . "</td>";
                                    echo "<td>" . $res['EndDt'] . "</td>";
                                    echo "<td>" . $res['LocalDocNo'] . "</td>";
                                    echo "<td>" . $res['LicensedBy'] . "</td>";
                                    // echo "<input type='hidden' name='CategoryCode2' value='".$res['CategoryCode']."' />";
                                    // echo "<input type='hidden' name='FileName1' value='".$res['FileLocation']."' />";
                                    // echo "<input type='hidden' name='FileName2' value='dist/pdf/".$_SESSION['vdcode']."_".$res['CategoryCode']."_".date("Ymdhis").".pdf' />";
                                    echo "<td><a href='".$upload_dir.$res['FileLocation']."' target='_blank'><button type='button' class='btn-xs btn-info'> Detail </button></a>";
                                    if($res['ValidInd'] != "Y")
                                        echo " &nbsp;&nbsp; <button type='button' id='".$res['FileLocation']."' name='delete' class='btnDel btn-xs btn-danger'> Delete </button></td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </form>                 
                </div>
                <!-- /.box-body -->
        </div>
    </div>
</div>

<script type="text/javascript" src="UploadFilev1.0.1.js"></script>
<script>
    $(".btnDel").click(function(){
    var file = $(this).attr('id');

    console.log("file ", file);
    if(confirm("Are you sure want to delete ?")){
        $.ajax({
        url:'LegalDoc.php',
        method: 'post',
        data : {delete:file},
        error: function(err) {
            console.log('Error ' + err);
        },
        success:function(data){
            console.log('Success  ' + data);
            window.location = 'LegalDoc.php';
        }
    })
    }
});
</script>
<?php
} // else not submit
include "footer.php"
?>