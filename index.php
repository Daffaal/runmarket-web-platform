<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

if (strlen($_SESSION['vdcode']) > 0 && strlen($_SESSION['vdname']) > 0) {
?>

	<div class="row">
		<div class="col-lg-12 col-sm-12">
			<div class="row modified-font">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="Vendor.php">
							<center><img src="dist/icon/company profile.svg" style="width: 70px; padding-top: 5px; padding-bottom: 5px"></center>
							<p class="text-center">Company Profile</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="LegalDoc.php">
							<center><img src="dist/icon/legaldoc.svg" style="width: 70px; padding-top: 5px; padding-bottom: 5px"></center>
							<p class="text-center">Legal Document</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="HistoricalOrder.php">
							<center><img src="dist/icon/historicalorder.svg" style="width: 70px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center">Historical Order</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="VendorExpertise.php">
							<center><img src="dist/icon/companyexpertise.svg" style="width: 70px; padding-top: 5px; padding-bottom: 5px"></center>
							<p class="text-center">Company Expertise</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="VendorProject.php">
							<center><img src="dist/icon/projectexperience.svg" style="width: 55px; padding-top: 5px;"></center>
							<p class="text-center">Project Experience</p>
						</a>
					</div>
				</div>

				<div id="MRI" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="RequestQuotation.php">
							<span id="newMRI" style="position: absolute; background: red; color: white; padding:1%; visibility:hidden">New</span>
							<center><img src="dist/icon/availablereq.svg" style="width: 70px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center">Available Request</p>
						</a>
					</div>
				</div>

				<!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="QtHistory.php">
							<center><img src="dist/icon/quotationhistory.svg" style="width: 65px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center">Quotation History</p>
						</a>
					</div>
				</div> -->

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="PurchaseOrder.php">
							<center><img src="dist/icon/purchaseorder.svg" style="width: 70px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center">Purchase Order</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="DeliveryHistory.php">
							<center><img src="dist/icon/delivery.svg" style="width: 65px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center">Delivery</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="Invoice.php">
							<center><img src="dist/icon/invoice.png" style="width: 65px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center">Invoice</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="Payment.php">
							<center><img src="dist/icon/wallet.png" style="width: 65px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center">Payment</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="Tender.php">
							<center><img src="dist/icon/tender.svg" style="width: 55px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center">Tender</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="ECatalogue.php">
							<center><img src="dist/icon/youritem.svg" style="width: 70px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center" style="font-family:'Gotham Rounded'">Your Item</p>
						</a>
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="square-box" style="color: white; background-color:white;">
						<a class="modified-font" href="ECatalogue.php">
							<center><img src="dist/icon/additem.svg" style="width: 70px; padding-top: 5px; padding-bottom: 0px"></center>
							<p class="text-center" style="font-family:'Gotham Rounded'">Add Item</p>
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>

	<?php  //handle new notification
	if (isset($_SESSION['newMR'])) {
		if ($_SESSION['newMR']) {
			echo "<script>
                  $(function (){
                    document.getElementById('newMRI').style.visibility = 'visible';
					$('#MRI').click(function(){
						document.getElementById('newMR').style.visibility = 'hidden';
						createCookie('newMR', 'click', '0.1')
					  });
                  });
              </script>";
		} else {
			echo "<script>
                  $(function (){
                    document.getElementById('newMRI').style.visibility = 'hidden';
					$('#MRI').click(function(){
						createCookie('newMR', 'notClick', 0.1)
					  });
                  });
              </script>";
		}
	}
	if (isset($_SESSION['newPO'])) {
		if ($_SESSION['newPO']) {
			echo "<script>
					$(function (){
						document.getElementById('newPOR').style.visibility = 'visible';
						$('#POR').click(function() {
							document.getElementById('newPO').style.visibility = 'hidden';
							createCookie('newPO', 'click', '0.1')
						});
					});
				</script>";
		} else {
			echo "<script>
					$(function(){
						document.getElementById('newPOR).style.visibility = 'hidden';
						$('#POR').click(function() {
							createCookie('newPO', 'notClick', 0.1)
						});
					});
				</script>";
		}
	}
	if (isset($_SESSION['newRFQ'])) {
		if ($_SESSION['newRFQ']) {
			echo "<script>
					$(function() {
						document.getElementById('newQH').style.visibility = 'visible';
						$('#QH').click(function() {
							document.getElementById('newRFQ').style.visibility = 'hidden';
							createCookie('newRFQ', 'click', '0.1')
						});
					});
				</script>";
		} else {
			echo "<script>
					$(function() {
						document.getElementById('newQH').style.visibility = 'hidden';
						$(#QH').click(function() {
							createCookie('newRFQ', 'notClick', 0.1)
						});
					});
				</script>";
		}
	}
	?>
	<script>
		function createCookie(name, value, days) {
			var expires;

			if (days) {
				var date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();
			} else {
				expires = "";
			}

			document.cookie = escape(name) + "=" +
				escape(value) + expires + "; path=/";
		}
	</script>
<?php
}	// end if isset($_SESSION['vdcode'])
include 'footer.php';

?>