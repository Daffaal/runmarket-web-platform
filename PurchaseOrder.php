<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";
?>
<style>
    #PODetail {
        height: 80vh;
        overflow-y: auto;
        overflow-x: auto;
    }

    #ActivePODetail {
        overflow-y: auto;
        overflow-x: auto;
    }
</style>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title">Purchase Order</h4>
            </div>
            <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#PORequest" data-toggle="tab">Purchase Order Offer</a></li>
                            <li><a href="#ActivePO" data-toggle="tab">List of Active PO</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="PORequest">
                                <table class="table POHistory" id="tblpohistory">
                                    <thead>
                                        <tr>
                                            <th>PO#</th>                                            
                                            <th>Company (Entity)</th>
                                            <th>Expired Date</th>
                                            <th>Offering Date</th>
                                            <th>Grand Total</th>
                                            <th>PO Status</th>                                        
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $sql = "
                                                Select A.DocNo, Date_Format(A.DocDt, '%d-%m-%Y') DocDt, Group_Concat(Distinct IfNull(H.EntName, '')) EntName, Date_Format(A.ExpDt, '%d-%m-%Y') ExpDt, I.OfferingDt,
                                                Sum(B.Qty * B.EstimatePrice) As GrandTotal, A.Remark,
                                                Case 
                                                    When (B.CancelInd = 'Y' Or B.Status = 'C') Then 'Cancel'
                                                    Else
                                                    Case 
                                                        When B.TakeOrderInd Is Null Then 'Outstanding'
                                                        When B.TakeOrderInd = 'N' Then 'Reject'
                                                        Else
                                                        Case 
                                                            When J.PORequestDocNo Is Not Null Then 'Deal'
                                                            Else 'On Progress'
                                                        End
                                                    End
                                                End As POStatus
                                                From TblPORequestHdr A
                                                Inner Join TblPORequestDtl B On A.DocNo = B.DocNo
                                                    And B.CancelInd = 'N'
                                                    And B.Status = 'A'
                                                    And A.ExpDt Is Not Null
                                                Inner Join TblQtHdr C On B.QtDocNo = C.DocNo
                                                    And C.MaterialRequestDocNo Is Not Null
                                                    And C.VdCode = '". $_SESSION['vdcode'] ."'
                                                Inner Join TblQtDtl D On B.QtDocNo = D.DocNo And B.QtDNo = D.DNo
                                                Inner Join TblMaterialRequestHdr E On C.MaterialRequestDocNo = E.DocNo
                                                Left Join TblSite F On E.SiteCode = F.SiteCode
                                                Left Join TblProfitCenter G On F.ProfitCenterCode = G.ProfitCenterCode
                                                Left Join TblEntity H On G.EntCode = H.EntCode
                                                Left Join
                                                (
                                                    Select T1.DocNo, Date_Format(Left(T2.LastUpDt, 8), '%d-%m-%Y') OfferingDt
                                                    From
                                                    (
                                                        Select DocType, DocNo, DNo, Max(ApprovalDNo) ApprovalDNo
                                                        From TblDocApproval
                                                        Where DocType = 'PORequest'
                                                        And UserCode Is Not Null
                                                        And Status = 'A'
                                                        Group By DocType, DocNo, DNo
                                                    ) T1
                                                    Inner Join TblDocApproval T2 On T1.DocType = T2.DocType And T1.DocNo = T2.DocNo
                                                        And T1.DNo = T2.DNo And T1.ApprovalDNo = T2.ApprovalDNo
                                                ) I On A.DocNo = I.DocNo
                                                Left Join 
                                                (
                                                    Select T1.PORequestDocNo, T1.PORequestDNo
                                                    From TblPODtl T1
                                                    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo = T2.DocNo And T1.PORequestDNo = T2.DNo
                                                        And T1.CancelInd = 'N'
                                                    Group By T1.PORequestDocNo, T1.PORequestDNo
                                                ) J On B.DocNo = J.PORequestDocNo And B.DNo = J.PORequestDNo
                                                Group By A.DocNo, A.DocDt, A.ExpDt;
                                            ";

                                            $result = mysqli_query($conn, $sql) or die(mysqli_errno($conn));

                                            $i = 0;

                                            while ($res = mysqli_fetch_assoc($result)) {
                                                echo "<tr>";
                                                echo "<td>" . $res['DocNo'] . "</td>";
                                                echo "<td>" . $res['EntName']. "</td>";
                                                echo "<td>" . $res['ExpDt'] . "</td>";
                                                echo "<td>" . $res['OfferingDt'] . "</td>";
                                                echo "<td align='right'>" . number_format($res['GrandTotal'], 2, '.',',') .  "</td>";
                                                echo "<td>" . $res['POStatus'] . "</td>";
                                                echo "<td>";
                                                echo ("<button data-toggle='modal' 
                                                    data-target='#Detail'
                                                    id='".$res['DocNo']."'
                                                    data-id='".$res['DocNo']."' 
                                                    data-date='".$res['DocDt']."' 
                                                    data-ent='".$res['EntName']."' 
                                                    data-postatus='".$res['POStatus']."'
                                                    data-grandtotal='".number_format($res['GrandTotal'], 2, ",", ".")."'
                                                    data-remarkpo='".$res['Remark']."'
                                                    data-isactivepo='0'
                                                    class='poDetail btn btn-info'>Detail</button> <br>");
                                                echo "</td>";
                                                echo "</tr>";
                                            }
                                        
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="tab-pane" id="ActivePO">
                                <table class="table PaymentHistory" id="tblpayhistory">
                                <thead>
                                        <tr>
                                            <th>PO Offer#</th>                                            
                                            <th>Company (Entity)</th>
                                            <th>PO Date</th>
                                            <th>Expired Date</th>
                                            <th>Grand Total</th>
                                            <th>#</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 

                                            $sql = ("
                                                Select A.DocNo, B.PORequestDocNo, Group_Concat(Distinct IfNull(H.EntName, '')) EntName, Date_Format(D.ExpDt, '%d-%m-%Y') ExpDt, 
                                                Date_Format(A.DocDt, '%d-%m-%Y') DocDt, A.Amt, A.TaxAmt, A.LocalDocNo, A.ShipTo, A.BillTo, A.Remark, A.FileName
                                                From TblPOHdr A
                                                Inner Join TblPODtl B On A.DocNo = B.DocNo
                                                    And A.VdCode = '".$_SESSION['vdcode']."'
                                                    And B.CancelInd = 'N'
                                                Inner Join TblPORequestDtl C On B.PORequestDocNo = C.DocNo ANd B.PORequestDNo = C.DNo
                                                Inner Join TblPORequestHdr D On B.PORequestDocNo = D.DocNo
                                                    And D.ExpDt Is Not Null
                                                Inner Join TblMaterialRequestHdr E On C.MaterialRequestDocNo = E.DocNo
                                                Left Join TblSite F On E.SiteCode = F.SiteCode
                                                Left Join TblProfitCenter G On F.ProfitCenterCode = G.ProfitCenterCode
                                                Left Join TblEntity H On G.EntCode = H.EntCode
                                                Group By A.DocNo, B.PORequestDocNo, D.ExpDt, A.DocDt, A.Amt, A.TaxAmt, A.LocalDocNo, A.ShipTo, A.BillTo, A.Remark;
                                            ");

                                            $result = mysqli_query($conn, $sql) or die(mysqli_errno($conn));

                                            while ($res = mysqli_fetch_assoc($result)) {
                                                echo "<tr>";
                                                echo "<td>" . $res['PORequestDocNo'] . "</td>";
                                                echo "<td>" . $res['EntName']. "</td>";
                                                echo "<td>" . $res['ExpDt'] . "</td>";
                                                echo "<td>" . $res['DocDt'] . "</td>";
                                                echo "<td align='right'>" . number_format($res['Amt'], 2, '.',',') .  "</td>";
                                                echo "<td>";
                                                echo ("<button data-toggle='modal' 
                                                    data-target='#DetailActivePo'
                                                    data-podocno='".$res['DocNo']."' 
                                                    data-date='".$res['DocDt']."' 
                                                    data-ent='".$res['EntName']."' 
                                                    data-grandtotal='".number_format($res['Amt'], 2, ",", ".")."' 
                                                    data-expdate='".$res['ExpDt']."'
                                                    data-shipto='".$res['ShipTo']."'
                                                    data-billto='".$res['BillTo']."'
                                                    data-localdocno='".$res['LocalDocNo']."'
                                                    data-remark='".$res['Remark']."'
                                                    data-taxamt='".number_format($res['TaxAmt'], 2, ",", ".")."'
                                                    data-file='".$res['FileName']."'
                                                    data-isactivepo='1'
                                                    class='poDetail btn btn-info'>Detail</button> <br>");                                                
                                                echo "</td>";
                                                echo "</tr>";                                            
                                            }
                                        
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                    </div>
                </div>
            </div>
            <!-- modal for detail -->
            <div class="modal fade" id="Detail" >
                <div class="modal-dialog" style="width:90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <center><h4 class="modal-title" id="myModalLabel"></h4></center>
                        </div>
                            <div class="modal-body">
                                <div class="container-fluid">                        
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">PO Request#</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control id" id="id" name="id" readonly />
                                        </div>       
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Remark</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control remark" id="remark" name="remark" readonly />
                                        </div>                                 
                                    </div>                                                                                                                                            
                                </div>
                                <div class="container-fluid">                        
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Total Amount</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control grandtotal" id="grandtotal" name="grandtotal"readonly style="text-align: end;"/>
                                        </div>
                                    </div>                                                                                                                                            
                                </div>
                                <div class="container-fluid">                        
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Company(Entity)</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control ent" id="ent" name="ent" readonly />
                                        </div>
                                    </div>                                                                                                                                            
                                </div>
                                <div class="container-fluid">                        
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Date</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control date" id="date" name="date" readonly />
                                        </div>
                                    </div>                                                                                                                                            
                                </div>
                                <div style="height:30px;"></div>
                                <div id="PODetail"></div>
                                <div class="container-fluid" style="margin-bottom:2%">                        
                                    <div class="row">
                                        <div class="col-md-10 col-xs-10" style="text-align:end; margin-left: 7%;">
                                            <button class="btn btn-default" id="btnRejectOrder" data-toggle='modal' data-dismiss="modal" href='#RejectOrder' style="position:relative; top:7px;">Reject Order</button>
                                        </div>                                       
                                        <div class="col-md-1 col-xs-1" style="text-align:end; ">
                                            <button class="btn btn-info" id="btnTakeOrder" data-toggle='modal' data-dismiss="modal" href='#TakeOrder' style="position:relative; top:7px;">Take Order</button>
                                        </div>                                       
                                    </div>                                                                                                                                            
                                </div>                      
                            </div>
                    </div>
                </div>
            </div>
            <!-- modal for detail active purchase-->
            <div class="modal fade" id="DetailActivePo" >
                <div class="modal-dialog" style="width:90%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <center><h4 class="modal-title" id="modalActivePO"></h4></center>
                        </div>
                            <div class="modal-body">
                                <div class="container-fluid">                        
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Purchase Order</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control id" id="poDocNo" name="id" readonly />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Local Doc PO</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control" id="localDocNo" name="localDocNo"readonly />
                                        </div>                           
                                    </div>                                                                                                                                            
                                </div>
                                <div class="container-fluid">                        
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Total Amount</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control poActiveGrandTotal" id="poActiveGrandTotal" name="poActiveGrandTotal"readonly style="text-align: end;"/>
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Remark</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control actRemark" id="actRemark" name="actRemark" readonly />
                                        </div>
                                    </div>                                                                                                                                            
                                </div>
                                <div class="container-fluid">                        
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Company(Entity)</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control poActiveEnt" id="poActiveEnt" name="poActiveEnt" readonly />
                                        </div>
                                        <div id="tax">
                                            <div class="col-md-2 col-xs-2">
                                                <label class="control-label " style="position:relative; top:7px;">Tax</label>
                                            </div>
                                            <div class="col-md-4 col-xs-4" style="text-align:end">
                                                <input type="text" class="form-control taxAmt" id="taxAmt" name="taxAmt" readonly style="text-align: end;"/>
                                                <div style="height: 8px;"></div>
                                                <a href="#DetailTax" id="detailTax" data-toggle="modal" style="font-size: 1.2rem;"><i class="fa fa-search" aria-hidden="true"></i> See Detail</a>

                                            </div>
                                        </div>
                                    </div>                                                                                                                                            
                                </div>
                                <div class="container-fluid">                        
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Date</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control poActiveDate" id="poActiveDate" name="poActiveDate" readonly />
                                        </div>
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">File</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4" style="text-align: end;">
                                            <i style="position: absolute; padding:10px; right:4%;" class="fa fa-download"></i>                                                                                   
                                            <input type="text" class="form-control file" id="file" name="file" onclick="download(this)" readonly >  
                                        </div>                                         
                                    </div>                                                                                                                                            
                                </div>
                                <div class="container-fluid">                        
                                    <div class="row">
                                        <div class="col-md-2 col-xs-2">
                                            <label class="control-label " style="position:relative; top:7px;">Bill To</label>
                                        </div>
                                        <div class="col-md-4 col-xs-4">
                                            <input type="text" class="form-control billTo" id="billTo" name="billTo" readonly />
                                        </div>
                                        <div id="tax">
                                            <div class="col-md-2 col-xs-2">
                                                <label class="control-label " style="position:relative; top:7px;">Ship To</label>
                                            </div>
                                            <div class="col-md-4 col-xs-4">
                                                <input type="text" class="form-control shipTo" id="shipTo" name="shipTo" readonly />                                            
                                            </div>
                                        </div>
                                    </div>                                                                                                                                            
                                </div>
                                <div style="height:30px;"></div>
                                <div id="ActivePODetail"></div>                                                      
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <!-- modal for take order -->
 <div class="modal fade" id="TakeOrder" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <center><h4 class="modal-title" id="modalTake">Take Order</h4></center>
            </div>
                <div class="modal-body">
                    <form action="TakeOrder.php" method="POST">
                        <div class="form-group">
                            <textarea placeholder="Remark" class="form-control" id="TakeOrderRemark" name="TakeOrderRemark" rows="5"></textarea>
                            <input type="hidden" name="DocNo" id="docnoTakeOrder" />
                            <input type="hidden" name="TakeOrderInd" value="Y" />
                            <div style="height: 30px;"></div>
                            <div class="col-md-auto" style="text-align: end;">
                                <button type="submit" data-dismiss="modal" class="btn btn-default btn-md">Cancel</button>
                                <button type="submit" class="btn btn-info btn-md">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
<!-- modal for reject order -->
<div class="modal fade" id="RejectOrder" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <center><h4 class="modal-title" id="modalReject">Reject Order</h4></center>
            </div>
                <div class="modal-body" >
                    <form action="TakeOrder.php" method="POST">
                        <div class="form-group">
                            <textarea placeholder="Remark" class="form-control" id="TakeOrderRemark" name="TakeOrderRemark" rows="5"></textarea>
                            <input type="hidden" name="DocNo" id="docnoRejectOrder" />
                            <input type="hidden" name="TakeOrderInd" value="N" />
                            <div style="height: 30px;"></div>
                            <div class="col-md-auto" style="text-align: end;">
                                <button type="submit" data-dismiss="modal" class="btn btn-default btn-md">Cancel</button>
                                <button type="submit" class="btn btn-info btn-md">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>  
<div class="modal fade" id="DetailTax" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <center><h4 class="modal-title" id="modalTax">Tax Information</h4></center>
            </div>
                <div class="modal-body" >
                    <div id="listTax"></div>
                </div>
        </div>
    </div>
</div> 

<script type="text/javascript">
function download() {
    var fileName = event.target.value;
    if(fileName != ''){
        window.open('POFile.php?File='+ fileName);
    }else {
        alert("File not found.");
    }
}
$(function(){
    $("#tblpohistory").DataTable({
    responsive: true,
    autoWidth: false,
    scrollX: true,
    fixedColumns: {
    leftColumns: 1
    }
    });
    
});
</script>

<script>
    $(function (){
        $('.poDetail').click(function(){

            var id = $(this).data('id');
            var date = $(this).data('date');
            var ent = $(this).data('ent');
            var grandtotal = $(this).data('grandtotal');
            var postatus = $(this).data('postatus');
            var isActivePO = $(this).data('isactivepo');
            var poDocNo = $(this).data('podocno');
            var expDate = $(this).data('expdate');
            var shipTo = $(this).data('shipto');
            var billTo = $(this).data('billto');
            var localDocNo = $(this).data('localdocno');
            var remark = $(this).data('remark');
            var taxamt = $(this).data('taxamt');
            var remarkPO = $(this).data('remarkpo');
            var file = $(this).data('file');

            $("#id").val(id);
            $("#date").val(date);
            $("#poActiveDate").val(date);
            $("#ent").val(ent);
            $("#poActiveEnt").val(ent);
            $("#grandtotal").val(grandtotal)
            $("#poActiveGrandTotal").val(grandtotal)
            $("#postatus").val(postatus);
            $("#docnoTakeOrder").val(id);
            $("#docnoRejectOrder").val(id);
            $("#poDocNo").val(poDocNo);
            $("#shipTo").val(shipTo);
            $("#localDocNo").val(localDocNo);
            $("#actRemark").val(remark);
            $("#billTo").val(billTo);
            $("#taxAmt").val(taxamt);
            $("#file").val(file);

            
            var DocNo = $(this).attr('id');
            $("#btnRejectOrder").data("docno", id);
            $("#btnTakeOrder").data("docno", id);


            if(isActivePO == 0){
                
                $("#myModalLabel").text('Purchase Order Offer');
                $("#tax").hide();
                $("#remark").val(remarkPO);
                console.log(remarkPO);

                $.ajax({
                url: 'PODetail.php?a='+isActivePO,
                method: 'post',
                data: {DocNo:DocNo},
                error: function(err) {
                    console.log('Error ' + err);
                },
                success:function(data){
                    console.log('Success  ' + data);
                    $('#PODetail').html(data);                                        
                    $('#Detail').modal("show");
                }
            });   
            }else {
                
                $("#modalActivePO").text('Active Purchase Order');
                $("#tax").show();

                $("#detailTax").data('docno', poDocNo);

                $.ajax({
                url: 'PODetail.php?a='+isActivePO,
                method: 'post',
                data: {DocNo:poDocNo},
                error: function(err) {
                    console.log('Error ' + err);
                },
                success:function(data){
                    $('#ActivePODetail').html(data);                                        
                    $('#DetailActivePo').modal("show");
                }
            });   
            }                             

            if (postatus != "Outstanding") 
            {
                $("#btnRejectOrder").hide();
                $("#btnTakeOrder").hide();
            }else {
                $("#btnRejectOrder").show();
                $("#btnTakeOrder").show();
            }

                    
        });

        $('#detailTax').click(function () {

            var poDocNo = $(this).data('docno');

            console.log(poDocNo);
            $.ajax({
                url: "POTax.php",
                method: 'post',
                data: {DocNo : poDocNo},
                error: function(err){
                    console.log("Error " + err);
                },
                success: function(data){
                    console.log("Success " + data);
                    $("#listTax").html(data);
                    $("#DetailTax").show();
                }
            });
        })
    });
</script>

<script>
    function confirmPO()
{
	if(confirm("Do you want to cancel this process ?") == true)
	{
		
	}
}
</script>

<?php
include "footer.php";
?>