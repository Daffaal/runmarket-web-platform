<?php
  $variables = [
      'DB_HOST' => 'HOST',
      'DB_USERNAME' => 'USERNAME',
      'DB_PASSWORD' => 'PASSWORD',
      'DB_NAME_DEV' => 'DB_NAME_DEV',
      'DB_NAME_PROD' => 'DB_NAME_PROD',
      'DB_PORT' => 'PORT',
  ];

  foreach ($variables as $key => $value) {
      putenv("$key=$value");
  }
?>