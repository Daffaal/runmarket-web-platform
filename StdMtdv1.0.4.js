// 01/03/2017 [WED] ubah dateFormat

$(document).ready(function(){
setInterval(function(){
        $.get("check.php", function(data){
        if(data==0) window.location.href="logout.php";
        });
    },1*60*1000);
});

function confirmCancel()
{
	if(confirm("Do you want to cancel this process ?") == true)
	{
		location.reload();
	}
}

function confirmSave()
{
	return confirm("Do you want to save this data ?");
}

function timeFormat(id)
{
	if(document.getElementById(id).value != "")
	{
		var a = document.getElementById(id).value;
		var time = "00".concat(a);
		document.getElementById(id).value = time.slice(-2);
	}
}

function numberFormat(id)
{
	var n = document.getElementById(id);

	if(n.value != "")
	{
		if(isNaN(n.value))
		{
			alert("Only numbers allowed.");
		}
		else
		{
			var number = Number(n.value).toLocaleString(undefined, {minimumFractionDigits: 2, maximumFractionDigits: 2});
			n.value = number;
		}
	}
	else
	{
		n.value = "0.00";
	}
}

function dateFormat(id)
{
	if(document.getElementById(id).value != "")
	{
		var dt = document.getElementById(id).value;
		var splitDt = dt.split("/");
		return splitDt[2] + splitDt[1] + splitDt[0];
	}
}

function reverseDate(dt)
{
	if(dt != "")
	{
		var year = dt.getFullYear();
		var month = dt.getMonth() + 1;
		var day = dt.getDate();
		var reversed = year.toString() + ("00" + month.toString()).slice(-2) + ("00" + day.toString()).slice(-2);
	}
	else
	{
			var reversed = "0";
	}

	return reversed;
}

function SetControlEditValueNull(field)
{
	var countField = field.length;
	for(i = 0; i < countField; i++)
	{
		document.getElementById(field[i]).value = "";
	}
}

function SetControlNumValueZero(field)
{
	var countField = field.length;
	for(i = 0; i < countField; i++)
	{
		document.getElementById(field[i]).value = "0";
	}
}

function SetControlReadOnly(field, status)
{
	var countField = field.length;
	for(i = 0; i < countField; i++)
	{
		document.getElementById(field[i]).readOnly = status;
	}
}

function SetControlDisabled(field, status)
{
	var countField = field.length;
	for(i = 0; i < countField; i++)
	{
		document.getElementById(field[i]).disabled = status;
	}
}

function ClearAllErrorMessage(field)
{
	var countField = field.length;
	for(i = 0; i < countField; i++)
	{
		document.getElementById(field[i]).innerHtml = "";
	}
}

function IsControlEmpty(field, error, message)
{
	/*var result = true;

	if(document.getElementById(field).value == "")
	{
		document.getElementById(error).innerHTML = message + " should not be empty.";
	}
	else
	{
		result = false;
	}

	return result;*/

	if(document.getElementById(field).value == "")
	{
		document.getElementById(error).innerHTML = message + " should not be empty.";
		return false;
	}
}

function IsControlZero(field, error, message)
{
	var result = true;

	if(document.getElementById(field).value == "")
	{
		document.getElementById(error).innerHTML = message + " should not be empty.";
	}
	else
	{
		if(document.getElementById(field).value == "0")
		{
			document.getElementById(error).innerHTML = message + " should not be 0.";
		}
		else
		{
			result = false;
		}
	}

	return result;
}

function AddDays(date, number)
{
	var date = new Date(
					date.getFullYear(),
					date.getMonth+1,
					date.getDate()+number,
					date.getHours(),
					date.getMinutes(),
					date.getSeconds()
				);

	return date;
}

function AddMonth(date, number)
{
	var date = new Date(
					date.getFullYear(),
					date.getMonth+1+number,
					date.getDate(),
					date.getHours(),
					date.getMinutes(),
					date.getSeconds()
				);

	return date;
}

function AddYear(date, number)
{
	var date = new Date(
					date.getFullYear()+number,
					date.getMonth+1,
					date.getDate(),
					date.getHours(),
					date.getMinutes(),
					date.getSeconds()
				);

	return date;
}

function compareDateWithSlash(Dt1, Dt2, ErrorDiv)
{
	// ini untuk yang ada slash nya
	if(document.getElementById(Dt1).value != "" && document.getElementById(Dt2).value != "")
	{
		var d1 = dateFormat(Dt1);
		var d2 = dateFormat(Dt2);

		if(Number(d2) < Number(d1))
		{
			document.getElementById(ErrorDiv).innerHTML = "End date should be bigger than Start date.";
      		return false;
		}
	}
}

function compareDate(Dt1, Dt2, ErrorDiv)
{
	// ini untuk yang tidak ada slash nya (format sudah Ymd)
	if(Dt1 != "" && Dt2 != "")
	{
		if(Number(Dt2) < Number(Dt1))
		{
			document.getElementById(ErrorDiv).innerHTML = "End date should be bigger than Start date.";
      		return false;
		}
	}
}