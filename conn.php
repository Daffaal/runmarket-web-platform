<?php

	include "autoload.php";
	$conn = mysqli_connect(env('DB_HOST'), env('DB_USERNAME'), env('DB_PASSWORD'), env('DB_NAME_DEV'), env('DB_PORT'));
	

	if(!$conn)
	{
		echo "Error Log : " . mysqli_connect_error();
	}
	else
	{
		mysqli_autocommit($conn, FALSE);
		
		// base directory
		$base_dir = __DIR__;

		// document root, without the script file name
		$doc_root = preg_replace("!${_SERVER['SCRIPT_NAME']}$!", '', $_SERVER['SCRIPT_FILENAME']);

		// server protocol
		$protocol = empty($_SERVER['HTTPS']) ? 'http' : 'https';

		// domain name
		$domain = $_SERVER['SERVER_NAME'];

		//request URI
		$requestUri = $_SERVER['REQUEST_URI'];

		// base url
		$base_url = preg_replace("!^${doc_root}!", '', $base_dir);

		// server port
		$port = $_SERVER['SERVER_PORT'];
		$disp_port = ($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port";

		$self = $_SERVER['PHP_SELF'];

		$baseUrl = "/procurement";
	}
	//timezone

	date_default_timezone_set("Asia/Bangkok");


?>
