<?php
session_start();
if(!isset($_SESSION['vdusercode']))
{
	echo "<script type='text/javascript'>window.location='login.php'</script>";
	exit();
}
else
{
	if(isset($_SESSION['vdlast_activity']) && isset($_SESSION['vdexpire_time']))
	{
		if( $_SESSION['vdlast_activity'] < time()-$_SESSION['vdexpire_time'] ) 
		{ //have we expired?
		    //redirect to logout.php
		    echo "0";
		}
		else
		{ //if we haven't expired:
			echo "1";
		    //$_SESSION['last_activity'] = time(); //this was the moment of last activity.
		}
	}
}
?>