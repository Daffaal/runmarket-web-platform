<style>
.hidden {
    display: none;
}

.selected {
    background-color: #3D3;
}
</style>

<?php 
    include "auth.php";
    include "conn.php";
    
    $sql = "";

    if(isset($_POST['DocNo'])){

        $DocNo = $_POST['DocNo'];

        $sql = "Select T.DocNo, T.DNo, T.RecvVdDocNo, T.RecvVdDNo, T.DocDt, T.ItName, T.InventoryUomCode, T.EstRecvDt, T.POQty, T.RecvQty, T.DTName,
        T.CurCode, T.UPrice, T.TaxAmt, ((T.POQty * T.UPrice) + T.TaxAmt) TotalPrice
        From
        (
            Select A.DocNo, B.DNo, C.DocNo RecvVdDocNo, C.DNo RecvVdDNo, Date_Format(D.DocDt, '%d-%m-%Y') DocDt, E.ItName, E.InventoryUomCode,
            Date_Format(B.EstRecvDt, '%d-%m-%Y') EstRecvDt, B.Qty POQty, C.Qty RecvQty, I.DTName,
            A.CurCode, H.UPrice, 
            (
                ((B.Qty * H.UPrice) * IfNull((J.TaxRate * 0.01), 0.00)) +
                ((B.Qty * H.UPrice) * IfNull((K.TaxRate * 0.01), 0.00)) +
                ((B.Qty * H.UPrice) * IfNull((L.TaxRate * 0.01), 0.00))
            ) TaxAmt
            From TblPOHdr A
            Inner Join TblPODtl B On A.DocNo = B.DocNo
                And A.DocNo = '".$DocNo."'
            Inner Join TblRecvVdDtl C On B.DocNo = C.PODocNo And B.DNo = PODNo
                And C.`Status` = 'A'
                And C.CancelInd = 'N'
            Inner Join TblRecvVdHdr D On C.DocNo = D.DocNo
            Inner Join TblItem E On C.ItCode = E.ItCode
            Inner Join TblPORequestDtl F On B.PORequestDocNo = F.DocNo And B.PORequestDNo = F.DNo
            Inner Join TblQtHdr G On F.QtDocNo = G.DocNo And G.MaterialRequestDocNo Is Not Null
            Inner Join TblQtDtl H On G.DocNo = H.DocNo And F.QtDNo = H.DNo
            Left Join TblDeliveryType I On G.DTCode = I.DTCode
            Left Join TblTax J On A.TaxCode1 = J.TaxCode
            Left Join TblTax K On A.TaxCode2 = K.TaxCode
            Left Join TblTax L On A.TaxCode3 = L.TaxCode
        ) T 
        Order by T.DocNo, T.DNo, T.RecvVdDocNo, T.RecvVdDNo";

        echo "<div class='box-body' style='margin-left:5px'> ";
    echo "    <div style='margin-top: 10px;'>";
    echo "        <h4 class='box-title' style='margin-bottom: 5px;'>Delivery Status</h4>";
    echo "    </div>";
    echo "    <table id='DetailTable' class='table table-bordered ' width='100%'>";
    echo "        <thead>";
    echo "            <tr>";
    echo "                <th width='200''>Received Document Number</th>";
    echo "                <th>Received Date</th>";
    echo "                <th>Item</th>";
    echo "                <th>Uom</th>";
    echo "                <th>Received Item</th>";
    echo "                <th>Delivery Type</th>";
    echo "                <th>Currency</th>";
    echo "                <th>Unit Price</th>";
    echo "                <th>Total</th>";
    echo "                <th class='hidden'>checkbox</th>";
    echo "            </tr>";
    echo "        </thead>";
    echo "        <tbody>";
                    $result2 = mysqli_query($conn,$sql) or die (mysqli_error($conn));
                    $currOutstandingQty = 0;
                    $DNo = "";
                    $index = 0;
                    while ($res2 = mysqli_fetch_assoc($result2))
                    {
                        if (strlen($DNo) == 0) $DNo = $res2['DNo'];
                        
                        if ($DNo != $res2['DNo']) 
                        {
                            $index = 0;
                            $DNo = $res2['DNo'];
                        }

                        if ($index == 0) $currOutstandingQty = $res2['POQty'] - $res2['RecvQty'];
                        else $currOutstandingQty -= $res2['RecvQty'];

                        echo "<tr>";
                        echo "<td class='recvDocNo'>". $res2['RecvVdDocNo'] ."</td>";
                        echo "<td class='docDt'>". $res2['DocDt'] ."</td>";
                        echo "<td class='itName'>". $res2['ItName'] ."</td>";
                        echo "<td class='uom'>". $res2['InventoryUomCode'] ."</td>";
                        echo "<td class='qty' style='text-align: right'>". number_format($res2['RecvQty'], 2) ."</td>";
                        echo "<td class='dtName'>". $res2['DTName'] ."</td>";
                        echo "<td class='curCode'>". $res2['CurCode'] ."</td>";
                        echo "<td class='price' style='text-align: right'>". number_format($res2['UPrice'], 2, ",", ".") ."</td>";
                        echo "<td class='total' style='text-align: right'>". number_format($res2['TotalPrice'], 2, ",", ".") ."</td>";
                        echo "<td class='select hidden'><input type='checkbox'></td>";
                        echo "</tr>";

                        $index++;
                    }
    echo "        </tbody>";
    echo "     </table>";
    echo " </div>";
    }
?>

<script>
$('#DetailTable').DataTable( {
    // scrollX: true,
    responsive: true,
    scrollCollapse: true,
    paging:         true,
    columnDefs: [
        { width: 200, targets: 0 }
    ],
    fixedColumns: {
    	leftColumns: 1
    }
} );
</script>
<script>
    'use strict';
     $('#DetailTable tbody tr').click(function (e) {
        //when the row is clicked...
        var self = $(this), //cache this
            checkbox = self.find('.select > input[type=checkbox]'), //get the checkbox
            isChecked = checkbox.prop('checked'); //and the current state
        if (!isChecked) {
            //about to be checked so clear all other selections
            $('#DetailTable .select > input[type=checkbox]').prop('checked', false).parents('tr').removeClass('selected');
        }
        checkbox.prop('checked', !isChecked).parents('tr').addClass('selected'); //toggle current state
    });

    $('#btnChoose').click(function (e) {      
      var selectedRow = $('#DetailTable .select :checked'),
            tr = selectedRow.parents('tr'), //get the parent row
            recvDocNo = tr.find('.recvDocNo').text(), //get the name
            qty = parseInt(tr.find('.qty').text(), 10); //get the age and convert to int
            //p = $('<p />'); //create a p element
    //   var newRow = $("<tr>");
    //   var cols = "";        
    //   cols += '<td>'+ name +'</td>';
    //   cols += '<td>'+ age +'</td>';
    //   newRow.append(cols);
    //   $('table.Name').append(newRow);
    console.log(recvDocNo, qty);
    });
</script>