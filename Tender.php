<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

$sql = "Select VdCode From TblVendor Where VdCode = '".$_SESSION['vdcode']."' And ProcurementStatus Is Not Null And ProcurementStatus = 'V' Limit 1; ";
if(strlen(GetValue($sql)) > 0)
{
?>

<!-- <a href="QtHistory.php" target="_blank"><button class="btn btn-info"><strong>Your Quotation History</strong></button></a> -->
<br /><br />
<div class="box">
<div class="box-header">
   <h3 class="box-title">List of Available Tender</h3>
</div>
<!-- /.box-header -->
<div class="box-body">
   <table id="example1" class="table table-bordered table-striped">
      <thead>
         <tr>
            <th>Tender#</th>
            <th>Start Date</th>
            <th>Expired Date</th>
            <th>Tender Name</th>
            <th>Currency</th>
            <th>Estimated Price</th>
            <th>Action</th>
         </tr>
      </thead>
      <tbody>
          <?php
            $sql = "Select A.DocNo, DATE_FORMAT(A.DocDt, '%d %b %Y') DocDt, A.TenderName, A.MaterialRequestDocNo, A.MaterialRequestDNo, ";
            $sql = $sql . "DATE_FORMAT(A.ExpiredDt, '%d %b %Y') ExpiredDt, ";
            $sql = $sql . "B.CurCode, B.EstPrice, A.ExpiredDt As ExpiredDt2, A.DocDt As DocDt2, C.QtDocNo ";
            $sql = $sql . "From TblTender A ";
            $sql = $sql . "Inner Join TblMaterialRequestDtl B On A.MaterialRequestDocNo = B.DocNo And A.MaterialRequestDNo = B.DNo ";
            $sql = $sql . "Left Join ";
            $sql = $sql . "( ";
            $sql = $sql . "    Select T1.TenderDocNo, T1.QtDocNo ";
            $sql = $sql . "    From TblTenderQuotationRequest T1 ";
            $sql = $sql . "    Where T1.CancelInd = 'N' ";
            $sql = $sql . "    And T1.QtDocNo In ";
            $sql = $sql . "    ( ";
            $sql = $sql . "        Select X1.DocNo ";
            $sql = $sql . "        From TblQtHdr X1 ";
            $sql = $sql . "        Where X1.TenderDocNo Is Not Null ";
            $sql = $sql . "        And X1.VdCode = '".$_SESSION['vdcode']."' ";
            $sql = $sql . "    ) ";
            $sql = $sql . ") C On A.DocNo = C.TenderDocNo ";
            $sql = $sql . "Where A.ActInd = 'Y' And A.Status = 'O' ";
            $sql = $sql . "And A.DocDt <= '".date("Ymd")."' ";
            $sql = $sql . "Order By A.CreateDt Desc; ";

            $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
            while($res = mysqli_fetch_assoc($result))
            {
                echo "<tr>";
                echo "<td>" . $res['DocNo'] . "</td>";
                echo "<td>" . $res['DocDt'] . "</td>";
                echo "<td>" . $res['ExpiredDt'] . "</td>";
                echo "<td>" . $res['TenderName'];

                $sctr = "Select SectorName From TblSector Where SectorCode In (Select SectorCode From TblTenderDtl Where DocNo = '".$res['DocNo']."'); ";
                $qs = mysqli_query($conn, $sctr) or die(mysqli_error($conn));
                if(mysqli_num_rows($qs) > 0) echo "<br />";
                while($rs = mysqli_fetch_assoc($qs))
                {
                    echo "<span style='color:#273c75'>- " . $rs['SectorName'] . "</span><br />";
                }
                echo "</td>";
                echo "<td>" . $res['CurCode'] . "</td>";
                echo "<td align='right'>" . number_format($res['EstPrice'], 2, ",", ".") . "</td>";
                echo "<td>";
                if($res['DocDt2'] <= date("Ymd") && date("Ymd") <= $res['ExpiredDt2'])
                {
                    if(strlen($res['QtDocNo']) <= 0)
                    {
                        echo "<a href='#addnew' data-toggle='modal' data-id='".$res['DocNo']."' data-estprice='".$res['CurCode']. " ".number_format($res['EstPrice'], 2, ",", ".")."' class='openModalX btn btn-success'><span class = 'fa fa-plus'></span> &nbsp;Bid</a>";
                    }
                }
                echo "</td>";
                echo "</tr>";
            }
          ?>
      </tbody>
   </table>
</div>
<!-- /.box-body -->
</div>

<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel"></h4></center>
            </div>
            <form method="POST" action="Qt.php">
                <div class="modal-body">
                    <div class="container-fluid">

                        <div id="QTHistory"></div>

                        <input type="hidden" id="DocNo" name="DocNo" />
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Term of Payment</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectPtCode" name="PtCode" id="PtCode" style="width: 100%;">
                                    <option value=""></option>
                                    <?php

                                        $sql = "Select PtCode As Col1, PtName As Col2 From TblPaymentTerm Order By PtName;";
                                        $result = mysqli_query($conn,$sql) or die(mysql_error());

                                        while($res = mysqli_fetch_assoc($result))
                                        {
                                            echo "<option value='".$res['Col1']."'>".$res['Col2']."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Delivery Type</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select class="form-control selectDTCode" name="DTCode" id="DTCode" style="width: 100%;">
                                    <option value=""></option>
                                    <?php

                                        $sql = "Select DTCode As Col1, DTName As Col2 From TblDeliveryType Order By DTName;";
                                        $result = mysqli_query($conn,$sql) or die(mysql_error());

                                        while($res = mysqli_fetch_assoc($result))
                                        {
                                            echo "<option value='".$res['Col1']."'>".$res['Col2']."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Expired Date</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control ExpiredDt" id="ExpiredDt" name="ExpiredDt" data-field="date" />
                                </div>

                                <div id="DteExpiredDt"></div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Estimated Price</label>
                                </div>
                                <div class="estPrice col-md-9 col-xs-9"></div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Currency</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <select required class="form-control selectCurCode" name="CurCode" id="CurCode" style="width: 100%;">
                                    <option value=""></option>
                                    <?php

                                        $sql = "Select CurCode As Col1, CurName As Col2 From TblCurrency Order By CurName;";
                                        $result = mysqli_query($conn,$sql) or die(mysql_error());

                                        while($res = mysqli_fetch_assoc($result))
                                        {
                                            echo "<option value='".$res['Col1']."'";
                                            if($res['Col1'] == "IDR") echo " selected";
                                            echo ">".$res['Col2']."</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label mandatory-field" style="position:relative; top:7px;">Price</label>
                                    <br /><small>(No decimal points, no symbols)</small>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" autocomplete="off" class="form-control angka numberOnly" name="UPrice" required />
                                </div>
                            </div>
                            <div style="height:10px;"></div>
                            <div class="row">
                                <div class="col-md-3 col-xs-3">
                                    <label class="control-label" style="position:relative; top:7px;">Remark</label>
                                </div>
                                <div class="col-md-9 col-xs-9">
                                    <input type="text" class="form-control" name="Remark">
                                </div>
                            </div>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Save</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="Tender.js"></script>

<?php
} // end Vendor Verified
else
{
    echo "<script type='text/javascript'>alert('You do not have access on this page.);</script>";
    echo "<script type='text/javascript'>window.location='index.php';</script>";
}
include "footer.php";
?>