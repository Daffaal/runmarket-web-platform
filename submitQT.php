<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

if (isset($_POST['submit'])) {
    if (isset($_POST['Dno'])) {

        $DocNo = $_GET['id'];
        $DNo = $_POST['Dno'];
        $SeqNo = $_POST['SeqNo'];
        $RFQ3DNo = $_POST['Rfq3DNo'];

        foreach ($DNo as $key => $row) {            
            $sql = submitQT($DocNo, $row);
            $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

            foreach ($result as $res) {
                $jumlah[] = $row;
                $sumRow = count($jumlah);
            }
        }
?>
        <form method="post" action="QtMr.php" enctype="multipart/form-data" onsubmit="confirmForm();">
            <input type="hidden" name="SeqNo" value="<?= $SeqNo ?>">
            <input type="hidden" name="RFQ3DNo" value="<?= $RFQ3DNo ?>">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                        <div class="box-header">
                            <h4 class="box-title">Submit Quotation</h4>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2">Quotation</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="DocNoHdr" value="<?= $res['DocNo'] ?>" readonly />
                                    </div>
                                    <label for="inputEmail3" class="col-sm-2">Remark</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="remarkHdr" value="<?= $res['Remark'] ?>" readonly />
                                    </div>
                                </div>
                            </div>
                            <div style="height: 5px;"></div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2">Total RFQ Amount</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="totalAmt" name="totalAmt[]" value="<?= number_format($res['TotalPrice'], 2, ',', '.') ?>" style="text-align: right;" readonly />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2">Total Quotation Amount</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="totalQtAmt" name="totalQtAmt[]" style="text-align: right;" readonly />
                                    </div>
                                </div>
                            </div>
                            <div style="height: 5px;"></div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2">Company (Entity)</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="EntName[]" value="<?= $res['EntName'] ?>" readonly />
                                    </div>
                                </div>
                            </div>
                            <div style="height: 5px;"></div>
                            <div class="row">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2">Date</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="DocDt[]" value="<?= $res['DocDt'] ?>" readonly />
                                    </div>
                                </div>
                            </div>
                            <div style="height: 20px;"></div>
                            <?php for ($j = 1; $j <= $sumRow; $j++) { ?>
                                <div class="modal fade" id="UploadFile<?= $j ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Quotation</h4>
                                            </div>
                                            <div class="modal-body" id="QTDetailHistory">
                                                <div class="container-fluid">
                                                    <table class="table File<?= $j ?>" id="tblBankAccount">
                                                        <thead>
                                                            <tr>
                                                                <th>File</th>
                                                                <th>#</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            <input type='hidden' name='countVdBankAccount' id='countVdBankAccount' />
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="5" style="text-align: left;">
                                                                    <input type="button" class="btn btn-lg btn-block " id="addrow<?= $j ?>" value="Add File" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    <div align="right">
                                                        <input type="reset" class="btn" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="background-color: #58585A; color:white" />
                                                        <button type="submit" class="btn btn-info" data-dismiss="modal">Ok</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer"></div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <table class="table table-bordered table-striped" id="tblrfq">
                                <thead>
                                    <tr>
                                        <th>DocNo</th>
                                        <th></th>
                                        <th>Item</th>
                                        <th>Quantity</th>
                                        <th>UoM</th>
                                        <th>Unit Price</th>
                                        <th>Currency</th>
                                        <th>Term of Payment</th>
                                        <th>Delivery Type</th>
                                        <th>Received Date</th>
                                        <th>Expired Date</th>
                                        <th>Total Price</th>
                                        <th>Remark</th>
                                    </tr>
                                </thead>
                                <?php
                                $no = 0;
                                foreach ($DNo as $key => $row) {
                                    $no++;
                                    $sql = submitQT($DocNo, $row);

                                    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                                    foreach ($result as $res) {

                                ?>
                                        <input type="hidden" class="form-control" name="DNo[]" value="<?= $row ?>" />
                                        <input type="hidden" class="form-control" name="DNoHdr" value="<?= $row ?>" />
                                        <input type="hidden" class="form-control" id="countRow" name="countRow" value="<?= $sumRow ?>" />
                                        <tbody>
                                            <td><input type="text" name="DocNo[]" class="form-control" value="<?= $res['DocNo'] ?>" readonly></input></td>
                                            <td>
                                                <a href="#UploadFile<?= $no ?>" class="btn btn-danger" data-toggle="modal"><button type="button" class="fa fa-upload"></button> Upload file</a>
                                                <!-- <input type="file" name="VdFile[]" id="VPFile" accept="application/pdf, image/png, image/jpg, image/jpeg" multiple /> -->
                                            </td>
                                            <td><input type="text" name="ItName[]" class="form-control" value="<?= $res['ItName'] ?>" readonly></input></td>
                                            <td><input type="number" oninput="quantityLiveChange('<?= $no; ?>')" id="qty<?= $no ?>" name="qty[]" class="form-control" value="<?= number_format($res['Qty'], 0, '', '') ?>" style="text-align: right;" required/></td>
                                            <td><input type="text" name="UOM[]" class="form-control" value="<?= $res['UOM'] ?>" readonly></input></td>
                                            <td><input type="text" oninput="unitPriceLiveChange('<?= $no; ?>')" id="unitPrice<?= $no ?>" name="unitPrice[]" class="form-control" style="text-align: right;" required/></td>
                                            <td>
                                                <select required class="form-control" name="CurCode" id="CurCode">
                                                    <option value=""></option>
                                                    <?php
                                                    $sql = "Select CurCode As Col1, CurName As Col2 From TblCurrency Order By CurName;";
                                                    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

                                                    foreach ($result as $row) {
                                                        echo "<option value='" . $row['Col1'] . "'";
                                                        if ($res['CurCode'] == $row['Col1']) echo " selected";
                                                        echo ">" . $row['Col2'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select required class="form-control" name="PtCode" id="PtCode">
                                                    <option value="">
                                                        <?php
                                                        $sql = "Select PtCode As Col1, PtName As Col2 From TblPaymentTerm Order By PtName; ";
                                                        $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

                                                        foreach ($result as $row) {
                                                            echo "<option value='" . $row['Col1'] . "'";
                                                            if ($res['PtName'] == $row['Col2']) echo " selected";
                                                            echo ">" . $row['Col2'] . "</option>";
                                                        }
                                                        ?>
                                                    </option>
                                                </select>
                                            </td>
                                            <td>
                                                <select required class="form-control" name="DTCode" id="DTCode">
                                                    <option value=""></option>
                                                    <?php
                                                    $sql = "Select DTCode As Col1, DTName As Col2 From TblDeliveryType Order By DTName;";
                                                    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

                                                    foreach ($result as $row) {
                                                        echo "<option value='" . $row['Col1'] . "'>" . $row['Col2'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control" name="DocDt[]" value="<?= $res['DocDt'] ?>" readonly></input></td>
                                            <td>
                                                <input type="text" class="form-control" name="ExpiredDt" value="<?= $res['ExpDt'] ?>" data-field="date" ></input>
                                                <div id="DteExpiredDt"></div>
                                            </td>
                                            <td><input type="text" class="form-control" id="totalPrice<?= $no ?>" name="totalPrice[]" value="<?= number_format($res['TotalPrice'], 0, '', '') ?>" style="text-align: right;" readonly></input></td>
                                            <td><input type="text" name="remark[]" class="form-control" /></td>
                                        </tbody>
                                    <?php } ?>
                                <?php } ?>
                            </table>
                            <div align="right">
                                <input type="reset" class="btn" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="background-color: #58585A;    color:white" />
                                <button type="submit" name="submit" class="btn btn-info">Save Quotation</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    <?php } ?>

<?php } ?>

<script>
    $("#DteExpiredDt").DateTimePicker();

    var qtyValue;
    var qtyID;
    var unitPriceID;
    var totalPriceID;
    var unitPriceValue;
    var countTotalPrice;
    var countRow;
    var total;

    function quantityLiveChange(index) {
        qtyID = "qty" + index;
        unitPriceID = "unitPrice" + index;
        totalPriceID = "totalPrice" + index;

        qtyValue = document.getElementById(qtyID).value;
        unitPriceValue = document.getElementById(unitPriceID).value;
        document.getElementById(totalPriceID).value = (qtyValue.replaceAll(".", "") * unitPriceValue.replaceAll(".", ""));

        totalAmout();
    }

    function unitPriceLiveChange(index) {
        qtyID = "qty" + index;
        unitPriceID = "unitPrice" + index;
        totalPriceID = "totalPrice" + index;

        qtyValue = document.getElementById(qtyID).value;
        unitPriceValue = document.getElementById(unitPriceID).value;
        document.getElementById(totalPriceID).value = (qtyValue.replaceAll(".", "") * unitPriceValue.replaceAll(".", ""));

        totalAmout();
    }

    function totalAmout() {
        countTotalPrice = 0;
        countRow = document.getElementById('countRow').value;
        for (var i = 1; i <= countRow; i++) {
            totalPriceID = "totalPrice" + i;
            if (document.getElementById(totalPriceID) != null) {
                total = document.getElementById(totalPriceID).value;
            }
            countTotalPrice = countTotalPrice + parseInt(total.replaceAll(".", ""));
        }
        if (!Number.isNaN(countTotalPrice)) {
            document.getElementById('totalQtAmt').value = new Intl.NumberFormat('id-ID', {style: 'currency', currency: 'IDR'}).format(countTotalPrice);
        } else {
            document.getElementById('totalQtAmt').value = ""
        }
    }

    $('#tblrfq').DataTable({
        responsive: true,
        autoWidth: false,
        scrollX: true,
        paging: false,
        columnDefs: [{
            width: 200,
            targets: 0
        }]
    })

    $('form input').keydown(function(e) {
        if (e.keyCode == 13) {
            var inputs = $(this).parents("form").eq(0).find(":input");
            if (inputs[inputs.index(this) + 1] != null) {
                inputs[inputs.index(this) + 1].focus();
            }
            e.preventDefault();
            return false;
        }
    });

    countRow = document.getElementById('countRow').value;
    for (var i = 1; i <= countRow; i++) {
        $("#unitPrice" + i).inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': '.',
            'autoGroup': true,
            'digits': 2,
            'radixPoint': ".",
            'digitsOptional': false,
            'allowMinus': false,
            'placeholder': ''
        });

        $("#totalPrice" + i).inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': '.',
            'autoGroup': true,
            'digits': 2,
            'radixPoint': ".",
            'digitsOptional': false,
            'allowMinus': false,
            'placeholder': ''
        });
    }
</script>
<script>
    $(document).ready(function() {
        var countVdBankAccount = document.getElementById("countVdBankAccount").value;
        var counterBA = 5;
        var t = '<?= $sumRow ?>'

        for (let i = 1; i <= t; i++) {

            $("#addrow" + i).on("click", function() {
                var newRow = $("<tr>");
                var cols = "";

                cols += '<td><input type="file" name="VdFile' + i + '[]" id="VPFile' + i + '" accept="application/pdf, image/png, image/jpg, image/jpeg" required/></td>';
                // cols += '<td><div class="form-group">';                              
                // cols +=         '<div class="">'
                // cols +=            '<div class="input-group date">'
                // cols +=                '<input type="text" class="form-control validto" id="validto" name="ValidToDtl[]" data-field="date" />'
                // cols +=             '</div>'
                // cols +=         '</div>'
                // cols +=         '<div id="DteValidTo"></div>'
                // cols +=    '</div>'
                // cols +=  '</td>';
                cols += '<td>';
                cols += '<input type="button" class="BAbtnDel' + i + ' btn btn-xs btn-danger" value="Delete"><br>';

                cols += '</td>';
                newRow.append(cols);
                $("table.File" + i).append(newRow);

                for (var j = 0; j <= counterBA; j++)
                    $(".selectBankCode" + j + "").select2();

                // counterBA++;
            });


            $("table.File" + i).on("click", ".BAbtnDel" + i, function(event) {
                $(this).closest("tr").remove();
                // counterBA -= 1
            });
        }
    });
</script>