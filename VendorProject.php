<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

$host = FTP_HOST;
$user = FTP_USER;
$pass = FTP_PASS;

function file_exist_on_ftp_server($fileName, $ftpConn){
    $fileSize = ftp_size($ftpConn,FTP_DESTINATION.$fileName);
    if ($fileSize < 0) {
        return false;
    }
    
    return true;
}

if(isset($_POST['submit']))
{

    $sql = "";
    $DNo = GetValue("Select IfNull(Right(Concat('000', Max(DNo) + 1), 3), '') DNo From TblVendorProject Where VdCode = '".$_SESSION['vdcode']."'; ");
    if(strlen($DNo) <= 0) $DNo = "001";

    if(isset($_FILES['VdFile'])){

        $imgFile = $_FILES['VdFile']['name'];
        $tmp_dir = $_FILES['VdFile']['tmp_name'];
        $imgSize = $_FILES['VdFile']['size'];
        $imgExt = array();
        $VdFilesLoc = array();
        $DNo2 = array();
        $i = 1;
        $upload_dir = "dist/pdf/";
        $valid_extension = array('pdf', 'png', 'jpg', 'jpeg');
        $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;
        $sqlDNo = "
            Select (Max(DNo2) + 1) DNo
            From TblVendorProjectUpload
            Where VdCode = '".$_SESSION['vdcode']."'
            and FileLocation regexp '".$_POST['Project']."'
            ;
        ";
        // generate dno
        //$rows = GetValue("Select IfNull(Right(Concat('000', Max(DNo2) + 1), 3), '') DNo From TblVendorProjectUpload Where VdCode = '".$_SESSION['vdcode']."' and FileLocation regexp '". $_POST['Project'] ."' ; ");
        $rows = GetValue($sqlDNo);
        $generate = null;

        if (strlen($rows) == 0) $rows = "0";
        
        if($rows == "0"){
            for ($i = 0; $i < count($imgFile); $i++) {
                $generate = str_pad(($rows + 1) + $i, 3, "0", STR_PAD_LEFT);
                array_push($DNo2, $generate);
            }
        }else {
            for ($i = 0; $i < count($imgFile); $i++) {
                $generate = str_pad($rows + $i, 3, "0", STR_PAD_LEFT);
                array_push($DNo2, $generate);
            }
        }
        // end

        // if($rows == 0){
        //     for ($i=0; $i < count($imgFile); $i++) { 
        //         $generate = str_pad($i + 1, 3, "0", STR_PAD_LEFT);
        //         array_push($DNo2, $generate);
        //     }
        // }else {
        //     // array_push($DNo2, str_pad($rows + 1, 3, "0", STR_PAD_LEFT));
        //     for ($i= 0 ; $i < count($imgFile); $i++) { 
        //         $generate = str_pad($rows + $i, 3, "0", STR_PAD_LEFT);
        //         array_push($DNo2, $generate);
        //     }
        // }

        for ($i=0; $i < count($imgFile); $i++) { 
        $ext = strtolower(pathinfo($imgFile[$i],PATHINFO_EXTENSION));
        array_push($imgExt, $ext);
        }

        for ($i=0; $i < count($imgExt); $i++) {                 
            $filesLoc = $_SESSION['vdcode']."_". $_POST['Project'] ."-". $DNo2[$i]. "_". date("Ymdhms") .".".$imgExt[$i];
            array_push($VdFilesLoc, $filesLoc);
        }

        for($i = 0; $i < count($imgFile); $i++){

            if(in_array($imgExt[$i], $valid_extension)) // check kalau extensi nya valid
            {

                if($imgSize[$i] == 0)
                {

                    echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                    echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";
                }
                else
                {

                    if($imgSize[$i] < $mMaxFile) // if image size < 1025 KB
                    {
                        // echo "<script type='text/javascript'>alert('".$tmp_dir[$i]." || ".$upload_dir.$VdFilesLoc[$i]."');</script>";
                        move_uploaded_file($tmp_dir[$i], $upload_dir.$VdFilesLoc[$i]);
    
                        $sql = $sql . "Insert Into TblVendorProjectUpload(VdCode, DNo ,DNo2, Status, FileLocation, EndDt, CreateBy, CreateDt) ";
                        $sql = $sql . "Values('".$_SESSION['vdcode']."','".$DNo."','".$DNo2[$i]."', 'A', '".$VdFilesLoc[$i]."', ";                            
                        
                        if(isset($_POST['ValidTo']) && strlen($_POST['ValidTo'][$i]) > 0)
                            $sql = $sql . "'".date("Ymd", strtotime($_POST['ValidTo'][$i]))."', ";
                        else
                            $sql = $sql . "NULL, ";
                        
                        $sql = $sql . "'".$_SESSION['vdusercode']."', CurrentDateTime());";

                    }
                    else
                    {

                        echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                        echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";
                    }
                    }
            }
            else
            {                  
                echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
                echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";
            }
        
        }
        

        echo "<br>";        

        if (FTP_STATUS === "enable") {
            try {
                $ftpConn = ftp_connect($host);
                $login = ftp_login($ftpConn,$user,$pass);
                $ftpPasv = ftp_pasv($ftpConn, true);

                if ((!$ftpConn) || (!$login)) {
                    echo "FTP connection has failed! Attempted to connect to ". $host.".";
                }

                if (!$ftpPasv) {
                    echo "Cannot switch to passive mode";
                }

                if($login) {
                    
                    $ftpSuccess = false;
                    for ($i=0; $i < count($VdFilesLoc); $i++) { 
                        if (!ftp_put($ftpConn, FTP_DESTINATION.$VdFilesLoc[$i], $upload_dir.$VdFilesLoc[$i], FTP_BINARY)) {
                            echo "<script>alert('Error uploading ". $VdFilesLoc[$i] .".');</script>";
                        }else {
                            $ftpSuccess = true;
                        } 
                    }
                    if($ftpSuccess){
                        echo "<script>alert('Success uploading File.');</script>";
                    }
                    ftp_close($ftpConn);
                }
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
    }

    $ContractDt = ""; 
    if(isset($_POST['ContractDt'])) 
        if(strlen($_POST['ContractDt']) == 10) 
            $ContractDt = date('Ymd', strtotime($_POST['ContractDt']));

    $ContractEndDt = ""; 
    if(isset($_POST['ContractEndDt'])) 
        if(strlen($_POST['ContractEndDt']) == 10) 
            $ContractEndDt = date('Ymd', strtotime($_POST['ContractEndDt']));

    $R1 = array(" ", ".", ",");
    $R2 = array("", "", ".");
    $ProjectValue = 0;
    if(isset($_POST['ProjectValue']))
        if(strlen($_POST['ProjectValue']) > 0)
            $ProjectValue = str_replace($R1, $R2, $_POST['ProjectValue']);

    $sql = $sql . "Insert Into TblVendorProject(VdCode, DNo, ProfileCode,Project, ProjectCategory, ";
    $sql = $sql . "ContractNumber, ContractDt, Owner, OwnerAddress, ProjectLocation, Results, ";
    $sql = $sql . "ProjectValue, CurCode, ContractEndDt, WorkAchievement, ProgressCode, CreateBy, CreateDt) ";
    $sql = $sql . "Values('".$_SESSION['vdcode']."', '".$DNo."', '".$DNo."' ,NullIf('".$_POST['Project']."', ''), NullIf('".$_POST['ProjectCategory']."', ''), ";
    $sql = $sql . "NullIf('".$_POST['ContractNumber']."', ''), NullIf('".$ContractDt."', ''), NullIf('".$_POST['Owner']."', ''), NullIf('".$_POST['OwnerAddress']."', ''), NullIf('".$_POST['ProjectLocation']."', ''), NullIf('".$_POST['Results']."', ''), ";
    $sql = $sql . "'".$ProjectValue."', NullIf('".$_POST['LueCurCode']."', ''), NullIf('".$ContractEndDt."', ''), NullIf('".$_POST['WorkAchievement']."', ''), NullIf('".$_POST['LueProgressCode']."', ''), '".$_SESSION['vdusercode']."', CurrentDateTime());";

    $expSQL = explode(";",$sql);
    $countSQL = count($expSQL);
    $success = false;

    for($i = 0; $i < $countSQL - 1; $i++)
	{
		if (mysqli_query($conn, $expSQL[$i])) {
			$success = true;
		} else {
			echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
		}
	}

	if($success)
	{
		mysqli_commit($conn);
		echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";
	}

    
}
else if(isset($_POST['DNoDel']))
{
    $ProfileDNo = $_POST['DNoDel'];

    echo $ProfileDNo;
    $sql1 = "select FileLocation from TblVendorProjectUpload Where DNo='".$ProfileDNo."';";
    $result1 = mysqli_query($conn, $sql1);

    $sql2 = "";
    
    $sql2 = $sql2 . "Delete From TblVendorProjectUpload ";
    $sql2 = $sql2 . "Where DNo = '".$ProfileDNo."'; ";

    $sql2 = $sql2 . "Delete From TblVendorProject ";
    $sql2 = $sql2 . "Where VdCode = '".$_SESSION['vdcode']."' ";
    $sql2 = $sql2 . "And DNo = '".$ProfileDNo."'; ";

    $expSQL = explode(";", $sql2);
	$countSQL = count($expSQL);
	$success = false;

    echo "<br/>";

	for($i = 0; $i < $countSQL - 1; $i++)
	{
		if (mysqli_query($conn, $expSQL[$i])) {
			$success = true;
		} else {
			echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
		}
	}

    if ($success) 
    {
        
        mysqli_commit($conn);
        if (FTP_STATUS === "enable") {      
            while ($res = mysqli_fetch_assoc($result1)) {
                try {
                    $ftpConn = ftp_connect($host);
                    $login = ftp_login($ftpConn,$user,$pass);
                    $ftpPasv = ftp_pasv($ftpConn, true);
        
                    if ((!$ftpConn) || (!$login)) {
                        echo "FTP connection has failed! Attempted to connect to ". $host.".";
                    }
        
                    if (!$ftpPasv) {
                        echo "Cannot switch to passive mode";
                    }
        
                    if($login) {
        
                        if(file_exist_on_ftp_server(basename($res['FileLocation']), $ftpConn)){
                            ftp_delete($ftpConn,FTP_DESTINATION.basename($res['FileLocation']));
                            // if (!$deleteFile) {
                            //     echo "Could not delete ". basename($res['FileLocation']);
                            // }
                            // return true;
                        }
                        ftp_close($ftpConn);

                    }
                } catch (Exception $e) {
                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                }
            }
            
        }
        echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";
    } 

} else if(isset($_POST['file'])){
    $sql2 = "";
    $FileLoc = $_POST['file'];
    
    $sql2 = $sql2 . "Delete From TblVendorProjectUpload ";
    $sql2 = $sql2 . "Where FileLocation = '".$FileLoc."' ";

    if (mysqli_query($conn,$sql2)) 
    {
        mysqli_commit($conn);
        if (FTP_STATUS === "enable") {
            try {
                $ftpConn = ftp_connect($host);
                $login = ftp_login($ftpConn,$user,$pass);
                $ftpPasv = ftp_pasv($ftpConn, true);
    
                if ((!$ftpConn) || (!$login)) {
                    echo "FTP connection has failed! Attempted to connect to ". $host.".";
                }
    
                if (!$ftpPasv) {
                    echo "Cannot switch to passive mode";
                }
    
                if($login) {
    
                    if(file_exist_on_ftp_server(basename($FileLoc), $ftpConn)){
                        $deleteFile = ftp_delete($ftpConn,FTP_DESTINATION.basename($FileLoc));
                        if (!$deleteFile) {
                            echo "Could not delete $FileLoc";
                        }
                        ftp_close($ftpConn);
                        return true;
                    }
                }
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";
    } 
    else 
    {
        echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
        mysqli_rollback($conn);
    }
} else if(isset($_POST['edit'])){

    $sql= "";
    if(isset($_FILES['VdFileDtl'])){

        $imgFile = $_FILES['VdFileDtl']['name'];
        $tmp_dir = $_FILES['VdFileDtl']['tmp_name'];
        $imgSize = $_FILES['VdFileDtl']['size'];
        $imgExt = array();
        $VdFilesLoc = array();
        $DNo2 = array();
        $i = 1;
        $upload_dir = "dist/pdf/";
        $valid_extension = array('pdf', 'png', 'jpg', 'jpeg');
        $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;
        
        $sqlDNo = "
            Select (Max(DNo2) + 1) DNo
            From TblVendorProjectUpload
            Where VdCode = '".$_SESSION['vdcode']."'
            and FileLocation regexp '".$_POST['Project']."'
            ;
        ";
        // generate dno
        //$rows = GetValue("Select IfNull(Right(Concat('000', Max(DNo2) + 1), 3), '') DNo From TblVendorProjectUpload Where VdCode = '".$_SESSION['vdcode']."' and FileLocation regexp '". $_POST['Project'] ."' ; ");
        $rows = GetValue($sqlDNo);
        $generate = null;

        if (strlen($rows) == 0) $rows = "0";
        
        if ($rows == "0") {
            for ($i = 0; $i < count($imgFile); $i++) {
                $generate = str_pad(($rows + 1) + $i, 3, "0", STR_PAD_LEFT);
                array_push($DNo2, $generate);
            }
        }else {
            for ($i = 0; $i < count($imgFile); $i++) {
                $generate = str_pad($rows + $i, 3, "0", STR_PAD_LEFT);
                array_push($DNo2, $generate);
            }
        }
        // end

        for ($i=0; $i < count($imgFile); $i++) { 
        $ext = strtolower(pathinfo($imgFile[$i],PATHINFO_EXTENSION));
        array_push($imgExt, $ext);
        }

        for ($i=0; $i < count($imgExt); $i++) { 

            $filesLoc = $_SESSION['vdcode']."_". $_POST['Project'] ."-". $DNo2[$i]. "_". date("Ymdhms") .".".$imgExt[$i];
            array_push($VdFilesLoc, $filesLoc);
        }

        for($i = 0; $i < count($imgFile); $i++){

            if(in_array($imgExt[$i], $valid_extension)) // check kalau extensi nya valid
            {

                if($imgSize[$i] == 0)
                {

                    echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                    echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";
                }
                else
                {

                    if($imgSize[$i] < $mMaxFile) // if image size < 1025 KB
                    {
                        // echo "<script type='text/javascript'>alert('".$tmp_dir[$i]." || ".$upload_dir.$VdFilesLoc[$i]."');</script>";
                        move_uploaded_file($tmp_dir[$i], $upload_dir.$VdFilesLoc[$i]);

                        $sql = $sql . "Insert Into TblVendorProjectUpload(VdCode, DNo, DNo2, Status, FileLocation, EndDt, CreateBy, CreateDt) ";
                        $sql = $sql . "Values('".$_SESSION['vdcode']."', '".$_POST['DNo']."', '".$DNo2[$i]."' , 'A', '".$VdFilesLoc[$i]."', ";                        
                        
                        if(isset($_POST['ValidToDtl']) && strlen($_POST['ValidToDtl'][$i]) > 0)
                            $sql = $sql . "'".date("Ymd", strtotime($_POST['ValidToDtl'][$i]))."', ";
                        else
                            $sql = $sql . "NULL, ";
                        
                        $sql = $sql . "'".$_SESSION['vdusercode']."', CurrentDateTime());";
                        
                    }
                    else
                    {

                        echo "<script type='text/javascript'>alert('Maximum document size is ".GetParameter("FileSizeMaxUploadFTPClient")." MB.')</script>";
                        echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";
                    }
                    }
            }
            else
            {                  
                echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
                echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";
            }
        
        }        

        if (FTP_STATUS === "enable") {
            try {
                $ftpConn = ftp_connect($host);
                $login = ftp_login($ftpConn,$user,$pass);
                $ftpPasv = ftp_pasv($ftpConn, true);

                if ((!$ftpConn) || (!$login)) {
                    echo "FTP connection has failed! Attempted to connect to ". $host.".";
                }

                if (!$ftpPasv) {
                    echo "Cannot switch to passive mode";
                }

                if($login) {
                    
                    $ftpSuccess = false;
                    for ($i=0; $i < count($VdFilesLoc); $i++) { 
                        if (!ftp_put($ftpConn, FTP_DESTINATION.$VdFilesLoc[$i], $upload_dir.$VdFilesLoc[$i], FTP_BINARY)) {
                            echo "<script>alert('Error uploading ". $VdFilesLoc[$i] .".');</script>";
                        }else {
                            $ftpSuccess = true;
                        } 
                    }
                    if ($ftpSuccess) {
                        echo "<script>alert('Success uploading File.');</script>";
                    }
                    ftp_close($ftpConn);
                }
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
    
    }

    $ContractDt = ""; 
    $ContractDt1 = "";
    if(isset($_POST['ContractDt'])) {
        if(strlen($_POST['ContractDt']) == 10) {
            $ContractDt = date('Ymd', strtotime($_POST['ContractDt']));
        }else {
            $ContractDt1 = date('Ymd', strtotime($_POST['ContractDt']));

        }
    }
        

    $ContractEndDt = ""; 
    if(isset($_POST['ContractEndDt'])){ 
        if(strlen($_POST['ContractEndDt']) == 10){ 
            $ContractEndDt = date('Ymd', strtotime($_POST['ContractEndDt']));
        }else {
            $ContractEndDt = date('Ymd', strtotime($_POST['ContractEndDt']));

        }
    }

    $R1 = array(" ", ".", ",");
    $R2 = array("", "", ".");
    $ProjectValue = 0;
    if(isset($_POST['ProjectValue'])){
        if(strlen($_POST['ProjectValue']) > 0){
            $ProjectValue = str_replace($R1, $R2, $_POST['ProjectValue']);
        }else {
            $ProjectValue = str_replace($R1, $R2, $_POST['ProjectValue']);
            
        }
            
    }
        

    $sql = $sql. "update tblvendorproject a         
        set a.Project = NullIf('". $_POST['Project'] ."', '') , a.ProjectCategory = NullIf('". $_POST['ProjectCategory'] ."', '') , a.ContractNumber = NullIf('". $_POST['ContractNumber'] ."', '') , a.ContractDt = '". ($ContractDt != "" ? $ContractDt : $ContractDt1) ."', 
            a.Owner = NullIf('". $_POST['Owner'] ."', ''), a.OwnerAddress = NullIf('". $_POST['OwnerAddress'] ."', ''), a.ProjectLocation = NullIf('". $_POST['ProjectLocation'] ."', ''), a.Results = NullIf('". $_POST['Results'] ."', ''),
            a.ProjectValue = '". $ProjectValue ."', a.CurCode = NullIf('". $_POST['CurCode'] ."', ''), a.ContractEndDt = '". $ContractEndDt ."', a.WorkAchievement = NullIf('". $_POST['WorkAchievement'] ."', ''), 
            a.ProgressCode = NullIf('". $_POST['ProgressName'] ."', ''), a.LastUpBy = '". $_SESSION['vdcode'] ."', a.LastUpDt = CurrentDateTime()
        where a.VdCode = '". $_SESSION['vdcode'] ."' and a.DNo = '". $_POST['DNo'] ."';";

    $expSQL = explode(";", $sql);
    $countSQL = count($expSQL);
    $success = false;

    for ($i=0; $i < $countSQL -1; $i++) { 
        
        if(mysqli_query($conn, $expSQL[$i])){
            $success = true;
        }else{
            echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
        }
    }

    if ($success) {
        mysqli_commit($conn);
		echo "<script type='text/javascript'>window.location='VendorProject.php'</script>";

    }
}
else
{
?>

<form class="form-horizontal" name="insertVendor" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
            <h4 class="box-title">Project Experience</h4>
            </div>
                <div class="box-body">
                    <!-- <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Profile Name</label>

                        <div class="col-sm-5">
                            <select class="form-control selectProfileCode" name="LueProfileCode" id="LueProfileCode" style="width: 100%;" required>
                                <option value=""></option>
                            <?php
                                $q = "Select ProfileCode As Col1, ProfileName  As Col2 ";
                                $q = $q . "From TblVendorProfile ";
                                $q = $q . "Order By ProfileName ";

                                $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                                while($q2 = mysqli_fetch_assoc($q1))
                                {
                                    echo "<option value='".$q2['Col1']."' ";
                                    echo ">".$q2['Col2']."</option>";
                                }
                            ?>
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Project Experience</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Project" id="Project" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Project Category</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="ProjectCategory" id="ProjectCategory" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Contract Number</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="ContractNumber" id="ContractNumber" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Contract Date</label>

                        <div class="col-sm-5">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control ContractDt" id="ContractDt" name="ContractDt" data-field="date" />
                            </div>
                        </div>

                        <div id="DteContractDt"></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Owner</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Owner" id="Owner" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Owner Address</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="OwnerAddress" id="OwnerAddress" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Project Location</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="ProjectLocation" id="ProjectLocation" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Results</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Results" id="Results" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Currency</label>

                        <div class="col-sm-5">
                            <select class="form-control selectCurCode" name="LueCurCode" id="LueCurCode" style="width: 100%;">
                                <option value=""></option>
                            <?php
                                $q = "Select CurCode As Col1, CurName  As Col2 ";
                                $q = $q . "From TblCurrency ";
                                $q = $q . "Order By CurName ";

                                $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                                while($q2 = mysqli_fetch_assoc($q1))
                                {
                                    echo "<option value='".$q2['Col1']."' ";
                                    echo ">".$q2['Col2']."</option>";
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Project Value</label>
                        <div class="col-sm-5">
                            <input type="text" autocomplete="off" class="form-control angka numberOnly" name="ProjectValue" id="ProjectValue" value="0" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">End of Contract</label>

                        <div class="col-sm-5">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control ContractEndDt" id="ContractEndDt" name="ContractEndDt" data-field="date" />
                            </div>
                        </div>

                        <div id="DteContractEndDt"></div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Work Achievement</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="WorkAchievement" id="WorkAchievement" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Progress</label>

                        <div class="col-sm-5">
                            <select class="form-control selectProgressCode" name="LueProgressCode" id="LueProgressCode" style="width: 100%;">
                                <option value=""></option>
                            <?php
                                $q = "Select OptCode As Col1, OptDesc As Col2 ";
                                $q = $q . "From TblOption ";
                                $q = $q . "Where OptCat = 'VendorProgress' ";
                                $q = $q . "Order By OptDesc; ";

                                $q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
                                while($q2 = mysqli_fetch_assoc($q1))
                                {
                                    echo "<option value='".$q2['Col1']."' ";
                                    echo ">".$q2['Col2']."</option>";
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <table class="table File" id="tblBankAccount">
                        <thead>
                            <tr>
                                <th>File</th>
                                <th>Valid To</th>
                                <th>#</th>								
                            </tr>
                        </thead>
                        <tbody>
                            <tr>									
                            <td class='col-sm-3'>
                            <input type='file' name='VdFile[]' id="file" accept="application/pdf, image/png, image/jpg, image/jpeg" required/>
                            </td>
                            <td class='col-sm-3'>
                                <div class="form-group">                              
                                    <div class="col-sm-8">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control validto" id="validto" name="ValidTo[]" data-field="date" required/>
                                        </div>
                                    </div>

                                    <div id="DteValidTo"></div>
                                </div>
                            </td>									
                            <td>
                                <input type='button' id='BAbtnDel' class='BAbtnDel btn btn-md btn-danger' value='Delete'>
                                <!-- <br>
                                <input type='button' id='btnUpload' name="upload" class='btnUpload btn btn-md btn-success' value='Upload'> -->
                            </td>
                            </tr>
                            <input type='hidden' name='countVdBankAccount' id='countVdBankAccount'/>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5" style="text-align: left;">
                                    <input type="button" class="btn btn-lg btn-block " id="addrow" value="Add Row"  />
                                </td>
                            </tr>
                            <tr>
                            </tr>
                        </tfoot>
                    </table>
                <hr />
                <input type="reset" class="btn " onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="background-color: #58585A; color:white"/>
                <button type="submit" name="submit" class="btn pull-right" id="BtnSave" style="background-color: #D61E2F; color:white">Save</button>    
                </div>
                <!-- /.box-body -->
        </div>
    </div>
</div>
</form>

<br />
<div class="row">
    <div class="col-md-12">
        <div class="box box-default">
            <div class="box-header with-border">
            <h4 class="box-title">Manage your data</h4>
            </div>
                <div class="box-body">
                <form action="" name="manageUpload" role="form" enctype="multipart/form-data" method="POST">
                    <table id="VdProject" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <!-- <th>Profile Name</th> -->
                                <th>Project Experience</th>
                                <th>Project Category</th>
                                <th>Contract Number</th>
                                <th>Contract Date</th>
                                <th>Owner</th>
                                <th>Owner Address</th>
                                <th>Project Location</th>
                                <th>Results</th>
                                <th>Currency</th>
                                <th>Project Value</th>
                                <th>End of Contract</th>
                                <th>Work Achievement</th>
                                <th>Progress</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                                $sql = "";

                                $sql = $sql . "Select A.*, C.CurName, D.OptDesc As ProgressName ";
                                $sql = $sql . "From TblVendorProject A ";
                                // $sql = $sql . "Inner Join TblVendorProfile B On A.VdCode = B.VdCode ";
                                $sql = $sql . "Left Join TblCurrency C On A.CurCode = C.CurCode ";
                                $sql = $sql . "Left Join TblOption D On A.ProgressCode = D.OptCode And D.OptCat = 'VendorProgress' ";
                                $sql = $sql . "Where A.VdCode = '".$_SESSION['vdcode']."' ";
                                $sql = $sql . "Order By A.DNo; ";

                                $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                                while($res = mysqli_fetch_assoc($result))
                                {
                                    echo "<tr>";
                                    // echo "<td>" . $res['ProfileName'] . "</td>";
                                    echo "<td>" . $res['Project'] . "</td>";
                                    echo "<td>" . $res['ProjectCategory'] . "</td>";
                                    echo "<td>" . $res['ContractNumber'] . "</td>";
                                    if(strlen($res['ContractDt'] > 0))
                                        echo "<td>" . date('d-m-Y', strtotime($res['ContractDt'])) . "</td>";
                                    else
                                        echo "<td></td>";
                                    echo "<td>" . $res['Owner'] . "</td>";
                                    echo "<td>" . $res['OwnerAddress'] . "</td>";
                                    echo "<input type='hidden' name='ProfileCode' value='".$res['ProfileCode']."' />";
                                    echo "<input type='hidden' name='ProfileDNo' value='".$res['DNo']."' />";                               
                                    echo "<td>" . $res['ProjectLocation'] . "</td>";
                                    echo "<td>" . $res['Results'] . "</td>";
                                    echo "<td>" . $res['CurCode'] . "</td>";
                                    echo "<td align='right'>" . number_format($res['ProjectValue'], 2, '.', ',') . "</td>";
                                    if(strlen($res['ContractEndDt'] > 0))
                                        echo "<td>" . date('d-m-Y', strtotime($res['ContractEndDt'])) . "</td>";
                                    else
                                        echo "<td></td>";
                                    echo "<td>" . $res['WorkAchievement'] . "</td>";
                                    echo "<td>" . $res['ProgressName'] . "</td>";
                                    echo "<td>";
                                    echo "<button type='button' name='detail' class='vpDetail btn-xs btn-info' 
                                        data-toggle='modal'
                                        data-target='#VPDetail'
                                        data-name='" . $res['Project'] . "'
                                        data-dno='" . $res['DNo'] . "'
                                        > Detail </button>";
                                    echo "<div style='height:10px;'></div>";
                                    echo "<button type='button' id='" . $res['DNo'] . "' name='delete' class='btnDelete btn-xs btn-danger'> Delete </button>";
                                    echo "</td>";
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </form>                 
                </div>
                <!-- /.box-body -->
        </div>
    </div>
</div>

<div class="modal fade" id="VPDetail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <center><h4 class="modal-title" id="myModalLabel"></h4></center>
            </div>
            <div id="Detail"></div>
        </div>
    </div>
</div>

<script type="text/javascript" src="VendorProject.js"></script>
<script>
$(document).ready(function () {
	var countVdBankAccount = document.getElementById("countVdBankAccount").value;
	var counterBA = 5;

	$("#addrow").on("click", function () {
		var newRow = $("<tr>");
		var cols = "";

		cols += '<td><input type="file" name="VdFile[]" id="file" accept="application/pdf, image/png, image/jpg, image/jpeg" required/></td>';
		cols += '<td><div class="form-group">';                              
        cols +=         '<div class="col-sm-8">'
        cols +=            '<div class="input-group date">'
        cols +=                 '<div class="input-group-addon">'
        cols +=                     '<i class="fa fa-calendar"></i>'
        cols +=                 '</div>'
        cols +=                '<input type="text" class="form-control validto" id="validto" name="ValidTo[]" data-field="date" required/>'
        cols +=             '</div>'
        cols +=         '</div>'
        cols +=         '<div id="DteValidTo"></div>'
        cols +=    '</div>'
        cols +=  '</td>';
		cols += '<td>';
        cols += '<input type="button" class="BAbtnDel btn btn-md btn-danger" value="Delete"><br>';
        
        cols += '</td>';
		newRow.append(cols);
		$("table.File").append(newRow);

		for(var j = 0; j <= counterBA; j++)
			$(".selectBankCode" + j + "").select2();

		counterBA++;
	});

	$("table.File").on("click", ".BAbtnDel", function (event) {
		$(this).closest("tr").remove();       
		counterBA -= 1
	});
});
</script>
<script>
    $(function (){
        $('.vpDetail').click(function(){

            var name = $(this).data('name');
            var dno = $(this).data('dno');

            var profileName =  name;
            var Dno = dno;
            
                
            $.ajax({
                url: 'VendorProjectDtl.php',
                method: 'post',
                data: {name:profileName, dno:Dno},
                error: function(err) {
                    console.log('Error ' + err);
                },
                success:function(data){
                    console.log('Success  ' + data);
                    $('#Detail').html(data);                                        
                    $('#VPDetail').modal("show");
                }
            });
        }); 

        $('.btnDelete').click(function(){
            var dno = $(this).attr('id');

            if(confirm("Are you sure want to delete ?")){
                $.ajax({
                url:'VendorProject.php',
                method: 'post',
                data : {DNoDel:dno},
                error: function(err) {
                    console.log('Error ' + err);
                },
                success:function(data){
                    console.log('Success  ' + data);
                    window.location = 'VendorProject.php';
                }
            })
            }
        });

    });
</script>
<?php
} // else not submit
include "footer.php"
?>