<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";
?>

<div class="box">
<div class="box-header">
   <h3 class="box-title">Tender Quotation History</h3>
</div>
<!-- /.box-header -->
<div class="box-body">
   <table id="QTHistory" class="table table-bordered table-striped">
      <thead>
         <tr>
            <th>QT#</th>
            <th>Date</th>
            <th>Quotation Status</th>
            <th>Tender#</th>
            <th>Tender Name</th>
            <th>Currency</th>
            <th>Price</th>
            <th>Action</th>
         </tr>
      </thead>
      <tbody>
          <?php

            $sql = "";

            $sql = $sql . "Select A.DocNo, DATE_FORMAT(A.DocDt, '%d %b %Y') DocDt, ";
            $sql = $sql . "Case B.ActInd When 'Y' Then 'Active' When 'N' Then 'Inactive' End As ActInd, ";
            $sql = $sql . "Case C.Status When 'O' Then 'Open' When 'C' Then 'Closed' Else '' End As TenderStatus, ";
            $sql = $sql . "E.TQRDocNo As TheChosen, ";
            $sql = $sql . "A.TenderDocNo, C.TenderName, A.CurCode, B.UPrice ";
            $sql = $sql . "From TblQtHdr A ";
            $sql = $sql . "Inner Join TblQtDtl B On A.DocNo = B.DocNo ";
            $sql = $sql . "Inner Join TblTender C On A.TenderDocNo = C.DocNo ";
            $sql = $sql . "Left Join TblTenderQuotationRequest D On A.DocNo = D.QtDocNo And C.DocNo = D.TenderDocNo And D.CancelInd = 'N' ";
            $sql = $sql . "Left Join TblTenderQuotation E On D.DocNo = E.TQRDocNo ";
            $sql = $sql . "Where A.VdCode = '".$_SESSION['vdcode']."' ";
            $sql = $sql . "Order By A.CreateDt Desc, A.DocNo Desc; ";

            $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
            while($res = mysqli_fetch_assoc($result))
            {
                echo "<tr>";
                echo "<td>" . $res['DocNo'] . "</td>";
                echo "<td>" . $res['DocDt'] . "</td>";
                echo "<td>";
                if($res['ActInd'] == "Active")
                {
                  echo "<span class='label label-success'>".$res['ActInd']."</span>";
                }
                else
                {
                  echo "<span class='label label-danger'>".$res['ActInd']."</span>";
                }
                echo " ";
                if($res['TheChosen'] != "")
                {
                  echo "<span class='label label-default'>Chosen</span>";
                }
                echo "</td>";
                echo "<td>" . $res['TenderDocNo'] . "</td>";
                echo "<td>";
                if($res['TenderStatus'] == "Open")
                {
                  echo "<span class='label label-info'>".$res['TenderStatus']."</span>";
                }
                else if($res['TenderStatus'] == "Closed")
                {
                  echo "<span class='label label-warning'>".$res['TenderStatus']."</span>";
                }
                echo " " . $res['TenderName'] . "</td>";
                echo "<td>" . $res['CurCode'] . "</td>";
                echo "<td align='right'>" . number_format($res['UPrice'], 2, ",", ".") . "</td>";
                echo "<td><button type='button' class='view_data btn btn-info' data-toggle='modal' id='".$res['DocNo']."' data-target='#QTDetail'> Detail </button></td>";
                echo "</tr>";
            }
          ?>
      </tbody>
   </table>
</div>
<!-- /.box-body -->
</div>

<div class="modal fade" id="QTDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Quotation</h4>
      </div>
      <div class="modal-body" id="QTDetailHistory"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
  $(function () {
    $("#QTHistory").DataTable(
    {
      responsive: true,
      autoWidth: false,
      scrollX: true,
      fixedColumns: 
      {
        leftColumns: 1
      }
    });

    $('.view_data').click(function(){
			var QTDocNo = $(this).attr("id");
      $('#QTDetailHistory').text("");
			
			$.ajax({
				url: 'QTDetailHistory.php',
				method: 'post',
				data: {DocNo:QTDocNo},
				success:function(data){
					$('#QTDetailHistory').html(data);
					$('#QTDetail').modal("show");
                    console.log("Success " + data);
				}
			});
		});

  });
</script>

<?php
include "footer.php";
?>