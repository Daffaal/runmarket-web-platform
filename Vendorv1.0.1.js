$(document).ready(function () {
    var countVdContactPerson = document.getElementById("countVdContactPerson").value;
    var counter = Number(countVdContactPerson);

    $("#CPaddrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        cols += '<td><input type="text" class="form-control" name="CPName' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="CPPos' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="CPNo' + counter + '"/></td>';

        cols += '<td><input type="button" class="CPbtnDel btn btn-md btn-danger" value="Delete"></td>';
        newRow.append(cols);
        $("table.Contact-Person").append(newRow);
        counter++;
    });

    $("table.Contact-Person").on("click", ".CPbtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });

});

function validate()
{
    var tblItCt = 0;
    var tblVdSector = 0;
    tblItCt = document.getElementById("tblItCt").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
    tblVdSector = document.getElementById("tblVdSector").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
    if(tblItCt > 1)
    {
        for(var i = 0; i <= Number(tblItCt); i++)
        {
            for(var j = (i + 1); j < Number(tblItCt); j++)
            {
                var recentItCt = "LueVdItCt" + i.toString() + "";
                var nextItCt = "LueVdItCt" + j.toString() + "";
                
                if(document.getElementById(recentItCt).value == document.getElementById(nextItCt).value)
                {
                    $(".nav-tabs a[href='#ItCt']").tab("show");
                    document.getElementById("eTblItCt").innerHTML = "Cannot input the same Item Category."; 
                    document.getElementById("eTblItCt").scrollIntoView();
                    tblItCt = 0; 
                    return false;
                }
            }
        }
    }

    if(tblVdSector <= 0)
    {
        $(".nav-tabs a[href='#VdSector']").tab("show");
        document.getElementById("eTblVdSector").innerHTML = "You need to input at least one Vendor's Sector."; 
        document.getElementById("eTblVdSector").scrollIntoView();
        return false;
    }

    if(tblVdSector > 1)
    {
        for(var i = 0; i <= Number(tblVdSector); i++)
        {
            for(var j = (i + 1); j < Number(tblVdSector); j++)
            {
                var recentVdSector = "LueVdSector" + i.toString() + "";
                var nextVdSector = "LueVdSector" + j.toString() + "";
                
                if(document.getElementById(recentVdSector).value == document.getElementById(nextVdSector).value)
                {
                    $(".nav-tabs a[href='#VdSector']").tab("show");
                    document.getElementById("eTblVdSector").innerHTML = "Cannot input the same Vendor's Sector."; 
                    document.getElementById("eTblVdSector").scrollIntoView();
                    tblVdSector = 0; 
                    return false;
                }
            }
        }
    }


    if(confirm("Do you want to save this document ?") == false) return false;
}