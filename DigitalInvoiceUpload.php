<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

if(!isset($_GET['DocNo']))
{
	echo "<script type='text/javascript'>alert('Invalid Request.')</script>";
	echo "<script type='text/javascript'>window.location='index.php'</script>";
}
else
{

$DocNo = $_GET['DocNo'];

$sql = "";

$sql = $sql . "Select A.DocNo, B.VdCode, DATE_FORMAT(A.DocDt, '%d/%m/%Y') DocDt, ";
$sql = $sql . "Case A.CancelInd When 'N' Then '' When 'Y' Then 'checked' End As CancelInd, ";
$sql = $sql . "A.CancelReason, A.RecvVdDocNo, Concat(A.RecvVdDocNo, ' - (', Date_Format(B.DocDt, '%d/%b/%Y'), ') ', C.WhsName) RecvVdDesc ";
$sql = $sql . "From TblDigitalInvoiceHdr A ";
$sql = $sql . "Inner Join TblRecvVdHdr B On A.RecvVdDocNo = B.DocNo And B.VdCode = '".$_SESSION['vdcode']."' ";
$sql = $sql . "Inner Join TblWarehouse C On B.WhsCode = C.WhsCode ";
$sql = $sql . "Where A.DocNo = '".$DocNo."' limit 1; ";

$result = mysqli_query($conn,$sql);
$row = mysqli_num_rows($result);
if($row <= 0)
{
	echo "<script type='text/javascript'>alert('No Data.')</script>";
	echo "<script type='text/javascript'>window.location='index.php'</script>";
}
else
{
	if(isset($_POST['submit']))
	{
		$sql = "";

		$imgFile = $_FILES['VdFile']['name'];
		$tmp_dir = $_FILES['VdFile']['tmp_name'];
		$imgSize = $_FILES['VdFile']['size'];

		$upload_dir = "dist/pdf/";
		$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
		$valid_extension = array('pdf');
		$mDocNo = str_replace("/", "-", $DocNo);
		$VdFilesLoc = $mDocNo."_".$_POST['LueDNo'].".".$imgExt;
		$mMaxFile = 3 * 1025 * 1024;

		if(in_array($imgExt, $valid_extension)) // check kalau extensi nya valid
		{
			if($imgSize == 0)
			{
				echo "<script type='text/javascript'>alert('Maximum document size is 3 MB.')</script>";
				echo "<script type='text/javascript'>window.location='DigitalInvoiceUpload.php?DocNo=".$DocNo."'</script>";
			}
			else
			{
				if($imgSize < $mMaxFile) // if image size < 3075 KB
				{
					echo "<script type='text/javascript'>alert('".$tmp_dir." || ".$upload_dir.$VdFilesLoc."');</script>";
					move_uploaded_file($tmp_dir, $upload_dir.$VdFilesLoc);

					$sql = "";

					$sql = $sql . "Update TblDigitalInvoiceDtl Set ";
					$sql = $sql . "    File = '".$upload_dir.$VdFilesLoc."', LastUpBy = '".$_SESSION['vdusercode']."', LastUpDt = CurrentDateTime() ";
					$sql = $sql . "Where DocNo = '".$DocNo."' And DNo = '".$_POST['LueDNo']."'; ";

					//echo $sql;

					if (mysqli_query($conn,$sql)) 
					{
						mysqli_commit($conn);
						echo "<script type='text/javascript'>window.location='DigitalInvoiceUpload.php?DocNo=".$DocNo."'</script>";
					} 
					else 
					{
						echo "Error: " . $sql . "<br>" . mysqli_error($conn);
						mysqli_rollback($conn);
					}
				}
				else
				{
					echo "<script type='text/javascript'>alert('Maximum document size is 3 MB.')</script>";
					echo "<script type='text/javascript'>window.location='DigitalInvoiceUpload.php?DocNo=".$DocNo."'</script>";
				}
			}
		}
		else
		{
			echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
			echo "<script type='text/javascript'>window.location='DigitalInvoiceUpload.php?DocNo=".$DocNo."'</script>";
		}
	}
	else
	{
		while($res = mysqli_fetch_assoc($result))
		{
			if($res['VdCode'] != $_SESSION['vdcode'])
			{
				echo "<script type='text/javascript'>alert('You have no access on this document.')</script>";
				echo "<script type='text/javascript'>window.location='index.php'</script>";
			}
			else
			{
				$DocDt = $res['DocDt'];
				$CancelInd = $res['CancelInd'];
				$CancelReason = $res['CancelReason'];
				$RecvVdDocNo = $res['RecvVdDocNo'];
				$RecvVdDesc = $res['RecvVdDesc'];
?>

<div class="alert alert-info">
    <h4><i class="icon fa fa-info"></i> File Criteria</h4>
    Only allowed <strong>pdf file</strong> document, with maximum of <strong>3 MB</strong> each file.
</div>

<a href="DigitalInvoiceFind.php"><button type="button" class="btn btn-info btn-flat"> <i class="fa fa-search"></i><strong> Find</strong></button></a>
&nbsp;
<a href="DigitalInvoice.php"><button type="button" class="btn btn-success btn-flat"> <i class="fa fa-plus"></i><strong> Insert</strong></button></a>
<br /><br />

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
            <h4 class="box-title">Upload File - <?php echo $DocNo; ?></h4>
            </div>

            <form class="form-horizontal" name="createLeaveRequest" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">
				<div class="box-body">
					<div class="form-group">
		            	<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">No.</label>

		            	<div class="col-sm-5">
                            <select class="form-control selectDNo" required name="LueDNo" id="LueDNo" style="width: 100%;" required>
                                <option value=""></option>
                            <?php
								$q = "";
                                $q = $q . "Select A.DNo As Col1, Concat(A.DNo, ' - ', B.OptDesc, IfNull(Concat(' (', A.DocNumber, ')'), '')) As Col2  ";
								$q = $q . "From TblDigitalInvoiceDtl A ";
								$q = $q . "Left Join TblOption B On A.DocType = B.OptCode And B.OptCat = 'PurchaseInvoiceDocType' ";
								$q = $q . "Where A.DocNo = '".$DocNo."' And A.File Is Null; ";

								$q1 = mysqli_query($conn, $q) or die(mysqli_error($conn));
								while($q2 = mysqli_fetch_assoc($q1))
								{
									echo "<option value='".$q2['Col1']."' ";
									echo ">".$q2['Col2']."</option>";
								}
			                ?>
			                </select>
		            	</div>

		            </div>
		            <div class="form-group">
		            	<label for="inputEmail3" class="col-sm-2 control-label mandatory-field">File</label>

		            	<div class="col-sm-5">
		            		<input type="file" class="form-control" name="VdFile" id="VdFile" required accept="application/pdf" required />
		            	</div>
                        <div class="col-sm-5">
			            	<span class="error-message" id="eVdFile"></span>
			            </div>
		            </div>
		        </div>
				<div class="box-footer">
		        	<input type="reset" class="btn btn-warning" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" />
		            <button type="submit" name="submit" class="btn btn-success pull-right" id="BtnSave">Save</button>
		        </div>
		        <!-- /.box-footer -->
		</div> <!-- box primary -->

		<div class="box box-primary">
				<div class="box-body table-responsive">
					<table class="table table-stripped">
						<thead>
							<tr>
								<th>No.</th>
								<th>Type</th>
								<th>Document#</th>
								<th>Tax Invoice Date</th>
								<th>Amount</th>
								<th>Indicator</th>
								<th>Remark</th>
								<th>File</th>
								<th>#</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$sql = "";

								$sql = $sql . "Select A.DocNo, A.DNo, A.DocType, B.OptDesc As DocTypeDesc, A.DocInd, C.OptDesc As DocIndDesc, ";
								$sql = $sql . "A.DocNumber, IfNull(Date_Format(A.TaxInvDt, '%d/%b/%Y'), '') TaxInvDt, A.Amt, A.Remark, ";
								$sql = $sql . "Case When Length(A.`File`) > 0 Then 'checked' Else '' End As FileInd, A.File ";
								$sql = $sql . "From TblDigitalInvoiceDtl A ";
								$sql = $sql . "Left Join TblOption B On A.DocType = B.OptCode And B.OptCat = 'PurchaseInvoiceDocType' ";
								$sql = $sql . "Left Join TblOption C On A.DocInd = C.OptCode And C.OptCat = 'PurchaseInvoiceDocInd' ";
								$sql = $sql . "Where A.DocNo = '".$DocNo."'; ";

								$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
								while($res = mysqli_fetch_assoc($result))
								{
									echo "<tr>";
									echo "<td>" . $res['DNo'] . "</td>";
									echo "<td>" . $res['DocTypeDesc'] . "</td>";
									echo "<td>" . $res['DocNumber'] . "</td>";
									echo "<td>" . $res['TaxInvDt'] . "</td>";
									echo "<td align='right'>" . number_format($res['Amt'], 2, ".", ",") . "</td>";
									echo "<td>" . $res['DocIndDesc'] . "</td>";
									echo "<td>" . $res['Remark'] . "</td>";
									echo "<td><input type='checkbox' " . $res['FileInd'] . " disabled='disabled' /></td>";
									if(strlen($res['FileInd']) > 0)
										echo "<td><a href='".$res['File']."' target='_blank'> <i class='fa fa-search'></i></a></td>";
									echo "</tr>";
								}
							?>
						</tbody>
					</table>
				</div>
			</form>

        </div> <!-- box primary -->
	</div>
</div>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".selectDNo").select2();

    //Date picker
    $('.datepicker').datepicker({
      autoclose: true
    });
  });
</script>

<script type="text/javascript">
	function validate()
	{
		if(confirm("Do you want to upload this data ?\n\nWARNING ! You cannot delete any file that you have uploaded.") == false) return false;
	}
</script>

<?php
			}
		}
	}
}

} // end else GET  DocNo
include 'footer.php';
?>
