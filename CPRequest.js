function validate()
{
  document.getElementById("eOldPass").innerHTML = "";
  document.getElementById("eNewPass").innerHTML = "";
  document.getElementById("eCNewPass").innerHTML = "";

  var OldPass = document.getElementById("OldPass").value;
  var NewPass = document.getElementById("NewPass").value;
  var CNewPass = document.getElementById("CNewPass").value;

  if(OldPass == "") { document.getElementById("eOldPass").innerHTML = "Old Password should not be empty."; document.getElementById("eOldPass").scrollIntoView(); return false; }
  if(NewPass == "") { document.getElementById("eNewPass").innerHTML = "New Password should not be empty."; document.getElementById("eNewPass").scrollIntoView();  return false; }
  if(CNewPass == "") { document.getElementById("eCNewPass").innerHTML = "Confirm New Password should not be empty."; document.getElementById("eCNewPass").scrollIntoView();  return false; }
  if(NewPass != CNewPass) { document.getElementById("eCNewPass").innerHTML = "Confirm New Password must be the same with New Password."; document.getElementById("eCNewPass").scrollIntoView();  return false; }
  if(NewPass == CNewPass)
  {
    if(CNewPass == OldPass)
    {
      document.getElementById("eCNewPass").innerHTML = "You should assign different password than older password."; 
      document.getElementById("eCNewPass").scrollIntoView();  
      return false;
    }
  }

  if(confirm("Do you want to change your password ?") == false) return false;

  //return false;
}