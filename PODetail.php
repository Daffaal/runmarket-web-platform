<?php
include "auth.php";
include "conn.php";
include "StdMtd.php";

if($_REQUEST['a'] == 0){
    if(isset($_POST['DocNo'])){

        $docno = $_POST['DocNo'];
    
        $sql = ("
            Select B.MaterialRequestDocNo, Date_Format(D.DocDt, '%d-%m-%Y') MaterialRequestDocDt, 
            B.QtDocNo, Date_Format(G.DocDt, '%d-%m-%Y') QtDocDt, E.ItName, B.Qty, E.PurchaseUOMCode UoM, B.EstimatePrice As UPrice, 
            G.CurCode, I.PtName, H.DTName, Date_Format(C.UsageDt, '%d-%m-%Y') UsageDt, (B.Qty * B.EstimatePrice) Total, B.Remark
            From TblPORequestHdr A
            Inner Join TblPORequestDtl B On A.DocNo = B.DocNo
                And A.DocNo = '".$docno."'
            Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo
            Inner Join TblMaterialRequestHdr D On B.MaterialRequestDocNo = D.DocNo
            Inner Join TblItem E On C.ItCode = E.ItCode
            Inner Join TblQtDtl F On B.QtDocNo = F.DocNo And B.QtDNo = F.DNo
            Inner Join TblQtHdr G On B.QtDocNo = G.DocNo
            Left Join TblDeliveryType H On G.DTCode = H.DTCode
            Left Join TblPaymentTerm I On G.PtCode = I.PtCode;
        ");
    
        $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
        echo "<style>table-layout: fixed; word-wrap:break-word;</style>";
        echo    "<table class='table RFQ' id='tblpodetail'>";
        echo    "    <thead>";
        echo    "        <tr>";
        echo    "            <th>RFQ#</th>";
        echo    "            <th>RFQ Date</th>";
        echo    "            <th>Quotation#</th>";
        echo    "            <th>Quotation Date</th>";
        echo    "            <th>Item</th>";
        echo    "            <th>Quantity</th>";
        echo    "            <th>UoM</th>";
        echo    "            <th>Unit Price</th>";
        echo    "            <th>Currency</th>";
        echo    "            <th>Term of Payment</th>";
        echo    "            <th>Delivery Type</th>";
        echo    "            <th>Usage Date</th>";
        echo    "            <th>Remark</th>";
        echo    "            <th>Total Price</th>";
        echo    "        </tr>";
        echo    "    </thead>";
    
        while($res = mysqli_fetch_assoc($result))
        {
    
        echo    "    <tbody>";
        echo    "        <td>". $res['MaterialRequestDocNo'] ."</td>";
        echo    "        <td>". $res['MaterialRequestDocDt'] ."</td>";
        echo    "        <td>". $res['QtDocNo'] ."</td>";
        echo    "        <td>". $res['QtDocDt'] ."</td>";
        echo    "        <td>" . $res['ItName'] ."</td>";
        echo    "        <td class='angka'>". number_format($res['Qty'], 2, ",", ".") ."</td>";
        echo    "        <td>" . $res['UoM'] . "</td>";
        echo    "        <td class='angka'>". number_format($res['UPrice'], 2, ",", ".") ."</td>";
        echo    "        <td>" . $res['CurCode'] . "</td>";
        echo    "        <td>" . $res['PtName'] . "</td>";
        echo    "        <td>" . $res['DTName'] . "</td>";
        echo    "        <td>". $res['UsageDt'] ."</td>";
        echo    "        <td>". $res['Remark'] ."</td>";
        echo    "        <td class='angka'>". number_format($res['Total'], 2, ",", ".") ."</td>";
        echo    "    </tbody>";
    
        }
    
        echo    "</table>";
        echo "<script>$('#tblpodetail').DataTable({responsive: true,autoWidth: true ,
            scrollX: true, 
            scrollY: true
            columnDefs: [
                { width: 200, targets: 0 },
            ]
            }) 
          </script>";
    }
}else {
    if(isset($_POST['DocNo'])){

        $docno = $_POST['DocNo'];
    
        $sql = ("
        Select B.MaterialRequestDocNo, Date_Format(D.DocDt, '%d-%m-%Y') MaterialRequestDocDt,
        B.QtDocNo, Date_Format(E.DocDt, '%d-%m-%Y') QtDocDt, G.ItName, A.Qty, G.PurchaseUoMCode UoM,
        F.UPrice, E.CurCode, H.PtName, I.DTName, J.RecvVdDocDt,
        A.PORequestDocNo, A.DiscountAmt, A.Discount, Date_Format(A.EstRecvDt, '%d-%m-%Y') EstRecvDt,
        A.Remark, (A.Qty * F.UPrice) TotalPrice
        From TblPODtl A
        Inner Join TblPORequestDtl B On A.PORequestDocNo = B.DocNo And A.PORequestDNo = B.DNo
            And A.DocNo = '".$docno."'
        Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo
        Inner Join TblMaterialRequestHdr D On B.MaterialRequestDocNo = D.DocNo
        Inner Join TblQtHdr E On B.QtDocNo = E.DocNo
        Inner Join TblQtDtl F On B.QtDocNo = F.DocNo And B.QtDNo = F.DNo
        Inner Join TblItem G On C.ItCode = G.ItCode
        Left Join TblPaymentTerm H On E.PtCode = H.PtCode
        Left Join TblDeliveryType I On E.DTCode = I.DTCode
        Left Join
        (
            Select T1.PODocNo, T1.PODNo, Group_Concat(Distinct Date_Format(T2.DocDt, '%d-%m-%Y')) RecvVdDocDt
            From TblRecvVdDtl T1
            Inner Join TblRecvVdHdr T2 On T1.DocNo = T2.DocNo
            Where T1.CancelInd = 'N'
            And T1.`Status` = 'A'
            And T1.PODocNo = '".$docno."'
            Group By T1.PODocNo, T1.PODNo
        ) J On A.DocNo = J.PODocNo And A.DNo = J.PODNo;
    
        ");
    
        $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
        echo "<style>table-layout: fixed; word-wrap:break-word;</style>";
        echo    "<table class='table RFQ' id='tblactivepodetail'>";
        echo    "    <thead>";
        echo    "        <tr>";
        echo    "            <th>RFQ#</th>";
        echo    "            <th>RFQ Date</th>";
        echo    "            <th>Quotation#</th>";
        echo    "            <th>Quotation Date</th>";
        echo    "            <th>Item</th>";
        echo    "            <th>Qty</th>";
        echo    "            <th>UoM</th>";
        echo    "            <th>Unit Price</th>";
        echo    "            <th>Currency</th>";
        echo    "            <th>Term of Payment</th>";
        echo    "            <th>Delivery Type</th>";
        echo    "            <th>Received Date</th>";
        echo    "            <th>PO Offer#</th>";
        echo    "            <th>Discount</th>";
        echo    "            <th>Discount%</th>";
        echo    "            <th>Estimated Received Date</th>";
        echo    "            <th>Remark</th>";    
        echo    "            <th>Total Price</th>";
        echo    "        </tr>";
        echo    "    </thead>";
    
        while($res = mysqli_fetch_assoc($result))
        {
    
        echo    "    <tbody>";
        echo    "        <td>". $res['MaterialRequestDocNo'] ."</td>";
        echo    "        <td>". $res['MaterialRequestDocDt'] ."</td>";
        echo    "        <td>". $res['QtDocNo'] ."</td>";
        echo    "        <td>". $res['QtDocDt'] ."</td>";
        echo    "        <td>" . $res['ItName'] ."</td>";
        echo    "        <td class='angka'>". number_format($res['Qty'], 2, ",", ".") ."</td>";
        echo    "        <td>" . $res['UoM'] . "</td>";
        echo    "        <td class='angka'>". number_format($res['UPrice'], 2, ",", ".") ."</td>";
        echo    "        <td>" . $res['CurCode'] . "</td>";
        echo    "        <td>" . $res['PtName'] . "</td>";
        echo    "        <td>" . $res['DTName'] . "</td>";
        echo    "        <td>" . $res['RecvVdDocDt'] . "</td>";
        echo    "        <td>" . $res['PORequestDocNo'] . "</td>";
        echo    "        <td style='text-align:end'>". number_format($res['DiscountAmt'], 2, '.', ',') ."</td>";
        echo    "        <td style='text-align:end'>". number_format($res['Discount'], 2 , '.', ',') ."</td>";
        echo    "        <td>". $res['EstRecvDt'] ."</td>";
        echo    "        <td>". $res['Remark'] ."</td>";
        echo    "        <td class='angka'>". number_format($res['TotalPrice'], 2, ",", ".") ."</td>";
        echo    "    </tbody>";
    
        }
    
        echo    "</table>";
        echo "<script>
        $('#tblactivepodetail').DataTable({
            responsive: true,
            autoWidth: true,
            scrollX: true, 
            scrollY: true
            columnDefs: [
                { width: 200, targets: 0 },
            ]
        });
          </script>";
        // echo "<script type='text/javascript'>
        // $(function(){
        //     $('#tblactivepodetail').DataTable({
        //         responsive: true,
        //         autoWidth: false,
        //         scrollX: true, 
        //         scrollCollapse: true,
        //         paging: false,
        //         fixedColumns: {
        //             leftColumns: 1
        //         }
        //     });
        // });
        //   </script>";
    }
}

?>