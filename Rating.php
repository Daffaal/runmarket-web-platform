<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <h4 class="box-title">Rating Result</h4>
    </div>
    <div class="box-body">

        <table class="table table-striped table-bordered">
            <thead>
                <th>No.</th>
                <th>Indicator</th>
                <th>Result</th>
            </thead>
            <tbody>

            <?php
            $sql = "";

            $sql = $sql . "select A.Description, C.OptDesc As Value  ";
            $sql = $sql . "from tblrating A ";
            $sql = $sql . "inner join tblvendorrating B On A.RatingCode = B.RatingCode And B.VdCode = '".$_SESSION['vdcode']."' ";
            $sql = $sql . "inner join tbloption C On B.RatingVendorAssesmentCode = C.OptCode And C.OptCat = 'RatingVendorAssesment' ";
            $sql = $sql . "Where A.ActInd = 'Y' ";
            $sql = $sql . "Order By A.RatingCode; ";

            $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
            $nomor = 1;
            if(mysqli_num_rows($result) > 0)
            {
                while($res = mysqli_fetch_assoc($result))
                {
                    echo "<tr>";
                    echo "<td>" . $nomor++ . ". </td>";
                    echo "<td>" . $res['Description'] . "</td>";
                    echo "<td>" . $res['Value'] . "</td>";
                    echo "</tr>";
                }
            }
            else
            {
                echo "<tr>";
                echo "<td colspan='3'>No data. </td>";
                echo "</tr>";
            }

            ?>

            </tbody>
        </table>

    </div>
</div>
    

<?php

include "footer.php";
?>