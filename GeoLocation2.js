var LocLat = document.getElementById("LocLat");
var LocLng = document.getElementById("LocLng");
var LocCity = document.getElementById("LocCity");

function cekLokasi()
{
	if (navigator.geolocation)
	{
			navigator.geolocation.getCurrentPosition(showPosition, showError);
	}
	else
	{
			alert("Unfortunately, your browser doesn't support the Geolocation service.");
	}
}

function showPosition(position)
{
	var request = new XMLHttpRequest();

	var method = 'GET';
	var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+position.coords.longitude+'&sensor=true';
	var async = true;

	request.open(method, url, async);
	request.onreadystatechange = function(){
		if(request.readyState == 4 && request.status == 200){
			var data = JSON.parse(request.responseText);
			var address = data.results[1];
			var city = address.formatted_address;

			var splitCity = city.split(",");

			//view.innerHTML = "Latitude: " + position.coords.latitude +
			//" || Longitude: " + position.coords.longitude + " || City: " + splitCity[0];
      LocLat.value = position.coords.latitude;
      LocLng.value = position.coords.longitude;
      LocCity.value = splitCity[splitCity.length - 2];
		}
	};
	request.send();
}

function showError(error)
{
	switch(error.code)
	{
			case error.PERMISSION_DENIED:
					alert("Access to your location is denied.");
					break;
			case error.POSITION_UNAVAILABLE:
					alert("Your position is unavailable.");
					break;
			case error.TIMEOUT:
					alert("Your location request is timed out.");
					break;
			case error.UNKNOWN_ERROR:
					alert("An unknown error occurred.");
					break;
	}
}
