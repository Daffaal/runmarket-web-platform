<?php
    define('FTP_STATUS', 'enable');
    define('FTP_HOST', '46.17.173.6');
    define('FTP_USER', 'runsystem@runsystem.id');
    define('FTP_PASS', 'runsystemUSD1B');
    define('FTP_DESTINATION', '/runmarket/dev/');


    $host = FTP_HOST;
    $user = FTP_USER;
    $pass = FTP_PASS;

    $ftpConn;
    $login;
    $ftpPasv;

	if (FTP_STATUS === "enable") {
        try {
            $ftpConn = ftp_connect($host);
            $login = ftp_login($ftpConn,$user,$pass);
            $ftpPasv = ftp_pasv($ftpConn, true);

            if ((!$ftpConn) || (!$login)) {
                return "FTP connection has failed! Attempted to connect to ". $host.".";
            }

            if (!$ftpPasv) {
                return "Cannot switch to passive mode";
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    } else {
        return false;
    }

	 function upload_to_ftp_server($fileName, $source) {
		if ($GLOBALS['login']) {
			$ftpPut = ftp_put($GLOBALS['ftpConn'], FTP_DESTINATION.$fileName, $source, FTP_BINARY);
			if (!$ftpPut) {
				return "<script>alert('Error uploading ". $fileName .".');</script>";
			}

			return true;
		}
		
	}

	 function file_exist_on_ftp_server($fileName){
		$fileSize = ftp_size($GLOBALS['ftpConn'],FTP_DESTINATION.$fileName);
		if ($fileSize < 0) {
			return false;
		}
		
		return true;
	}

	 function delete_file_on_ftp_server($fileName){
		$deleteFile = ftp_delete($GLOBALS['ftpConn'],FTP_DESTINATION.$fileName);
		if (!$deleteFile) {
			return "Could not delete $fileName";
		}
		
		return true;
	}

	 function download_file_from_ftp_server($fileName, $directoryName = null){
		if (is_null($directoryName)) {
			$target_dir = "tmp/";
			if (!file_exists($target_dir)) {
			  mkdir($target_dir, 0777, true);
			}
		} else {
			$target_dir = "tmp/".$directoryName;
			if (!file_exists($target_dir)) {
			  mkdir($target_dir, 0777, true);
			}
		}

		$downloadFile = ftp_get($GLOBALS['ftpConn'], $target_dir.$fileName, FTP_DESTINATION.$fileName, FTP_BINARY);
		if (!$downloadFile) {
			return false;
		}
		
		return true;
	}

	 function closeFTP() {
		ftp_close($GLOBALS['ftpConn']);
	}
	 function download_from_ftp_server_to_local_server($request) {
        $fileName = $request["fileName"];
        if (file_exist_on_ftp_server($fileName)) {
            if (download_file_from_ftp_server($fileName)) {
                echo "File found";
                exit;
            }
        } else {
            echo "File not found"; 
            exit;
        }      
    }


