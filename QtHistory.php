<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

$host = FTP_HOST;
$user = FTP_USER;
$pass = FTP_PASS;

if (isset($_POST['file'])) {
    $sql2 = "";
    $FileLoc = $_POST['file'];
    $Filename = $_POST['name'];
    $DocNo = $_POST['docno'];
    $sql2 = $sql2 . "Delete From TblQtDtl2 ";
    $sql2 = $sql2 . "Where DNo2 = '" . $FileLoc . "' and DocNo='" . $DocNo . "'";

    if (mysqli_query($conn, $sql2)) {

        mysqli_commit($conn);
        if (FTP_STATUS === "enable") {
            try {
                $ftpConn = ftp_connect($host);
                $login = ftp_login($ftpConn, $user, $pass);
                $ftpPasv = ftp_pasv($ftpConn, true);

                if ((!$ftpConn) || (!$login)) {
                    echo "FTP connection has failed! Attempted to connect to " . $host . ".";
                }

                if (!$ftpPasv) {
                    echo "Cannot switch to passive mode";
                }

                if ($login) {

                    if (file_exist_on_ftp_server(basename($Filename), $ftpConn)) {
                        $deleteFile = ftp_delete($ftpConn, FTP_DESTINATION . basename($Filename));
                        if (!$deleteFile) {
                            return "Could not delete $fileName";
                        }

                        return true;
                    }
                }
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
        echo "<script type='text/javascript'>window.location='QtHistory.php'</script>";
    } else {
        echo "Error: " . $sql2 . "<br>" . mysqli_error($conn);
        mysqli_rollback($conn);
    }
}

function file_exist_on_ftp_server($fileName, $ftpConn)
{
    $fileSize = ftp_size($ftpConn, FTP_DESTINATION . $fileName);
    if ($fileSize < 0) {
        return false;
    }

    return true;
}

if (isset($_FILES['VdFile'])) {

    $imgFile = $_FILES['VdFile']['name'];
    $tmp_dir = $_FILES['VdFile']['tmp_name'];
    $imgSize = $_FILES['VdFile']['size'];
    $imgExt = array();
    $VdFilesLoc = array();
    $DNo2 = array();
    $i = 1;
    $upload_dir = "dist/pdf/";
    $valid_extension = array('pdf', 'png', 'jpg', 'jpeg');
    $mMaxFile = GetParameter("FileSizeMaxUploadFTPClient") * 1025 * 1024;

    $QtDocNo = $_POST['DocNo'];

    // generate dno2
    $q = ("select (Max(t.DNo2) + 1) DNo2
    from tblqtdtl2 t 
    inner join tblqthdr t2 on t2.DocNo = t.DocNo 
    where t2.DocNo = '" . $_POST['DocNo'] . "';
    ");

    $result = mysqli_query($conn, $q);
    $rows = GetValue($q);
    $generate = null;

    if (strlen($rows) == 0) $rows = "0";
    if ($rows == "0") {
        for ($i = 0; $i < count($imgFile); $i++) {
            $generate = str_pad(($rows + 1) + $i, 3, "0", STR_PAD_LEFT);
            array_push($DNo2, $generate);
        }
    } else {
        // array_push($DNo2, str_pad($rows + 1, 3, "0", STR_PAD_LEFT));
        for ($i = 0; $i < count($imgFile); $i++) {
            $generate = str_pad($rows + $i, 3, "0", STR_PAD_LEFT);
            array_push($DNo2, $generate);
        }
    }

    //separating extension
    for ($i = 0; $i < count($imgFile); $i++) {
        $ext = strtolower(pathinfo($imgFile[$i], PATHINFO_EXTENSION));
        array_push($imgExt, $ext);
    }

    //generate file name
    for ($i = 0; $i < count($imgExt); $i++) {

        $filesLoc = $_SESSION['vdcode'] . "_" . str_replace("/", "-", $QtDocNo) . "_Item-" . $DNo2[$i] . "_" . date("Ymdhms") . "." . $imgExt[$i];
        array_push($VdFilesLoc, $filesLoc);
    }

    // var_dump($imgFile);
    // echo "<br>";
    // var_dump($VdFilesLoc);
    // echo "<br>";
    // var_dump(basename($VdFilesLoc[0]));
    // echo "<br>";
    // var_dump($DNo2);
    //uploading and saving to db
    for ($i = 0; $i < count($imgFile); $i++) {

        if (in_array($imgExt[$i], $valid_extension)) // check kalau extensi nya valid
        {

            if ($imgSize[$i] == 0) {

                echo "<script type='text/javascript'>alert('Maximum document size is " . GetParameter("FileSizeMaxUploadFTPClient") . " MB.')</script>";
                echo "<script type='text/javascript'>window.location='QtHistory.php'</script>";
            } else {

                if ($imgSize[$i] < $mMaxFile) // if image size < 1025 KB
                {


                    // echo "<script type='text/javascript'>alert('".$tmp_dir[$i]." || ".$upload_dir.$VdFilesLoc[$i]."');</script>";
                    move_uploaded_file($tmp_dir[$i], $upload_dir . basename($VdFilesLoc[$i]));

                    $sql = "";

                    $sql = $sql . "Insert Into TblQtDtl2(DocNo, DNo, DNo2, FileLocation, CreateBy, CreateDt) ";
                    $sql = $sql . "Values('" . $QtDocNo . "', '001', '" . $DNo2[$i] . "', '" . $VdFilesLoc[$i] . "', 
                    '" . $_SESSION['vdcode'] . "', CurrentDateTime()); ";

                    //echo $sql;

                    if (mysqli_query($conn, $sql)) {
                        mysqli_commit($conn);
                        echo "<script type='text/javascript'>window.location='QtHistory.php'</script>";
                    } else {
                        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                        mysqli_rollback($conn);
                    }
                } else {

                    echo "<script type='text/javascript'>alert('Maximum document size is " . GetParameter("FileSizeMaxUploadFTPClient") . " MB.')</script>";
                    echo "<script type='text/javascript'>window.location='QtHistory.php'</script>";
                }
            }
        } else {
            echo "<script type='text/javascript'>alert('Only allowed PDF document.')</script>";
            echo "<script type='text/javascript'>window.location='QtHistory.php'</script>";
        }
    }


    echo "<br>";
    // foreach (glob("dist/pdf/*.*") as $filename){
    //     echo "<br>". basename($filename). "<br>";
    // }

    if (FTP_STATUS === "enable") {
        try {
            $ftpConn = ftp_connect($host);
            $login = ftp_login($ftpConn, $user, $pass);
            $ftpPasv = ftp_pasv($ftpConn, true);

            if ((!$ftpConn) || (!$login)) {
                echo "FTP connection has failed! Attempted to connect to " . $host . ".";
            }

            if (!$ftpPasv) {
                echo "Cannot switch to passive mode";
            }

            if ($login) {
                $ftpSuccess = false;
                for ($i = 0; $i < count($VdFilesLoc); $i++) {
                    if (!ftp_put($ftpConn, FTP_DESTINATION . $VdFilesLoc[$i], $upload_dir . $VdFilesLoc[$i], FTP_BINARY)) {
                        echo "<script>alert('Error uploading " . $VdFilesLoc[$i] . ".');</script>";
                    } else {
                        $ftpSuccess = true;
                    }
                }
                if ($ftpSuccess) {
                    echo "<script>alert('Success uploading file.');</script>";
                    ftp_close($ftpConn);
                }
            }
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}

$RFQDocNo = str_replace('-', '/', $_REQUEST['i']);

$sql = (" Select T.MaterialRequestDocNo DocNo, T.DocNo Quotation, T.EntName, T.ProcurementTypeDesc, T.StageName, 
        IF(WinCount != '0' || CloseCount != '0' || CancelCount != '0' , Concat(WinCount, ' Win/', CloseCount, ' Close/', CancelCount, ' Cancel'), 
            IF(T.ActInd != 'Y', 'Updated', 'Waiting')) StatusDesc
        From 
        (
            Select A.MaterialRequestDocNo, A.DocNo, H.EntName, Group_Concat(Distinct E.OptDesc) ProcurementTypeDesc,
            Group_Concat(Distinct D.StageName) StageName, IfNull(I.CancelCount, 0) CancelCount,
            IfNull(J.WinCount, 0) WinCount, IfNull(K.CloseCount, 0) CloseCount, L.ActInd
            From TblQtHdr A
            Inner Join TblMaterialRequestHdr B On A.MaterialRequestDocNo = B.DocNo AND B.DocNo = '".$RFQDocNo."'
                And A.VdCode = '".$_SESSION['vdcode']."'
            Inner Join TblRFQHdr C On A.MaterialRequestDocNo = C.MaterialRequestDocNo
                And Find_In_Set(C.SeqNo, A.RFQSeqNo)
            Inner Join TblRFQDtl3 D On A.MaterialRequestDocNo = D.MaterialRequestDocNo
                And Find_In_Set(D.SeqNo, A.RFQSeqNo)
                And Find_In_Set(D.DNo, A.RFQ3DNo)
            Inner Join TblOption E On C.ProcurementType = E.OptCode And E.OptCat = 'ProcurementType'
            Left Join TblSite F On B.SiteCode = F.SiteCode
            Left Join TblProfitCenter G On F.ProfitCenterCode = G.ProfitCenterCode
            Left Join TblEntity H On G.EntCode = H.EntCode
            Left Join
            (
                Select DocNo, Count(*) CancelCount
                From TblMaterialRequestDtl
                Where DocNo In
                (
                    Select Distinct MaterialRequestDocNo
                    From TblQtHdr
                    Where VdCode = '".$_SESSION['vdcode']."'
                    And MaterialRequestDocNo Is Not Null
                )
                And (CancelInd = 'Y' Or Status = 'C')
                Group By DocNo
            ) I On B.DocNo = I.DocNo
            Left Join
            (
                Select T3.QtDocNo, Count(*) WinCount
                From TblPOHdr T1
                Inner Join TblPODtl T2 On T1.DocNo = T2.DocNo
                    And T2.CancelInd = 'N'
                    And T1.VdCode = '".$_SESSION['vdcode']."'
                Inner Join TblPORequestDtl T3 On T2.PORequestDocNo = T3.DocNo
                    And T2.PORequestDNo = T3.DNo
                Group By T3.QtDocNo
            ) J On A.DocNo = J.QtDocNo
            Left Join
            (
                Select T3.MaterialRequestDocNo, Count(*) CloseCount
                From TblPOHdr T1
                Inner Join TblPODtl T2 On T1.DocNo = T2.DocNo
                    And T2.CancelInd = 'N'
                    And T1.VdCode != '".$_SESSION['vdcode']."'
                Inner Join TblPORequestDtl T3 On T2.PORequestDocNo = T3.DocNo
                    And T2.PORequestDNo = T3.DNo
                Group By T3.MaterialRequestDocNo
            ) K On B.DocNo = K.MaterialRequestDocNo
            inner join TblQtDtl L on A.DocNo = L.DocNo 
            Group By A.MaterialRequestDocNo, A.DocNo, H.EntName
        ) T ;
        ");

$rq = mysqli_query($conn, $sql) or die(mysqli_error($conn));
while ($rows = mysqli_fetch_assoc($rq)) {
    // $ptcode = $rows['PtCode'];
    // $dtcode = $rows['DTCode'];
}

?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Quotation History</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="QTHistory" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>RFQ#</th>
                    <th>Quotation#</th>
                    <th>Company (Entity)</th>
                    <th>Procurement Status</th>
                    <th>Stage</th>
                    <th>Status</th>                    
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $docno = array();

                $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                while ($res = mysqli_fetch_assoc($result)) {
                    echo "<tr>";
                    echo "<td>" . $res['DocNo'] . "</td>";
                    echo "<td>" . $res['Quotation'] . "</td>";
                    echo "<td>" . $res['EntName'] . "</td>";
                    echo "<td>" . $res['ProcurementTypeDesc'] . "</td>";
                    echo "<td>" . $res['StageName'] . "</td>";
                    echo "<td>" . $res['StatusDesc'] . "</td>";

                    echo "<td>";
                    echo ("<a href='#detail' data-toggle='modal'
                    data-id='" . $res['Quotation'] . "'
                    data-docno='" . $res['DocNo'] . "'                    
                    class='detailQt btn btn-info' style='margin-bottom: 5px;'>Detail</a>");
                    // echo "<button type='button' name='detail' class='btnAttach btn btn-md btn-info' 
                    // data-toggle='modal'
                    // data-target='#attachment'
                    // data-docno='" . $res['Quotation'] . "'
                    // data-dno='" . $res['DNo'] . "'
                    // style='margin-bottom: 5px;'> Attachment </button>";
                    echo '<input type="button" class="btnDelete btn btn-md btn-success" data-toggle="modal" data-target="#contactBuyer" value="Contact Buyer">';
                    echo "</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- modal for Detail -->
<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Quotation History</h4>
            </div>
            <div class="modal-body" id="QtHistoryEdit">
                <input type="hidden" id="DocNo" name="DocNo" />
                <input type="hidden" id="DNo" name="DNo" />
                <!-- <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;"> Request For Quotation</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control item" id="docno" name="docno" readonly />
                            </div>
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;">RFQ Date</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control item" id="docdt" name="docdt" readonly />
                            </div>
                        </div>
                        <div style="height:10px;"></div>
                        <div class="row">
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;">Total Amount</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control item" id="totalprice" name="totalprice" readonly />
                            </div>
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;">RFQ Expired Date</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control item" id="ExpiredDt" name="ExpiredDt" readonly />
                            </div>
                        </div>
                        <div style="height:10px;"></div>
                        <div class="row">
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;">Company(Enity)</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control item" id="ent" name="ent" readonly />
                            </div>
                        </div>
                        <div style="height:10px;"></div>
                        <div class="row">
                            <div class="col-md-2 col-xs-2">
                                <label class="control-label " style="position:relative; top:7px;">Date</label>
                            </div>
                            <div class="col-md-4 col-xs-4">
                                <input type="text" class="form-control item" id="docdt" name="docdt" readonly />
                            </div>
                        </div>
                        <div style="height:10px;"></div>
                        <div class="row">
                            <div class="modal-body" id="QtHistoryEdit"></div>
                        </div>
                    </div> -->
            </div>
        </div>
    </div>
</div>
<!-- modal for Contact Buyer -->
<div class="modal fade" id="contactBuyer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <form action="">
            <div class="modal-body" id="QTDetailHistory">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <span class="glyphicon glyphicon-comment"></span> To : Perum Perhutani
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-default btn-xs " data-dismiss="modal">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="panel-body" id="scrollbox">
                                    <ul>
                                        <li>
                                            <div>
                                                <center><strong>29 Juli 2021</strong></center>
                                                <hr>
                                                <ul class="chat">
                                                    <li class="left clearfix">
                                                        <div class="chat-body clearfix">
                                                            <div class="header">
                                                                <strong class="primary-font">Perum Perhutani</strong> <small class="pull-right text-muted">
                                                                    <span class="glyphicon glyphicon-time"></span>12 mins ago</small>
                                                            </div>
                                                            <div style="height: 30px;"></div>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                                                dolor, quis ullamcorper ligula sodales.
                                                            </p>
                                                        </div>
                                                        <hr>
                                                    </li>
                                                    <li class="right clearfix">
                                                        <div class="chat-body clearfix">
                                                            <div class="header">
                                                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                                                <strong class="pull-right primary-font">Vendor PT Logitech</strong>
                                                            </div>
                                                            <div style="height: 30px;"></div>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                                                dolor, quis ullamcorper ligula sodales.
                                                            </p>
                                                        </div>
                                                        <hr>
                                                    </li>
                                                    <li class="left clearfix">
                                                        <div class="chat-body clearfix">
                                                            <div class="header">
                                                                <strong class="primary-font">Perum Perhutani</strong> <small class="pull-right text-muted">
                                                                    <span class="glyphicon glyphicon-time"></span>12 mins ago</small>
                                                            </div>
                                                            <div style="height: 30px;"></div>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                                                dolor, quis ullamcorper ligula sodales.
                                                            </p>
                                                        </div>
                                                        <hr>
                                                    </li>
                                                    <li class="right clearfix">
                                                        <div class="chat-body clearfix">
                                                            <div class="header">
                                                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                                                <strong class="pull-right primary-font">Vendor PT Logitech</strong>
                                                            </div>
                                                            <div style="height: 30px;"></div>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                                                dolor, quis ullamcorper ligula sodales.
                                                            </p>
                                                        </div>
                                                        <hr>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li>
                                            <div>
                                                <center><strong>2 Agustus 2021</strong></center>
                                                <hr>
                                                <ul class="chat">
                                                    <li class="left clearfix">
                                                        <div class="chat-body clearfix">
                                                            <div class="header">
                                                                <strong class="primary-font">Perum Perhutani</strong> <small class="pull-right text-muted">
                                                                    <span class="glyphicon glyphicon-time"></span>12 mins ago</small>
                                                            </div>
                                                            <div style="height: 30px;"></div>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                                                dolor, quis ullamcorper ligula sodales.
                                                            </p>
                                                        </div>
                                                        <hr>
                                                    </li>
                                                    <li class="right clearfix">
                                                        <div class="chat-body clearfix">
                                                            <div class="header">
                                                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                                                <strong class="pull-right primary-font">Vendor PT Logitech</strong>
                                                            </div>
                                                            <div style="height: 30px;"></div>
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                                                dolor, quis ullamcorper ligula sodales.
                                                            </p>
                                                        </div>
                                                        <hr>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="panel-footer">
                                    <div class="input-group">
                                        <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." />
                                        <span class="input-group-btn">
                                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                                Send</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- modal for upload file -->
<div class="modal fade" id="attachment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="" method="POST">
                <div class="modal-body" id="QTDetailHistory">
                    <input type="hidden" id="DocNo" name="DocNo" />
                    <input type="hidden" id="DNo" name="DNo" />
                    <div class="container-fluid">
                        <div style="height:30px;"></div>
                        <div id="TblFile"></div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function() {
        $("#QTHistory").DataTable({
            responsive: true,
            autoWidth: true,
            scrollX: true,
            fixedColumns: {
                leftColumns: 1
            }
        });

        $("#tblrfq").DataTable({
            responsive: true,
            autoWidth: false,
            scrollX: true,
        });

        // $('.btnSave').click(function(){
        // 		var QTDocNo = $(this).attr("id");
        //   $('#QTDetailHistory').text("");

        // 		$.ajax({
        // 			url: 'UpdateQtMr.php',
        // 			method: 'post',
        // 			data: {DocNo:QTDocNo},
        //             error : function(data){
        //                 consolse.log("Error " + data)
        //             },
        // 			success:function(data){
        //                 console.log("Success  " + data);
        // 			}
        // 		});
        // 	});

    });
</script>
<script>
    $(function() {
        $("textarea[maxlength]").bind('input propertychange', function() {
            var maxLength = $(this).attr('maxlength');
            if ($(this).val().length > maxLength) {
                $(this).val($(this).val().substring(0, maxLength));
            }
        })
    });

    $(".detailQt").click(function() {
        var qt = $(this).data('id');
        var docno = $(this).data('docno');
        // var dno = $(this).data('dno');
        // var uom = $(this).data('uom');
        // var estPrice = $(this).data('estprice');
        // var item = $(this).data('item');
        // var expDt = $(this).data('expdate');
        // var uom = $(this).data('uom');
        // var qty = $(this).data('qty');
        // var ptcode = $(this).data('ptcode');
        // var dtcode = $(this).data('dtcode');
        // var remark = $(this).data('remark');
        // var totalprice = $(this).data('totalprice');
        // var ent = $(this).data('ent');
        // var docdt = $(this).data('docdt');

        // $("#qt").val(myBookId);
        // $("#docno").val(docno);
        // $("#DNo").val(dno);
        // $("#uom").val(uom);
        // $("#unitPrice").val(parseInt(estPrice));
        // $("#item").val(item);
        // $("#ExpiredDt").val(expDt);
        // $("#qty").val(qty);
        // $("#ptcode").val(ptcode);
        // $("#dtcode").val(dtcode);
        // $("#remark").val(remark);
        // $("#totalprice").val(totalprice);
        // $("#ent").val(ent);
        // $("#docdt").val(docdt);

        // $("#unitPrice").change(function() {

        //     var price = Number($(this).val());
        //     var qty = $("#qty").val();
        //     if (price == estPrice) {
        //         var total = Intl.NumberFormat("id-ID").format((parseInt(estPrice) * parseInt(qty))) + ",00";
        //         if (total != "NaN.00") {
        //             $("#TPrice").val(total);
        //         }
        //     } else {
        //         var total = Intl.NumberFormat("id-ID").format((parseInt(price) * parseInt(qty))) + ",00";
        //         if (total != "NaN.00") {
        //             $("#TPrice").val(total);
        //         }
        //     }
        // });

        // $("#qty").change(function() {

        //     var qty = Number($(this).val());
        //     var price = $("#unitPrice").val();
        //     if (price == estPrice) {
        //         var total = Intl.NumberFormat("id-ID").format((parseInt(estPrice) * parseInt(qty))) + ",00";
        //         if (total != "NaN.00") {
        //             $("#TPrice").val(total);
        //         }
        //     } else {
        //         var total = Intl.NumberFormat("id-ID").format((parseInt(price) * parseInt(qty))) + ",00";
        //         if (total != "NaN.00") {
        //             $("#TPrice").val(total);
        //         }
        //     }
        // });

        // function updatePrice(val) {
        //     $("#qty").val(val);
        //     $("#qty").trigger('change');
        //     $("#unitPrice").trigger('change')
        // }
        // updatePrice(parseInt(qty))

        $("#QtHistoryEdit").text("");
        $.ajax({
            url: 'QtHistoryDetail.php',
            method: 'post',
            data: {
                DocNo: docno,
                Qt: qt
            },
            error: function(data) {
                // consols.log("Error " + data)
            },
            success: function(data) {
                // console.log("Success  " + data);
                $('#QtHistoryEdit').html(data);
                $("#detail").modal();
            }
        })

        // $(".selectPtCode").select2();
        // $(".selectCurCode").select2();
        // $(".selectDTCode").select2();
        // $("#DteExpiredDt").DateTimePicker();
    });
</script>

<script>
    $(document).on("click", ".btnAttach", function() {
        var docno = $(this).data('docno');

        $.ajax({
            url: 'QtAttachment.php',
            method: 'post',
            data: {
                DocNo: docno,
            },
            error: function(data) {
                consols.log("Error " + data)
            },
            success: function(data) {
                console.log("Success  " + data);
                $('#TblFile').html(data);
                $("#attachment").modal();
            }
        });
    });
</script>
<?php
include "footer.php";
?>