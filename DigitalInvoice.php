<?php
include "auth.php";
include 'header.php';
include "StdMtd.php";

if(isset($_POST['submit']))
{
    $sqlU = "";

    $R1 = array("<p>", "</p>", "'");
    $R2 = array("", "", "\'");

    $mLueRecvVd = isset($_POST['LueRecvVd']) ? $_POST['LueRecvVd'] : "";
    $DocNo = GenerateDocNo(date("Ymd"), "DigitalInvoice", "TblDigitalInvoiceHdr");
	
	$sqlU = $sqlU . "Insert Into TblDigitalInvoiceHdr(DocNo, DocDt, CancelInd, RecvVdDocNo, CreateBy, CreateDt) ";
	$sqlU = $sqlU . "Values('".$DocNo."', '".date("Ymd")."', 'N', '".$mLueRecvVd."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";

	$mDNo = 0;
	for($i = 1; $i < 1000; $i++)
	{
		if(isset($_POST['LueDocType' . $i . '']))
		{
			if(!empty($_POST['LueDocType' . $i . '']))
			{
				$mBADocNumber = isset($_POST['BADocNumber' . $i . '']) ? str_replace($R1, $R2, $_POST['BADocNumber' . $i . '']) : "";
				$mBATaxInvDt = "";
				if(isset($_POST['BATaxInvDt' . $i . '']))
				{
					if(strlen($_POST['BATaxInvDt' . $i . '']) > 0)
					{
						$mBATaxInvDt = dateFormat($_POST['BATaxInvDt' . $i . '']);
					}
                }
                
                $mBAAmt = isset($_POST['BAAmt' . $i . '']) ? str_replace(",", "", trim($_POST['BAAmt' . $i . ''])) : "0";
                $mBATax = isset($_POST['BATax' . $i . '']) ? str_replace(",", "", trim($_POST['BATax' . $i . ''])) : "0";
				$mBALueDocInd = isset($_POST['LueDocInd' . $i . '']) ? $_POST['LueDocInd' . $i . ''] : "";
				$mBARemark = isset($_POST['BARemark' . $i . '']) ? str_replace($R1, $R2, $_POST['BARemark' . $i . '']) : "";
				
				$sqlU = $sqlU . "Insert Into TblDigitalInvoiceDtl(DocNo, DNo, DocType, DocNumber, DocInd, TaxInvDt, Amt, TaxAmt, Remark, CreateBy, CreateDt) ";
				$sqlU = $sqlU . "Values('".$DocNo."', '".substr('000' . ($mDNo + 1), -3)."', '".$_POST['LueDocType' . $i . '']."', '".$mBADocNumber."', '".$mBALueDocInd."', '".$mBATaxInvDt."', ".$mBAAmt.", ".$mBATax.", '".$mBARemark."', '".$_SESSION['vdusercode']."', CurrentDateTime()); ";

				$mDNo += 1;
			}
		}
		//else
		//{
		//	break;
		//}
	}

	$sqlU = $sqlU . "Update TblDigitalInvoiceDtl Set DocNumber = NULL Where DocNo = '".$DocNo."' And Length(DocNumber) <= 0; ";
	$sqlU = $sqlU . "Update TblDigitalInvoiceDtl Set TaxInvDt = NULL Where DocNo = '".$DocNo."' And Length(TaxInvDt) <= 0; ";
	$sqlU = $sqlU . "Update TblDigitalInvoiceDtl Set Remark = NULL Where DocNo = '".$DocNo."' And Length(Remark) <= 0; ";

	//echo $sqlU;
    $expSQL = explode(";", $sqlU);
	$countSQL = count($expSQL);
	$success = false;

	for($i = 0; $i < $countSQL - 1; $i++)
	{
		if (mysqli_query($conn, $expSQL[$i])) {
			$success = true;
		} else {
			echo "Error: " . $expSQL[$i] . "<br>" . mysqli_error($conn);
			$success = false;
			mysqli_rollback($conn);
			break;
		}
	}

	if($success)
	{
		mysqli_commit($conn);
		echo "<script type='text/javascript'>window.location='DigitalInvoiceShow.php?DocNo=".$DocNo."'</script>";
	}
}

?>

<a href="DigitalInvoiceFind.php"><button type="button" class="btn btn-info btn-flat"> <i class="fa fa-search"></i><strong> Find</strong></button></a>
<br /><br />
<form class="form-horizontal" name="insertVendor" action="" role="form" enctype="multipart/form-data" method="POST" onsubmit="return validate();">

<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title">Digital Invoice</h4>
            </div>
            <div class="box-body">

				<div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label mandatory-field">Received#</label>

                    <div class="col-sm-5">
                        <select class="form-control selectRecvVd" name="LueRecvVd" id="LueRecvVd" style="width: 100%;" required>
                            <option value=""></option>
                        <?php
                            $sql = "";

                            $sql = $sql . "Select Distinct A.DocNo As Col1, Concat(A.DocNo, ' - (', Date_Format(A1.DocDt, '%d/%b/%Y'), ') ', A2.WhsName) As Col2 ";
                            $sql = $sql . "From TblRecvVdDtl A ";
                            $sql = $sql . "Inner Join TblRecvVdHdr A1 On A.DocNo = A1.DocNo ";
                            $sql = $sql . "Inner Join TblWarehouse A2 On A1.WhsCode = A2.WhsCode ";
                            $sql = $sql . "Inner Join TblPODtl B On A.PODocNo = B.DocNo And A.PODNo = B.DNo ";
                            $sql = $sql . "    And A.CancelInd = 'N' And A.Status = 'A' ";
                            $sql = $sql . "Inner Join TblPOHdr C On A.PODocNo = C.DocNo And C.VdCode = '" . $_SESSION['vdcode'] . "' ";
                            $sql = $sql . "Where A.DocNo Not In ";
                            $sql = $sql . "( ";
                            $sql = $sql . "    Select Distinct T1.RecvVdDocNo ";
                            $sql = $sql . "    From TblPurchaseInvoiceDtl T1 ";
                            $sql = $sql . "    Inner Join TblPurchaseInvoiceHdr T2 On T1.DocNo = T2.DocNo And T2.CancelInd = 'N' ";
                            $sql = $sql . ") ";
                            $sql = $sql . "And A.DocNo Not In ";
                            $sql = $sql . "( ";
                            $sql = $sql . "    Select Distinct X1.RecvVdDocNo ";
                            $sql = $sql . "    From TblDigitalInvoiceHdr X1 ";
                            $sql = $sql . "    Where X1.CancelInd = 'N' ";
                            $sql = $sql . ") ";
                            $sql = $sql . "Order By A1.DocDt, A1.DocNo, A2.WhsName ";
                            $sql = $sql . "; ";

                            $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));

                            while($res = mysqli_fetch_assoc($result))
                            {
                                echo "<option value='".$res['Col1']."'>".$res['Col2']."</option>";
                            }
                        ?>
                        </select>
                    </div>

                    <div id="ShowPO"></div>
                    <div id="resultA"></div>

                </div>

			</div>
		</div>
	</div> <!-- /.col-md-12 -->
</div> <!-- /.row -->

<div class="row">
	<div class="col-xs-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#DocInfo" data-toggle="tab">Document Information</a></li>
			</ul>
			<div class="tab-content">
				
				<div class="active tab-pane table-responsive" id="DocInfo">
                    <span id="eTblDocInfo" class="error-message"></span>
					<table class="table DocInfo" id="tblDocInfo">
						<thead>
							<tr>
								<th width="15%">Type</th>
								<th>Document#</th>
								<th width="9%">Tax Invoice Date</th>
								<th>Amount</th>
                                <th>Tax</th>
                                <th width="10%">Indicator</th>
                                <th>Remark</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="7" style="text-align: left;">
									<input type="button" class="btn btn-lg btn-block " id="BAaddrow" value="Add Row" />
								</td>
							</tr>
							<tr>
							</tr>
						</tfoot>
					</table>
				</div>
			
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- /.nav-tabs-custom -->

		<hr />

        <input type="reset" class="btn btn-warning" onclick="return confirmCancel();" value="Cancel" id="BtnCancel"  />
        <button type="submit" name="submit" class="btn btn-success pull-right" id="BtnSave" >Save</button>

	</div>
</div>

</form>

<script>
  $(function () {
    //Initialize Select2 Elements
	$(".selectRecvVd").select2();
  });
</script>

<script type="text/javascript">
$(document).ready(function () {
	var counterBA = 1;

	$("#BAaddrow").on("click", function () {
		var newRow = $("<tr>");
		var cols = "";

		cols += '<td><select class="form-control selectDocType' + counterBA + '" name="LueDocType' + counterBA + '" id="LueDocType' + counterBA + '" style="width: 100%;" required>';
		cols += '<option value=""></option>';
		<?php
			$sqlB = "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='PurchaseInvoiceDocType' Order By OptDesc ";
			$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
			while($resB = mysqli_fetch_assoc($resultB))
			{
			?>
				cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
			<?php
			}
		?>
		cols += '</select></td>';
		
        cols += '<td><input type="text" class="form-control" name="BADocNumber' + counterBA + '"/></td>';
		cols += '<td><input type="text" class="form-control datepicker" name="BATaxInvDt' + counterBA + '" id="BATaxInvDt' + counterBA + '" /></td>';
		cols += '<td><input type="text" class="form-control angka numberOnly' + counterBA + '" name="BAAmt' + counterBA + '" value="0" /></td>';
        cols += '<td><input type="text" autocomplete="off" class="form-control angka TnumberOnly' + counterBA + '" name="BATax' + counterBA + '" value="0" /></td>';
        
        cols += '<td><select class="form-control selectDocInd' + counterBA + '" name="LueDocInd' + counterBA + '" id="LueDocInd' + counterBA + '" style="width: 100%;" required>';
		cols += '<option value=""></option>';
		<?php
			$sqlB = "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='PurchaseInvoiceDocInd' Order By OptDesc ";
			$resultB = mysqli_query($conn, $sqlB) or die (mysqli_error($conn));
			while($resB = mysqli_fetch_assoc($resultB))
			{
			?>
				cols += '<option value="<?php echo $resB['Col1']; ?>"><?php echo str_replace("'", "\'", $resB['Col2']); ?></option>';
			<?php
			}
		?>
		cols += '</select></td>';
        
        cols += '<td><input type="text" class="form-control" name="BARemark' + counterBA + '"/></td>';

		cols += '<td><input type="button" class="BAbtnDel btn btn-md btn-danger" value="Delete"></td>';
		newRow.append(cols);
		$("table.DocInfo").append(newRow);

		for(var j = 1; j <= counterBA; j++)
        {
            $(".selectDocType" + j + "").select2();
            $(".selectDocInd" + j + "").select2();
            $("#BATaxInvDt" + j + "").datepicker({
                autoclose: true
            });

            var mAmt = new Cleave('.numberOnly' + j + '',
            {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            });

            var mTaxAmt = new Cleave('.TnumberOnly' + j + '',
            {
                numeral: true,
                numeralThousandsGroupStyle: 'thousand'
            });
        }

		counterBA++;
	});

	$("table.DocInfo").on("click", ".BAbtnDel", function (event) {
		$(this).closest("tr").remove();       
		counterBA -= 1
	});

});
</script>

<script type="text/javascript" src="DigitalInvoice.js"></script>

<?php
include "footer.php";
?>