<?php 

    include "conn.php";
    include "auth.php";
    include "StdMtd.php";

    if(isset($_POST['Qt']) && isset($_POST['Po'])){

        $QtDocNo = $_POST['Qt'];
        $PoDocNo = $_POST['Po'];

        $sql = ("Select B.QtDocNo, D.ItName, A.Qty, D.PurchaseUomCode UOM, E.UPrice as EstPrice, F.CurCode, G.PtName, (A.Qty * E.UPrice) as Total
        From TblPODtl A
        Inner Join TblPORequestDtl B On A.PORequestDocNo = B.DocNo And A.PORequestDNo = B.DNo
            And A.DocNo = '".$PoDocNo."'
            And B.QtDocNo = '".$QtDocNo."'
        Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo
        Inner Join TblItem D On C.ItCode = D.ItCode
        Inner Join TblQtDtl E On B.QtDocNo = E.DocNo And B.QtDNo = E.DNo
        Inner Join TblQtHdr F On E.DocNo = F.DocNo
        Left Join TblPaymentTerm G On F.PtCode = G.PtCode
        group by B.QtDocNo, D.ItName, A.Qty, D.PurchaseUOMCode, E.UPrice, F.CurCode, G.PtName
        ;");

        $result = mysqli_query($conn, $sql) or mysqli_error($conn);
                                    
        echo    "<table class='table FileDtl' id=''>";
        echo    "    <thead>";
        echo    "        <tr>";
        echo    "            <th>Quotation</th>";
        echo    "            <th>Item</th>";
        echo    "            <th>Quantity</th>";
        echo    "            <th>UoM</th>";
        echo    "            <th>Unit Price</th>"; 
        echo    "            <th>Currency</th>"; 
        echo    "            <th>Term of Payment</th>"; 
        echo    "            <th>Total Item Price</th>"; 
        echo    "        </tr>";
        echo    "    </thead>";

        while ($res = mysqli_fetch_assoc($result)) {
            echo "<tbody>";
            echo "<tr>";
            echo "<td>" . $res['QtDocNo'] . "</td>";
            echo "<td>" . $res['ItName'] . "</td>";
            echo "<td>" . number_format($res['Qty'], 2, ',', '.') . "</td>";
            echo "<td>" . $res['UOM'] . "</td>";
            echo "<td>" . number_format($res['EstPrice'], 2, ',', '.') . "</td>";
            echo "<td>" . $res['CurCode'] . "</td>";
            echo "<td>" . $res['PtName'] . "</td>";
            echo "<td align='right'>" . number_format($res['Total'], 2, ',', '.') . "</td>";
            echo "</tr>";
            echo "</tbody>";
            echo "</table>";
            

        }
    }

?>