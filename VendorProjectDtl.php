<?php 
include "auth.php";
include "conn.php";
include "StdMtd.php";

$pathFile = 'dist/pdf/';
if(isset($_POST['dno'])) {

    $sql = "";
    $sql = $sql . "Select A.*, Date_format(A.ContractDt, '%d-%m-%Y') ContractDt, Date_format(A.ContractEndDt, '%d-%m-%Y') ContractEndDt , C.CurName, D.OptDesc As ProgressName ";
    $sql = $sql . "From TblVendorProject A ";
    // $sql = $sql . "Inner Join TblVendorProfile B On A.VdCode = B.VdCode ";
    $sql = $sql . "Left Join TblCurrency C On A.CurCode = C.CurCode ";
    $sql = $sql . "Left Join TblOption D On A.ProgressCode = D.OptCode And D.OptCat = 'VendorProgress' ";
    $sql = $sql . "Where A.VdCode = '".$_SESSION['vdcode']."' and DNo = '". $_POST['dno'] ."'";

    $sql1 = ("select t.FileLocation , date_format(t.EndDt, '%d-%m-%Y') EndDt
        from TblVendorProjectUpload t 
        where t.DNo='".$_POST['dno']."' and t.VdCode = '".$_SESSION['vdcode']."' 
        ;");

    $q = "Select CurCode As Col1, CurName As Col2 From TblCurrency Order By CurName;";

    $q1 = "Select OptCode As Col1, OptDesc As Col2 ";
    $q1 = $q1 . "From TblOption ";
    $q1 = $q1 . "Where OptCat = 'VendorProgress' ";
    $q1 = $q1 . "Order By OptDesc; ";

    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

    while ($dtl = mysqli_fetch_assoc($result)) {

    echo ("<form method='POST' action='VendorProject.php' id='formEdit' enctype='multipart/form-data' onsubmit='return validate();'>
    <div class='modal-body'>
        <div class='container-fluid'>                        

                <input type='hidden' class='form-control id' id='name' name='DNo' value='" . $dtl['DNo'] . "'/>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Project Experience</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control Project' id='Project' name='Project' value='" . $dtl['Project'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>            
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Project Category</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control ProjectCategory' id='ProjectCategory' name='ProjectCategory' value='" . $dtl['ProjectCategory'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Contact</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control ContractNumber' id='ContractNumber' name='ContractNumber' value='" . $dtl['ContractNumber'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>                                        
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Contract Date</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control ContractDt' id='ContractDt' name='ContractDt' value='" . $dtl['ContractDt'] . "' data-field='date' required/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Owner</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control Owner' id='Owner' name='Owner' value='" . $dtl['Owner'] . "'/>
                    </div>
                    <div id='DteBirthDt'></div>
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Owner Address</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control OwnerAddress' id='OwnerAddress' name='OwnerAddress' value='" . $dtl['OwnerAddress'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Project Location</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control ProjectLocation' id='ProjectLocation' name='ProjectLocation' value='" . $dtl['ProjectLocation'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Results</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control Results' id='Results' name='Results' value='" . $dtl['Results'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Currency</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <select class='form-control selectCurCode' name='CurCode' id='CurCode' style='width: 100%;'>
                            <option value=''></option>");

                            $result3 = mysqli_query($conn, $q);
                            while ($res = mysqli_fetch_assoc($result3)) {

                                if($dtl['CurCode'] == $res['Col1']){
                                    echo "<option value='".$res['Col1']."' selected";
                                }else {
                                    echo "<option value='".$res['Col1']."' ";
                                }
                                echo ">".$res['Col2']."</option>";
                            }
echo ("                  </select>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Project Value</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' autocomplete='off' id='numberOnly' class='form-control angka' name='ProjectValue' id='ProjectValue' value='" . number_format($dtl['ProjectValue'], 0, '', '') . "' />
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>End Of Contract</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control ContractEndDt' id='ContractEndDt' name='ContractEndDt' value='" . $dtl['ContractEndDt'] . "' data-field='date' required/>
                    </div>
                    
                </div>
                
                <div style='height:10px;'></div>

                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Work Achievement</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                        <input type='text' class='form-control WorkAchievement' id='WorkAchievement' name='WorkAchievement' value='" . $dtl['WorkAchievement'] . "'/>
                    </div>
                    
                </div>
                <div style='height:10px;'></div>
                <div class='row'>
                    <div class='col-md-3 col-xs-3'>
                        <label class='control-label ' style='position:relative; top:7px;'>Progress</label>
                    </div>
                    <div class='col-md-9 col-xs-9'>
                    <select class='form-control selectProgress' name='ProgressName' id='ProgressName' style='width: 100%;'>
                        <option value=''></option> ");

                            $result4 = mysqli_query($conn, $q1);
                            while ($res = mysqli_fetch_assoc($result4)) {

                                if($dtl['ProgressName'] == $res['Col2']){
                                    echo "<option value='".$res['Col1']."' selected";                                    
                                }else {
                                    echo "<option value='".$res['Col1']."' ";                                   
                                }
                                echo ">".$res['Col2']."</option>";                                

                            }

echo ("             </select>
                    </div>
                    
                </div>                
                <div style='height:30px;'></div>");
                $result1 = mysqli_query($conn, $sql1) or die(mysqli_error($conn));

                echo    "<table class='table FileDtl' id='FileDtl'>";
                echo    "    <thead>";
                echo    "        <tr>";
                echo    "            <th>File</th>";
                echo    "            <th>Valid To</th>";
                echo    "            <th>#</th>";   
                echo    "        </tr>";
                echo    "    </thead>";
            
                while($res = mysqli_fetch_assoc($result1))
                {        
            
                echo    "    <tbody>";
                echo    "       <tr>";
                echo    "        <td>". basename($res['FileLocation']) ."</td>";
                echo    "        <td>" . $res['EndDt'] ."</td>";
                echo    "        <td>";
                echo    "           <a href='".$pathFile.basename($res['FileLocation'])."' target='_blank'><button type='button' class='btn-xs btn-info'> Preview </button></a>";
                echo    "           <button type='button' id='". $res['FileLocation'] ."' name='deleteFile' class='delete btn-xs btn-danger'> Delete </button>";
                echo    "        </td>";
                echo    "       </tr>";
                echo    "        <input type='hidden' name='countVPUpload' id='countVPUpload'/>";
                echo    "    </tbody>";    
                }
            
                echo    "    <tfoot>";
                echo    "        <tr>";
                echo    "            <td colspan='5' style='text-align: left;'>";
                echo    "                <input type='button' class='btn btn-lg btn-block' id='addFile' value='Add Row'  />";
                echo    "            </td>";
                echo    "        </tr>";
                echo    "        <tr>";
                echo    "        </tr>";
                echo    "    </tfoot>";
                echo    "</table>";    
echo ("         
                <input type='button' class='btn' data-dismiss='modal' value='Cancel' id='BtnCancel' style='background-color: #58585A; color:white'/>                
                <button type='submit' name='edit' class='edit btn pull-right' id='BtnSave' style='background-color: #D61E2F; color:white'>Save</button>        
                </div>                            
        </div> 
    </div>                
</form>");    
    
    }
}

?>

<script>
$(function(){              
        
    $('.delete').click(function (){
            var fileLoc = $(this).attr('id');
  
            if(confirm('Are you sure want to delete ?')){
                $.ajax({
                url: 'VendorProject.php',
                method: 'post',
                data: {file:fileLoc},
                error: function(err) {
                    console.log('Error ' + err);
                },
                success:function(data){
                    console.log('Success  ' + data);
                    window.location='VendorProject.php'                    
                }
            });
            }
        });      

        $(".selectCurCode").select2();
        $(".selectProgress").select2();
        $(".selectGender").select2();
        $("#ContractEndDt").DateTimePicker();
        $("#DteValidTo").DateTimePicker();
    });
</script>

<script>
$(document).ready(function () {
	// var countVdBankAccount = document.getElementById("countVPUpload").value;
	var counterBA = 5;

	$("#addFile").on("click", function () {
		var newRow = $("<tr>");
		var cols = "";

		cols += '<td><input type="file" name="VdFileDtl[]" id="VPFile" accept="application/pdf, image/png, image/jpg, image/jpeg" required/></td>';
		cols += '<td><div class="form-group">';                              
        cols +=         '<div class="">'
        cols +=            '<div class="input-group date">'
        cols +=                 '<div class="input-group-addon">'
        cols +=                     '<i class="fa fa-calendar"></i>'
        cols +=                 '</div>'
        cols +=                '<input type="text" class="form-control validto" id="validto" name="ValidToDtl[]" data-field="date" required/>'
        cols +=             '</div>'
        cols +=         '</div>'
        cols +=         '<div id="DteValidTo"></div>'
        cols +=    '</div>'
        cols +=  '</td>';
		cols += '<td>';
        cols += '<input type="button" class="BAbtnDel btn btn-md btn-danger" value="Delete"><br>';
        
        cols += '</td>';
		newRow.append(cols);
		$("table.FileDtl").append(newRow);

		for(var j = 0; j <= counterBA; j++)
			$(".selectBankCode" + j + "").select2();

		counterBA++;
	});

	$("table.FileDtl").on("click", ".BAbtnDel", function (event) {
		$(this).closest("tr").remove();       
		counterBA -= 1
	});
});
</script>
<script>
$('#numberOnly').on('keydown', function(e){
	
    if(this.selectionStart || this.selectionStart == 0){
        // selectionStart won't work in IE < 9
        
        var key = e.which;
        var prevDefault = true;
        
        var thouSep = " ";  // your seperator for thousands
        var deciSep = ",";  // your seperator for decimals
        var deciNumber = 0; // how many numbers after the comma
        
        var thouReg = new RegExp(thouSep,"g");
        var deciReg = new RegExp(deciSep,"g");
        
        function spaceCaretPos(val, cPos){
            /// get the right caret position without the spaces
            
            if(cPos > 0 && val.substring((cPos-1),cPos) == thouSep)
            cPos = cPos-1;
            
            if(val.substring(0,cPos).indexOf(thouSep) >= 0){
            cPos = cPos - val.substring(0,cPos).match(thouReg).length;
            }
            
            return cPos;
        }
        
        function spaceFormat(val, pos){
            /// add spaces for thousands
            
            if(val.indexOf(deciSep) >= 0){
                var comPos = val.indexOf(deciSep);
                var int = val.substring(0,comPos);
                var dec = val.substring(comPos);
            } else{
                var int = val;
                var dec = "";
            }
            var ret = [val, pos];
            
            if(int.length > 3){
                
                var newInt = "";
                var spaceIndex = int.length;
                
                while(spaceIndex > 3){
                    spaceIndex = spaceIndex - 3;
                    newInt = thouSep+int.substring(spaceIndex,spaceIndex+3)+newInt;
                    if(pos > spaceIndex) pos++;
                }
                ret = [int.substring(0,spaceIndex) + newInt + dec, pos];
            }
            return ret;
        }
        
        $(this).on('keyup', function(ev){
            
            if(ev.which == 8){
                // reformat the thousands after backspace keyup
                
                var value = this.value;
                var caretPos = this.selectionStart;
                
                caretPos = spaceCaretPos(value, caretPos);
                value = value.replace(thouReg, '');
                
                var newValues = spaceFormat(value, caretPos);
                this.value = newValues[0];
                this.selectionStart = newValues[1];
                this.selectionEnd   = newValues[1];
            }
        });
        
        if((e.ctrlKey && (key == 65 || key == 67 || key == 86 || key == 88 || key == 89 || key == 90)) ||
            (e.shiftKey && key == 9)) // You don't want to disable your shortcuts!
            prevDefault = false;
        
        if((key < 37 || key > 40) && key != 8 && key != 9 && prevDefault){
            e.preventDefault();
            
            if(!e.altKey && !e.shiftKey && !e.ctrlKey){
            
                var value = this.value;
                if((key > 95 && key < 106)||(key > 47 && key < 58) ||
                (deciNumber > 0 && (key == 110 || key == 188 || key == 190))){
                    
                    var keys = { // reformat the keyCode
                    48: 0, 49: 1, 50: 2, 51: 3,  52: 4,  53: 5,  54: 6,  55: 7,  56: 8,  57: 9,
                    96: 0, 97: 1, 98: 2, 99: 3, 100: 4, 101: 5, 102: 6, 103: 7, 104: 8, 105: 9,
                    110: deciSep, 188: deciSep, 190: deciSep
                    };
                    
                    var caretPos = this.selectionStart;
                    var caretEnd = this.selectionEnd;
                    
                    if(caretPos != caretEnd) // remove selected text
                    value = value.substring(0,caretPos) + value.substring(caretEnd);
                    
                    caretPos = spaceCaretPos(value, caretPos);
                    
                    value = value.replace(thouReg, '');
                    
                    var before = value.substring(0,caretPos);
                    var after = value.substring(caretPos);
                    var newPos = caretPos+1;
                    
                    if(keys[key] == deciSep && value.indexOf(deciSep) >= 0){
                        if(before.indexOf(deciSep) >= 0){ newPos--; }
                        before = before.replace(deciReg, '');
                        after = after.replace(deciReg, '');
                    }
                    var newValue = before + keys[key] + after;
                    
                    if(newValue.substring(0,1) == deciSep){
                        newValue = "0"+newValue;
                        newPos++;
                    }
                    
                    while(newValue.length > 1 && 
                    newValue.substring(0,1) == "0" && newValue.substring(1,2) != deciSep){
                        newValue = newValue.substring(1);
                        newPos--;
                    }
                    
                    if(newValue.indexOf(deciSep) >= 0){
                        var newLength = newValue.indexOf(deciSep)+deciNumber+1;
                        if(newValue.length > newLength){
                        newValue = newValue.substring(0,newLength);
                        }
                    }
                    
                    newValues = spaceFormat(newValue, newPos);
                    
                    this.value = newValues[0];
                    this.selectionStart = newValues[1];
                    this.selectionEnd   = newValues[1];
                }
            }
        }
        
        $(this).on('blur', function(e){
            
            if(deciNumber > 0){
                var value = this.value;
                
                var noDec = "";
                for(var i = 0; i < deciNumber; i++)
                noDec += "0";
                
                if(value == "0"+deciSep+noDec)
                this.value = ""; //<-- put your default value here
                else
                if(value.length > 0){
                    if(value.indexOf(deciSep) >= 0){
                        var newLength = value.indexOf(deciSep)+deciNumber+1;
                        if(value.length < newLength){
                        while(value.length < newLength){ value = value+"0"; }
                        this.value = value.substring(0,newLength);
                        }
                    }
                    else this.value = value + deciSep + noDec;
                }
            }
        });
    }
});
</script>
