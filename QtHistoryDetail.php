<?php
include "auth.php";
include "conn.php";
include "StdMtd.php";

if (isset($_POST['DocNo'])) {

    $DocNo = $_POST['DocNo'];

    $Qt = $_POST['Qt'];

    $sql = "SELECT
            A.MATERIALREQUESTDOCNO DocNo,  Date_Format(B.DocDt , '%d-%m-%Y') RFQDate, Date_Format(B.ExpDt , '%d-%m-%Y') RFQExpDt ,
            A.DOCNO Quotation,  Date_Format(A.DocDt , '%d-%m-%Y') DocDt, Date_Format(A.ExpiredDt , '%d-%m-%Y') ExpiredDt,
            H.EntName, I.DNo ,
            GROUP_CONCAT(DISTINCT E.OPTDESC) PROCUREMENTTYPEDESC,
            GROUP_CONCAT(DISTINCT D.STAGENAME) STAGENAME,
            M.TotalAmount, A.Remark , I.UPrice,
            IF(IFNULL(N.CancelCount, '') != '', 'Cancel',
                if(IFNULL(O.WinCount, '') != '', 'Win', 
                    if(IFNULL(P.CloseCount, '') != '', 'Close', 'Waiting'))) Status
        FROM
            TBLQTHDR A
        INNER JOIN TBLMATERIALREQUESTHDR B ON
            A.MATERIALREQUESTDOCNO = B.DOCNO
            AND A.VDCODE = '".$_SESSION['vdcode']."'
        INNER JOIN TBLRFQHDR C ON
            A.MATERIALREQUESTDOCNO = C.MATERIALREQUESTDOCNO
            AND FIND_IN_SET(C.SEQNO, A.RFQSEQNO)
        INNER JOIN TBLRFQDTL3 D ON
            A.MATERIALREQUESTDOCNO = D.MATERIALREQUESTDOCNO
            AND FIND_IN_SET(D.SEQNO, A.RFQSEQNO)
            AND FIND_IN_SET(D.DNO, A.RFQ3DNO)
        INNER JOIN TBLOPTION E ON
            C.PROCUREMENTTYPE = E.OPTCODE
            AND E.OPTCAT = 'PROCUREMENTTYPE'
        LEFT JOIN TBLSITE F ON
            B.SITECODE = F.SITECODE
        LEFT JOIN TBLPROFITCENTER G ON
            F.PROFITCENTERCODE = G.PROFITCENTERCODE
        LEFT JOIN TBLENTITY H ON
            G.ENTCODE = H.ENTCODE
        INNER JOIN TBLQTDTL I ON A.DOCNO = I.DOCNO 
        INNER JOIN TBLPAYMENTTERM J ON A.PTCODE = J.PTCODE 
        INNER JOIN TBLMATERIALREQUESTDTL K ON B.DOCNO = K.DOCNO And I.MaterialRequestDNo = K.DNo
        INNER JOIN TBLITEM L ON K.ITCODE = L.ITCODE 
        INNER JOIN (
            SELECT DOCNO, SUM(UPRICE * QTY) TOTALAMOUNT
            FROM TBLQTDTL
            GROUP BY DOCNO
        ) M ON I.DOCNO = M.DOCNO
        Left Join
            (
                Select DocNo, Count(*) CancelCount
                From TblMaterialRequestDtl
                Where DocNo In
                (
                    Select Distinct MaterialRequestDocNo
                    From TblQtHdr
                    Where VdCode = '".$_SESSION['vdcode']."'
                    And MaterialRequestDocNo Is Not Null
                )
                And (CancelInd = 'Y' Or Status = 'C')
                Group By DocNo
            ) N On B.DocNo = N.DocNo
            Left Join
            (
                Select T3.QtDocNo, Count(*) WinCount
                From TblPOHdr T1
                Inner Join TblPODtl T2 On T1.DocNo = T2.DocNo
                    And T2.CancelInd = 'N'
                    And T1.VdCode = '".$_SESSION['vdcode']."'
                Inner Join TblPORequestDtl T3 On T2.PORequestDocNo = T3.DocNo
                    And T2.PORequestDNo = T3.DNo
                Group By T3.QtDocNo
            ) O On A.DocNo = O.QtDocNo
            Left Join
            (
                Select T3.MaterialRequestDocNo, Count(*) CloseCount
                From TblPOHdr T1
                Inner Join TblPODtl T2 On T1.DocNo = T2.DocNo
                    And T2.CancelInd = 'N'
                    And T1.VdCode != '".$_SESSION['vdcode']."'
                Inner Join TblPORequestDtl T3 On T2.PORequestDocNo = T3.DocNo
                    And T2.PORequestDNo = T3.DNo
                Group By T3.MaterialRequestDocNo
            ) P On B.DocNo = P.MaterialRequestDocNo
        WHERE B.DOCNO = '".$DocNo."' AND I.DOCNO = '".$Qt."'
        GROUP by K.DNo;
        ";
    $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
    while ($res = mysqli_fetch_array($result)) {
?>

        <form method="post" action="QtHistoryEdit.php" enctype="multipart/form-data">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;"> Request For Quotation</label>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <input type="text" class="form-control item" name="DocNo" value="<?= $res['DocNo'] ?>" readonly />
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Quotation</label>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <input type="text" class="form-control item" name="Qt" value="<?= $res['Quotation'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Total Amount</label>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <input type="text" style="text-align: right;" class="form-control item" value="<?= number_format($res['TotalAmount'], 2, ",", ".") ?>" readonly />
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">RFQ Date</label>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <input type="text" class="form-control item" value="<?= $res['RFQDate'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Company(Enity)</label>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <input type="text" class="form-control item" value="<?= $res['EntName'] ?>" readonly />
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">RFQ Expired Date</label>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <input type="text" class="form-control item" value="<?= $res['RFQExpDt'] ?>" readonly />
                    </div>
                </div>
                <div style="height:10px;"></div>
                <div class="row">
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Date</label>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <input type="text" class="form-control item" value="<?= $res['DocDt'] ?>" readonly />
                    </div>
                    <div class="col-md-2 col-xs-2">
                        <label class="control-label " style="position:relative; top:7px;">Remark</label>
                    </div>
                    <div class="col-md-4 col-xs-4">
                        <input type="text" class="form-control item" value="<?= $res['Remark'] ?>" readonly />
                    </div>
                </div>
                <div style="height:30px;"></div>                
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- <style>table-layout: fixed; word-wrap:break-word;</style> -->
                    <table class="table table-bordered table-striped" id="tblQtHistoryEdit">
                        <thead>
                            <tr>
                                <!-- <th><input type="checkbox" id="checkAll"></th> -->
                                <th>Item</th>
                                <th>Quantity</th>
                                <th>UOM</th>
                                <th>Unit Price</th>
                                <th>Currency</th>
                                <th>Term of Payment</th>
                                <th>Delivery Type</th>
                                <th>End Date</th>
                                <th>Total Price</th>
                                <th>Status</th>
                                <th>Remark</th>
                            </tr>
                        </thead>
                        <?php
                        $sql = "SELECT
                                A.MATERIALREQUESTDOCNO DocNo, Date_Format(B.DocDt, '%d-%m-%Y') DocDt, Date_Format(B.ExpDt , '%d-%m-%Y') ExpiredDt,
                                A.DOCNO Quotation,  Date_Format(A.DocDt , '%d-%m-%Y') DocDt, Date_Format(A.ExpiredDt , '%d-%m-%Y') ExpiredDt,
                                H.EntName, I.DNo ,
                                GROUP_CONCAT(DISTINCT E.OPTDESC) PROCUREMENTTYPEDESC,
                                GROUP_CONCAT(DISTINCT D.STAGENAME) STAGENAME,
                                L.ItName , I.Qty , L.PurchaseUOMCode Uom, I.Amt TotalPrice, M.TotalAmount, A.Remark , I.UPrice, K.CurCode ,J.PtName, T.DTName,
                                
                                IF(IFNULL(N.CancelCount, '') != '', 'Cancel',
                                    if(IFNULL(O.WinCount, '') != '', 'Win', 
                                        if(IFNULL(P.CloseCount, '') != '', 'Close', 'Waiting'))) Status, I.ActInd
                            FROM
                                TBLQTHDR A
                            INNER JOIN TBLMATERIALREQUESTHDR B ON
                                A.MATERIALREQUESTDOCNO = B.DOCNO
                                AND A.VDCODE = '".$_SESSION['vdcode']."'
                            INNER JOIN TBLRFQHDR C ON
                                A.MATERIALREQUESTDOCNO = C.MATERIALREQUESTDOCNO
                                AND FIND_IN_SET(C.SEQNO, A.RFQSEQNO)
                            INNER JOIN TBLRFQDTL3 D ON
                                A.MATERIALREQUESTDOCNO = D.MATERIALREQUESTDOCNO
                                AND FIND_IN_SET(D.SEQNO, A.RFQSEQNO)
                                AND FIND_IN_SET(D.DNO, A.RFQ3DNO)
                            INNER JOIN TBLOPTION E ON
                                C.PROCUREMENTTYPE = E.OPTCODE
                                AND E.OPTCAT = 'PROCUREMENTTYPE'
                            LEFT JOIN TBLSITE F ON
                                B.SITECODE = F.SITECODE
                            LEFT JOIN TBLPROFITCENTER G ON
                                F.PROFITCENTERCODE = G.PROFITCENTERCODE
                            LEFT JOIN TBLENTITY H ON
                                G.ENTCODE = H.ENTCODE
                            INNER JOIN TblQtDtl I ON A.DOCNO = I.DOCNO 
                            INNER JOIN TBLPAYMENTTERM J ON A.PTCODE = J.PTCODE 
                            INNER JOIN TBLMATERIALREQUESTDTL K ON B.DOCNO = K.DOCNO And I.MaterialRequestDNo = K.DNo
                            INNER JOIN TBLITEM L ON K.ITCODE = L.ITCODE 
                            INNER JOIN TBLDELIVERYTYPE T on A.DTCode = T.DTCode
                            INNER JOIN (
                                SELECT DOCNO, SUM(UPRICE * QTY) TOTALAMOUNT
                                FROM TBLQTDTL
                                GROUP BY DOCNO
                            ) M ON I.DOCNO = M.DOCNO
                            Left Join
                                (
                                    Select DocNo, Count(*) CancelCount
                                    From TblMaterialRequestDtl
                                    Where DocNo In
                                    (
                                        Select Distinct MaterialRequestDocNo
                                        From TblQtHdr
                                        Where VdCode = '".$_SESSION['vdcode']."'
                                        And MaterialRequestDocNo Is Not Null
                                    )
                                    And (CancelInd = 'Y' Or Status = 'C')
                                    Group By DocNo
                                ) N On B.DocNo = N.DocNo
                                Left Join
                                (
                                    Select T3.QtDocNo, Count(*) WinCount
                                    From TblPOHdr T1
                                    Inner Join TblPODtl T2 On T1.DocNo = T2.DocNo
                                        And T2.CancelInd = 'N'
                                        And T1.VdCode = '".$_SESSION['vdcode']."'
                                    Inner Join TblPORequestDtl T3 On T2.PORequestDocNo = T3.DocNo
                                        And T2.PORequestDNo = T3.DNo
                                    Group By T3.QtDocNo
                                ) O On A.DocNo = O.QtDocNo
                                Left Join
                                (
                                    Select T3.MaterialRequestDocNo, Count(*) CloseCount
                                    From TblPOHdr T1
                                    Inner Join TblPODtl T2 On T1.DocNo = T2.DocNo
                                        And T2.CancelInd = 'N'
                                        And T1.VdCode != '".$_SESSION['vdcode']."'
                                    Inner Join TblPORequestDtl T3 On T2.PORequestDocNo = T3.DocNo
                                        And T2.PORequestDNo = T3.DNo
                                    Group By T3.MaterialRequestDocNo
                                ) P On B.DocNo = P.MaterialRequestDocNo
                            WHERE B.DOCNO = '".$DocNo."' AND I.DOCNO = '".$Qt."'
                            GROUP by K.DNo; ";

                        $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
                        while ($res = mysqli_fetch_array($result)) { 
                                $Status = '';
                                if($res['ActInd'] == 'N') {
                                    $Status = 'Updated';
                                } else {
                                    $Status = $res['Status'];
                                }
                            ?>

                            <tbody>
                                <!-- <td><input type="checkbox" class="checkItem" name="DNo[]" value="<?= $res['DNo'] ?>"></td> -->
                                <td><?= $res['ItName'] ?></td>
                                <td align="right"><?= number_format($res['Qty'], 2, ",", ".") ?></td>
                                <td><?= $res['Uom'] ?></td>
                                <td align="right"><?= number_format($res['UPrice'], 2, ",", ".") ?></td>
                                <td><?= $res['CurCode'] ?></td>
                                <td><?= $res['PtName'] ?></td>
                                <td><?= $res['DTName'] ?></td>
                                <td><?= $res['ExpiredDt'] ?></td>
                                <td align="right"><?= number_format($res['TotalPrice'], 2, ",", ".") ?></td>
                                <td><?= $Status ?></td>
                                <td><?= $res['Remark'] ?></td>
                            </tbody>
                        <?php } ?>
                    <?php } ?>
                    </table>
                    <!-- <div align="right">
                        <input type="reset" class="btn" onclick="return confirmCancel();" value="Cancel" id="BtnCancel" style="background-color: #58585A; color:white" />
                        <button type="submit" name="submit" id="btnEdit" class="btn btn-info btnEdit">Edit Quotation</button>
                    </div> -->
                </div>
            </div>
        </form>

        <script>
            $(document).ready(function() {
                $('#checkAll').change(function() {
                    var checkboxes = $(this).closest('form').find(':checkbox');
                    checkboxes.prop('checked', $(this).is(':checked'));
                });

                $('#btnEdit').click(function() {
                    checked = $("input[type=checkbox]:checked").length;

                    if (!checked) {
                        alert("You must check at least one checkbox.");
                        return false;
                    }

                });
            })

            setTimeout(function() {
                var table = $('#tblQtHistoryEdit').DataTable({
                    responsive: true,
                    autowidth: true,
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: false,
                });
            }, 200);
        </script>

    <?php } ?>